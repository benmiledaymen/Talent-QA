# Documentation Generator

The purpose of this project is to generate Career API documentation.

### Installation

Before starting the generation process, make sure that the solution has been compiled successfully. Then execute the following commandes :

```sh
$ cd Documentation/Api
$ yarn build
```

The documentation will be generated in `WebSite/api/documentation`. Then just launch the browser and navigate to `api/documentation`.
