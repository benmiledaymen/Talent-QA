// Gulp and plugins
var gulp = require('gulp'),
    apiDoc = require('gulp-apidoc'),
    childProcess = require('child_process'),
	destPath = __dirname + '/../../WebSite/api/documentation',
	fs = require('fs'),
    inline = require('gulp-inline'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    replace = require('gulp-replace');

var command;

// Update with desired version
var apiDocNetVersion = '1.1.4.1';
var apiDocNetPath = 'TalentSoft.ApiDocNet.' + apiDocNetVersion + '\\tools\\TalentSoft.ApiDocNet.exe';

gulp.task('default', ['build']);

gulp.task('nuget', function(done) {
    childProcess.exec('nuget install TalentSoft.ApiDocNet -Source http://srv-rd-packages.talentsoft.com/nuget/Talentsoft -Version ' + apiDocNetVersion, function(err, stdout, stderr){
         console.log(stdout);
         if (err) return done(err);
         done();
    });
   
   //Debug
//   apiDocNetPath = 'F:\\Git\\ApiDocNet\\TalentSoft.ApiDocNet\\bin\\Debug\\TalentSoft.ApiDocNet.exe';
//   done();
});

gulp.task('swagger:template',  function(){
    gulp.src(['node_modules/swagger-ui/dist/**/*', '!node_modules/swagger-ui/dist/index.html', '!node_modules/swagger-ui/dist/images/favicon*.*']).pipe(gulp.dest('../../WebSite/api/documentation'));
})

gulp.task('swagger:build', ['swagger:template', 'nuget'], function(done){
    command = 
			apiDocNetPath + ' -title "Career API" "..\\..\\WebSite\\bin\\TalentSoft.Career.Directory.WebSiteModule.dll"'
			+ ' "..\\..\\WebSite\\bin\\TalentSoft.Career.Evaluation.WebSiteModule.dll"'
			+ ' "..\\..\\WebSite\\bin\\TalentSoft.Career.Common.WebSiteModule.dll"'
            + ' -include "src\\token.json"'
            + ' -swagger "..\\..\\WebSite\\api\\documentation\\swagger.json" '
    childProcess.exec(command, function(err, stdout, stderr){
        if (err) return done(err);
        done();
    })
});

gulp.task('swagger:uicopycss', ['swagger:template'], function(){
	gulp.src('template/css/*').pipe(gulp.dest('../../WebSite/api/documentation/css'));
})

gulp.task('swagger:uicopyimages', ['swagger:template'], function(){
	gulp.src('template/images/*').pipe(gulp.dest('../../WebSite/api/documentation/images'));
})

gulp.task('swagger:html', ['swagger:template'], function(){
	gulp.src('template/index.html').pipe(replace('"http://petstore.swagger.io/v2/swagger.json"', "window.location.origin + window.location.pathname.replace(/api\\\/.*/i,'') + 'api/documentation/swagger.json'"))
        .pipe(gulp.dest('../../WebSite/api/documentation'));
})

gulp.task('swagger:publish', ['swagger:html', 'swagger:uicopyimages', 'swagger:uicopycss', 'swagger:build'], function(){
})

gulp.task('bootprint', ['swagger:build'], function (done) {
    require('bootprint')
      .load(require('bootprint-swagger'))
      .build('../../WebSite/api/documentation/swagger.json', 'public')
      .generate()
      .done(() => done());
});

gulp.task('bootprint-inline', ['bootprint'], function(){
    gulp.src('public/index.html')
    .pipe(inline({
        base: 'public/',
        js: uglify,
        css: minifyCss,
        disabledTypes: ['svg', 'img', 'js'], // Only inline css files 
        ignore: ['./css/do-not-inline-me.css']
    }))
    .pipe(gulp.dest('../../WebSite/api/documentation/print'));
});

gulp.task('build', ['bootprint-inline', 'swagger:publish']);
