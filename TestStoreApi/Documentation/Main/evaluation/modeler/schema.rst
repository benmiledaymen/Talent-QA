Modeler DataBase Schema
=======================

The database schema of the modeler is divided in three main parts:

- The definition of the model, which will be edited through the editor pages
- The definition of the workflow, which will be edited through the configuration pages of form types and campaigns
- The form filling, which will be edited when models are used to instantiate and fill forms.

Models Definition
-----------------

.. image:: ModelerEngine.png

In that model, the first thing to create is the list of the forms types (TypeFormulaire).
They allow to group models under a functional goal (ex: annual review), and to specify how models of such type should be used (through a campaign or in self-service, with which workflow, etc.) 

Then you've got the list of the models that have been created through the editor pages.
Models are linked to specif nodes (Modeles_SpecifNoeud) to perform matching with individual and select the best appropriated model for someone.

A model is composed of FormCtrls which are the component that the user put in the model.
The self-reference allows to build a tree of FormCtrl and is used by component that are container for other components.
The XMLProperties column allows to store all properties that are specific to each component type.
For component whose configuration involves foreign key tables like that, ItemBindings table should be added to the model.
The ItemBindings table is used by all components that allow to evaluate items whose list is known statically (SingleItem, GroupItem, GroupItemCalculated, PickAndChoose, etc.).
The ItemDependency is used by components that compute items to generate an evaluation based on a formula (ItemInFormulaireCalculated, GroupItemCalculated). It allows to determine computation order when they are dependencies between them.

Worklow Definition
------------------

.. image:: ModelerWorkflow.png

A FormWorkflow is mainly defined by its type (TypeValidation) : 1, 2 or 3. That type will determine how the configuration can be used.
In particular if we should create as many forms as there are actors for the first step (type 2), or if we should create only one form and allow other actors to take back the lead on the form (type 1 and 3).
See detailed articles on FormWorkflows for more details.

All FormWorflow are supposed to be referenced only once by form type or by campaign. To avoid that any modification in a screen, modify another configuration screen.

FormFilling
-----------

.. image:: FormFilling.png

Finally, the form filling schema which can be divided in four sub parts:

- The core context, where the FormAppraisees and FormAppraisers tables represent a form instance (the sheet of paper that someone has to fill for somebody). This also contains the Campagnes table as a read context.
- The context history. This correspond to tables used by components to store their parameters or record some data so that even if some reference tables change after the validation of a form, the component is still able to display the same result as when the form was validated. Some tables in that group are generic and usable by any component like the FormulaireParamsHistos table. Some others are specific to some components like the FormulaireJobItems. This is just a sample of tables of the type, and more of them exists and can be added.
- The CapuredData. This correspond to the tables where data captured during the form filling operation are stored. This is just a sample of tables of the type, and more of them exist and can be added.
- The displayed historised data. This is a bit like the context history tables but oriented for component that have displayed a history and that store there the history they have displayed. Once again the goal is to be able to display the same history than the one that was computed at the time of the form validation, even years later. This is just a sample of tables of the type, and more of them exist and can be added.​
    
