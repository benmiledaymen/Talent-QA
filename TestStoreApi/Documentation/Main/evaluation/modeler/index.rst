Modeler
=======

The form modeler, generally simply called modeler, is a tool that allow users to build their own functional forms to evaluate people through a web interface.
It is used to perform functional tasks like performance review, competencies evaluation, mobility or training requests, etc.

The underlying infrastructure is based on components development. Components will be used in two main modes:

- Design mode (the modeler itself): it's where the user select components and configure them to build a form.
- Form filling mode: it's the time where a form is used to capture data (through an evaluation campaign for example).

Plus two sub modes:

- ​Form display or printing: it's the time where a form is displayed without any possibility to be modified. Generally done through a PDF representation, this is useful to look at validated forms, but it can also be used by someone who has only a read access on a form that is still in an active state.
- Empty form printing: it's the time where a form is generating in PDF with blank lines to allow to print the form and perform an old school form filling operation (with a hand and a pencil). This is used when a manager knows that he or she won't be able to have a computer or an internet connection during the review of an employee.

Table of content
----------------

.. toctree::
   :maxdepth: 2
   
   schema