Building the TalentSoft.Career.Evaluation.Client project
========================================================

This is a standard HTML5 application that uses:

* `Knockout <http://knockoutjs.com/>`_ as a two-ways databinder and components manager
* `TypeScript <http://www.typescriptlang.org/>`_ as the language that is compiled to Javascript
* `Gulp <http://gulpjs.com/>`_ as the build system
* `Bower <http://bower.io/>`_ as the client libraries package manager

Build pre-requisites
--------------------

Typescript and bower must be globaly installed using `yarn <https://yarnpkg.com>`_::

	yarn global add typescript  
	yarn global add bower  
	yarn global add tsd  
	
The last command install tsd which is used to get the TypeScript definition files.	

	 
Building
--------

Type the following command to install build packages (like gulp)::

	yarn install
	 
Then the following command to install the client libraries like knockout::
	 
	bower install
	 
tsd is then used to install the TypeScript definition files::

	tsd install 

Then compile the applications using::

	gulp
	
The output directory is the ``evaluation`` directory in the ``WebSite`` root project.

.. admonition:: To do

    The build should be done in Visual Studio.

The gulpfile.js
---------------

The `gulpfile.js` contains the build flow. It exposes the following commands:

* ``default``: the default command that builds the application
* ``ts``: only compiles the TypeScript files to Javascript files in the output directory
* ``bower``: only copies the client libraries to the output directory
* ``html``: only copies the HTML files to the output directory
* ``watch``: copies the HTML files and compiles the TypeScript files as they are modified

The normal development flow would to build one time then type::

	gulp watch
	
The output directory would then be updated as soon as the source files are modified and saved to disk.

.. admonition:: To do

    There should be a release build that packs the TypeScript and HTML source files in one
    source file. 