Overview
========

The following changes have been done. Each one is detailed in the following chapters:

* A new ``TalentSoft.Career.Evaluation.Client`` project has been added. It is an HTML5/Javascript :abbr:`SPA (Single Page Application)` written in Knockout and TypeScript. It uses ES6 standard objects and polyfills for the old navigators.
* This SPA uses a component system: each form component is one HTML template file and one TypeScript view model class. 
* Changes to the ``WebSite/TS_Performance/Formulaire.aspx`` file that allows to display the new components inside the current WebForm page.

