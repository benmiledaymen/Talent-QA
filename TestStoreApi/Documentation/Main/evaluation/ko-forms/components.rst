The components
==============

.. _component-source-files:

Sources files
-------------

Each form component source code and template are in their own directory. The name of this directory,
the name of the TypeScript file, and the name of the HTML file must be exactly :file:`form-control-{name}`
where :samp:`{name}` is the lowercase component type name as defined in the Career source code. 

For example, the FreeNumeric component name must be ``form-control-freenumeric``.

All the components are in the ``src/components`` directory.
 
Registering a component
-----------------------

Each component must be registered in order to be usable in a web page. The registration is done
in the ``src/app/startup.ts`` file.

Call :samp:`register("{name}")` for the new component.

.. admonition:: To do

    The component should be registered in the build system in order to be included in the
    release bundle (because there are no direct references to a component source code from
    the entry point source code).
    
The component view model
------------------------

It should export a class named viewModel.

This class will be instantiated when the component is displayed. The constructor will be called
with a ``FormControlParameters<T>`` object (defined in ``app/form-control``). ``T`` is the 
excepted properties interface, specific to the component.

Any public property will be available in the HTML template.

Example:

.. code-block:: javascript

    import ko = require("knockout");
    import formControl = require('app/form-control');

    interface FreeTextProperties {
        boundLabel: formControl.BoundLabel
    }

    export class viewModel {
        public value: KnockoutObservable<string>;
        public properties: FreeTextProperties;

        constructor (params: formControl.FormControlParameters<FreeTextProperties>) {
            this.properties = params.model.properties;
            this.value = params.data.valueString;        
        }

        public dispose() {
        }
    }

The ``value`` and ``properties`` properties are bindable to HTML elements in the template.

In the following example, the ``value`` property is binded to an Input text box:

.. code-block:: HTML

    <div class="FormCtrl FreeTextCtrl FreeTextCtrlWithCSSLabel">
        <div>
            <input class="FreeTextCtrl_TextBox" type="text" data-bind="value: value">
        </div>
    </div>

See the `Knockout documentation <http://knockoutjs.com/documentation/introduction.html>`_ for more information.
 
Adding the component to the supported components in the C# WebSite
------------------------------------------------------------------

The ``TalentSoft.ObjectModel\Modeles\Web\WebFormCtrlLoader.cs`` must be modified. There is a list of components that returns
a ``KnockoutComponent`` user control, the new component type must be added.

.. code-block:: csharp

            if (allowKnockout && (webFormCtrlType == "InfoLabel" || webFormCtrlType == "FreeText" || <add your comparison here>))
            {
                return new KnockoutComponent(formCtrl);
            }
           
.. admonition:: To do

    This should be in an array that is put in a central place, for example the ``TalentSoft.Career.DependencyResolutions`` project,
    when it will be renamed to a more generic name.