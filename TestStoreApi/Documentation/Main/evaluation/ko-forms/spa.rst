The Single Page Application
===========================

bower.json
    The :abbr:`bower (A package management system for client-side programming. It depends on Node.js)` packages list. These are the packages that are needed by the Web client (e.g. Knockout)
    
gulpfile.js
    The build file used by gulp. If you need to change the build process, edit this file.
    
package.json
    The node packages. Used by the build system.
    
tsd.json
    The list of TypeScript definition files. Used by TypeScript to declare interface on the external javascript bower libraries.
    
src/app/fetch-helpers.ts
    Some helpers to call fetch, the standard library to make Ajax call.
    
src/app/form-control.ts
    Some interfaces used by the Web API services that are needed by the SPA.
    
src/app/require-webform.config.ts
    The require configuration used in the WebForm Formulaire page. The url is not the same as in require.config.js
    
src/app/require.config.ts
    The require configuration used by the SPA. It allows to use simple names to reference the bower libraries.
    
src/app/startup.ts
    The entry point: registers all the components and binds the view models to the HTML page.
    
src/components
    The directory with the components, one for each component.
    
src/components/form-fill
    The component that is used by the SPA to create a form.
    
src/index.html
    The SPA main page.
    
src/reference.d.ts
    A reference to the TypeScript definition files
    
src/tsconfig.json
    The TypeScript project file.
    
    

