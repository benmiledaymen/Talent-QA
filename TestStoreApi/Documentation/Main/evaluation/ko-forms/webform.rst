Integration in the WebForm page
===============================

The components can be integrated in the :file:`WebSite/TS_Performance/Formulaire.aspx` file. 

.. admonition:: To do

    For now, the components are only integrated in the ``Formulaire.aspx`` page, excluding the PDF output and the form modeler. 
    This means that there must be a legacy component for these pages.
    
The infrastructure
------------------

WebFormCtrlLoader
^^^^^^^^^^^^^^^^^

The ``WebFormCtrlLoader`` class is used to create the components in a page. It has been modified to allow the creation of a knockout component:

* A ``allowKnockout`` parameter has been added to the ``LoadWebFormCtrl`` methods. Its default value is ``false`` and it is only set to ``true`` by the ``Formulaire`` component. 
* If this boolean is set to ``true``, a ``KnockoutComponent`` user component is returned in place of the legacy component.
* If this boolean is set to ``false``, the legacy component is created

KnockoutComponent
^^^^^^^^^^^^^^^^^
 
The new ``KnockoutComponent`` is a generic component that will output the necessary HTML to insert a knockout component in the form.

The ``LoadResults`` and ``SaveResults`` will load and save the freefields. ``SaveResults`` assumes that the free field data are in
a form field whose name is the control identifier.

The main work is done in the ``RenderControl`` methods that output directly the necessary HTML:

* A script that put the free field data in a global object named ``freeFields``
* The knockout component, whose name is computed according the rules given in :ref:`component-source-files`.
* A hidden input text element, which will be binded to the free field data. This way, the free field data can be post back when the form is saved
