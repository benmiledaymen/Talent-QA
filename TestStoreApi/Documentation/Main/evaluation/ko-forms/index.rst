Knockout-Forms (Work in progress)
=================================

The goal of this project is to have a more modern approach to
display the evaluation forms.

The final step of this long term project would be to ditch the
current Webform implementation and replace it with an HTML5/Typescript client
using WebAPI to retrieve the data. The intent is to have much
better performance by offloading most of the process to the client, that
would be responsible of the layout.

.. toctree::
   :maxdepth: 2
   
   overview
   spa
   building
   components
   webform
   