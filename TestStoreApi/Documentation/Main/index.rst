.. Folke documentation master file, created by
   sphinx-quickstart on Tue Nov 17 10:41:16 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. highlight:: csharp

Welcome to Talentsoft documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2
   
   general/index
   common/index
   evaluation/index
   infrastructure/index
  

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

