TsObjects
=========

TsObjects are the base entities of the inheritance hierarchy for the most recent Career entities.

The database storage model of TsObject is Table-Per-Type 

- a table [Reflection].[TSObjects] holding the common data for all types: [TSObjectId], [CreatorIndividualId], [ModifierIndividualId], [CreationDateUTC], [ModificationDateUTC], etc.
- child tables holding specific data of subtypes with a foreign key to [Reflection].[TSObjects]
- a column [ClassNameId] acting as a discriminator value for the object type through a foreign key  to table [Reflection].[ClassNames],​ (ClassNameId is also a foreign key to table ClassName)

Note that ClassNameId, though a discriminator value, can be different from one tenant to the other for the same type, because it is generated through identity.
As a consequence, any code ( C# or SQL ) targetting a specific class of TsObject should never have an explicit reference on a ClassNameId. The code should rely on a join with table ClassName and filter on the FullName column

Basically, the class provides the following services :

- unicity of identifier accross the whole scope of TsObject. This allows to be able to retrieve an object just by knowing its id ;
- cache genericity : this is a direct consequence of the previous point. Objects can be cached just based on their ids. Knowing a modified object id is enough to load an object and and have it expire from the cache ;
- generic tracking for creation (creator's id, creation date, last modificator's id,  last modification date)

TsObjects inherit from interface TalentSoft​.IDbRowObject which point is to provide an identifier property (namely a key) and is the previous attempt at defining a base entity concept.
Database implementation

TsObject are (almost) Database agnostic; They have almost no knowledge that the underlying storage is SqlServer and that the ORM used to access it is Linq2Sql).
The mapping beween TsObjects and database entities is done by the Manager, an implementation of ATsObjectManager<TManagedObject, TAssociatedDbRowObject> where TManagedObject is the TsObject type, and TAssociatedDbRowObject is the associated DbObjectType (a linq2Sql mapped type, or a EntityFramework mapped type)
The manager will hold all specific mapping, storing and retrieving methods for TsObjects.
​​Linq2Sql mapping

Implementing Table-Per-Type inheritance with Linq2Sql is not possible out of the box. The workaround has been to map Linq2Sql objects to views. That way, Linq2Sql always sees only one table when mapping objects..
You then have to add 

Il faut construire des procédures stockées pour l'insertion, la mise à jour et la lecture.

La gestion du cache avec TSCache est largement automatisée.
