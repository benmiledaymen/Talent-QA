Import/Export
==============

The import/export process is used to synchronized huge volume of data between the application and some external systems.
In general imports are used to push data from the main system of a client to the Career application. It's the case when Career application is the slave of the client core administration system.
And exports are used to synchronize Career application with some client applications. It's the case when Career application is the master core administration of the client.

In advanced scenario, for a unique client, imports and exports can be used on differents set of data because Career can be slave for some data (ex: list of the employees), but master on some other data (ex: salary proposals for each employee after a salary review)

.. toctree::
   :maxdepth: 2
   
   BusinessRules
   ImplementationRules
