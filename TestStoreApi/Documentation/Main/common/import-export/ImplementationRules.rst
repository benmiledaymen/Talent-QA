Implementation rules
--------------------------

Rules for import classes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Any new import class must implements the ``ICsvImportable2`` insterface. The ``ICsvImportable`` interface is deprecated for new usages.

Rules for import parsing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* To parse data look at the helpers in ``ImportParseExtensions`` (compatible with ``ICsvImportable2`` interface).
* Some hints about parsing implementation and methods compatibles with ``ICsvImportable`` interface:
    * To parse DateTime entries use one of this helper methods:
        * ``ImportUtcDateWithLocalOffset(string dateField, CultureInfo culture, TimeZoneInfo timeZone, TimeSpan defaultOffset)`` If the date is a start or end date, and use the *BeginDayLocalOffsetTimeSpan* or *EndDayLocalOffset* enterprise parameter for the *defaultOffset* argument.
        * ``ImportUtcDate(string dateField, CultureInfo culture, TimeZoneInfo timeZone)`` If the date do not have a particular functional time in the day known by the application. If time is not specified in the import file it will be considered as midnight of the enterprise timezone.
    * To parse boolean, the ``ParseToBool`` or the ``ParseToBoolNull`` extension methods from TextHelpers must be used to support strings that are not supported by the native ``Bool.Parse``
* When implementing the ``ICsvImportable`` interface, parsing issues must be reported by raising ``FormatException`` that will be managed by the engine.
* When implementing the ``ICsvImportable2`` interface, no exception should be raised during parsing. Problems must be logged in the context. This is handled by the methods of the ``ImportParseExtensions``

Rules for import errors management
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* You can raise exceptions of type ``ImportException``, or a derivated type, if you need to functionaly produce a bloquing error.
* Multiple exceptions of type ``ImportException`` can be aggregated in one of type ``AggregateException`` to report multiple bloquing errors at once.