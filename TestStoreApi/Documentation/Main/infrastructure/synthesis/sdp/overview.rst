Overview
========

Introduction
------------

Le projet d'infrastructure TalentSoft.Career.SynthesisDataProvider sert à créer une requête SQL à partir d'une configuration Synthesis. La requête est lancée par une SqlCommand et les résultats sont lus avec le SqlDataReader et retournés pour être soit écris dans un Excel avec le projet ExcelWriter soit dans une grille Synthesis.

Dans le cadre d'un rapport, ce projet est appelé depuis la classe Report d'ObjectModel si la caractéristique entreprise "IsNewDataProviderEnabled" est à true. Si l'appel échoue parce que le reflection path ou le search operand n'est pas compris (ou pour toute autre raison), le code normal de génération des rapports est appelé.

Dans le cadre d'une grille Synthesis, les services doivent être appelés explicitement.

TalentSoft.Career.SynthesisDataProvider
---------------------------------------

SynthesisDataProvider
^^^^^^^^^^^^^^^^^^^^^

C'est la classe principale qui sert à créer la requête SQL à partir d'une configuration Synthesis et à l'exécuter.

Pour cela elle conserve en mémoire la liste des tables sur lesquelles la requête a lieu et la liste des colonnes à récupérer dans ces tables
(sous forme d'une interface ISelectedColumn). Elle enregistre également le texte des commandes WHERE, GROUP BY et HAVING qui peuvent être générés
par les SearchOperand ou la gestion des habilitations. Une fois que toutes ces informations ont été collectées, le texte de la commande SQL est 
créé et exécuté.

Ces différentes listes sont modifiables grâce à des méthodes qui pourront être appelées par les classes qui implémentent IPropertyMapping. 
Elles sont notamment chargées de créer des objets qui implémentent ISelectedColumn.

IClassMapping
^^^^^^^^^^^^^

Cette interface sert à décrire comment un objet est enregistré en base de donnée, avec la restriction qu'un objet ne peut être stocké que dans une seule table. On a :

- une collection d'IPropertyMapping qui décrivent chacune des propriétés de l'objet
- une collection d'IMethodMapping qui décrivent les méthodes de l'objet
- une collection d'Alias qui permettent de stocker des propriétés dans une table différente (par exemple la propriété UserName d'Individual se trouve dans la table aspnet_Users, l'alias "UserName" pointera alors vers "User.UserName")
- différentes autres propriétés telles que la clé primaire, la liste des colonnes utilisées lors d'une recherche full text ou la colonne indiquant qui possède l'objet (pour les droits d'accès)

IPropertyMapping
^^^^^^^^^^^^^^^^

IPropertyMapping et les différentes interfaces qui en dérivent (IMethodMapping, ISelectablePropertyMapping, 
IPropertyMappingWithColumn, IMethodMapping...) servent à décrire les différentes propriétés d'un objet et comment on peut
les récupérer depuis la base de donnée.

TableMapping
^^^^^^^^^^^^

C'est la principale implémentation de IClassMapping. Elle utilise une interface fluent pour créer des configurations de mapping.

Comme elle est conçue pour fonctionner avec des reflection path référençant ObjectModel, chaque propriété prend en paramètre le nom (en string) de la propriété telle qu'elle est définie dans ObjectModel. Cela permet une robustesse au refactoring.
Exemples d'implémentations de IPropertyMapping

En plus d'implémentations spécifiques à un reflection path en particulier, il existe quelques implémentations génériques de
IPropertyMapping, utilisées notamment par la classe ClassMapping.

PropertyMapping
    Cette classe va lire une colonne d'une table et renvoyer son contenu tel quel si c'est un type de base

TableReferenceMapping
    Cette classe fait un left join sur une autre table sur la valeur de la colonne et la clé de l'autre table si c'est une clé étrangère : le SynthesisDataProvider sera alors appelé récursivement sur la table jointe

AutoLocaleDatePropertyMapping
    Cette classe sert à lire une date UTC d'une colonne de la table et à la renvoyer transformée dans l'heure locale.

ConcatPropertyMapping
    Cette classe sert à lire le texte de deux colonnes d'une même table et à renvoyer la concaténation des deux valeurs, séparées par un espace.

ManyToManyMapping
    Cette classe sert à récupérer un ensemble de valeurs d'une autre table (qui lui est jointe par une table de liaison à spécifier) et à concaténer les résultats sous forme d'une unique chaîne avec une virgule comme séparateur.

ManyToOneMapping
    Cette classe récupère le premier résultat d'une sous-requête à laquelle on peut appliquer un certain nombre de filtres WHERE et ORDER BY (qui implémentent l'interface ISubSelectFilter).

ResourcePropertyMapping
    Cette classe récupère deux colonnes : l'identifiant dans les ressources et la valeur par défaut. Si l'identifiant est trouvé, la valeur localisée du texte est renvoyée, sinon la valeur par défaut est renvoyée.

TalentSoft.Career.Synthesis.Mappings
------------------------------------

Cette classe contient toutes les informations qui permettent de transformer un reflection path de TalentSoft.ObjectModel en une requête SQL.

Pour cela, on associe à chaque base de reflection path (par exemple TalentSoft.Individus.Employee) un objet qui répond à l'interface
IClassMapping. Cette interface renvoie des IPropertyMapping en fonction d'un élément du reflection path.

Les ``IPropertyMapping`` eux-même fournissent une méthode Select qui permet de continuer à avancer dans ce reflection path.