Synthesis configurations
========================

Introduction
------------

The Synthesis configurations describe what are the data to display to the user.

The interfaces
--------------

IGridDataConfig
^^^^^^^^^^^^^^^

A grid configuration must implement TalentSoft.Career.Shared.Synthesis.IGridDataConfig. It contains:

 - the Configuration.Synthesis table columns: ConfigId, ConfigKey, ResourceName, ObjectType, OutputMode, and Rank
 - a collection of IGridColumn (see below)
 - DataKeyColumn and DataKeyFields properties to describe the key

The interface is implemented by SynthesisConfiguration (a configuration created by code) and by SynthesisXmlConfiguration (a configuration created from a XML configuration stored in database).

IGridConfig
^^^^^^^^^^^

Add all what is presentation (size, etc.) to IGridDataConfig.

IGridColumn
^^^^^^^^^^^

Each column in a Synthesis grid configuration must implement IGridColumn.

Extended interfaces defined in TalentSoft.ObjectModel
-----------------------------------------------------

The implementations of these interfaces in TalentSoft.ObjectModel expose methods to retrieve the data directly, without an external service. They implement interfaces that add more methods:

- IGridConfig adds a CreateDataTable method that returns a DataTable object used to fill the Synthesis grid
- IGridConfig adds a AddColumn/PrependColumn methods in order to add custom columns to the DataTable created by CreateDataTable
- IGridColumnWithData adds a GetValue(o) method

Creating a configuration by code
--------------------------------

The class that creates a configuration must implement IConfigurationProvider<T>. It defines a GetDefaultGridConfig that returns 
the default configuration by name (each Synthesis Grid in the application should have an unique name).

This default configuration can be created using two distinct classes.

Using TalentSoft.Career.SynthesisDataProvider.SynthesisGridConfiguration<T>
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is the prefered way to create a configuration from a service because it has no dependency on TalentSoft.ObjectModel. 
The main restriction of this class is that the data for this configuration can only be retrieved by the SynthesisDataService.

Using TalentSoft.Common.Synthesis.SynthesisConfiguration<T>
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This object returns an object that has methods to retrieve directly the data, without relying on a external service like
SynthesisDataProvider. It exposes the AddReflectionColumn that is the easiest way to add a new column. The parameter is a lambda expression that returns the property to display and it may call methods and use properties to navigate in the object.

It is the prefered way to create a configuration if a dependency to TalentSoft.ObjectModel is not an issue.

XML Configurations
------------------

The Synthesis configurations (that are also used by the reports elsewhere in the application) can be overriden in the database. That allows the 
users to have customized grids (this customization may be done by the Technical Consulting or the client himself). 
There may be any number of customization for a given config key, but if any customization exists in database for a
given config key, the default configuration is ignored.

The configurations are stored in the [Configuration].[Synthesis] table, whose columns are:

ConfigKey
    the configuration identifier, which is refered to in the code. There may be multiple configurations for a same config key and that allow the user to choose the version that he wishes (not completely implemented in MVC)
    
Name
    a non-localized default name for the configuration
    
ResourceName
    the resource name for the configuration name
    
ObjectType
    the object type to display

OutputMode
    flags that define in which output mode the configuration is available (see the ConfigOutputMode enumeration), 1 for web, 2 for excel or csv exports. It allows to have more columns for an export than for a web data grid.

Rank
    in the case of several configurations with the same ConfigKey, it allows to rank them in the list (the lowest ranked configuration will be used by default in the MVC pages)

Field
    obsolete (used by old versions of the configuration)
    
IsActive
    the configuration is disabled if the value is 0 and enabled if the value is 1

Configuration
    the XML configuration

.. warning::
 
    Configurations are very sensitive to any changes in the source code that may alter the properties or the methods of a class that is accessed 
    by reflection. See the :ref:`migration guide <properties-migration>`

The configuration XML
---------------------

The XSD schemas that ensure that the configurations are valid are in the Schemas directory in the Career solution: Schemas/Synthesis.xsd. In the
same directory there is a sample configuration.

The fields are:

GlobalConfiguration : the Synthesis grid global parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Width
    table width (default: 100%) - obsolete field, not read anymore. Set the column width instead. If the sum of each column width is larger than
    the screen, there will be a scroll bar.

Height
    table height (the default value depends on the number of results per page. Obsolete field.

Paging
    the number of results per page

noDataElem
    text displayed if the table is empty. Obsolete field.

AllowRowFiltering
    enables the filtering options on each column (true by default)

DefaultColumnWidth
    the default column width (if not specified, 100% divided by the number of columns)

Columns
^^^^^^^

A block for each column. The columns will be displayed in the order they are declared. Each columns may be a ReflectionColumn or a ConstantColumn

HeaderText
    the text to display in the header. By default, the text to display is taken from the source code but there is no contextual information in this 
    text. For example, the Name property will display "Name" even if it really is the manager name.

Sorting
    the sort order: Desc or Asc (Asc by default)

DataFormat
    a valid format string for String.Format. See the MSDN documentation: http://msdn.microsoft.com/fr-fr/library/fht0f5be(v=VS.90).aspx

Width
    the column width (if not specified, use DefaultColumnWith from GlobalConfiguration)

HeaderStyle
    the header text style​

DataStyle
    the data cells text style

ColumnKey
    unique column key name (auto-generated if not specified). Allows scripting.

property
    full path to the property value to display (only for ReflectionColumns)

Value
    the value (only for ConstantColumns)

The HeaderStyle and DataStyle elements have two attributes (not supported by the MVC component):

HAlign
    horizontal alignement (Center, Justified, Left, or Right)

VAlign
    vertical alignement (Bottom, Middle, or Top)

Example :

.. code-block:: xml

    <SynthesisConfiguration xmlns="http://schemas.talentsoft.com/Synthesis.xsd">
    <Columns>
        <ReflectionColumn property="Plan.Name">
        <HeaderText ResourceName="DefaultReport_IndividualTrainingWishPlan_PlanHeader">Plan Name</HeaderText>
        </ReflectionColumn>
        <ReflectionColumn property="Wish.IndividualWisher.LastName" />
        <ReflectionColumn property="Wish.IndividualWisher.FirstName" />
        <ConstantColumn><HeaderText>Pi</HeaderText><Value>3.14</Value></ConstantColumn>
    </Columns>
    </SynthesisConfiguration>
    
Loading a configuration from DataBase
-------------------------------------

Using the SynthesisConfigurationService
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This service exposes a GetSynthesisConfiguration method that allows to get a configuration from the database by object type and configuration name.

The implementations of the interface are locate in TalentSoft.Career.SynthesisDataProvider.

Using GridConfigFactory
^^^^^^^^^^^^^^^^^^^^^^^

This object from TalentSoft.ObjectModel does the same. The implementations are:

SynthesisXmlConfiguration
    A configuration loaded from the database, the configuration is encoded in an XML column.

ReflectionColumn
    A IGridColumn implementation that get the data by reflection.

ConstantColumn
    A IGridColumn implementation that returns a constant value for any row.

