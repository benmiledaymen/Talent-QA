.. _properties-migration:

Migrate properties and methods
==============================

Présentation
------------
 
Ce document introduit  les bonnes pratiques et usages à respecter lors d’une migration d’un élément dans l’application Carrière.
Avant toutes migrations, il faut suivre les points qui sont énoncés dans ce document.
 
Membres à migrer
----------------

Une liste des membres obsolète à migrer est disponible sur Baloo à l’adresse suivante : 
`Migration_Synthesis_Configurations.xls <https://tscorp.sharepoint.com/r%26dteam/_layouts/15/WopiFrame.aspx?sourcedoc={850D57CC-C69A-4C0F-86B9-3068A7FFC138}&file=Migration_Synthesis_Configurations.xlsx&action=default&DefaultItemOpen=1>`_
Elle contient la liste des membres qui ont été migrés ainsi que ceux restant à migrer. 

Les impacts
-----------
 
Avant toute migration, il faut au préalable vérifier que l’élément que l’on souhaite migrer n’est pas présent dans les cas suivant.

Reflection path utilisé dans l’application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L’application Carrière utilise la reflection pour permettre de configurer certain éléments comme les tableaux de synthèse ou les rapports. 
A l’heure actuelle il existe 5 tables dans l’application qui utilise les reflection paths.
- Configuration.DetailConfigDefinitions
- Entreprise.ConfigurationDashBoard
- Entreprise.MonProfilConf
- Exploitation.NotificationsMailsRecipients
- Configuration.Synthesis

Le consulting technique met à disposition sur un serveur ftp les configurations utilisées par l’ensemble des clients.
 
Adresse
    ftps://files.talent-soft.com/
    
Login
    Tstechnique
    
password
    528757JB

Un projet Jenkins permet de voir la liste des membres obsolètes dans l’application qui sont utilisés dans les configurations des clients.
http://bld.rd.talentsoft.com/job/Career/job/qualif/job/Confs/
 
Il est possible d’utiliser le DBUpdates.cs pour migrer les membres au sein de la table Configuration.Synthesis.
Pour les autres tables, seule une migration « à la main » est pour le moment possible.

Les formulaires sérialisés en base
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 
Les formulaires sont sérialisés en base de données, et font appel directement à  certaines pages ou méthodes de l’application. Ces informations sont stockées dans la table : 

Definition.FormCtrls

Nous n’avons pas la possibilité de changer les objets sérialisé dans cette base car cela reviendrait à changer directement le formulaire d’un client. De plus nous n’avons pas accès à ces informations. 

Les SearchOperands
------------------
 
Les SearchOperands utilisent aussi des reflection paths et sont stockées dans ``Intelligence.SearchOperands``

Nous n’avons pas accès à cette information non plus et il faut les migrer à la main pour le moment.
  
Procédure
---------

La procédure à suivre pour migrer un membre est la suivante :

- Vérifier que le membre n’est pas dans l’un des cas cités en 3.
- S’il est dans un des cas précédents, il faut le migrer.
- Il faut attendre 2 sprints (pour des raisons de sécurité) après migration pour le supprimer du code. 
- Vérifier que l’élément a bien été migré (voir le TS-config du CCnet).
- Il ne reste plus qu’à supprimer l’élément migré du code.  


 

