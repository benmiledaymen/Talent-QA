Synthesis
=========

.. toctree::
   :maxdepth: 2
   
   configurations
   migration
   legacy/index
   sdp/index