Prefetch
========

Some columns may use reflection paths that access data that need to be loaded from the database.
In order to avoid to make one request for each line to display, a prefetch must be added to the objects.

Example::

    [GlobalPreFetch(PreFetchContexts.Individual, (int)IndividualPreFetchContext.GlobalPreFetchs.EmploymentContracts)]

.. caution::

    Il y a deux types de prefetch: local et global.
