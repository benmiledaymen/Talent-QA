Grid
====

Introduction
------------

The Synthesis control is a grid used by the application. It allows to display any object and it is fully configurable. It is able to load data by specifying an entire reflection path, with navigation between properties. For example, if an employee is displayed, one column may show the employee manager name.

.. image:: grid.png

Main features
-------------

- Sorting on any column. If the column data does not have the ``IComparable`` interface, the ``ToString()`` method is called.
- Filter on any column, after formatting.
- Multiple selection (in which case there must be only one primary key)

Sections
--------

.. toctree::
   :maxdepth: 2
   
   webform
   mvc