Grid in Webform
===============

The control definition is in ``SynthesisGrid``. It provides its features available using wrappers (``Grid``, ``RowSelector``, ``PagingManager``).
The GridConfig accessor allows to choose the configuration. Internaly, it calls the ``InitColumns()`` method that use the ``ApplyGridConf`` method from ``InfragisticsHelpers`` (a ``WebDataGrid`` extension) to apply the ``IGridConfig`` configuration to the Infragistics infragistics.

Example:

.. code-block:: xml

    <ts:SynthesisGrid ID="SearchResultGrid" runat="server" ObjectsType="TalentSoft.Individus.Individual, TalentSoft" ConfName="Administration.Employee.SearchResult">
        <RowSelector EnableCheckBoxesSelection="false" OnSelectedRowChanged="RowSelector_SelectedRowChanged" />
        <PagingManager PageSize="12" />
    </ts:SynthesisGrid>

Parameters
----------

ObjectsType
    the type of the objects to display in the grid

ConfName
    the unique configuration name
    
Objects
    a data source to fill with the objects to display

Misc.
-----

Extra fixed columns (that do not appear in the grid configuration) may be added in the ``OnAfterColumnAdded`` event.