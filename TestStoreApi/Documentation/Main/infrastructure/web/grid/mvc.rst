MVC Grid
========

The View Model
--------------

The MVC Synthesis component is configured by a ``SynthesisGridModel`` object that you must put in one of your ``ViewModel`` properties. For example:

.. code-block:: csharp

        public class IndexViewModel
        {
            public SynthesisGridModel GridModel { get; set; }
            public string OtherProperty { get; set; }
        }
        
The action
----------

The ``SynthesisGridModel`` property must be initialized in the controller action that displays the view:

.. code-block:: csharp

        var viewModel = new IndexViewModel();
        var gridModel = new SynthesisGridModel();

        gridModel.SetConfigurations<TestElement>(CONFIGURATION_NAME); // The configuration name uniquely identify this grid for the TestElement type
        gridModel.Title = "Grid Title"; // The grid title (create a resource)
        gridModel.AddControllerActions(this, this.HttpContext.GetIndividual());
        gridModel.DataSourceUrl = this.Url.Action("GetGridData"); // The action that will return the grid data
        viewModel.GridModel = gridModel;

The AddControllerActions method search the Controller class for actions with the GridAction attribute. See below.
The configuration
The configuration name is used to get the configuration from the TestElement manager or the database if the configuration has been overriden.

You should have implemented the GetDefaultGridConfig method in your provider. For example:

.. code-block:: csharp

            public IGridConfig GetDefaultGridConfig(string configKey)
            {
                var configuration = new SynthesisConfiguration<TestElement>();

                switch (configKey)
                {
                    case CONFIGURATION_NAME:
                        configuration.AddReflectionColumn(x => x.SomeProperty);
                        break;
                }
                return configuration;
           }
          
The action that returns the grid data
-------------------------------------

This sample GetGridData action will returns the data needed by the grid. Only the data for the current page are retrieved, using an ajax request.

.. code-block:: csharp

        [GET]
        public ActionResult GetGridData(GridFilterModel gridFilterModel, PlanExportFormat? exportFormat)
        {
            gridFilterModel = gridFilterModel ?? new GridFilterModel();


            // The GridDataSourceOf use the model and the Query String to know how the data must be acquired
            // (for example if there are filters, sorting...)
            var gridDataSource = new GridDataSourceOf<TestElement>(gridFilterModel, this.HttpContext.Request.QueryString);

            // The grid data are automaticaly stored in the data base. The data must only refreshed if the IsDirty is true.
            if (gridDataSource.IsDirty)
            {
                var me = this.HttpContext.GetIndividual();
                IEnumerable<long> objectIds = _service.GetAllIds(); // A method that returns all the objects id to display before any filtering or sorting
                gridDataSource.DataSourceItems = gridFilterModel.GetDataSource(me, () => objectIds, Comparer<TestElement>.Default);
            }

            return gridDataSource.ToActionResult(gridFilterModel.ConfigId, CONFIGURATION_NAME, exportFormat);
        }
        
In this sample, the action does not take any parameter beside the grid filter model and the export format
(which are automaticaly provided by the Synthesis grid), but you may add other parameters if you need some parameters to retreive the ids to display.

The View
--------

Finally, you must include the SynthesisGrid in the view:

.. code-block:: html

    @(Html.TalentSoft().SynthesisGrid("test-synthesis", Model.GridModel))

``test-synthesis`` is the id of the grid, if you need to use it in a script.

Grid Actions
------------

If you want to add an action that is available in the scroll down list above the grid, you must mark the action with the ``GridAction`` attribute.
For example:
 
.. code-block:: csharp

        [PUT]
        [GridAction]
        public ActionResult TestAction(GridSelectionOf<TestElement> elements)
        {
            _service.DoSomething(elements);
            return GridActionResult.Success("A message");
        }

The parameter may be ``GridSelectionOf<TestElement>`` if there are checkbox, TestElement if only one element is selected or nothing if
it is not needed to select anything.