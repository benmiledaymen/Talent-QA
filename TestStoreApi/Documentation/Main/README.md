Test Store documentation
====================

How to build
------------

Prerequisites: Python 3.x must be installed. Use Chocolate to install it if necessary:

    $ choco install python3
    
Install Sphinx with pip:

    $ pip install sphinx
    
Install Sphinx auto-build with pip: 
    
    $ pip install sphinx-autobuild
    
Install the readthedocs theme:

    $ pip install sphinx_rtd_theme

All these command may run with the ``install.bat`` batch file (run as an administrator).

Then run make:

    $ ./make html
    
If you want to live edit the documentation, run:

    $ ./make livehtml
    
The documentation will be available in the browser at the URL
http://localhost:8000/

Each change in the files will rebuild the documentation and 
reload the page in the browser. 

If you run into an error mentioning that sphinx-autobuild is not recognized you 
might have to add the tools/Scripts/ directory located where python is installed 
to your PATH environment variable.  
By default it should be "C:\ProgramData\chocolatey\lib\python3\tools\Scripts"