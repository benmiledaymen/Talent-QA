Buisness Rules
--------------

Global rules for Import and Export
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* The format of an export must always be compatible with the format of the same import.
  That means that if we export a file and then import that file, it should works and change nothing into the database.
* Import and export are based on CSV files.
* Format of the CSV files depdends on the culture. This include:  
    * The list separator (comma in english, semi-colon in french)
    * The decimal separator for parsing numbers (dot in english, comma in french)
    * The date format for parsing dates. Even if we recomand usage of an universal format like YYYY-mm-ddTHH:MM:SS.mmmk.
    * The encoding for text file in ANSI. Even if we recomand to use UTF-8 with BOM.

Rules for import parsing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Parsing of DateTime entries depends on the enterprise timezone when the timezone is not explicit in the file.
* Parsing of DateTime entries may depends on the enterprise default offsets for start date and end date when only a date without time is present in the file.
* Parsing of the boolean must support "true/false" strings, but also "1/0", "yes/no", and "y/n"
* Parsing errors should be reported as warnings

Rules for import logs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* The import log depends on the culture.
    * That means that all messages produced by an import must come from the resources.
    * Except the messages of level ``TechnicalError``, which may be displayed in French or in English.
* The level ``TechnicalError`` is reserved for the import engine and should not be used in import classes.
    * This log level is used to log unmanaged exceptions (like SQL timeout, out of memory, etc.), and some special errors of the import engine.
    * They generally depends on the culture of the machine, or are in plain english. This allow to the Support and Production Teams to be able to read this errors.
    * They must represent code issues or infrastructure issues.
    * They must not represent business errors that can be fixed by fixing the import file or the data.

Rules for import errors management
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Non bloquing errors must be reported as ``warning``
* Report as ``error`` only errors that force the import to stop before the end. The import should not modify any data if an ``error`` occurs.