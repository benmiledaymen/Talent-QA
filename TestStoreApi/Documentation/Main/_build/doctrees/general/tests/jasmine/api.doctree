���(      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Testing an API�h]�h �Text����Testing an API�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�RD:\sources\Talent-QA\TestStoreApi\Documentation\main\general\tests\jasmine\api.rst�hKubh
)��}�(hhh]�(h)��}�(h�Schema validator�h]�h�Schema validator�����}�(hhhh.hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh+hhhh*hKubh �	paragraph���)��}�(h�uThe schema of the response must be tested. The schemas must be written with
`JSON Schema <http://json-schema.org/>`_.�h]�(h�LThe schema of the response must be tested. The schemas must be written with
�����}�(hhhh>hhhNhNubh �	reference���)��}�(h�(`JSON Schema <http://json-schema.org/>`_�h]�h�JSON Schema�����}�(hhhhHubah}�(h]�h!]�h#]�h%]�h']��name��JSON Schema��refuri��http://json-schema.org/�uh)hFhh>ubh �target���)��}�(h� <http://json-schema.org/>�h]�h}�(h]��json-schema�ah!]�h#]��json schema�ah%]�h']��refuri�hYuh)hZ�
referenced�Khh>ubh�.�����}�(hhhh>hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKhh+hhubh=)��}�(h�Sample schema:�h]�h�Sample schema:�����}�(hhhhthhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK
hh+hhubh �literal_block���)��}�(hX�  {
    "type": "object",
    "title": "PostalAddress",
    "properties": {
        "countryCode": {
            "type": ["null", "string"]
        },
        "state": {
            "type": ["null", "string"]
        },
        "city": {
            "type": ["null", "string"]
        },
        "postalCode": {
            "type": ["null", "string"]
        },
        "rank":{
            "type": "number"
        }
    },
    "required": [
        "rank"
    ]
}�h]�hX�  {
    "type": "object",
    "title": "PostalAddress",
    "properties": {
        "countryCode": {
            "type": ["null", "string"]
        },
        "state": {
            "type": ["null", "string"]
        },
        "city": {
            "type": ["null", "string"]
        },
        "postalCode": {
            "type": ["null", "string"]
        },
        "rank":{
            "type": "number"
        }
    },
    "required": [
        "rank"
    ]
}�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve��language��json��linenos���highlight_args�}�uh)h�hh*hKhh+hhubh=)��}�(h��The library used to verify the schema is `jsen <https://github.com/bugventure/jsen>`_. It works
in browsers like phantomjs (and it may work in node.js).�h]�(h�)The library used to verify the schema is �����}�(hhhh�hhhNhNubhG)��}�(h�,`jsen <https://github.com/bugventure/jsen>`_�h]�h�jsen�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��name��jsen�hX�"https://github.com/bugventure/jsen�uh)hFhh�ubh[)��}�(h�% <https://github.com/bugventure/jsen>�h]�h}�(h]��jsen�ah!]�h#]��jsen�ah%]�h']��refuri�h�uh)hZhiKhh�ubh�C. It works
in browsers like phantomjs (and it may work in node.js).�����}�(hhhh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK'hh+hhubh=)��}�(h��The schemas are located in the ``schemas`` directory of the project. A main ``schemas`` Typescript module
loads the json and parse them as JSON schema validators using JSEN.�h]�(h�The schemas are located in the �����}�(hhhh�hhhNhNubh �literal���)��}�(h�``schemas``�h]�h�schemas�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�ubh�" directory of the project. A main �����}�(hhhh�hhhNhNubh�)��}�(h�``schemas``�h]�h�schemas�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�ubh�V Typescript module
loads the json and parse them as JSON schema validators using JSEN.�����}�(hhhh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK*hh+hhubeh}�(h]��schema-validator�ah!]�h#]��schema validator�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Helpers�h]�h�Helpers�����}�(hhhj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj  hhhh*hK.ubh=)��}�(h�eThe ``helpers`` module contains helpers that create a client as an administrator or as a
random user.�h]�(h�The �����}�(hhhj  hhhNhNubh�)��}�(h�``helpers``�h]�h�helpers�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj  ubh�V module contains helpers that create a client as an administrator or as a
random user.�����}�(hhhj  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK0hj  hhubh=)��}�(h�8For example, let's create a client before all the tests:�h]�h�:For example, let’s create a client before all the tests:�����}�(hhhj6  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK3hj  hhubh�)��}�(hX`  import { ServiceHelpers, failOnError } from '../helpers/helpers';

describe("Sample", () => {
    // Create an API client
    var adminClient = new ServiceHelpers();

    // Log in before any test is run
    beforeAll(done =>
        adminClient.common.login.login('admin', 'talentsoft')
            .then(token => done(), failOnError(done))
    );
});�h]�hX`  import { ServiceHelpers, failOnError } from '../helpers/helpers';

describe("Sample", () => {
    // Create an API client
    var adminClient = new ServiceHelpers();

    // Log in before any test is run
    beforeAll(done =>
        adminClient.common.login.login('admin', 'talentsoft')
            .then(token => done(), failOnError(done))
    );
});�����}�(hhhjD  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h��ts�h��h�}�uh)h�hh*hK5hj  hhubh=)��}�(h�4This client may be used to create a random employee:�h]�h�4This client may be used to create a random employee:�����}�(hhhjT  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKEhj  hhubh�)��}�(h��it("creates an employee", done => adminClient.employeeHelpers.createEmployee()
    .then(employee => done(), failOnError(done));�h]�h��it("creates an employee", done => adminClient.employeeHelpers.createEmployee()
    .then(employee => done(), failOnError(done));�����}�(hhhjb  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h��ts�h��h�}�uh)h�hh*hKGhj  hhubh=)��}�(h�8Or create another client that logs as a random employee:�h]�h�8Or create another client that logs as a random employee:�����}�(hhhjr  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKLhj  hhubh=)��}�(h�pThe ``failOnError(done)`` method is a small helper that fails and calls the ``done()`` method
when it is called.�h]�(h�The �����}�(hhhj�  hhhNhNubh�)��}�(h�``failOnError(done)``�h]�h�failOnError(done)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj�  ubh�3 method is a small helper that fails and calls the �����}�(hhhj�  hhhNhNubh�)��}�(h�
``done()``�h]�h�done()�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj�  ubh� method
when it is called.�����}�(hhhj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKRhj  hhubh �note���)��}�(h�RThis could be in a npm component that may be shared between all the jasmine tests.�h]�h=)��}�(hj�  h]�h�RThis could be in a npm component that may be shared between all the jasmine tests.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKUhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj  hhhh*hNubeh}�(h]��helpers�ah!]�h#]��helpers�ah%]�h']�uh)h	hhhhhh*hK.ubeh}�(h]��testing-an-api�ah!]�h#]��testing an api�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j  h�hehbh�h�j�  j�  u�	nametypes�}�(j�  Nj  Nhe�h��j�  Nuh}�(j�  hh�h+hbh\h�h�j�  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�(h=)��}�(h�Title underline too short.�h]�h�Title underline too short.�����}�(hhhjY  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hjV  ubh�)��}�(h�Testing an API
===========�h]�h�Testing an API
===========�����}�(hhhjg  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hjV  ubeh}�(h]�h!]�h#]�h%]�h']��level�K�type��WARNING��line�K�source�h*uh)jT  hhhhhh*hKubjU  )��}�(hhh]�(h=)��}�(h�LError in "code-block" directive:
maximum 1 argument(s) allowed, 11 supplied.�h]�h�PError in “code-block” directive:
maximum 1 argument(s) allowed, 11 supplied.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hj�  ubh�)��}�(h��.. code-block:: ts
    it("creates a client", done => adminClient.employeeHelpers.createNewEmployeeClient()
        .then(client => done(), failOnError(done));
�h]�h��.. code-block:: ts
    it("creates a client", done => adminClient.employeeHelpers.createNewEmployeeClient()
        .then(client => done(), failOnError(done));
�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hj�  ubeh}�(h]�h!]�h#]�h%]�h']��level�K�type��ERROR��line�KN�source�h*uh)jT  hj  hhhh*hKQube�transform_messages�]��transformer�N�
decoration�Nhhub.