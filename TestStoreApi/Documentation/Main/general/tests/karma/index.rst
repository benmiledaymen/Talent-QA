Karma
=====

`Karma <https://karma-runner.github.io/0.13/index.html>`_ is test runner for Javascript. 
It is available as a npm package.

It is able to preprocess source files (including compilation from Typescript), serve
files (including behind a proxy), and watch for changes. It is compatible with multiple
module loaders, including AMD-type modules.

Installation
^^^^^^^^^^^^

The project to test must be a node project. Add the following packages::

    yarn add --dev jasmine karma karma-requirejs karma-firefox-launcher karma-jasmine karma-phantomjs2-launcher karma-typescript-preprocessor karma-junit-reporter typescript phantomjs2 webpack ts-loader
    
You may need to add any npm package that are required by your tests. 
    
Then, should you copy the karma.conf.js, and tests/tests.webpack.js from the sample project
(``talentsoft-career-directory-services`` in ``Directory\TalentSoft.Career.Directory.Client``).

Finally, modify the package.json, the script section should include:

.. code-block:: json

"scripts": {
    "test": "karma start"
}
    
.. note::
    A yeoman scaffolding that creates an empty project including tests should be added.

Settings
^^^^^^^^

By default, our karma configuration adds a proxy for all the URL that starts with /tsdev and map
them to http://localhost:80/tsdev/
If this not where you local website run, you may create a ``karma.user.json`` with the content:

.. code-block:: json

    {
        "url": "http://localhost:80/mytenant/"
    }      
    
The available options are:

* `url`: the local server full URL
* `browser`: an array of browser names (may be `Chrome`, `Firefox`, and `Phantomjs`)
* `singleRun`: a boolean which says if the tests should be run only time or if karma should watch for changes and run them everytime a file is modified 
    
Running
^^^^^^^

In the directory, if it was not already done type::

    yarn install
    
Then type::

    yarn test
        
The test will be run a first time then again if a file changes.
  
Project structure
^^^^^^^^^^^^^^^^^

All the source code must be in the ``tests`` directory, alongside ``tests.webpack.js``. It will look for files that have the
``.spec.ts`` extension. The content must be a Jasmine test.

Result outputs
^^^^^^^^^^^^^^

The tests results are saved in the ``Build`` directory at the root of the repository.

Debugging
^^^^^^^^^

If you want to debug the code, you should change your settings in ``karma.user.json`` and use browser like Chrome or Firefox. Then run the test
and click on the Debug button.