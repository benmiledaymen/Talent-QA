Jasmine
=======

Jasmine is an unit testing framework for Javascript. It is the prefered way to write new unit tests
for this language (the former being `QUnit <https://qunitjs.com/>`_).

The documentation for `Jasmine <http://jasmine.github.io/edge/introduction.html>`_ can be found online.

Sample code:

.. code-block:: javascript

    describe("A suite", function() {
        it("contains spec with an expectation", function() {
            expect(true).toBe(true);
        });
    });


.. toctree::

    api