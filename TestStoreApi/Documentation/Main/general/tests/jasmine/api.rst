Testing an API
===========

Schema validator
^^^^^^^^^^^^^^^^

The schema of the response must be tested. The schemas must be written with
`JSON Schema <http://json-schema.org/>`_.

Sample schema:

.. code-block:: json

    {
        "type": "object",
        "title": "PostalAddress",
        "properties": {
            "countryCode": {
                "type": ["null", "string"]
            },
            "state": {
                "type": ["null", "string"]
            },
            "city": {
                "type": ["null", "string"]
            },
            "postalCode": {
                "type": ["null", "string"]
            },
            "rank":{
                "type": "number"
            }
        },
        "required": [
            "rank"
        ]
    }
    
The library used to verify the schema is `jsen <https://github.com/bugventure/jsen>`_. It works
in browsers like phantomjs (and it may work in node.js).

The schemas are located in the ``schemas`` directory of the project. A main ``schemas`` Typescript module
loads the json and parse them as JSON schema validators using JSEN.

Helpers
^^^^^^^

The ``helpers`` module contains helpers that create a client as an administrator or as a 
random user.

For example, let's create a client before all the tests:

.. code-block:: ts

    import { ServiceHelpers, failOnError } from '../helpers/helpers'; 
    
    describe("Sample", () => {
        // Create an API client    
        var adminClient = new ServiceHelpers();
     
        // Log in before any test is run
        beforeAll(done =>
            adminClient.common.login.login('admin', 'talentsoft')
                .then(token => done(), failOnError(done))
        );
    });
    

This client may be used to create a random employee:

.. code-block:: ts
    
    it("creates an employee", done => adminClient.employeeHelpers.createEmployee()
        .then(employee => done(), failOnError(done));
    
Or create another client that logs as a random employee:

.. code-block:: ts
    it("creates a client", done => adminClient.employeeHelpers.createNewEmployeeClient()
        .then(client => done(), failOnError(done));
        
The ``failOnError(done)`` method is a small helper that fails and calls the ``done()`` method
when it is called.

.. note:: This could be in a npm component that may be shared between all the jasmine tests.
