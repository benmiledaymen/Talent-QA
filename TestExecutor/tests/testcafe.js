const TIMEOUT = 5000;

module.exports = {
  "TestCafe\'s sample review": function(browser) {

    bindHelpers(browser);
    bindComponents(browser);

    var baseUrl = browser.launchUrl || "http://devexpress.github.io"

    browser
      .url(baseUrl, "/testcafe/example/", 832, 726, `Load page... "http://devexpress.github.io/testcafe/example/"`)
      .initNewPage("/testcafe/example/", `DOM ready & path is... "/testcafe/example/"`)
      .changeInput("[name=name]", `Bob Hope`, `Change input to... "Bob Hope"`)
      .click("div > fieldset:nth-of-type(2) > p > label", `Click element`)
      .click("div > fieldset:nth-of-type(2) > p:nth-of-type(3) > label", `Click element`)
      .click("div > fieldset:nth-of-type(2) > p:nth-of-type(5) > label", `Click element`)
      .click(".main-content .form-bottom label", `Click element`)
      .changeInput("[name=comments]", `SnapTest.io wants to integrate with TestCafe!`, `Change input to... "SnapTest.io wants to integrate with TestCafe!"`)
      .click("div:nth-of-type(2) > fieldset > p:nth-of-type(2) > label", `Click element`)
      .click("body form button", `Click element`)
      .initNewPage("/testcafe/example/thank-you.html", `DOM ready & path is... "/testcafe/example/thank-you.html"`)
      .elTextIs("h1", `Thank you, Bob Hope!`, `El text is... "Thank you, Bob Hope!"`)
      .elTextIs("p", `To learn more about TestCafe, please visit:`, `El text is... "To learn more about TestCafe, please visit:"`)
      .elTextIs("a", `devexpress.github.io/testcafe`, `El text is... "devexpress.github.io/testcafe"`)
      .click("a", `Click element`)
      .initNewPage("/testcafe/", `DOM ready & path is... "/testcafe/"`)
      .end();
  }
};

/*
 * Components 
 */

function bindComponents(browser) {

  browser.components = {};


}

/*
 * Auto-Generated helper code 
 */

const random = "" + parseInt(Math.random() * 1000000);
const random1 = "" + parseInt(Math.random() * 1000000);
const random2 = "" + parseInt(Math.random() * 1000000);
const random3 = "" + parseInt(Math.random() * 1000000);

function bindHelpers(browser) {

  var oldUrl = browser.url;
  var oldBack = browser.back;

  browser.url = function(baseUrl, pathname, width, height, description) {
    oldUrl(baseUrl + pathname);
    browser.resizeWindow(width, height);
    return this;
  };

  browser.back = function(description) {
    browser.perform(() => comment(description));
    browser.pause(5);
    oldBack();
    return this;
  };

  browser.initNewPage = function(pathname, description, timeout) {

    browser.perform(() => comment(description + " (navigating to " + pathname + ")"));

    var attempts = parseInt((timeout || TIMEOUT) / 1000);
    var currentAttempt = 0;

    function checkForPageLoadWithPathname(pathname) {
      browser.execute(function() {
        return {
          pathname: window.location.pathname,
          readyState: document.readyState
        };
      }, [], function(result) {
        if (result.value.pathname !== pathname || result.value.readyState !== "complete") {
          if (currentAttempt < attempts) {
            currentAttempt++;
            browser.pause(1000);
            checkForPageLoadWithPathname(pathname);
          } else {
            this.assert.ok(false, description)
          }
        }
      });
    }

    checkForPageLoadWithPathname(pathname);

    browser.execute(function() {
      window.alert = function() {};
      window.confirm = function() {
        return true;
      };
    }, []);

    return this;

  };

  browser.executeScript = function(description, script) {
    browser.perform(() => comment(description));
    browser.execute(function(script) {
      eval(script);
    }, [script], function(result) {});

    return this;
  };

  browser.switchToWindow = function(windowIndex, description) {
    browser.perform(() => comment(description));

    browser.windowHandles(function(result) {
      this.switchWindow(result.value[windowIndex]);
    });

    return this;
  };

  browser.scrollWindow = function(x, y, description) {
    browser.perform(() => comment(description));
    browser.execute(function(x, y) {
      window.scrollTo(x, y);
    }, [x, y], function(result) {});

    return this;
  };

  browser.scrollElement = function(selector, x, y, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);
    browser.execute(function(selector, x, y) {
      (function(el, x, y) {
        el.scrollLeft = x;
        el.scrollTop = y;
      })(document.querySelector(selector), x, y);
    }, [selector, x, y], function(result) {});

    return this;
  };

  browser.scrollWindowToElement = function(selector, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);
    browser.execute(function(selector, value) {
      (function(el) {
        if (el) {
          var elsScrollY = el.getBoundingClientRect().top + window.scrollY - el.offsetHeight;
          window.scrollTo(0, elsScrollY);
        }
      })(document.querySelector(selector), value);
    }, [selector]);

    return this;
  };

  browser.click = function(selector, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);
    browser.execute(function(selector) {

      (function(element) {

        function triggerMouseEvent(node, eventType) {
          var clickEvent = document.createEvent('MouseEvents');
          clickEvent.initEvent(eventType, true, true);
          node.dispatchEvent(clickEvent);
        }

        triggerMouseEvent(element, "mouseover");
        triggerMouseEvent(element, "mousedown");
        triggerMouseEvent(element, "mouseup");
        triggerMouseEvent(element, "click");

      })(document.querySelector(selector));

    }, [selector]);

    return this;

  };

  browser.changeInput = function(selector, value, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);
    browser.execute(function(selector, value) {

      (function(el) {
        function triggerKeyEvent(node, eventType) {
          var keydownEvent = document.createEvent('KeyboardEvent');
          keydownEvent.initEvent(eventType, true, false, null, 0, false, 0, false, 66, 0);
          node.dispatchEvent(keydownEvent);
        }

        if (el) {
          triggerKeyEvent(el, "keydown");
          el.focus();
          el.value = value;
          el.dispatchEvent(new Event('change', {
            bubbles: true
          }));
          el.dispatchEvent(new Event('input', {
            bubbles: true
          }));
          triggerKeyEvent(el, "keyup");
          triggerKeyEvent(el, "keypress");
        }
      })(document.querySelector(selector), value);

    }, [selector, value], function(result) {});

    return this;
  };

  browser.inputValueAssert = function(selector, value, description, timeout) {

    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);

    var attempts = parseInt((timeout || TIMEOUT) / 1000);
    var currentAttempt = 0;

    function checkforValue(selector, value) {
      browser.execute(function(selector) {
        var el = document.querySelector(selector);
        if (el) {
          if (el.type === 'checkbox' || el.type === 'radio') {
            return el.checked ? "true" : "false";
          } else {
            return el.value;
          }
        } else return null;
      }, [selector], function(result) {
        if (result.value !== value) {
          if (currentAttempt < attempts) {
            currentAttempt++;
            browser.pause(1000);
            checkforValue(selector, value);
          } else {
            this.assert.ok(false, description);
          }
        }
      });
    }

    checkforValue(selector, value);

    return this;

  };

  browser.elementPresent = function(selector, description, onFail, timeout) {

    browser.perform(() => comment(description));

    var attempts = parseInt((timeout || TIMEOUT) / 1000);
    var currentAttempt = 0;

    function checkforEl(selector) {
      browser.execute(function(selector) {
        return !!document.querySelector(selector);
      }, [selector], function(result) {
        if (!result.value && currentAttempt < attempts) {
          currentAttempt++;
          browser.pause(1000);
          checkforEl(selector);
        } else if (!result.value) {
          if (typeof onFail === "function") {
            onFail();
          } else {
            this.assert.ok(false, description);
          }
        }
      });
    }

    checkforEl(selector);

    return this;

  };

  browser.elementNotPresent = function(selector, description, timeout) {
    browser.perform(() => comment(description));
    browser.waitForElementNotPresent(selector, timeout || TIMEOUT);
    return this;
  };

  browser.focusOnEl = function(selector, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);

    browser.execute(function(selector) {
      (function(el) {
        var event = new FocusEvent('focus');
        el.dispatchEvent(event);
      })(document.querySelector(selector));
    }, [selector]);

    return this;
  };

  browser.formSubmit = function(selector, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);
    browser.execute(function(selector) {

      (function(el) {
        var event = new Event('submit');
        el.dispatchEvent(event);
      })(document.querySelector(selector));

    }, [selector]);

    return this;
  };

  browser.blurOffEl = function(selector, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);
    browser.execute(function(selector) {

      (function(el) {
        var event = new FocusEvent('blur');
        el.dispatchEvent(event);
      })(document.querySelector(selector));

    }, [selector]);

    return this;
  };

  browser.getElText = function(selector, onSuccess, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);

    var attempts = parseInt(TIMEOUT / 1000);
    var currentAttempt = 0;

    function checkforText(selector) {
      browser.execute(function(selector) {
        return (function(element) {
          if (!element) return null;
          var text = "";
          for (var i = 0; i < element.childNodes.length; ++i)
            if (element.childNodes[i].nodeType === 3)
              if (element.childNodes[i].textContent)
                text += element.childNodes[i].textContent;
          text = text.replace(/(\r\n|\n|\r)/gm, "");
          return text.trim();
        })(document.querySelector(selector));
      }, [selector], function(result) {
        if (result.value === "" && currentAttempt < attempts) {
          if (currentAttempt < attempts) {
            currentAttempt++;
            browser.pause(1000);
            checkforText(selector);
          } else {
            this.assert.ok(false, description);
          }
        } else {
          if (typeof onSuccess === "function") onSuccess.call(browser, result.value);
        }
      });
    }

    checkforText(selector);

    return this;

  };

  browser.elTextRegex = function(selector, regex, description, timeout) {
    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);
    return browser.getElText(selector, function(elsText) {
      var assertRegEx = new RegExp(regex);
      if (!assertRegEx.test(elsText)) {
        this.assert.ok(false, description);
      }
    })
  };

  browser.elTextIs = function(selector, assertText, description, timeout) {

    browser.perform(() => comment(description));
    browser.elementPresent(selector, null, () => {
      browser.assert.ok(false, description)
    }, timeout);

    var attempts = parseInt((timeout || TIMEOUT) / 1000);
    var currentAttempt = 0;

    function checkforText(selector, assertText) {
      browser.getElText(selector, function(elsText) {
        if (elsText !== assertText) {
          if (currentAttempt < attempts) {
            currentAttempt++;
            browser.pause(1000);
            checkforText(selector, assertText);
          } else {
            this.assert.ok(false, description);
          }
        }
      });
    }

    checkforText(selector, assertText);

    return this;

  };

  function comment(description) {
    if (description) {
      console.log(`${description}`);
    }
  }

}