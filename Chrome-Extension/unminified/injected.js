! function (e) {
    function n(r) {
        if (t[r]) return t[r].exports;
        var o = t[r] = {
            exports: {},
            id: r,
            loaded: !1
        };
        return e[r].call(o.exports, o, o.exports, n), o.loaded = !0, o.exports
    }
    var t = {};
    return n.m = e, n.c = t, n.p = "", n(0)
} ({
    0: function (e, n, t) {
        "use strict";

        function r(e) {
            var n = document.body.className.indexOf("snpa") !== -1,
                t = (0, o.isDescendant)(document.querySelector("#snpt-sandboxed"), e.target);
            !t && n && (e.stopPropagation(), e.preventDefault())
        }
        var o = t(99);
        window.document.documentElement.addEventListener("click", function (e) {
            r(e)
        }, !0), window.document.documentElement.addEventListener("submit", function (e) {
            r(e)
        }, !0), window.document.documentElement.addEventListener("change", function (e) {
            r(e)
        }, !0), window.document.documentElement.addEventListener("input", function (e) {
            r(e)
        }, !0)
    },
    99: function (e, n) {
        "use strict";

        function t(e) {
            return {
                selector: i(e)
            }
        }

        function r(e, n) {
            if (e == n) return !0;
            for (var t = n.parentNode; null != t;) {
                if (t == e) return !0;
                t = t.parentNode
            }
            return !1
        }

        function o(e) {
            if (e.length < 2) throw new Error("getCommonAncestor: not enough parameters");
            var n, t = "contains" in e[0] ? "contains" : "compareDocumentPosition",
                r = "contains" === t ? 1 : 16,
                o = e[0];
            e: for (; o = o.parentNode;) {
                for (n = e.length; n--;)
                    if ((o[t](e[n]) & r) !== r) continue e;
                return o
            }
            return null
        }

        function u(e) {
            if (e.length < 1) throw new Error("getLongestStringInArray: not enough parameters");
            if (e.length < 2) return e[0];
            var n = e[0];
            return e.forEach(function (e) {
                e.length > n.length && (n = e)
            }), n
        }

        function i(e) {
            var n = e;
            if (e instanceof Element) {
                for (var t = []; e.nodeType === Node.ELEMENT_NODE;) {
                    for (var r = e.nodeName.toLowerCase(), o = e, u = 1; o = o.previousElementSibling;) o.nodeName.toLowerCase() == r && u++;
                    1 != u && (r += ":nth-of-type(" + u + ")"), t.unshift(r), e = e.parentNode
                }
                for (var i = 0; i < t.length; i++) {
                    var c = t.slice(0);
                    if (t.shift(), document.querySelector(t.join(" > ")) !== n) return c.join(" > ")
                }
                return t.join(" > ")
            }
        }

        function c(e) {
            try {
                return 1 === document.querySelectorAll(e).length
            } catch (n) {
                return !1
            }
        }

        function a(e) {
            if (!e) return null;
            for (var n = "", t = 0; t < e.childNodes.length; ++t) 3 === e.childNodes[t].nodeType && e.childNodes[t].textContent && (n += e.childNodes[t].textContent);
            return n = n.replace(/(\r\n|\n|\r)/gm, ""), n.trim()
        }

        function d(e) {
            return "checkbox" === e.type || "radio" === e.type ? e.checked ? "true" : "false" : e.value
        }

        function l(e) {
            try {
                return document.querySelector(e)
            } catch (n) {
                return null
            }
        }
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.buildIdentifier = t, n.isDescendant = r, n.getCommonAncestor = o, n.getLongestStringInArray = u, n.getElSelector = i, n.checkUnique = c, n.getTextNode = a, n.getValue = d, n.safeQuerySelector = l
    }
});