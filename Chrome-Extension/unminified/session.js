! function (e) {
    function t(r) {
        if (n[r]) return n[r].exports;
        var o = n[r] = {
            exports: {},
            id: r,
            loaded: !1
        };
        return e[r].call(o.exports, o, o.exports, t), o.loaded = !0, o.exports
    }
    var n = {};
    return t.m = e, t.c = n, t.p = "", t(0)
} ([function (e, t, n) {
    "use strict";

    function r(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function o(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function i(e) {
        return {
            currentTabId: null,
            primaryTabId: null,
            activeTabs: [],
            tabsWithContextScript: [],
            user: null,
            appWindowId: e ? e : null,
            appWindowWidth: 600,
            appWindowX: 100,
            components: [],
            componentTree: l("Components"),
            tests: [],
            directory: l("tests"),
            activeTest: null,
            activeFolder: null,
            modal: null,
            selectedRows: [],
            expandedActions: [],
            recentSelectedIndex: null,
            isRecording: !1,
            isActivated: !1,
            isAssertMode: !1,
            isAutoSaving: !1,
            isPlayingBack: !1,
            playbackCursor: null,
            playbackActions: null,
            playbackBreakpoints: [],
            stepOverBreakpoints: [],
            playbackResult: {},
            playbackHasLoadedInitialPage: !1,
            isSelecting: !1,
            selectingForActionId: null,
            selectionCandidate: null,
            cursorIndex: null,
            processLoopMS: 0,
            viewStack: [new S["default"]("dashboard")],
            generatedCode: "",
            includeHarness: !1,
            selectedFramework: "nightwatch",
            selectedStyle: "flat",
            topDirName: "",
            actionElIndicatorSelector: null,
            shouldRefresh: !1,
            shouldGenNewTestActions: !1,
            showNWCode: !1,
            showHotkeys: !1,
            minimized: !1,
            viewMode: "window",
            viewX: 2,
            viewY: 2,
            viewWidth: 230,
            viewHeight: 25,
            hotkeyConfig: {
                TOGGLE_VIEW_HOTKEYS: "h",
                BACK: "b",
                MINIMIZE: "m",
                ASSERT_MODE: "a",
                RECORD_MODE: "r",
                CANCEL_MODE: "x",
                NEW_TEST: "n",
                SHOW_CODE: "c",
                DELETE_ROWS: "d",
                CLEAR_SELECTION: "z",
                ROLLUP_SELECTION: "option+c",
                UNDO: "q",
                REDO: "w",
                DOCK_RIGHT: "option+d",
                DOCK_LEFT: "option+a",
                DOCK_BOTTOM: "option+s",
                DOCK_TOP: "option+w",
                DOCK_WINDOW: "option+e",
                DOCK_MAX: "option+q",
                START_PLAYBACK: "t"
            },
            accountForms: {
                isProcessingLogin: !1,
                loginError: null,
                successMessage: null,
                email: "",
                password: "",
                password2: "",
                resettoken: null,
                acceptTerms: !1,
                type: K
            },
            activeTabAlert: !0,
            tutorialActivated: !1,
            tutorialStep: D.getStepName(0),
            tutorialStepNumber: 0
        }
    }

    function a(e) {
        s(e), e.cursorIndex = null, e.selectedRows = [], e.recentSelectedIndex = null, e.playbackResult = {}, e.expandedActions = []
    }

    function s(e) {
        e.isRecording = !1, e.isAssertMode = !1, e.isSelecting = !1, e.selectingForActionId = null, e.selectionCandidate = null, e.isPlayingBack = !1, e.playbackActions = null, e.playbackCursor = null, e.playbackHasLoadedInitialPage = !1, e.stepOverBreakpoints = []
    }

    function u(e) {
        return "codeviewer" === e.viewStack[e.viewStack.length - 1].name && e.activeTest
    }

    function l(e) {
        return {
            module: e,
            topLevel: !0,
            showGen: !0,
            children: []
        }
    }

    function c(e, t) { }
    var d = n(55),
        p = o(d),
        f = n(93),
        _ = o(f),
        h = n(56),
        v = o(h),
        m = n(222),
        y = o(m),
        g = n(321),
        w = n(315),
        T = n(7),
        b = o(T),
        E = n(28),
        x = (r(E), n(27)),
        S = o(x),
        A = n(255),
        k = o(A),
        O = n(32),
        R = n(237),
        N = n(316),
        C = n(317),
        I = n(142),
        P = n(309),
        L = o(P),
        U = n(257),
        D = r(U),
        K = "LOGIN_MODE",
        M = i();
    chrome.storage.sync.get(["user"], function (e) {
        e.user && (M.user = e.user, b["default"].toAll("stateChange", M), (0, R.loadItems)(e.user.apiKey).then(function (e) {
            M.tests = e.tests, M.components = e.components, e.directory && (M.directory = e.directory.tree), b["default"].toAll("stateChange", M)
        })["catch"](function () {
            M.user = null, chrome.storage.sync.set({
                user: null
            }), alert("We couldn't load your tests.  Please log in again. ")
        }))
    }), b["default"].onMessageFor(b["default"].SESSION, function (e, t, n) {
        var r = Date.now(),
            o = M.activeTest,
            l = {},
            d = u(M);
        switch (M.tutorialActivated && c(M, e), console.log(e.action), e.action) {
            case "startRecording":
                s(M), M.isRecording = !0, M.isAssertMode = !1, 0 === o.actions.length && (M.shouldGenNewTestActions = !0);
                break;
            case "setShouldGenNewTestActions":
                M.shouldGenNewTestActions = e.payload;
                break;
            case "setModal":
                M.modal = e.payload;
                break;
            case "openWindow":
                M.appWindowId ? chrome.windows.update(M.appWindowId, {
                    focused: !0
                }) : chrome.windows.create({
                    url: "window.html",
                    type: "popup",
                    width: M.appWindowWidth,
                    left: M.appWindowX,
                    focused: !0
                }, function (e) {
                    M.appWindowId = e.id, b["default"].toAll("stateChange", M)
                });
                break;
            case "setTutStep":
                (0, y["default"])(e.payload) ? (M.tutorialStep = D.getStepName(e.payload), M.tutorialStepNumber = e.payload) : (M.tutorialStep = e.payload, M.tutorialStepNumber = D.getStepIndex(e.payload));
                break;
            case "setTutActive":
                M.tutorialActivated = e.payload;
                break;
            case "loginWith":
                (0, N.loginProcess)(e.payload.email, e.payload.password, M);
                break;
            case "registerWith":
                (0, N.registerProcess)(e.payload.email, e.payload.password, e.payload.password2, e.payload.acceptTerms, M);
                break;
            case "forgotWith":
                (0, N.forgotProcess)(e.payload.email, M);
                break;
            case "resetWith":
                (0, N.resetProcess)(e.payload.resettoken, e.payload.password, e.payload.password2, M);
                break;
            case "resetLogin":
                M.accountForms.isProcessingLogin = !1, M.accountForms.loginError = null, M.accountForms.successMessage = null;
                break;
            case "setAccountForm":
                for (var f in e.payload) M.accountForms[f] = e.payload[f];
                break;
            case "logout":
                M = i(M.appWindowId), chrome.storage.sync.clear();
                break;
            case "setUser":
                M = (0, N.setUser)(e.payload, M);
                break;
            case "setHoverIndicator":
                M.actionElIndicatorSelector = e.payload;
                break;
            case "setIncludeHarness":
                M.includeHarness = e.payload;
                break;
            case "setFramework":
                M.selectedFramework = e.payload.framework, M.selectedStyle = e.payload.style;
                break;
            case "setTopDirName":
                M.topDirName = e.payload;
                break;
            case "shouldRefresh":
                M.shouldRefresh = e.payload;
                break;
            case "addTestFolder":
                M.directory.children.unshift({
                    collapsed: !1,
                    module: "New folder",
                    id: (0, O.generate)()
                }), (0, R.saveItemToDB)({
                    type: "directory",
                    tree: M.directory
                }, M, !0);
                break;
            case "saveTree":
                M.directory = e.payload, (0, R.saveItemToDB)({
                    type: "directory",
                    tree: M.directory
                }, M, !0);
                break;
            case "changeTestFolderName":
                var h = (0, I.findNodeById)(M.directory, e.payload.id);
                h.module = e.payload.name, (0, R.saveItemToDB)({
                    type: "directory",
                    tree: M.directory
                }, M, !0);
                break;
            case "startPlayback":
                M.isPlayingBack = !0, M.isRecording = !1, M.isAssertMode = !1, M.selectedRows.length > 0 ? M.playbackActions = (0, C.flattenActionsForPlayback)((0, L["default"])(o.actions.filter(function (e) {
                    return M.selectedRows.indexOf(e.id) !== -1
                })), (0, L["default"])(M.components), "component" === o.type ? o : null) : M.playbackActions = (0, C.flattenActionsForPlayback)((0, L["default"])(o.actions), (0, L["default"])(M.components), "component" === o.type ? o : null), M.playbackCursor || (M.playbackResult = {}), 0 === M.playbackActions.length && s(M);
                break;
            case "playbackUpdate":
                M.playbackCursor = e.payload.actionId, M.playbackResult[e.payload.actionId] = e.payload.result;
                break;
            case "playbackHasLoadedInitialPage":
                M.playbackHasLoadedInitialPage = !0;
                break;
            case "clearPlaybackResults":
                M.playbackResult = {};
                break;
            case "resetPlayback":
                s(M);
                break;
            case "playbackPause":
                M.isPlayingBack = !1;
                break;
            case "playbackPauseAt":
                M.isPlayingBack = !1, M.playbackCursor = e.payload;
                break;
            case "addStepOverBreakpoint":
                var m = (0, v["default"])(M.playbackActions, {
                    id: M.playbackCursor
                });
                m > -1 && m < M.playbackActions.length && M.stepOverBreakpoints.push(M.playbackActions[m + 1].id);
                break;
            case "setBreakpoint":
                var T = M.playbackBreakpoints.indexOf(e.payload);
                T === -1 && M.playbackBreakpoints.push(e.payload);
                break;
            case "removeBreakpoint":
                var T = M.playbackBreakpoints.indexOf(e.payload);
                T > -1 && M.playbackBreakpoints.splice(T, 1);
                break;
            case "clearBreakpoints":
                M.playbackBreakpoints = [];
                break;
            case "startSelection":
                s(M), M.isSelecting = !0, M.selectingForActionId = e.payload, M.selectionCandidate = null;
                break;
            case "cancelSelection":
                M.isSelecting = !1, M.selectingForActionId = null, M.selectionCandidate = null;
                break;
            case "setSelectionCandidate":
                M.selectionCandidate = e.payload;
                break;
            case "setSelection":
                var x = (0, L["default"])((0, _["default"])(M.activeTest.actions, {
                    id: M.selectingForActionId
                }));
                x.warnings = [], x.selector = e.payload, M.isSelecting = !1, M.selectingForActionId = null, M.selectionCandidate = null, (0, w.updateAction)(o, x), (0, R.saveItemToDB)(o, M);
                break;
            case "setShowHotkeys":
                M.showHotkeys = e.payload;
                break;
            case "setAssertMode":
                M.isAssertMode = e.payload, M.isRecording = !1;
                break;
            case "stopRecording":
                M.isRecording = !1, M.activeTabs = [];
                break;
            case "addNewTest":
                M.tests.push(e.payload), M.directory.children.unshift({
                    module: e.payload.name,
                    testId: e.payload.id,
                    leaf: !0,
                    id: (0, O.generate)(),
                    type: "test"
                }), (0, R.saveItemToDB)(o, M);
                break;
            case "duplicateTest":
                var S = (0, _["default"])(M.tests, {
                    id: e.payload
                }),
                    A = (0, p["default"])({}, S);
                A.id = (0, O.generate)(), A.name = "Copy of " + A.name, M.tests.push(A), (0, R.saveItemToDB)(A, M);
                break;
            case "deleteTest":
                var P = (0, v["default"])(M.tests, {
                    id: e.payload
                });
                (0, R.removeItemFromDB)(M.tests[P], M);
                var U = (0, I.findNode)(M.directory, {
                    testId: e.payload
                });
                M.directory = (0, I.removeNodeFromTree)(M.directory, U.id), M.tests.splice(P, 1);
                break;
            case "deleteComponent":
                var K = (0, v["default"])(M.components, {
                    id: e.payload
                });
                (0, R.removeItemFromDB)(M.components[K], M), M.components.splice(K, 1), (0, R.saveItemToDB)(o, M);
                break;
            case "addVarToComp":
                var j = (0, _["default"])(M.components, {
                    id: e.payload.id
                });
                j.variables.push(e.payload.compVar), (0, R.saveItemToDB)(o, M);
                break;
            case "updateVar":
                var W = (0, v["default"])(M.activeTest.variables, {
                    id: e.payload.id
                });
                M.activeTest.variables.splice(W, 1, e.payload), (0, R.saveItemToDB)(o, M);
                break;
            case "deleteVar":
                var F = (0, v["default"])(M.activeTest.variables, {
                    id: e.payload.id
                });
                M.activeTest.variables.splice(F, 1), (0, R.saveItemToDB)(o, M);
                break;
            case "setTestActive":
                M.activeTest = (0, _["default"])(M.tests, {
                    id: e.payload
                }), M.activeFolder = null, M.generatedCode = (0, g.generateNWCode)(M.activeTest.actions, M.activeTest.name, M.components);
                break;
            case "setFolderActive":
                M.activeTest = null, M.activeFolder = e.payload;
                break;
            case "setTests":
                (0, N.setTests)(e.payload, M);
                break;
            case "setComponents":
                (0, N.setComponents)(e.payload, M);
                break;
            case "setComponentActive":
                M.activeTest = (0, _["default"])(M.components, {
                    id: e.payload
                });
                break;
            case "pushRoute":
                a(M), M.viewStack.push(e.payload), u(M) && (M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components));
                break;
            case "generateCode":
                M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components);
                break;
            case "setActiveTabAlert":
                M.activeTabAlert = e.payload;
                break;
            case "backRoute":
                if (M.viewStack.length < 2) return;
                a(M), M.viewStack.pop();
                var B = M.viewStack[M.viewStack.length - 1];
                "testbuilder" === B.name && B.data.testId && (M.activeTest = (0, _["default"])(M.tests, {
                    id: B.data.testId
                })), "componentbuilder" === B.name && B.data.componentId && (M.activeTest = (0, _["default"])(M.components, {
                    id: B.data.componentId
                }));
                break;
            case "setAppRoute":
                M.appRoute = e.payload, d && (M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components));
                break;
            case "setActiveTab":
                M.primaryTabId = e.payload;
                break;
            case "setCurrentTab":
                M.currentTabId = e.payload;
                break;
            case "setCursor":
                M.cursorIndex = e.payload;
                break;
            case "setTestName":
                var G = (0, _["default"])(M.tests, {
                    id: e.payload.id
                });
                G.name = e.payload.value, o && (M.generatedCode = (0, g.generateNWCode)(o.actions, o.name, M.components)), (0, R.saveItemToDB)(G, M);
                break;
            case "setComponentName":
                var V = (0, _["default"])(M.components, {
                    id: e.payload.id
                });
                V.name = e.payload.value, o && (M.generatedCode = (0, g.generateNWCode)(o.actions, o.name, M.components)), (0, R.saveItemToDB)(V, M);
                break;
            case "updateNWAction":
                (0, w.updateAction)(o, e.payload), d && (M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components)), (0, R.saveItemToDB)(o, M);
                break;
            case "removeNWAction":
                (0, w.removeAction)(o, e.payload), d && (M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components)), (0, R.saveItemToDB)(o, M);
                break;
            case "removeNWActions":
                M.cursorIndex = null, (0, w.removeActions)(o, M.selectedRows), d && (M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components)), (0, R.saveItemToDB)(o, M);
                break;
            case "insertNWAction":
                if ((0, y["default"])(e.payload.insertAt)) (0, w.addAction)(o, e.payload.action, e.payload.insertAt);
                else if ((0, y["default"])(M.cursorIndex)) {
                    var H = o.actions.length;
                    (0, w.addAction)(o, e.payload.action, M.cursorIndex + 1), M.cursorIndex += o.actions.length - H
                } else (0, w.addAction)(o, e.payload.action);
                d && (M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components)), (0, R.saveItemToDB)(o, M);
                break;
            case "clearSelectedRows":
                M.selectedRows = [], M.recentSelectedIndex = null;
                break;
            case "setSelectedRows":
                M.selectedRows = e.payload.selectedRows, (0, y["default"])(e.payload.recentSelectedIndex) && e.payload.recentSelectedIndex > -1 && (M.recentSelectedIndex = e.payload.recentSelectedIndex);
                break;
            case "toggleActionExpanded":
                var z = M.expandedActions.indexOf(e.payload);
                z === -1 ? M.expandedActions.push(e.payload) : M.expandedActions.splice(z, 1);
                break;
            case "createComponentFromSelectedRows":
                var X = [],
                    q = null,
                    J = new k["default"]("New Component");
                o.actions.forEach(function (e, t) {
                    M.selectedRows.indexOf(e.id) !== -1 ? (q || (q = t), J.actions.push(e)) : X.push(e)
                }), o.actions = X, M.components.push(J);
                var $ = new E.ComponentAction(J.id);
                (0, w.addAction)(o, $, q), M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components), M.selectedRows = [], (0, R.saveItemToDB)(o, M), (0, R.saveItemToDB)(J, M);
                break;
            case "updateComponent":
                var Y = (0, v["default"])(M.components, {
                    id: e.payload.id
                });
                M.components.splice(Y, 1, e.payload), (0, R.saveItemToDB)(M.components[Y], M);
                break;
            case "setMinimized":
                M.minimized = e.payload;
                break;
            case "setViewMode":
                M.viewMode = e.payload;
                break;
            case "undo":
                var Z = (0, w.getPriorTestState)(o);
                if (Z)
                    if (M.activeTest = o = Z, "test" === o.type) {
                        var Q = (0, v["default"])(M.tests, {
                            id: o.id
                        });
                        M.tests.splice(Q, 1, Z)
                    } else {
                        var ee = (0, v["default"])(M.components, {
                            id: o.id
                        });
                        M.tests.splice(ee, 1, Z)
                    } (0, R.saveItemToDB)(o, M);
                break;
            case "redo":
                var te = (0, w.getLaterTestState)(o);
                if (te)
                    if (M.activeTest = o = te, "test" === o.type) {
                        var Q = (0, v["default"])(M.tests, {
                            id: o.id
                        });
                        M.tests.splice(Q, 1, te)
                    } else {
                        var ee = (0, v["default"])(M.components, {
                            id: o.id
                        });
                        M.tests.splice(ee, 1, te)
                    } (0, R.saveItemToDB)(o, M);
                break;
            case "setViewModeWindowAttr":
                M.viewX = e.payload.viewX, M.viewY = e.payload.viewY, M.viewWidth = e.payload.viewWidth, M.viewHeight = e.payload.viewHeight;
                break;
            case "setActivated":
                M.isActivated = e.payload, e.payload ? M.primaryTabId = M.currentTabId : (M.generatedCode = "", M.isRecording = !1, s(M));
                break;
            case "getState":
                return void n(M);
            case "addAction":
                if ((0, y["default"])(M.cursorIndex)) {
                    var H = o.actions.length;
                    (0, w.addRecordedAction)(o, e.payload, M.cursorIndex + 1), M.cursorIndex += o.actions.length - H
                } else (0, w.addRecordedAction)(o, e.payload);
                d && (M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components)), (0, R.saveItemToDB)(o, M);
                break;
            case "resetTest":
                M.generatedCode = "", M.isAssertMode = !1, M.isRecording = !1, M.cursorIndex = null, M.generatedCode = (0, g.generateNWCode)(o.actions, M.activeTest.name, M.components)
        }
        b["default"].toAll("stateChange", M), n((0, p["default"])({}, M, l)), console.log(Date.now() - r)
    }), chrome.webNavigation.onHistoryStateUpdated.addListener(function (e) {
        var t = M.activeTest;
        e.tabId === M.primaryTabId && M.isRecording && "link" === e.transitionType && setTimeout(function () {
            var n = new E.PushstateAction(e.url, t.actions.length);
            (0, w.addRecordedAction)(M.activeTest, n), M.generatedCode = (0, g.generateNWCode)(t.actions, M.activeTest.name, M.components), b["default"].toAll("stateChange", M), (0, R.saveItemToDB)(t, M)
        }, 100)
    }), chrome.webNavigation.onCommitted.addListener(function (e) {
        M.primaryTabId || (M.primaryTabId = e.tabId, M.currentTabId = e.tabId, M.activeTabs.indexOf(e.tabId) === -1 && M.activeTabs.push(e.tabId), b["default"].toAll("stateChange", M))
    }), chrome.webNavigation.onCompleted.addListener(function (e) {
        M.primaryTabId || (M.primaryTabId = e.tabId, M.currentTabId = e.tabId, M.activeTabs.indexOf(e.tabId) === -1 && M.activeTabs.push(e.tabId), b["default"].toAll("stateChange", M))
    }), chrome.tabs.onActivated.addListener(function (e) {
        setTimeout(function () {
            if (e.windowId !== M.appWindowId) {
                if (M.currentTabId = e.tabId, M.isRecording || M.isPlayingBack) {
                    M.primaryTabId = e.tabId;
                    var t = M.activeTabs.indexOf(e.tabId);
                    if (t === -1 && M.activeTabs.push(e.tabId), !M.isPlayingBack) {
                        var n = M.activeTabs.indexOf(e.tabId);
                        (0, w.addAction)(M.activeTest, new E.AutoChangeWindowAction(n + 1))
                    }
                }
                b["default"].toAll("stateChange", M)
            }
        }, 100)
    }), chrome.windows.onRemoved.addListener(function (e) {
        e === M.appWindowId && (M.appWindowId = null, b["default"].toAll("stateChange", M))
    }), chrome.runtime.onConnect.addListener(function (e) {
        b["default"].addDevToolWindow(e), e.onDisconnect.addListener(function (e) {
            b["default"].removeDevToolWindow(e)
        })
    }), chrome.system.display.getInfo(function (e) {
        var t = e[0];
        M.appWindowX = t.workArea.width - M.appWindowWidth
    }), chrome.runtime.onMessage.addListener(function (e, t, n) {
        "registerAsLoaded" === e && (M.tabsWithContextScript.indexOf(t.tab.id) === -1 && M.tabsWithContextScript.push(t.tab.id), n(t.tab.id))
    })
}, , function (e, t) {
    "use strict";
    t.__esModule = !0, t["default"] = function (e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(105),
        __esModule: !0
    }
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var o = n(101),
        i = r(o),
        a = n(100),
        s = r(a),
        u = n(63),
        l = r(u);
    t["default"] = function (e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + ("undefined" == typeof t ? "undefined" : (0, l["default"])(t)));
        e.prototype = (0, s["default"])(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (i["default"] ? (0, i["default"])(e, t) : e.__proto__ = t)
    }
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var o = n(63),
        i = r(o);
    t["default"] = function (e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" !== ("undefined" == typeof t ? "undefined" : (0, i["default"])(t)) && "function" != typeof t ? e : t
    }
}, , function (e, t, n) {
    "use strict";
    var r = [],
        o = {
            SESSION: "SESSION",
            POPUP: "POPUP",
            PANEL: "PANEL",
            TAB: "TAB",
            NOOP: function () { },
            to: function (e, t) {
                var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
                    r = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : this.NOOP;
                e === this.TAB ? chrome.tabs.query({
                    active: !0,
                    currentWindow: !0
                }, function (o) {
                    chrome.tabs.sendMessage(o[0].id, {
                        action: t,
                        payload: n,
                        destination: e
                    }, function (e) {
                        r(e)
                    })
                }) : chrome.runtime.sendMessage({
                    action: t,
                    payload: n,
                    destination: e
                }, function (e) {
                    r(e)
                })
            },
            toAll: function (e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                    n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : this.NOOP;
                chrome.runtime.sendMessage({
                    action: e,
                    payload: t,
                    destination: "all"
                }, function (e) {
                    n(e)
                }), "undefined" != typeof chrome.tabs && chrome.tabs.query({
                    active: !0
                }, function (r) {
                    r.forEach(function (r) {
                        chrome.tabs.sendMessage(r.id, {
                            action: e,
                            payload: t,
                            destination: "all"
                        }, function (e) {
                            n(e)
                        })
                    })
                }), r.length > 0 && r.forEach(function (n) {
                    n.postMessage({
                        action: e,
                        payload: t
                    })
                })
            },
            onMessageFor: function (e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.NOOP;
                chrome.runtime.onMessage.addListener(function (n, r, o) {
                    n.destination !== e && "all" !== n.destination || t(n, r, o)
                })
            },
            addDevToolWindow: function (e) {
                r.push(e)
            },
            removeDevToolWindow: function (e) {
                var t = r.indexOf(e);
                t !== -1 && r.splice(t, 1)
            }
        };
    e.exports = o
}, , function (e, t) {
    var n = e.exports = {
        version: "2.4.0"
    };
    "number" == typeof __e && (__e = n)
}, function (e, t) {
    var n = e.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")();
    "number" == typeof __g && (__g = n)
}, function (e, t, n) {
    var r = n(87),
        o = "object" == typeof self && self && self.Object === Object && self,
        i = r || o || Function("return this")();
    e.exports = i
}, function (e, t) {
    var n = Array.isArray;
    e.exports = n
}, function (e, t) {
    var n = {}.hasOwnProperty;
    e.exports = function (e, t) {
        return n.call(e, t)
    }
}, function (e, t, n) {
    var r = n(76),
        o = n(34);
    e.exports = function (e) {
        return r(o(e))
    }
}, function (e, t, n) {
    e.exports = !n(20)(function () {
        return 7 != Object.defineProperty({}, "a", {
            get: function () {
                return 7
            }
        }).a
    })
}, function (e, t, n) {
    var r = n(21),
        o = n(67),
        i = n(43),
        a = Object.defineProperty;
    t.f = n(15) ? Object.defineProperty : function (e, t, n) {
        if (r(e), t = i(t, !0), r(n), o) try {
            return a(e, t, n)
        } catch (s) { }
        if ("get" in n || "set" in n) throw TypeError("Accessors not supported!");
        return "value" in n && (e[t] = n.value), e
    }
}, function (e, t, n) {
    var r = n(10),
        o = n(9),
        i = n(65),
        a = n(18),
        s = "prototype",
        u = function (e, t, n) {
            var l, c, d, p = e & u.F,
                f = e & u.G,
                _ = e & u.S,
                h = e & u.P,
                v = e & u.B,
                m = e & u.W,
                y = f ? o : o[t] || (o[t] = {}),
                g = y[s],
                w = f ? r : _ ? r[t] : (r[t] || {})[s];
            f && (n = t);
            for (l in n) c = !p && w && void 0 !== w[l], c && l in y || (d = c ? w[l] : n[l], y[l] = f && "function" != typeof w[l] ? n[l] : v && c ? i(d, r) : m && w[l] == d ? function (e) {
                var t = function (t, n, r) {
                    if (this instanceof e) {
                        switch (arguments.length) {
                            case 0:
                                return new e;
                            case 1:
                                return new e(t);
                            case 2:
                                return new e(t, n)
                        }
                        return new e(t, n, r)
                    }
                    return e.apply(this, arguments)
                };
                return t[s] = e[s], t
            } (d) : h && "function" == typeof d ? i(Function.call, d) : d, h && ((y.virtual || (y.virtual = {}))[l] = d, e & u.R && g && !g[l] && a(g, l, d)))
        };
    u.F = 1, u.G = 2, u.S = 4, u.P = 8, u.B = 16, u.W = 32, u.U = 64, u.R = 128, e.exports = u
}, function (e, t, n) {
    var r = n(16),
        o = n(29);
    e.exports = n(15) ? function (e, t, n) {
        return r.f(e, t, o(1, n))
    } : function (e, t, n) {
        return e[t] = n, e
    }
}, function (e, t, n) {
    var r = n(41)("wks"),
        o = n(30),
        i = n(10).Symbol,
        a = "function" == typeof i,
        s = e.exports = function (e) {
            return r[e] || (r[e] = a && i[e] || (a ? i : o)("Symbol." + e))
        };
    s.store = r
}, function (e, t) {
    e.exports = function (e) {
        try {
            return !!e()
        } catch (t) {
            return !0
        }
    }
}, function (e, t, n) {
    var r = n(22);
    e.exports = function (e) {
        if (!r(e)) throw TypeError(e + " is not an object!");
        return e
    }
}, function (e, t) {
    e.exports = function (e) {
        return "object" == typeof e ? null !== e : "function" == typeof e
    }
}, function (e, t, n) {
    var r = n(72),
        o = n(35);
    e.exports = Object.keys || function (e) {
        return r(e, o)
    }
}, function (e, t, n) {
    function r(e) {
        return null == e ? void 0 === e ? u : s : l && l in Object(e) ? i(e) : a(e)
    }
    var o = n(47),
        i = n(183),
        a = n(208),
        s = "[object Null]",
        u = "[object Undefined]",
        l = o ? o.toStringTag : void 0;
    e.exports = r
}, function (e, t) {
    function n(e) {
        return null != e && "object" == typeof e
    }
    e.exports = n
}, function (e, t, n) {
    function r(e, t) {
        var n = i(e, t);
        return o(n) ? n : void 0
    }
    var o = n(168),
        i = n(185);
    e.exports = r
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(2),
        i = r(o),
        a = n(32),
        s = function u(e, t) {
            (0, i["default"])(this, u), this.name = "404", this.data = {}, this.id = (0, a.generate)(), e && (this.name = e), t && (this.data = t)
        };
    t["default"] = s
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.MetaScanAction = t.ScreenshotAction = t.PauseAction = t.RefreshAction = t.ForwardAction = t.BackAction = t.BlankAction = t.ElNotPresentAssertAction = t.ElPresentAssertAction = t.ValueAssertAction = t.TextRegexAssertAction = t.TextAssertAction = t.UrlChangeIndicatorAction = t.ComponentAction = t.PageloadAction = t.FullPageloadAction = t.AutoChangeWindowAction = t.ChangeWindowAction = t.PushstateAction = t.PopstateAction = t.ScrollElement = t.ScrollWindowToElement = t.ScrollWindow = t.ExecuteScriptAction = t.InputAction = t.BlurAction = t.FocusAction = t.MouseoverAction = t.MousedownAction = t.SubmitAction = t.KeydownAction = t.Action = t.META_SCAN = t.EXECUTE_SCRIPT = t.SCROLL_ELEMENT = t.SCROLL_WINDOW_ELEMENT = t.SCROLL_WINDOW = t.SUBMIT = t.BLUR = t.FOCUS = t.URL_CHANGE_INDICATOR = t.SCREENSHOT = t.PAUSE = t.COMPONENT = t.REFRESH = t.FORWARD = t.BACK = t.EL_NOT_PRESENT_ASSERT = t.EL_PRESENT_ASSERT = t.VALUE_ASSERT = t.TEXT_REGEX_ASSERT = t.PATH_ASSERT = t.TEXT_ASSERT = t.PAGELOAD = t.CHANGE_WINDOW_AUTO = t.CHANGE_WINDOW = t.FULL_PAGELOAD = t.PUSHSTATE = t.POPSTATE = t.INPUT = t.MOUSEOVER = t.MOUSEDOWN = t.KEYDOWN = t.BLANK = void 0;
    var o = n(3),
        i = r(o),
        a = n(5),
        s = r(a),
        u = n(4),
        l = r(u),
        c = n(2),
        d = r(c),
        p = n(32),
        f = t.BLANK = "BLANK",
        _ = t.KEYDOWN = "KEYDOWN",
        h = t.MOUSEDOWN = "MOUSEDOWN",
        v = t.MOUSEOVER = "MOUSEOVER",
        m = t.INPUT = "INPUT",
        y = t.POPSTATE = "POPSTATE",
        g = t.PUSHSTATE = "PUSHSTATE",
        w = t.FULL_PAGELOAD = "FULL_PAGELOAD",
        T = t.CHANGE_WINDOW = "CHANGE_WINDOW",
        b = t.CHANGE_WINDOW_AUTO = "CHANGE_WINDOW_AUTO",
        E = t.PAGELOAD = "PAGELOAD",
        x = t.TEXT_ASSERT = "TEXT_ASSERT",
        S = (t.PATH_ASSERT = "PATH_ASSERT", t.TEXT_REGEX_ASSERT = "TEXT_REGEX_ASSERT"),
        A = t.VALUE_ASSERT = "VALUE_ASSERT",
        k = t.EL_PRESENT_ASSERT = "EL_PRESENT_ASSERT",
        O = t.EL_NOT_PRESENT_ASSERT = "EL_NOT_PRESENT_ASSERT",
        R = t.BACK = "BACK",
        N = t.FORWARD = "FORWARD",
        C = t.REFRESH = "REFRESH",
        I = t.COMPONENT = "COMPONENT",
        P = t.PAUSE = "PAUSE",
        L = t.SCREENSHOT = "SCREENSHOT",
        U = t.URL_CHANGE_INDICATOR = "URL_CHANGE_INDICATOR",
        D = t.FOCUS = "FOCUS",
        K = t.BLUR = "BLUR",
        M = t.SUBMIT = "SUBMIT",
        j = t.SCROLL_WINDOW = "SCROLL_WINDOW",
        W = t.SCROLL_WINDOW_ELEMENT = "SCROLL_WINDOW_ELEMENT",
        F = t.SCROLL_ELEMENT = "SCROLL_ELEMENT",
        B = t.EXECUTE_SCRIPT = "EXECUTE_SCRIPT",
        G = t.META_SCAN = "META_SCAN",
        V = t.Action = function H(e, t, n) {
            (0, d["default"])(this, H), this.type = null, this.selector = "Unknown", this.nodeName = "Unknown", this.description = null, this.timeout = null, this.warnings = [], this.suggestions = [], this.id = (0, p.generate)(), "string" == typeof e && (this.selector = e), t && (this.warnings = t), n && (this.suggestions = n)
        };
    t.KeydownAction = function (e) {
        function t(e, n, r, o, a, u) {
            (0, d["default"])(this, t);
            var l = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, a, u));
            return l.keyValue = "", l.keyCode = "", l.inputValue = "", l.type = _, l.keyValue = n, l.keyCode = r, l.inputValue = o, l
        }
        return (0, l["default"])(t, e), t
    } (V), t.SubmitAction = function (e) {
        function t(e, n, r) {
            (0, d["default"])(this, t);
            var o = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, n, r));
            return o.type = M, o
        }
        return (0, l["default"])(t, e), t
    } (V), t.MousedownAction = function (e) {
        function t(e, n, r, o, a, u) {
            (0, d["default"])(this, t);
            var l = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, a, u));
            return l.textContent = null, l.textAsserted = !1, l.x = null, l.y = null, l.type = h, n && (l.textContent = n), r && (l.x = r), o && (l.y = o), l
        }
        return (0, l["default"])(t, e), t
    } (V), t.MouseoverAction = function (e) {
        function t(e, n, r) {
            (0, d["default"])(this, t);
            var o = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, n, r));
            return o.type = v, o
        }
        return (0, l["default"])(t, e), t
    } (V), t.FocusAction = function (e) {
        function t(e, n, r) {
            (0, d["default"])(this, t);
            var o = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, n, r));
            return o.type = D, o
        }
        return (0, l["default"])(t, e), t
    } (V), t.BlurAction = function (e) {
        function t(e, n, r) {
            (0, d["default"])(this, t);
            var o = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, n, r));
            return o.type = K, o
        }
        return (0, l["default"])(t, e), t
    } (V), t.InputAction = function (e) {
        function t(e, n, r, o, a) {
            (0, d["default"])(this, t);
            var u = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, o, a));
            return u.value = null, u.inputType = null, u.type = m, u.inputType = n, u.value = r, u
        }
        return (0, l["default"])(t, e), t
    } (V), t.ExecuteScriptAction = function (e) {
        function t() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
                n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
            (0, d["default"])(this, t);
            var r = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return r.script = null, r.description = "", r.type = B, r.script = e, r.description = n, r
        }
        return (0, l["default"])(t, e), t
    } (V), t.ScrollWindow = function (e) {
        function t() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
            arguments[2], arguments[3];
            (0, d["default"])(this, t);
            var r = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return r.x = 0, r.y = 0, r.type = j, r.x = e, r.y = n, r
        }
        return (0, l["default"])(t, e), t
    } (V), t.ScrollWindowToElement = function (e) {
        function t(e, n, r) {
            (0, d["default"])(this, t);
            var o = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, n, r));
            return o.type = W, o
        }
        return (0, l["default"])(t, e), t
    } (V), t.ScrollElement = function (e) {
        function t() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
            (0, d["default"])(this, t);
            var r = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return r.x = 0, r.y = 0, r.type = F, r.x = e, r.y = n, r
        }
        return (0, l["default"])(t, e), t
    } (V), t.PopstateAction = function (e) {
        function t(e, n) {
            (0, d["default"])(this, t);
            var r = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return r.value = "", r.index = null, r.type = y, r.value = e, r.index = n, r
        }
        return (0, l["default"])(t, e), t
    } (V), t.PushstateAction = function (e) {
        function t(e, n) {
            (0, d["default"])(this, t);
            var r = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return r.value = "", r.index = null, r.type = g, r.value = e, r.index = n, r
        }
        return (0, l["default"])(t, e), t
    } (V), t.ChangeWindowAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return n.value = 0, n.value = e, n.type = T, n
        }
        return (0, l["default"])(t, e), t
    } (V), t.AutoChangeWindowAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return n.value = 0, n.value = e, n.type = b, n
        }
        return (0, l["default"])(t, e), t
    } (V), t.FullPageloadAction = function (e) {
        function t() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "Set url...",
                n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 500,
                r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 500;
            (0, d["default"])(this, t);
            var o = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return o.value = "", o.isInitialLoad = !1, o.width = 500, o.height = 500, o.type = w, o.value = e, o.width = n, o.height = r, o
        }
        return (0, l["default"])(t, e), t
    } (V), t.PageloadAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return n.value = "", n.type = E, e && (n.value = e), n
        }
        return (0, l["default"])(t, e), t
    } (V), t.ComponentAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return n.componentId = null, n.variables = [], n.type = I, e && (n.componentId = e), n
        }
        return (0, l["default"])(t, e), t
    } (V), t.UrlChangeIndicatorAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return n.value = "", n.index = null, n.value = e, n.type = U, n
        }
        return (0, l["default"])(t, e), t
    } (V), t.TextAssertAction = function (e) {
        function t(e, n, r, o) {
            (0, d["default"])(this, t);
            var a = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, r, o));
            return a.value = "", a.type = x, a.value = n, a
        }
        return (0, l["default"])(t, e), t
    } (V), t.TextRegexAssertAction = function (e) {
        function t(e, n, r, o) {
            (0, d["default"])(this, t);
            var a = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, r, o));
            return a.value = ".+", a.type = S, a.value = n, a
        }
        return (0, l["default"])(t, e), t
    } (V), t.ValueAssertAction = function (e) {
        function t(e, n, r, o) {
            (0, d["default"])(this, t);
            var a = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, r, o));
            return a.value = "", a.type = A, a.value = n, a
        }
        return (0, l["default"])(t, e), t
    } (V), t.ElPresentAssertAction = function (e) {
        function t(e, n, r) {
            (0, d["default"])(this, t);
            var o = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, n, r));
            return o.type = k, o
        }
        return (0, l["default"])(t, e), t
    } (V), t.ElNotPresentAssertAction = function (e) {
        function t(e, n, r) {
            (0, d["default"])(this, t);
            var o = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e, n, r));
            return o.type = O, o
        }
        return (0, l["default"])(t, e), t
    } (V), t.BlankAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return e.type = f, e
        }
        return (0, l["default"])(t, e), t
    } (V), t.BackAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return e.type = R, e
        }
        return (0, l["default"])(t, e), t
    } (V), t.ForwardAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return e.type = N, e
        }
        return (0, l["default"])(t, e), t
    } (V), t.RefreshAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return e.type = C, e
        }
        return (0, l["default"])(t, e), t
    } (V), t.PauseAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return n.value = 2e3, n.type = P, e && (n.value = e), n
        }
        return (0, l["default"])(t, e), t
    } (V), t.ScreenshotAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return n.value = "<filename>", n.type = L, e && (n.value = e), n
        }
        return (0, l["default"])(t, e), t
    } (V), t.MetaScanAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, s["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this));
            return e.pTitle = null, e.pDescription = null, e.type = G, e
        }
        return (0, l["default"])(t, e), t
    } (V)
}, function (e, t) {
    e.exports = function (e, t) {
        return {
            enumerable: !(1 & e),
            configurable: !(2 & e),
            writable: !(4 & e),
            value: t
        }
    }
}, function (e, t) {
    var n = 0,
        r = Math.random();
    e.exports = function (e) {
        return "Symbol(".concat(void 0 === e ? "" : e, ")_", (++n + r).toString(36))
    }
}, function (e, t) {
    t.f = {}.propertyIsEnumerable
}, function (e, t, n) {
    "use strict";
    e.exports = n(232)
}, function (e, t) {
    function n(e) {
        var t = typeof e;
        return null != e && ("object" == t || "function" == t)
    }
    e.exports = n;
}, function (e, t) {
    e.exports = function (e) {
        if (void 0 == e) throw TypeError("Can't call method on  " + e);
        return e
    }
}, function (e, t) {
    e.exports = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")
}, function (e, t) {
    e.exports = {}
}, function (e, t) {
    e.exports = !0
}, function (e, t, n) {
    var r = n(21),
        o = n(119),
        i = n(35),
        a = n(40)("IE_PROTO"),
        s = function () { },
        u = "prototype",
        l = function () {
            var e, t = n(66)("iframe"),
                r = i.length,
                o = "<",
                a = ">";
            for (t.style.display = "none", n(113).appendChild(t), t.src = "javascript:", e = t.contentWindow.document, e.open(), e.write(o + "script" + a + "document.F=Object" + o + "/script" + a), e.close(), l = e.F; r--;) delete l[u][i[r]];
            return l()
        };
    e.exports = Object.create || function (e, t) {
        var n;
        return null !== e ? (s[u] = r(e), n = new s, s[u] = null, n[a] = e) : n = l(), void 0 === t ? n : o(n, t)
    }
}, function (e, t, n) {
    var r = n(16).f,
        o = n(13),
        i = n(19)("toStringTag");
    e.exports = function (e, t, n) {
        e && !o(e = n ? e : e.prototype, i) && r(e, i, {
            configurable: !0,
            value: t
        })
    }
}, function (e, t, n) {
    var r = n(41)("keys"),
        o = n(30);
    e.exports = function (e) {
        return r[e] || (r[e] = o(e))
    }
}, function (e, t, n) {
    var r = n(10),
        o = "__core-js_shared__",
        i = r[o] || (r[o] = {});
    e.exports = function (e) {
        return i[e] || (i[e] = {})
    }
}, function (e, t) {
    var n = Math.ceil,
        r = Math.floor;
    e.exports = function (e) {
        return isNaN(e = +e) ? 0 : (e > 0 ? r : n)(e)
    }
}, function (e, t, n) {
    var r = n(22);
    e.exports = function (e, t) {
        if (!r(e)) return e;
        var n, o;
        if (t && "function" == typeof (n = e.toString) && !r(o = n.call(e))) return o;
        if ("function" == typeof (n = e.valueOf) && !r(o = n.call(e))) return o;
        if (!t && "function" == typeof (n = e.toString) && !r(o = n.call(e))) return o;
        throw TypeError("Can't convert object to primitive value")
    }
}, function (e, t, n) {
    var r = n(10),
        o = n(9),
        i = n(37),
        a = n(45),
        s = n(16).f;
    e.exports = function (e) {
        var t = o.Symbol || (o.Symbol = i ? {} : r.Symbol || {});
        "_" == e.charAt(0) || e in t || s(t, e, {
            value: a.f(e)
        })
    }
}, function (e, t, n) {
    t.f = n(19)
}, function (e, t, n) {
    function r(e) {
        var t = -1,
            n = null == e ? 0 : e.length;
        for (this.clear(); ++t < n;) {
            var r = e[t];
            this.set(r[0], r[1])
        }
    }
    var o = n(194),
        i = n(195),
        a = n(196),
        s = n(197),
        u = n(198);
    r.prototype.clear = o, r.prototype["delete"] = i, r.prototype.get = a, r.prototype.has = s, r.prototype.set = u, e.exports = r
}, function (e, t, n) {
    var r = n(11),
        o = r.Symbol;
    e.exports = o
}, function (e, t, n) {
    function r(e, t) {
        for (var n = e.length; n--;)
            if (o(e[n][0], t)) return n;
        return -1
    }
    var o = n(92);
    e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        var n = e.__data__;
        return o(t) ? n["string" == typeof t ? "string" : "hash"] : n.map
    }
    var o = n(192);
    e.exports = r
}, function (e, t, n) {
    var r = n(26),
        o = r(Object, "create");
    e.exports = o
}, function (e, t, n) {
    function r(e) {
        if ("string" == typeof e || o(e)) return e;
        var t = e + "";
        return "0" == t && 1 / e == -i ? "-0" : t
    }
    var o = n(52),
        i = 1 / 0;
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        return "symbol" == typeof e || i(e) && o(e) == a
    }
    var o = n(24),
        i = n(25),
        a = "[object Symbol]";
    e.exports = r
}, function (e, t) {
    t.f = Object.getOwnPropertySymbols
}, function (e, t, n) {
    var r = n(34);
    e.exports = function (e) {
        return Object(r(e))
    }
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var o = n(96),
        i = r(o);
    t["default"] = i["default"] || function (e) {
        for (var t = 1; t < arguments.length; t++) {
            var n = arguments[t];
            for (var r in n) Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r])
        }
        return e
    }
}, function (e, t, n) {
    function r(e, t, n) {
        var r = null == e ? 0 : e.length;
        if (!r) return -1;
        var u = null == n ? 0 : a(n);
        return u < 0 && (u = s(r + u, 0)), o(e, i(t, 3), u)
    }
    var o = n(162),
        i = n(97),
        a = n(228),
        s = Math.max;
    e.exports = r
}, , function (e, t, n) {
    var r = n(26),
        o = n(11),
        i = r(o, "Map");
    e.exports = i
}, function (e, t, n) {
    function r(e) {
        var t = -1,
            n = null == e ? 0 : e.length;
        for (this.clear(); ++t < n;) {
            var r = e[t];
            this.set(r[0], r[1])
        }
    }
    var o = n(199),
        i = n(200),
        a = n(201),
        s = n(202),
        u = n(203);
    r.prototype.clear = o, r.prototype["delete"] = i, r.prototype.get = a, r.prototype.has = s, r.prototype.set = u, e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        if (o(e)) return !1;
        var n = typeof e;
        return !("number" != n && "symbol" != n && "boolean" != n && null != e && !i(e)) || (s.test(e) || !a.test(e) || null != t && e in Object(t))
    }
    var o = n(12),
        i = n(52),
        a = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
        s = /^\w*$/;
    e.exports = r
}, function (e, t) {
    function n(e) {
        return "number" == typeof e && e > -1 && e % 1 == 0 && e <= r
    }
    var r = 9007199254740991;
    e.exports = n
}, function (e, t, n) {
    "use strict";

    function r() {
        p = !1
    }

    function o(e) {
        if (!e) return void (c !== _ && (c = _, r()));
        if (e !== c) {
            if (e.length !== _.length) throw new Error("Custom alphabet for shortid must be " + _.length + " unique characters. You submitted " + e.length + " characters: " + e);
            var t = e.split("").filter(function (e, t, n) {
                return t !== n.lastIndexOf(e)
            });
            if (t.length) throw new Error("Custom alphabet for shortid must be " + _.length + " unique characters. These characters were not unique: " + t.join(", "));
            c = e, r()
        }
    }

    function i(e) {
        return o(e), c
    }

    function a(e) {
        f.seed(e), d !== e && (r(), d = e)
    }

    function s() {
        c || o(_);
        for (var e, t = c.split(""), n = [], r = f.nextValue(); t.length > 0;) r = f.nextValue(), e = Math.floor(r * t.length), n.push(t.splice(e, 1)[0]);
        return n.join("")
    }

    function u() {
        return p ? p : p = s()
    }

    function l(e) {
        var t = u();
        return t[e]
    }
    var c, d, p, f = n(235),
        _ = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
    e.exports = {
        characters: i,
        seed: a,
        lookup: l,
        shuffled: u
    }
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var o = n(103),
        i = r(o),
        a = n(102),
        s = r(a),
        u = "function" == typeof s["default"] && "symbol" == typeof i["default"] ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof s["default"] && e.constructor === s["default"] && e !== s["default"].prototype ? "symbol" : typeof e
        };
    t["default"] = "function" == typeof s["default"] && "symbol" === u(i["default"]) ? function (e) {
        return "undefined" == typeof e ? "undefined" : u(e)
    } : function (e) {
        return e && "function" == typeof s["default"] && e.constructor === s["default"] && e !== s["default"].prototype ? "symbol" : "undefined" == typeof e ? "undefined" : u(e)
    }
}, function (e, t) {
    var n = {}.toString;
    e.exports = function (e) {
        return n.call(e).slice(8, -1)
    }
}, function (e, t, n) {
    var r = n(109);
    e.exports = function (e, t, n) {
        if (r(e), void 0 === t) return e;
        switch (n) {
            case 1:
                return function (n) {
                    return e.call(t, n)
                };
            case 2:
                return function (n, r) {
                    return e.call(t, n, r)
                };
            case 3:
                return function (n, r, o) {
                    return e.call(t, n, r, o)
                }
        }
        return function () {
            return e.apply(t, arguments)
        }
    }
}, function (e, t, n) {
    var r = n(22),
        o = n(10).document,
        i = r(o) && r(o.createElement);
    e.exports = function (e) {
        return i ? o.createElement(e) : {}
    }
}, function (e, t, n) {
    e.exports = !n(15) && !n(20)(function () {
        return 7 != Object.defineProperty(n(66)("div"), "a", {
            get: function () {
                return 7
            }
        }).a
    })
}, function (e, t, n) {
    "use strict";
    var r = n(37),
        o = n(17),
        i = n(73),
        a = n(18),
        s = n(13),
        u = n(36),
        l = n(115),
        c = n(39),
        d = n(71),
        p = n(19)("iterator"),
        f = !([].keys && "next" in [].keys()),
        _ = "@@iterator",
        h = "keys",
        v = "values",
        m = function () {
            return this
        };
    e.exports = function (e, t, n, y, g, w, T) {
        l(n, t, y);
        var b, E, x, S = function (e) {
            if (!f && e in R) return R[e];
            switch (e) {
                case h:
                    return function () {
                        return new n(this, e)
                    };
                case v:
                    return function () {
                        return new n(this, e)
                    }
            }
            return function () {
                return new n(this, e)
            }
        },
            A = t + " Iterator",
            k = g == v,
            O = !1,
            R = e.prototype,
            N = R[p] || R[_] || g && R[g],
            C = N || S(g),
            I = g ? k ? S("entries") : C : void 0,
            P = "Array" == t ? R.entries || N : N;
        if (P && (x = d(P.call(new e)), x !== Object.prototype && (c(x, A, !0), r || s(x, p) || a(x, p, m))), k && N && N.name !== v && (O = !0, C = function () {
            return N.call(this)
        }), r && !T || !f && !O && R[p] || a(R, p, C), u[t] = C, u[A] = m, g)
            if (b = {
                values: k ? C : S(v),
                keys: w ? C : S(h),
                entries: I
            }, T)
                for (E in b) E in R || i(R, E, b[E]);
            else o(o.P + o.F * (f || O), t, b);
        return b
    }
}, function (e, t, n) {
    var r = n(31),
        o = n(29),
        i = n(14),
        a = n(43),
        s = n(13),
        u = n(67),
        l = Object.getOwnPropertyDescriptor;
    t.f = n(15) ? l : function (e, t) {
        if (e = i(e), t = a(t, !0), u) try {
            return l(e, t)
        } catch (n) { }
        if (s(e, t)) return o(!r.f.call(e, t), e[t])
    }
}, function (e, t, n) {
    var r = n(72),
        o = n(35).concat("length", "prototype");
    t.f = Object.getOwnPropertyNames || function (e) {
        return r(e, o)
    }
}, function (e, t, n) {
    var r = n(13),
        o = n(54),
        i = n(40)("IE_PROTO"),
        a = Object.prototype;
    e.exports = Object.getPrototypeOf || function (e) {
        return e = o(e), r(e, i) ? e[i] : "function" == typeof e.constructor && e instanceof e.constructor ? e.constructor.prototype : e instanceof Object ? a : null
    }
}, function (e, t, n) {
    var r = n(13),
        o = n(14),
        i = n(111)(!1),
        a = n(40)("IE_PROTO");
    e.exports = function (e, t) {
        var n, s = o(e),
            u = 0,
            l = [];
        for (n in s) n != a && r(s, n) && l.push(n);
        for (; t.length > u;) r(s, n = t[u++]) && (~i(l, n) || l.push(n));
        return l
    }
}, function (e, t, n) {
    e.exports = n(18)
}, function (e, t, n) {
    function r(e) {
        if (!i(e)) return !1;
        var t = o(e);
        return t == s || t == u || t == a || t == l
    }
    var o = n(24),
        i = n(33),
        a = "[object AsyncFunction]",
        s = "[object Function]",
        u = "[object GeneratorFunction]",
        l = "[object Proxy]";
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        return a(e) ? o(e) : i(e)
    }
    var o = n(158),
        i = n(136),
        a = n(81);
    e.exports = r
}, function (e, t, n) {
    var r = n(64);
    e.exports = Object("z").propertyIsEnumerable(0) ? Object : function (e) {
        return "String" == r(e) ? e.split("") : Object(e)
    }
}, function (e, t, n) {
    var r = n(165),
        o = n(25),
        i = Object.prototype,
        a = i.hasOwnProperty,
        s = i.propertyIsEnumerable,
        u = r(function () {
            return arguments
        } ()) ? r : function (e) {
            return o(e) && a.call(e, "callee") && !s.call(e, "callee")
        };
    e.exports = u
}, function (e, t, n) {
    (function (e) {
        var r = n(11),
            o = n(226),
            i = "object" == typeof t && t && !t.nodeType && t,
            a = i && "object" == typeof e && e && !e.nodeType && e,
            s = a && a.exports === i,
            u = s ? r.Buffer : void 0,
            l = u ? u.isBuffer : void 0,
            c = l || o;
        e.exports = c
    }).call(t, n(95)(e))
}, function (e, t, n) {
    var r = n(169),
        o = n(176),
        i = n(207),
        a = i && i.isTypedArray,
        s = a ? o(a) : r;
    e.exports = s
}, , function (e, t, n) {
    function r(e) {
        return null != e && i(e.length) && !o(e)
    }
    var o = n(74),
        i = n(61);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        var t = this.__data__ = new o(e);
        this.size = t.size
    }
    var o = n(46),
        i = n(213),
        a = n(214),
        s = n(215),
        u = n(216),
        l = n(217);
    r.prototype.clear = i, r.prototype["delete"] = a, r.prototype.get = s, r.prototype.has = u, r.prototype.set = l, e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        t = o(t, e);
        for (var n = 0, r = t.length; null != e && n < r;) e = e[i(t[n++])];
        return n && n == r ? e : void 0
    }
    var o = n(85),
        i = n(51);
    e.exports = r
}, function (e, t, n) {
    function r(e, t, n, a, s) {
        return e === t || (null == e || null == t || !i(e) && !i(t) ? e !== e && t !== t : o(e, t, n, a, r, s))
    }
    var o = n(166),
        i = n(25);
    e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        return o(e) ? e : i(e, t) ? [e] : a(s(e))
    }
    var o = n(12),
        i = n(60),
        a = n(218),
        s = n(229);
    e.exports = r
}, function (e, t, n) {
    function r(e, t, n, r, l, c) {
        var d = n & s,
            p = e.length,
            f = t.length;
        if (p != f && !(d && f > p)) return !1;
        var _ = c.get(e);
        if (_ && c.get(t)) return _ == t;
        var h = -1,
            v = !0,
            m = n & u ? new o : void 0;
        for (c.set(e, t), c.set(t, e); ++h < p;) {
            var y = e[h],
                g = t[h];
            if (r) var w = d ? r(g, y, h, t, e, c) : r(y, g, h, e, t, c);
            if (void 0 !== w) {
                if (w) continue;
                v = !1;
                break
            }
            if (m) {
                if (!i(t, function (e, t) {
                    if (!a(m, t) && (y === e || l(y, e, n, r, c))) return m.push(t)
                })) {
                    v = !1;
                    break
                }
            } else if (y !== g && !l(y, g, n, r, c)) {
                v = !1;
                break
            }
        }
        return c["delete"](e), c["delete"](t), v
    }
    var o = n(154),
        i = n(161),
        a = n(177),
        s = 1,
        u = 2;
    e.exports = r
}, function (e, t) {
    (function (t) {
        var n = "object" == typeof t && t && t.Object === Object && t;
        e.exports = n
    }).call(t, function () {
        return this
    } ())
}, function (e, t) {
    function n(e, t) {
        return t = null == t ? r : t, !!t && ("number" == typeof e || o.test(e)) && e > -1 && e % 1 == 0 && e < t
    }
    var r = 9007199254740991,
        o = /^(?:0|[1-9]\d*)$/;
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        return e === e && !o(e)
    }
    var o = n(33);
    e.exports = r
}, function (e, t) {
    function n(e, t) {
        return function (n) {
            return null != n && (n[e] === t && (void 0 !== t || e in Object(n)))
        }
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        if (null != e) {
            try {
                return o.call(e)
            } catch (t) { }
            try {
                return e + ""
            } catch (t) { }
        }
        return ""
    }
    var r = Function.prototype,
        o = r.toString;
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        return e === t || e !== e && t !== t
    }
    e.exports = n
}, function (e, t, n) {
    var r = n(262),
        o = n(56),
        i = r(o);
    e.exports = i
}, , function (e, t) {
    e.exports = function (e) {
        return e.webpackPolyfill || (e.deprecate = function () { }, e.paths = [], e.children = [], e.webpackPolyfill = 1), e
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(144),
        __esModule: !0
    }
}, function (e, t, n) {
    function r(e) {
        return "function" == typeof e ? e : null == e ? a : "object" == typeof e ? s(e) ? i(e[0], e[1]) : o(e) : u(e)
    }
    var o = n(170),
        i = n(171),
        a = n(221),
        s = n(12),
        u = n(224);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        if ("number" == typeof e) return e;
        if (i(e)) return a;
        if (o(e)) {
            var t = "function" == typeof e.valueOf ? e.valueOf() : e;
            e = o(t) ? t + "" : t
        }
        if ("string" != typeof e) return 0 === e ? e : +e;
        e = e.replace(s, "");
        var n = l.test(e);
        return n || c.test(e) ? d(e.slice(2), n ? 2 : 8) : u.test(e) ? a : +e
    }
    var o = n(33),
        i = n(52),
        a = NaN,
        s = /^\s+|\s+$/g,
        u = /^[-+]0x[0-9a-f]+$/i,
        l = /^0b[01]+$/i,
        c = /^0o[0-7]+$/i,
        d = parseInt;
    e.exports = r
}, , function (e, t, n) {
    e.exports = {
        "default": n(104),
        __esModule: !0
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(106),
        __esModule: !0
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(107),
        __esModule: !0
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(108),
        __esModule: !0
    }
}, function (e, t, n) {
    n(127);
    var r = n(9).Object;
    e.exports = function (e, t) {
        return r.create(e, t)
    }
}, function (e, t, n) {
    n(128), e.exports = n(9).Object.getPrototypeOf
}, function (e, t, n) {
    n(129), e.exports = n(9).Object.setPrototypeOf
}, function (e, t, n) {
    n(132), n(130), n(133), n(134), e.exports = n(9).Symbol
}, function (e, t, n) {
    n(131), n(135), e.exports = n(45).f("iterator")
}, function (e, t) {
    e.exports = function (e) {
        if ("function" != typeof e) throw TypeError(e + " is not a function!");
        return e
    }
}, function (e, t) {
    e.exports = function () { }
}, function (e, t, n) {
    var r = n(14),
        o = n(125),
        i = n(124);
    e.exports = function (e) {
        return function (t, n, a) {
            var s, u = r(t),
                l = o(u.length),
                c = i(a, l);
            if (e && n != n) {
                for (; l > c;)
                    if (s = u[c++], s != s) return !0
            } else
                for (; l > c; c++)
                    if ((e || c in u) && u[c] === n) return e || c || 0; return !e && -1
        }
    }
}, function (e, t, n) {
    var r = n(23),
        o = n(53),
        i = n(31);
    e.exports = function (e) {
        var t = r(e),
            n = o.f;
        if (n)
            for (var a, s = n(e), u = i.f, l = 0; s.length > l;) u.call(e, a = s[l++]) && t.push(a);
        return t
    }
}, function (e, t, n) {
    e.exports = n(10).document && document.documentElement
}, function (e, t, n) {
    var r = n(64);
    e.exports = Array.isArray || function (e) {
        return "Array" == r(e)
    }
}, function (e, t, n) {
    "use strict";
    var r = n(38),
        o = n(29),
        i = n(39),
        a = {};
    n(18)(a, n(19)("iterator"), function () {
        return this
    }), e.exports = function (e, t, n) {
        e.prototype = r(a, {
            next: o(1, n)
        }), i(e, t + " Iterator")
    }
}, function (e, t) {
    e.exports = function (e, t) {
        return {
            value: t,
            done: !!e
        }
    }
}, function (e, t, n) {
    var r = n(23),
        o = n(14);
    e.exports = function (e, t) {
        for (var n, i = o(e), a = r(i), s = a.length, u = 0; s > u;)
            if (i[n = a[u++]] === t) return n
    }
}, function (e, t, n) {
    var r = n(30)("meta"),
        o = n(22),
        i = n(13),
        a = n(16).f,
        s = 0,
        u = Object.isExtensible || function () {
            return !0
        },
        l = !n(20)(function () {
            return u(Object.preventExtensions({}))
        }),
        c = function (e) {
            a(e, r, {
                value: {
                    i: "O" + ++s,
                    w: {}
                }
            })
        },
        d = function (e, t) {
            if (!o(e)) return "symbol" == typeof e ? e : ("string" == typeof e ? "S" : "P") + e;
            if (!i(e, r)) {
                if (!u(e)) return "F";
                if (!t) return "E";
                c(e)
            }
            return e[r].i
        },
        p = function (e, t) {
            if (!i(e, r)) {
                if (!u(e)) return !0;
                if (!t) return !1;
                c(e)
            }
            return e[r].w
        },
        f = function (e) {
            return l && _.NEED && u(e) && !i(e, r) && c(e), e
        },
        _ = e.exports = {
            KEY: r,
            NEED: !1,
            fastKey: d,
            getWeak: p,
            onFreeze: f
        }
}, function (e, t, n) {
    var r = n(16),
        o = n(21),
        i = n(23);
    e.exports = n(15) ? Object.defineProperties : function (e, t) {
        o(e);
        for (var n, a = i(t), s = a.length, u = 0; s > u;) r.f(e, n = a[u++], t[n]);
        return e
    }
}, function (e, t, n) {
    var r = n(14),
        o = n(70).f,
        i = {}.toString,
        a = "object" == typeof window && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [],
        s = function (e) {
            try {
                return o(e)
            } catch (t) {
                return a.slice()
            }
        };
    e.exports.f = function (e) {
        return a && "[object Window]" == i.call(e) ? s(e) : o(r(e))
    }
}, function (e, t, n) {
    var r = n(17),
        o = n(9),
        i = n(20);
    e.exports = function (e, t) {
        var n = (o.Object || {})[e] || Object[e],
            a = {};
        a[e] = t(n), r(r.S + r.F * i(function () {
            n(1)
        }), "Object", a)
    }
}, function (e, t, n) {
    var r = n(22),
        o = n(21),
        i = function (e, t) {
            if (o(e), !r(t) && null !== t) throw TypeError(t + ": can't set as prototype!")
        };
    e.exports = {
        set: Object.setPrototypeOf || ("__proto__" in {} ? function (e, t, r) {
            try {
                r = n(65)(Function.call, n(69).f(Object.prototype, "__proto__").set, 2), r(e, []), t = !(e instanceof Array)
            } catch (o) {
                t = !0
            }
            return function (e, n) {
                return i(e, n), t ? e.__proto__ = n : r(e, n), e
            }
        } ({}, !1) : void 0),
        check: i
    }
}, function (e, t, n) {
    var r = n(42),
        o = n(34);
    e.exports = function (e) {
        return function (t, n) {
            var i, a, s = String(o(t)),
                u = r(n),
                l = s.length;
            return u < 0 || u >= l ? e ? "" : void 0 : (i = s.charCodeAt(u), i < 55296 || i > 56319 || u + 1 === l || (a = s.charCodeAt(u + 1)) < 56320 || a > 57343 ? e ? s.charAt(u) : i : e ? s.slice(u, u + 2) : (i - 55296 << 10) + (a - 56320) + 65536)
        }
    }
}, function (e, t, n) {
    var r = n(42),
        o = Math.max,
        i = Math.min;
    e.exports = function (e, t) {
        return e = r(e), e < 0 ? o(e + t, 0) : i(e, t)
    }
}, function (e, t, n) {
    var r = n(42),
        o = Math.min;
    e.exports = function (e) {
        return e > 0 ? o(r(e), 9007199254740991) : 0
    }
}, function (e, t, n) {
    "use strict";
    var r = n(110),
        o = n(116),
        i = n(36),
        a = n(14);
    e.exports = n(68)(Array, "Array", function (e, t) {
        this._t = a(e), this._i = 0, this._k = t
    }, function () {
        var e = this._t,
            t = this._k,
            n = this._i++;
        return !e || n >= e.length ? (this._t = void 0, o(1)) : "keys" == t ? o(0, n) : "values" == t ? o(0, e[n]) : o(0, [n, e[n]])
    }, "values"), i.Arguments = i.Array, r("keys"), r("values"), r("entries")
}, function (e, t, n) {
    var r = n(17);
    r(r.S, "Object", {
        create: n(38)
    })
}, function (e, t, n) {
    var r = n(54),
        o = n(71);
    n(121)("getPrototypeOf", function () {
        return function (e) {
            return o(r(e))
        }
    })
}, function (e, t, n) {
    var r = n(17);
    r(r.S, "Object", {
        setPrototypeOf: n(122).set
    })
}, function (e, t) { }, function (e, t, n) {
    "use strict";
    var r = n(123)(!0);
    n(68)(String, "String", function (e) {
        this._t = String(e), this._i = 0
    }, function () {
        var e, t = this._t,
            n = this._i;
        return n >= t.length ? {
            value: void 0,
            done: !0
        } : (e = r(t, n), this._i += e.length, {
            value: e,
            done: !1
        })
    })
}, function (e, t, n) {
    "use strict";
    var r = n(10),
        o = n(13),
        i = n(15),
        a = n(17),
        s = n(73),
        u = n(118).KEY,
        l = n(20),
        c = n(41),
        d = n(39),
        p = n(30),
        f = n(19),
        _ = n(45),
        h = n(44),
        v = n(117),
        m = n(112),
        y = n(114),
        g = n(21),
        w = n(14),
        T = n(43),
        b = n(29),
        E = n(38),
        x = n(120),
        S = n(69),
        A = n(16),
        k = n(23),
        O = S.f,
        R = A.f,
        N = x.f,
        C = r.Symbol,
        I = r.JSON,
        P = I && I.stringify,
        L = "prototype",
        U = f("_hidden"),
        D = f("toPrimitive"),
        K = {}.propertyIsEnumerable,
        M = c("symbol-registry"),
        j = c("symbols"),
        W = c("op-symbols"),
        F = Object[L],
        B = "function" == typeof C,
        G = r.QObject,
        V = !G || !G[L] || !G[L].findChild,
        H = i && l(function () {
            return 7 != E(R({}, "a", {
                get: function () {
                    return R(this, "a", {
                        value: 7
                    }).a
                }
            })).a
        }) ? function (e, t, n) {
            var r = O(F, t);
            r && delete F[t], R(e, t, n), r && e !== F && R(F, t, r)
        } : R,
        z = function (e) {
            var t = j[e] = E(C[L]);
            return t._k = e, t
        },
        X = B && "symbol" == typeof C.iterator ? function (e) {
            return "symbol" == typeof e
        } : function (e) {
            return e instanceof C
        },
        q = function (e, t, n) {
            return e === F && q(W, t, n), g(e), t = T(t, !0), g(n), o(j, t) ? (n.enumerable ? (o(e, U) && e[U][t] && (e[U][t] = !1), n = E(n, {
                enumerable: b(0, !1)
            })) : (o(e, U) || R(e, U, b(1, {})), e[U][t] = !0), H(e, t, n)) : R(e, t, n)
        },
        J = function (e, t) {
            g(e);
            for (var n, r = m(t = w(t)), o = 0, i = r.length; i > o;) q(e, n = r[o++], t[n]);
            return e
        },
        $ = function (e, t) {
            return void 0 === t ? E(e) : J(E(e), t)
        },
        Y = function (e) {
            var t = K.call(this, e = T(e, !0));
            return !(this === F && o(j, e) && !o(W, e)) && (!(t || !o(this, e) || !o(j, e) || o(this, U) && this[U][e]) || t)
        },
        Z = function (e, t) {
            if (e = w(e), t = T(t, !0), e !== F || !o(j, t) || o(W, t)) {
                var n = O(e, t);
                return !n || !o(j, t) || o(e, U) && e[U][t] || (n.enumerable = !0), n
            }
        },
        Q = function (e) {
            for (var t, n = N(w(e)), r = [], i = 0; n.length > i;) o(j, t = n[i++]) || t == U || t == u || r.push(t);
            return r
        },
        ee = function (e) {
            for (var t, n = e === F, r = N(n ? W : w(e)), i = [], a = 0; r.length > a;) !o(j, t = r[a++]) || n && !o(F, t) || i.push(j[t]);
            return i
        };
    B || (C = function () {
        if (this instanceof C) throw TypeError("Symbol is not a constructor!");
        var e = p(arguments.length > 0 ? arguments[0] : void 0),
            t = function (n) {
                this === F && t.call(W, n), o(this, U) && o(this[U], e) && (this[U][e] = !1), H(this, e, b(1, n))
            };
        return i && V && H(F, e, {
            configurable: !0,
            set: t
        }), z(e)
    }, s(C[L], "toString", function () {
        return this._k
    }), S.f = Z, A.f = q, n(70).f = x.f = Q, n(31).f = Y, n(53).f = ee, i && !n(37) && s(F, "propertyIsEnumerable", Y, !0), _.f = function (e) {
        return z(f(e))
    }), a(a.G + a.W + a.F * !B, {
        Symbol: C
    });
    for (var te = "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","), ne = 0; te.length > ne;) f(te[ne++]);
    for (var te = k(f.store), ne = 0; te.length > ne;) h(te[ne++]);
    a(a.S + a.F * !B, "Symbol", {
        "for": function (e) {
            return o(M, e += "") ? M[e] : M[e] = C(e)
        },
        keyFor: function (e) {
            if (X(e)) return v(M, e);
            throw TypeError(e + " is not a symbol!")
        },
        useSetter: function () {
            V = !0
        },
        useSimple: function () {
            V = !1
        }
    }), a(a.S + a.F * !B, "Object", {
        create: $,
        defineProperty: q,
        defineProperties: J,
        getOwnPropertyDescriptor: Z,
        getOwnPropertyNames: Q,
        getOwnPropertySymbols: ee
    }), I && a(a.S + a.F * (!B || l(function () {
        var e = C();
        return "[null]" != P([e]) || "{}" != P({
            a: e
        }) || "{}" != P(Object(e))
    })), "JSON", {
            stringify: function (e) {
                if (void 0 !== e && !X(e)) {
                    for (var t, n, r = [e], o = 1; arguments.length > o;) r.push(arguments[o++]);
                    return t = r[1], "function" == typeof t && (n = t), !n && y(t) || (t = function (e, t) {
                        if (n && (t = n.call(this, e, t)), !X(t)) return t
                    }), r[1] = t, P.apply(I, r)
                }
            }
        }), C[L][D] || n(18)(C[L], D, C[L].valueOf), d(C, "Symbol"), d(Math, "Math", !0), d(r.JSON, "JSON", !0)
}, function (e, t, n) {
    n(44)("asyncIterator")
}, function (e, t, n) {
    n(44)("observable")
}, function (e, t, n) {
    n(126);
    for (var r = n(10), o = n(18), i = n(36), a = n(19)("toStringTag"), s = ["NodeList", "DOMTokenList", "MediaList", "StyleSheetList", "CSSRuleList"], u = 0; u < 5; u++) {
        var l = s[u],
            c = r[l],
            d = c && c.prototype;
        d && !d[a] && o(d, a, l), i[l] = i.Array
    }
}, function (e, t, n) {
    function r(e) {
        if (!o(e)) return i(e);
        var t = [];
        for (var n in Object(e)) s.call(e, n) && "constructor" != n && t.push(n);
        return t
    }
    var o = n(138),
        i = n(206),
        a = Object.prototype,
        s = a.hasOwnProperty;
    e.exports = r
}, function (e, t, n) {
    var r = n(150),
        o = n(58),
        i = n(152),
        a = n(153),
        s = n(156),
        u = n(24),
        l = n(91),
        c = "[object Map]",
        d = "[object Object]",
        p = "[object Promise]",
        f = "[object Set]",
        _ = "[object WeakMap]",
        h = "[object DataView]",
        v = l(r),
        m = l(o),
        y = l(i),
        g = l(a),
        w = l(s),
        T = u;
    (r && T(new r(new ArrayBuffer(1))) != h || o && T(new o) != c || i && T(i.resolve()) != p || a && T(new a) != f || s && T(new s) != _) && (T = function (e) {
        var t = u(e),
            n = t == d ? e.constructor : void 0,
            r = n ? l(n) : "";
        if (r) switch (r) {
            case v:
                return h;
            case m:
                return c;
            case y:
                return p;
            case g:
                return f;
            case w:
                return _
        }
        return t
    }), e.exports = T
}, function (e, t) {
    function n(e) {
        var t = e && e.constructor,
            n = "function" == typeof t && t.prototype || r;
        return e === n
    }
    var r = Object.prototype;
    e.exports = n
}, function (e, t, n) {
    e.exports = {
        "default": n(259),
        __esModule: !0
    }
}, , , function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function o(e, t) {
        var n = null;
        return i(e, function (e) {
            e.id === t && (n = e)
        }), n
    }

    function i(e, t) {
        function n(e, r) {
            (0, c["default"])(e) && e.forEach(function (e, o) {
                t(e, r, o), n(e.children, e)
            })
        }
        t(e, null, null), n(e.children, e)
    }

    function a(e) {
        var t = 0;
        return i(e, function (n) {
            e !== n && t++
        }), t
    }

    function s(e, t) {
        var n;
        return i(e, function (e) {
            for (var r in t) e[r] === t[r] && (n = e)
        }), n
    }

    function u(e, t) {
        return i(e, function (e, n, r) {
            e.id === t && n.children.splice(r, 1)
        }), e
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var l = n(12),
        c = r(l);
    t.findNodeById = o, t.walkThroughTreeNodes = i, t.countNodesChildren = a, t.findNode = s, t.removeNodeFromTree = u
}, , function (e, t, n) {
    n(147), e.exports = n(9).Object.assign
}, , function (e, t, n) {
    "use strict";
    var r = n(23),
        o = n(53),
        i = n(31),
        a = n(54),
        s = n(76),
        u = Object.assign;
    e.exports = !u || n(20)(function () {
        var e = {},
            t = {},
            n = Symbol(),
            r = "abcdefghijklmnopqrst";
        return e[n] = 7, r.split("").forEach(function (e) {
            t[e] = e
        }), 7 != u({}, e)[n] || Object.keys(u({}, t)).join("") != r
    }) ? function (e, t) {
        for (var n = a(e), u = arguments.length, l = 1, c = o.f, d = i.f; u > l;)
            for (var p, f = s(arguments[l++]), _ = c ? r(f).concat(c(f)) : r(f), h = _.length, v = 0; h > v;) d.call(f, p = _[v++]) && (n[p] = f[p]);
        return n
    } : u
}, function (e, t, n) {
    var r = n(17);
    r(r.S + r.F, "Object", {
        assign: n(146)
    })
}, , , function (e, t, n) {
    var r = n(26),
        o = n(11),
        i = r(o, "DataView");
    e.exports = i
}, function (e, t, n) {
    function r(e) {
        var t = -1,
            n = null == e ? 0 : e.length;
        for (this.clear(); ++t < n;) {
            var r = e[t];
            this.set(r[0], r[1])
        }
    }
    var o = n(187),
        i = n(188),
        a = n(189),
        s = n(190),
        u = n(191);
    r.prototype.clear = o, r.prototype["delete"] = i, r.prototype.get = a, r.prototype.has = s, r.prototype.set = u, e.exports = r
}, function (e, t, n) {
    var r = n(26),
        o = n(11),
        i = r(o, "Promise");
    e.exports = i
}, function (e, t, n) {
    var r = n(26),
        o = n(11),
        i = r(o, "Set");
    e.exports = i
}, function (e, t, n) {
    function r(e) {
        var t = -1,
            n = null == e ? 0 : e.length;
        for (this.__data__ = new o; ++t < n;) this.add(e[t])
    }
    var o = n(59),
        i = n(210),
        a = n(211);
    r.prototype.add = r.prototype.push = i, r.prototype.has = a, e.exports = r
}, function (e, t, n) {
    var r = n(11),
        o = r.Uint8Array;
    e.exports = o
}, function (e, t, n) {
    var r = n(26),
        o = n(11),
        i = r(o, "WeakMap");
    e.exports = i
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, r = null == e ? 0 : e.length, o = 0, i = []; ++n < r;) {
            var a = e[n];
            t(a, n, e) && (i[o++] = a)
        }
        return i
    }
    e.exports = n
}, function (e, t, n) {
    function r(e, t) {
        var n = a(e),
            r = !n && i(e),
            c = !n && !r && s(e),
            p = !n && !r && !c && l(e),
            f = n || r || c || p,
            _ = f ? o(e.length, String) : [],
            h = _.length;
        for (var v in e) !t && !d.call(e, v) || f && ("length" == v || c && ("offset" == v || "parent" == v) || p && ("buffer" == v || "byteLength" == v || "byteOffset" == v) || u(v, h)) || _.push(v);
        return _
    }
    var o = n(174),
        i = n(77),
        a = n(12),
        s = n(78),
        u = n(88),
        l = n(79),
        c = Object.prototype,
        d = c.hasOwnProperty;
    e.exports = r
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, r = null == e ? 0 : e.length, o = Array(r); ++n < r;) o[n] = t(e[n], n, e);
        return o
    }
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, r = t.length, o = e.length; ++n < r;) e[o + n] = t[n];
        return e
    }
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, r = null == e ? 0 : e.length; ++n < r;)
            if (t(e[n], n, e)) return !0;
        return !1
    }
    e.exports = n
}, function (e, t) {
    function n(e, t, n, r) {
        for (var o = e.length, i = n + (r ? 1 : -1); r ? i-- : ++i < o;)
            if (t(e[i], i, e)) return i;
        return -1
    }
    e.exports = n
}, function (e, t, n) {
    function r(e, t, n) {
        var r = t(e);
        return i(e) ? r : o(r, n(e))
    }
    var o = n(160),
        i = n(12);
    e.exports = r
}, function (e, t) {
    function n(e, t) {
        return null != e && t in Object(e)
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        return i(e) && o(e) == a
    }
    var o = n(24),
        i = n(25),
        a = "[object Arguments]";
    e.exports = r
}, function (e, t, n) {
    function r(e, t, n, r, v, y) {
        var g = l(e),
            w = l(t),
            T = g ? _ : u(e),
            b = w ? _ : u(t);
        T = T == f ? h : T, b = b == f ? h : b;
        var E = T == h,
            x = b == h,
            S = T == b;
        if (S && c(e)) {
            if (!c(t)) return !1;
            g = !0, E = !1
        }
        if (S && !E) return y || (y = new o), g || d(e) ? i(e, t, n, r, v, y) : a(e, t, T, n, r, v, y);
        if (!(n & p)) {
            var A = E && m.call(e, "__wrapped__"),
                k = x && m.call(t, "__wrapped__");
            if (A || k) {
                var O = A ? e.value() : e,
                    R = k ? t.value() : t;
                return y || (y = new o), v(O, R, n, r, y)
            }
        }
        return !!S && (y || (y = new o), s(e, t, n, r, v, y))
    }
    var o = n(82),
        i = n(86),
        a = n(179),
        s = n(180),
        u = n(137),
        l = n(12),
        c = n(78),
        d = n(79),
        p = 1,
        f = "[object Arguments]",
        _ = "[object Array]",
        h = "[object Object]",
        v = Object.prototype,
        m = v.hasOwnProperty;
    e.exports = r
}, function (e, t, n) {
    function r(e, t, n, r) {
        var u = n.length,
            l = u,
            c = !r;
        if (null == e) return !l;
        for (e = Object(e); u--;) {
            var d = n[u];
            if (c && d[2] ? d[1] !== e[d[0]] : !(d[0] in e)) return !1
        }
        for (; ++u < l;) {
            d = n[u];
            var p = d[0],
                f = e[p],
                _ = d[1];
            if (c && d[2]) {
                if (void 0 === f && !(p in e)) return !1
            } else {
                var h = new o;
                if (r) var v = r(f, _, p, e, t, h);
                if (!(void 0 === v ? i(_, f, a | s, r, h) : v)) return !1
            }
        }
        return !0
    }
    var o = n(82),
        i = n(84),
        a = 1,
        s = 2;
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        if (!a(e) || i(e)) return !1;
        var t = o(e) ? _ : l;
        return t.test(s(e))
    }
    var o = n(74),
        i = n(193),
        a = n(33),
        s = n(91),
        u = /[\\^$.*+?()[\]{}|]/g,
        l = /^\[object .+?Constructor\]$/,
        c = Function.prototype,
        d = Object.prototype,
        p = c.toString,
        f = d.hasOwnProperty,
        _ = RegExp("^" + p.call(f).replace(u, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$");
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        return a(e) && i(e.length) && !!C[o(e)]
    }
    var o = n(24),
        i = n(61),
        a = n(25),
        s = "[object Arguments]",
        u = "[object Array]",
        l = "[object Boolean]",
        c = "[object Date]",
        d = "[object Error]",
        p = "[object Function]",
        f = "[object Map]",
        _ = "[object Number]",
        h = "[object Object]",
        v = "[object RegExp]",
        m = "[object Set]",
        y = "[object String]",
        g = "[object WeakMap]",
        w = "[object ArrayBuffer]",
        T = "[object DataView]",
        b = "[object Float32Array]",
        E = "[object Float64Array]",
        x = "[object Int8Array]",
        S = "[object Int16Array]",
        A = "[object Int32Array]",
        k = "[object Uint8Array]",
        O = "[object Uint8ClampedArray]",
        R = "[object Uint16Array]",
        N = "[object Uint32Array]",
        C = {};
    C[b] = C[E] = C[x] = C[S] = C[A] = C[k] = C[O] = C[R] = C[N] = !0, C[s] = C[u] = C[w] = C[l] = C[T] = C[c] = C[d] = C[p] = C[f] = C[_] = C[h] = C[v] = C[m] = C[y] = C[g] = !1, e.exports = r
}, function (e, t, n) {
    function r(e) {
        var t = i(e);
        return 1 == t.length && t[0][2] ? a(t[0][0], t[0][1]) : function (n) {
            return n === e || o(n, e, t)
        }
    }
    var o = n(167),
        i = n(182),
        a = n(90);
    e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        return s(e) && u(t) ? l(c(e), t) : function (n) {
            var r = i(n, e);
            return void 0 === r && r === t ? a(n, e) : o(t, r, d | p)
        }
    }
    var o = n(84),
        i = n(219),
        a = n(220),
        s = n(60),
        u = n(89),
        l = n(90),
        c = n(51),
        d = 1,
        p = 2;
    e.exports = r
}, function (e, t) {
    function n(e) {
        return function (t) {
            return null == t ? void 0 : t[e]
        }
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        return function (t) {
            return o(t, e)
        }
    }
    var o = n(83);
    e.exports = r
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, r = Array(e); ++n < e;) r[n] = t(n);
        return r
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        if ("string" == typeof e) return e;
        if (a(e)) return i(e, r) + "";
        if (s(e)) return c ? c.call(e) : "";
        var t = e + "";
        return "0" == t && 1 / e == -u ? "-0" : t
    }
    var o = n(47),
        i = n(159),
        a = n(12),
        s = n(52),
        u = 1 / 0,
        l = o ? o.prototype : void 0,
        c = l ? l.toString : void 0;
    e.exports = r
}, function (e, t) {
    function n(e) {
        return function (t) {
            return e(t)
        }
    }
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        return e.has(t)
    }
    e.exports = n
}, function (e, t, n) {
    var r = n(11),
        o = r["__core-js_shared__"];
    e.exports = o
}, function (e, t, n) {
    function r(e, t, n, r, o, E, S) {
        switch (n) {
            case b:
                if (e.byteLength != t.byteLength || e.byteOffset != t.byteOffset) return !1;
                e = e.buffer, t = t.buffer;
            case T:
                return !(e.byteLength != t.byteLength || !E(new i(e), new i(t)));
            case p:
            case f:
            case v:
                return a(+e, +t);
            case _:
                return e.name == t.name && e.message == t.message;
            case m:
            case g:
                return e == t + "";
            case h:
                var A = u;
            case y:
                var k = r & c;
                if (A || (A = l), e.size != t.size && !k) return !1;
                var O = S.get(e);
                if (O) return O == t;
                r |= d, S.set(e, t);
                var R = s(A(e), A(t), r, o, E, S);
                return S["delete"](e), R;
            case w:
                if (x) return x.call(e) == x.call(t)
        }
        return !1
    }
    var o = n(47),
        i = n(155),
        a = n(92),
        s = n(86),
        u = n(204),
        l = n(212),
        c = 1,
        d = 2,
        p = "[object Boolean]",
        f = "[object Date]",
        _ = "[object Error]",
        h = "[object Map]",
        v = "[object Number]",
        m = "[object RegExp]",
        y = "[object Set]",
        g = "[object String]",
        w = "[object Symbol]",
        T = "[object ArrayBuffer]",
        b = "[object DataView]",
        E = o ? o.prototype : void 0,
        x = E ? E.valueOf : void 0;
    e.exports = r
}, function (e, t, n) {
    function r(e, t, n, r, a, u) {
        var l = n & i,
            c = o(e),
            d = c.length,
            p = o(t),
            f = p.length;
        if (d != f && !l) return !1;
        for (var _ = d; _--;) {
            var h = c[_];
            if (!(l ? h in t : s.call(t, h))) return !1
        }
        var v = u.get(e);
        if (v && u.get(t)) return v == t;
        var m = !0;
        u.set(e, t), u.set(t, e);
        for (var y = l; ++_ < d;) {
            h = c[_];
            var g = e[h],
                w = t[h];
            if (r) var T = l ? r(w, g, h, t, e, u) : r(g, w, h, e, t, u);
            if (!(void 0 === T ? g === w || a(g, w, n, r, u) : T)) {
                m = !1;
                break
            }
            y || (y = "constructor" == h)
        }
        if (m && !y) {
            var b = e.constructor,
                E = t.constructor;
            b != E && "constructor" in e && "constructor" in t && !("function" == typeof b && b instanceof b && "function" == typeof E && E instanceof E) && (m = !1)
        }
        return u["delete"](e), u["delete"](t), m
    }
    var o = n(181),
        i = 1,
        a = Object.prototype,
        s = a.hasOwnProperty;
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        return o(e, a, i)
    }
    var o = n(163),
        i = n(184),
        a = n(75);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        for (var t = i(e), n = t.length; n--;) {
            var r = t[n],
                a = e[r];
            t[n] = [r, a, o(a)]
        }
        return t
    }
    var o = n(89),
        i = n(75);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        var t = a.call(e, u),
            n = e[u];
        try {
            e[u] = void 0;
            var r = !0
        } catch (o) { }
        var i = s.call(e);
        return r && (t ? e[u] = n : delete e[u]), i
    }
    var o = n(47),
        i = Object.prototype,
        a = i.hasOwnProperty,
        s = i.toString,
        u = o ? o.toStringTag : void 0;
    e.exports = r
}, function (e, t, n) {
    var r = n(157),
        o = n(225),
        i = Object.prototype,
        a = i.propertyIsEnumerable,
        s = Object.getOwnPropertySymbols,
        u = s ? function (e) {
            return null == e ? [] : (e = Object(e), r(s(e), function (t) {
                return a.call(e, t)
            }))
        } : o;
    e.exports = u
}, function (e, t) {
    function n(e, t) {
        return null == e ? void 0 : e[t]
    }
    e.exports = n
}, function (e, t, n) {
    function r(e, t, n) {
        t = o(t, e);
        for (var r = -1, c = t.length, d = !1; ++r < c;) {
            var p = l(t[r]);
            if (!(d = null != e && n(e, p))) break;
            e = e[p]
        }
        return d || ++r != c ? d : (c = null == e ? 0 : e.length, !!c && u(c) && s(p, c) && (a(e) || i(e)))
    }
    var o = n(85),
        i = n(77),
        a = n(12),
        s = n(88),
        u = n(61),
        l = n(51);
    e.exports = r
}, function (e, t, n) {
    function r() {
        this.__data__ = o ? o(null) : {}, this.size = 0
    }
    var o = n(50);
    e.exports = r
}, function (e, t) {
    function n(e) {
        var t = this.has(e) && delete this.__data__[e];
        return this.size -= t ? 1 : 0, t
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        var t = this.__data__;
        if (o) {
            var n = t[e];
            return n === i ? void 0 : n
        }
        return s.call(t, e) ? t[e] : void 0
    }
    var o = n(50),
        i = "__lodash_hash_undefined__",
        a = Object.prototype,
        s = a.hasOwnProperty;
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        var t = this.__data__;
        return o ? void 0 !== t[e] : a.call(t, e)
    }
    var o = n(50),
        i = Object.prototype,
        a = i.hasOwnProperty;
    e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        var n = this.__data__;
        return this.size += this.has(e) ? 0 : 1, n[e] = o && void 0 === t ? i : t, this
    }
    var o = n(50),
        i = "__lodash_hash_undefined__";
    e.exports = r
}, function (e, t) {
    function n(e) {
        var t = typeof e;
        return "string" == t || "number" == t || "symbol" == t || "boolean" == t ? "__proto__" !== e : null === e
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        return !!i && i in e
    }
    var o = n(178),
        i = function () {
            var e = /[^.]+$/.exec(o && o.keys && o.keys.IE_PROTO || "");
            return e ? "Symbol(src)_1." + e : ""
        } ();
    e.exports = r
}, function (e, t) {
    function n() {
        this.__data__ = [], this.size = 0
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        var t = this.__data__,
            n = o(t, e);
        if (n < 0) return !1;
        var r = t.length - 1;
        return n == r ? t.pop() : a.call(t, n, 1), --this.size, !0
    }
    var o = n(48),
        i = Array.prototype,
        a = i.splice;
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        var t = this.__data__,
            n = o(t, e);
        return n < 0 ? void 0 : t[n][1]
    }
    var o = n(48);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        return o(this.__data__, e) > -1
    }
    var o = n(48);
    e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        var n = this.__data__,
            r = o(n, e);
        return r < 0 ? (++this.size, n.push([e, t])) : n[r][1] = t, this
    }
    var o = n(48);
    e.exports = r
}, function (e, t, n) {
    function r() {
        this.size = 0, this.__data__ = {
            hash: new o,
            map: new (a || i),
            string: new o
        }
    }
    var o = n(151),
        i = n(46),
        a = n(58);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        var t = o(this, e)["delete"](e);
        return this.size -= t ? 1 : 0, t
    }
    var o = n(49);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        return o(this, e).get(e);
    }
    var o = n(49);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        return o(this, e).has(e)
    }
    var o = n(49);
    e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        var n = o(this, e),
            r = n.size;
        return n.set(e, t), this.size += n.size == r ? 0 : 1, this
    }
    var o = n(49);
    e.exports = r
}, function (e, t) {
    function n(e) {
        var t = -1,
            n = Array(e.size);
        return e.forEach(function (e, r) {
            n[++t] = [r, e]
        }), n
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        var t = o(e, function (e) {
            return n.size === i && n.clear(), e
        }),
            n = t.cache;
        return t
    }
    var o = n(223),
        i = 500;
    e.exports = r
}, function (e, t, n) {
    var r = n(209),
        o = r(Object.keys, Object);
    e.exports = o
}, function (e, t, n) {
    (function (e) {
        var r = n(87),
            o = "object" == typeof t && t && !t.nodeType && t,
            i = o && "object" == typeof e && e && !e.nodeType && e,
            a = i && i.exports === o,
            s = a && r.process,
            u = function () {
                try {
                    return s && s.binding && s.binding("util")
                } catch (e) { }
            } ();
        e.exports = u
    }).call(t, n(95)(e))
}, function (e, t) {
    function n(e) {
        return o.call(e)
    }
    var r = Object.prototype,
        o = r.toString;
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        return function (n) {
            return e(t(n))
        }
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        return this.__data__.set(e, r), this
    }
    var r = "__lodash_hash_undefined__";
    e.exports = n
}, function (e, t) {
    function n(e) {
        return this.__data__.has(e)
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        var t = -1,
            n = Array(e.size);
        return e.forEach(function (e) {
            n[++t] = e
        }), n
    }
    e.exports = n
}, function (e, t, n) {
    function r() {
        this.__data__ = new o, this.size = 0
    }
    var o = n(46);
    e.exports = r
}, function (e, t) {
    function n(e) {
        var t = this.__data__,
            n = t["delete"](e);
        return this.size = t.size, n
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        return this.__data__.get(e)
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        return this.__data__.has(e)
    }
    e.exports = n
}, function (e, t, n) {
    function r(e, t) {
        var n = this.__data__;
        if (n instanceof o) {
            var r = n.__data__;
            if (!i || r.length < s - 1) return r.push([e, t]), this.size = ++n.size, this;
            n = this.__data__ = new a(r)
        }
        return n.set(e, t), this.size = n.size, this
    }
    var o = n(46),
        i = n(58),
        a = n(59),
        s = 200;
    e.exports = r
}, function (e, t, n) {
    var r = n(205),
        o = /^\./,
        i = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
        a = /\\(\\)?/g,
        s = r(function (e) {
            var t = [];
            return o.test(e) && t.push(""), e.replace(i, function (e, n, r, o) {
                t.push(r ? o.replace(a, "$1") : n || e)
            }), t
        });
    e.exports = s
}, function (e, t, n) {
    function r(e, t, n) {
        var r = null == e ? void 0 : o(e, t);
        return void 0 === r ? n : r
    }
    var o = n(83);
    e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        return null != e && i(e, t, o)
    }
    var o = n(164),
        i = n(186);
    e.exports = r
}, function (e, t) {
    function n(e) {
        return e
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        return "number" == typeof e || i(e) && o(e) == a
    }
    var o = n(24),
        i = n(25),
        a = "[object Number]";
    e.exports = r
}, function (e, t, n) {
    function r(e, t) {
        if ("function" != typeof e || null != t && "function" != typeof t) throw new TypeError(i);
        var n = function () {
            var r = arguments,
                o = t ? t.apply(this, r) : r[0],
                i = n.cache;
            if (i.has(o)) return i.get(o);
            var a = e.apply(this, r);
            return n.cache = i.set(o, a) || i, a
        };
        return n.cache = new (r.Cache || o), n
    }
    var o = n(59),
        i = "Expected a function";
    r.Cache = o, e.exports = r
}, function (e, t, n) {
    function r(e) {
        return a(e) ? o(s(e)) : i(e)
    }
    var o = n(172),
        i = n(173),
        a = n(60),
        s = n(51);
    e.exports = r
}, function (e, t) {
    function n() {
        return []
    }
    e.exports = n
}, function (e, t) {
    function n() {
        return !1
    }
    e.exports = n
}, function (e, t, n) {
    function r(e) {
        if (!e) return 0 === e ? e : 0;
        if (e = o(e), e === i || e === -i) {
            var t = e < 0 ? -1 : 1;
            return t * a
        }
        return e === e ? e : 0
    }
    var o = n(98),
        i = 1 / 0,
        a = 1.7976931348623157e308;
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        var t = o(e),
            n = t % 1;
        return t === t ? n ? t - n : t : 0
    }
    var o = n(227);
    e.exports = r
}, function (e, t, n) {
    function r(e) {
        return null == e ? "" : o(e)
    }
    var o = n(175);
    e.exports = r
}, function (e, t, n) {
    "use strict";

    function r(e) {
        var t = o.shuffled();
        return {
            version: 15 & t.indexOf(e.substr(0, 1)),
            worker: 15 & t.indexOf(e.substr(1, 1))
        }
    }
    var o = n(62);
    e.exports = r
}, function (e, t, n) {
    "use strict";

    function r(e, t) {
        for (var n, r = 0, i = ""; !n;) i += e(t >> 4 * r & 15 | o()), n = t < Math.pow(16, r + 1), r++;
        return i
    }
    var o = n(234);
    e.exports = r
}, function (e, t, n) {
    "use strict";

    function r() {
        var e = "",
            t = Math.floor(.001 * (Date.now() - f));
        return t === u ? s++ : (s = 0, u = t), e += c(l.lookup, _), e += c(l.lookup, h), s > 0 && (e += c(l.lookup, s)), e += c(l.lookup, t)
    }

    function o(t) {
        return l.seed(t), e.exports
    }

    function i(t) {
        return h = t, e.exports
    }

    function a(e) {
        return void 0 !== e && l.characters(e), l.shuffled()
    }
    var s, u, l = n(62),
        c = n(231),
        d = n(230),
        p = n(233),
        f = 1459707606518,
        _ = 6,
        h = n(236) || 0;
    e.exports = r, e.exports.generate = r, e.exports.seed = o, e.exports.worker = i, e.exports.characters = a, e.exports.decode = d, e.exports.isValid = p
}, function (e, t, n) {
    "use strict";

    function r(e) {
        if (!e || "string" != typeof e || e.length < 6) return !1;
        for (var t = o.characters(), n = e.length, r = 0; r < n; r++)
            if (t.indexOf(e[r]) === -1) return !1;
        return !0
    }
    var o = n(62);
    e.exports = r
}, function (e, t) {
    "use strict";

    function n() {
        if (!r || !r.getRandomValues) return 48 & Math.floor(256 * Math.random());
        var e = new Uint8Array(1);
        return r.getRandomValues(e), 48 & e[0]
    }
    var r = "object" == typeof window && (window.crypto || window.msCrypto);
    e.exports = n
}, function (e, t) {
    "use strict";

    function n() {
        return o = (9301 * o + 49297) % 233280, o / 233280
    }

    function r(e) {
        o = e
    }
    var o = 1;
    e.exports = {
        nextValue: n,
        seed: r
    }
}, function (e, t) {
    "use strict";
    e.exports = 0
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function o(e, t) {
        var n = (0, _["default"])({}, e),
            r = (0, y["default"])(w, {
                id: n.id
            });
        b = t, r === -1 ? w.push(n) : w.splice(r, 1, n), E()
    }

    function i(e, t) {
        var n = (0, _["default"])({}, e),
            r = (0, y["default"])(w, {
                id: n.id
            });
        b = t, n.toDelete = !0, r === -1 ? w.push(n) : w.splice(r, 1, n), E()
    }

    function a(e) {
        return fetch("https://www.snaptest.io/api/load", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                apikey: e
            }
        }).then(function (e) {
            return e.json()
        }).then(function (e) {
            return {
                tests: e.tests,
                components: e.components,
                directory: e.directory
            }
        })
    }

    function s() {
        w = w.map(function (e) {
            return "component" === e.type || "test" === e.type ? (e.actions = (0, p["default"])(e.actions), e.variables = (0, p["default"])(e.variables), (0, c["default"])({}, e)) : (0, c["default"])({}, e)
        })
    }

    function u() {
        fetch("https://www.snaptest.io/api/save", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                apikey: b.user.apiKey
            },
            body: (0, p["default"])(w)
        }).then(function (e) {
            return e.json()
        })["catch"](function () { })
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var l = n(55),
        c = r(l),
        d = n(139),
        p = r(d),
        f = n(96),
        _ = r(f),
        h = n(263),
        v = r(h),
        m = n(56),
        y = r(m);
    t.saveItemToDB = o, t.removeItemFromDB = i, t.loadItems = a;
    var g = n(7),
        w = (r(g), []),
        T = !1,
        b = null,
        E = (0, v["default"])(function () {
            !T && w.length > 0 && (s(), u(), w = [])
        }, 3e3)
}, function (e, t, n) {
    "use strict";

    function r(e) {
        var t = d.exec(e);
        return {
            protocol: t[1] ? t[1].toLowerCase() : "",
            slashes: !!t[2],
            rest: t[3]
        }
    }

    function o(e, t) {
        for (var n = (t || "/").split("/").slice(0, -1).concat(e.split("/")), r = n.length, o = n[r - 1], i = !1, a = 0; r--;) "." === n[r] ? n.splice(r, 1) : ".." === n[r] ? (n.splice(r, 1), a++) : a && (0 === r && (i = !0), n.splice(r, 1), a--);
        return i && n.unshift(""), "." !== o && ".." !== o || n.push(""), n.join("/")
    }

    function i(e, t, n) {
        if (!(this instanceof i)) return new i(e, t, n);
        var a, s, d, f, _, h, v = p.slice(),
            m = typeof t,
            y = this,
            g = 0;
        for ("object" !== m && "string" !== m && (n = t, t = null), n && "function" != typeof n && (n = c.parse), t = l(t), s = r(e || ""), a = !s.protocol && !s.slashes, y.slashes = s.slashes || a && t.slashes, y.protocol = s.protocol || t.protocol || "", e = s.rest, s.slashes || (v[2] = [/(.*)/, "pathname"]); g < v.length; g++) f = v[g], d = f[0], h = f[1], d !== d ? y[h] = e : "string" == typeof d ? ~(_ = e.indexOf(d)) && ("number" == typeof f[2] ? (y[h] = e.slice(0, _), e = e.slice(_ + f[2])) : (y[h] = e.slice(_), e = e.slice(0, _))) : (_ = d.exec(e)) && (y[h] = _[1], e = e.slice(0, _.index)), y[h] = y[h] || (a && f[3] ? t[h] || "" : ""), f[4] && (y[h] = y[h].toLowerCase());
        n && (y.query = n(y.query)), a && t.slashes && "/" !== y.pathname.charAt(0) && ("" !== y.pathname || "" !== t.pathname) && (y.pathname = o(y.pathname, t.pathname)), u(y.port, y.protocol) || (y.host = y.hostname, y.port = ""), y.username = y.password = "", y.auth && (f = y.auth.split(":"), y.username = f[0] || "", y.password = f[1] || ""), y.origin = y.protocol && y.host && "file:" !== y.protocol ? y.protocol + "//" + y.host : "null", y.href = y.toString()
    }

    function a(e, t, n) {
        var r = this;
        switch (e) {
            case "query":
                "string" == typeof t && t.length && (t = (n || c.parse)(t)), r[e] = t;
                break;
            case "port":
                r[e] = t, u(t, r.protocol) ? t && (r.host = r.hostname + ":" + t) : (r.host = r.hostname, r[e] = "");
                break;
            case "hostname":
                r[e] = t, r.port && (t += ":" + r.port), r.host = t;
                break;
            case "host":
                r[e] = t, /:\d+$/.test(t) ? (t = t.split(":"), r.port = t.pop(), r.hostname = t.join(":")) : (r.hostname = t, r.port = "");
                break;
            case "protocol":
                r.protocol = t.toLowerCase(), r.slashes = !n;
                break;
            case "pathname":
                r.pathname = t.length && "/" !== t.charAt(0) ? "/" + t : t;
                break;
            default:
                r[e] = t
        }
        for (var o = 0; o < p.length; o++) {
            var i = p[o];
            i[4] && (r[i[1]] = r[i[1]].toLowerCase())
        }
        return r.origin = r.protocol && r.host && "file:" !== r.protocol ? r.protocol + "//" + r.host : "null", r.href = r.toString(), r
    }

    function s(e) {
        e && "function" == typeof e || (e = c.stringify);
        var t, n = this,
            r = n.protocol;
        r && ":" !== r.charAt(r.length - 1) && (r += ":");
        var o = r + (n.slashes ? "//" : "");
        return n.username && (o += n.username, n.password && (o += ":" + n.password), o += "@"), o += n.host + n.pathname, t = "object" == typeof n.query ? e(n.query) : n.query, t && (o += "?" !== t.charAt(0) ? "?" + t : t), n.hash && (o += n.hash), o
    }
    var u = n(303),
        l = n(307),
        c = n(301),
        d = /^([a-z][a-z0-9.+-]*:)?(\/\/)?([\S\s]*)/i,
        p = [
            ["#", "hash"],
            ["?", "query"],
            ["/", "pathname"],
            ["@", "auth", 1],
            [NaN, "host", void 0, 1, 1],
            [/:(\d+)$/, "port", void 0, 1],
            [NaN, "hostname", void 0, 1, 1]
        ];
    i.prototype = {
        set: a,
        toString: s
    }, i.extractProtocol = r, i.location = l, i.qs = c, e.exports = i
}, , , , , , , , , , , , , , , , , function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(2),
        i = r(o),
        a = n(32),
        s = function u(e, t) {
            (0, i["default"])(this, u), this.name = "Unnamed component", this.actions = [], this.id = (0, a.generate)(), this.isBlank = !0, this.type = "component", this.variables = [], e && (this.name = e, this.isBlank = !1), t && (this.actions = t, this.isBlank = !1)
        };
    t["default"] = s
}, , function (e, t) {
    "use strict";

    function n(e) {
        return S[S.indexOf(e)]
    }

    function r(e) {
        return S[e]
    }

    function o(e) {
        var t = S.indexOf(step);
        return S[t + 1]
    }

    function i(e) {
        var t = S.indexOf(step);
        return S[t - 1]
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.getStepIndex = n, t.getStepName = r, t.getNextStep = o, t.getLastStep = i;
    var a = t.INTRO_START = "INTRO_START",
        s = t.INTRO_WINDOWS = "INTRO_WINDOWS",
        u = t.FIRSTTEST_CREATE = "FIRSTTEST_CREATE",
        l = t.FIRSTTEST_EDIT = "FIRSTTEST_EDIT",
        c = (t.FIRSTTEST_PRETHOUGHT = "FIRSTTEST_PRETHOUGHT", t.FIRSTTEST_NAVTOSNAP = "FIRSTTEST_NAVTOSNAP"),
        d = t.FIRSTTEST_PRETEND = "FIRSTTEST_PRETEND",
        p = t.FIRSTTEST_STOPRECORD = "FIRSTTEST_STOPRECORD",
        f = t.FIRSTTEST_ASSERT = "FIRSTTEST_ASSERT",
        _ = t.FIRSTTEST_STOPASSERT = "FIRSTTEST_STOPASSERT",
        h = t.FIRSTTEST_SIMULATE = "FIRSTTEST_SIMULATE",
        v = t.FIRSTTEST_SIMULATE2 = "FIRSTTEST_SIMULATE2",
        m = t.FIRSTTEST_NAMESTEP = "FIRSTTEST_NAMESTEP",
        y = t.FIRSTTEST_ADJUSTSELECTOR = "FIRSTTEST_ADJUSTSELECTOR",
        g = t.FIRSTTEST_MANUALADD = "FIRSTTEST_MANUALADD",
        w = t.FIRSTTEST_MANAGE = "FIRSTTEST_MANAGE",
        T = t.FIRSTTEST_MANAGE2 = "FIRSTTEST_MANAGE2",
        b = t.FIRSTTEST_MANAGE3 = "FIRSTTEST_MANAGE3",
        E = t.EXISTTEST_KICKOFF = "EXISTTEST_KICKOFF",
        x = (t.EXISTTEST_FIX = "EXISTTEST_FIX", t.SUITE_CODE = "SUITE_CODE", t.SUITE_CLI = "SUITE_CLI", t.END = "END"),
        S = t.ORDER = [a, s, u, l, c, d, p, f, _, h, v, m, y, g, w, T, b, E, x]
}, , function (e, t, n) {
    var r = n(9),
        o = r.JSON || (r.JSON = {
            stringify: JSON.stringify
        });
    e.exports = function (e) {
        return o.stringify.apply(o, arguments)
    }
}, , , function (e, t, n) {
    function r(e) {
        return function (t, n, r) {
            var s = Object(t);
            if (!i(t)) {
                var u = o(n, 3);
                t = a(t), n = function (e) {
                    return u(s[e], e, s)
                }
            }
            var l = e(t, n, r);
            return l > -1 ? s[u ? t[l] : l] : void 0
        }
    }
    var o = n(97),
        i = n(81),
        a = n(75);
    e.exports = r
}, function (e, t, n) {
    function r(e, t, n) {
        function r(t) {
            var n = y,
                r = g;
            return y = g = void 0, x = t, T = e.apply(r, n)
        }

        function c(e) {
            return x = e, b = setTimeout(f, t), S ? r(e) : T
        }

        function d(e) {
            var n = e - E,
                r = e - x,
                o = t - n;
            return A ? l(o, w - r) : o
        }

        function p(e) {
            var n = e - E,
                r = e - x;
            return void 0 === E || n >= t || n < 0 || A && r >= w
        }

        function f() {
            var e = i();
            return p(e) ? _(e) : void (b = setTimeout(f, d(e)))
        }

        function _(e) {
            return b = void 0, k && y ? r(e) : (y = g = void 0, T)
        }

        function h() {
            void 0 !== b && clearTimeout(b), x = 0, y = E = g = b = void 0
        }

        function v() {
            return void 0 === b ? T : _(i())
        }

        function m() {
            var e = i(),
                n = p(e);
            if (y = arguments, g = this, E = e, n) {
                if (void 0 === b) return c(E);
                if (A) return b = setTimeout(f, t), r(E)
            }
            return void 0 === b && (b = setTimeout(f, t)), T
        }
        var y, g, w, T, b, E, x = 0,
            S = !1,
            A = !1,
            k = !0;
        if ("function" != typeof e) throw new TypeError(s);
        return t = a(t) || 0, o(n) && (S = !!n.leading, A = "maxWait" in n, w = A ? u(a(n.maxWait) || 0, t) : w, k = "trailing" in n ? !!n.trailing : k), m.cancel = h, m.flush = v, m
    }
    var o = n(33),
        i = n(265),
        a = n(98),
        s = "Expected a function",
        u = Math.max,
        l = Math.min;
    e.exports = r
}, , function (e, t, n) {
    var r = n(11),
        o = function () {
            return r.Date.now()
        };
    e.exports = o
}, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , function (e, t, n) {
    var r, o;
    ! function () {
        function n(e, t) {
            var n, r = {};
            for (n in e) n !== t && (r[n] = e[n]);
            if (t in e)
                for (n in e[t]) r[n] = e[t][n];
            return r
        }

        function i(e, t) {
            function r() {
                return x = e.charAt(++A), x || ""
            }

            function o(t) {
                var n = "",
                    o = A;
                return t && c(), n = e.charAt(A + 1) || "", A = o - 1, r(), n
            }

            function u(t) {
                for (var n = A; r();)
                    if ("\\" === x) r();
                    else {
                        if (t.indexOf(x) !== -1) break;
                        if ("\n" === x) break
                    }
                return e.substring(n, A + 1)
            }

            function l(e) {
                var t = A,
                    n = u(e);
                return A = t - 1, r(), n
            }

            function c() {
                for (var e = ""; S.test(o());) r(), e += x;
                return e
            }

            function d() {
                var e = "";
                for (x && S.test(x) && (e = x); S.test(r());) e += x;
                return e
            }

            function p(t) {
                var n = A;
                for (t = "/" === o(), r(); r();) {
                    if (!t && "*" === x && "/" === o()) {
                        r();
                        break
                    }
                    if (t && "\n" === x) return e.substring(n, A)
                }
                return e.substring(n, A) + x
            }

            function f(t) {
                return e.substring(A - t.length, A).toLowerCase() === t
            }

            function _() {
                for (var t = 0, n = A + 1; n < e.length; n++) {
                    var r = e.charAt(n);
                    if ("{" === r) return !0;
                    if ("(" === r) t += 1;
                    else if (")" === r) {
                        if (0 === t) return !1;
                        t -= 1
                    } else if (";" === r || "}" === r) return !1
                }
                return !1
            }

            function h() {
                N++ , O += R
            }

            function v() {
                N-- , O = O.slice(0, -m)
            }
            t = t || {}, t = n(t, "css"), e = e || "";
            var m = t.indent_size ? parseInt(t.indent_size, 10) : 4,
                y = t.indent_char || " ",
                g = void 0 === t.selector_separator_newline || t.selector_separator_newline,
                w = void 0 !== t.end_with_newline && t.end_with_newline,
                T = void 0 === t.newline_between_rules || t.newline_between_rules,
                b = void 0 !== t.space_around_combinator && t.space_around_combinator;
            b = b || void 0 !== t.space_around_selector_separator && t.space_around_selector_separator;
            var E = t.eol ? t.eol : "auto";
            t.indent_with_tabs && (y = "\t", m = 1), "auto" === E && (E = "\n", e && a.test(e || "") && (E = e.match(a)[0])), E = E.replace(/\\r/, "\r").replace(/\\n/, "\n"), e = e.replace(s, "\n");
            var x, S = /^\s+$/,
                A = -1,
                k = 0,
                O = e.match(/^[\t ]*/)[0],
                R = new Array(m + 1).join(y),
                N = 0,
                C = 0,
                I = {};
            I["{"] = function (e) {
                I.singleSpace(), P.push(e), I.newLine()
            }, I["}"] = function (e) {
                I.newLine(), P.push(e), I.newLine()
            }, I._lastCharWhitespace = function () {
                return S.test(P[P.length - 1])
            }, I.newLine = function (e) {
                P.length && (e || "\n" === P[P.length - 1] || I.trim(), P.push("\n"), O && P.push(O))
            }, I.singleSpace = function () {
                P.length && !I._lastCharWhitespace() && P.push(" ")
            }, I.preserveSingleSpace = function () {
                W && I.singleSpace()
            }, I.trim = function () {
                for (; I._lastCharWhitespace();) P.pop()
            };
            for (var P = [], L = !1, U = !1, D = !1, K = "", M = ""; ;) {
                var j = d(),
                    W = "" !== j,
                    F = j.indexOf("\n") !== -1;
                if (M = K, K = x, !x) break;
                if ("/" === x && "*" === o()) {
                    var B = 0 === N;
                    (F || B) && I.newLine(), P.push(p()), I.newLine(), B && I.newLine(!0)
                } else if ("/" === x && "/" === o()) F || "{" === M || I.trim(), I.singleSpace(), P.push(p()), I.newLine();
                else if ("@" === x)
                    if (I.preserveSingleSpace(), "{" === o()) P.push(u("}"));
                    else {
                        P.push(x);
                        var G = l(": ,;{}()[]/='\"");
                        G.match(/[ :]$/) && (r(), G = u(": ").replace(/\s$/, ""), P.push(G), I.singleSpace()), G = G.replace(/\s$/, ""), G in i.NESTED_AT_RULE && (C += 1, G in i.CONDITIONAL_GROUP_RULE && (D = !0))
                    } else "#" === x && "{" === o() ? (I.preserveSingleSpace(), P.push(u("}"))) : "{" === x ? "}" === o(!0) ? (c(), r(), I.singleSpace(), P.push("{}"), I.newLine(), T && 0 === N && I.newLine(!0)) : (h(), I["{"](x), D ? (D = !1, L = N > C) : L = N >= C) : "}" === x ? (v(), I["}"](x), L = !1, U = !1, C && C-- , T && 0 === N && I.newLine(!0)) : ":" === x ? (c(), !L && !D || f("&") || _() || f("(") ? (f(" ") && " " !== P[P.length - 1] && P.push(" "), ":" === o() ? (r(), P.push("::")) : P.push(":")) : (P.push(":"), U || (U = !0, I.singleSpace()))) : '"' === x || "'" === x ? (I.preserveSingleSpace(), P.push(u(x))) : ";" === x ? (U = !1, P.push(x), I.newLine()) : "(" === x ? f("url") ? (P.push(x), c(), r() && (")" !== x && '"' !== x && "'" !== x ? P.push(u(")")) : A--)) : (k++ , I.preserveSingleSpace(), P.push(x), c()) : ")" === x ? (P.push(x), k--) : "," === x ? (P.push(x), c(), g && !U && k < 1 ? I.newLine() : I.singleSpace()) : (">" === x || "+" === x || "~" === x) && !U && k < 1 ? b ? (I.singleSpace(), P.push(x), I.singleSpace()) : (P.push(x), c(), x && S.test(x) && (x = "")) : "]" === x ? P.push(x) : "[" === x ? (I.preserveSingleSpace(), P.push(x)) : "=" === x ? (c(), x = "=", P.push(x)) : (I.preserveSingleSpace(), P.push(x))
            }
            var V = "";
            return O && (V += O), V += P.join("").replace(/[\r\n\t ]+$/, ""), w && (V += "\n"), "\n" !== E && (V = V.replace(/[\n]/g, E)), V
        }
        var a = /\r\n|[\n\r\u2028\u2029]/,
            s = new RegExp(a.source, "g");
        i.NESTED_AT_RULE = {
            "@page": !0,
            "@font-face": !0,
            "@keyframes": !0,
            "@media": !0,
            "@supports": !0,
            "@document": !0
        }, i.CONDITIONAL_GROUP_RULE = {
            "@media": !0,
            "@supports": !0,
            "@document": !0
        }, r = [], o = function () {
            return {
                css_beautify: i
            }
        }.apply(t, r), !(void 0 !== o && (e.exports = o))
    } ()
}, function (e, t, n) {
    var r, o;
    Object.values || (Object.values = function (e) {
        if (e !== Object(e)) throw new TypeError("Object.values called on a non-object");
        var t, n = [];
        for (t in e) Object.prototype.hasOwnProperty.call(e, t) && n.push(e[t]);
        return n
    }),
        function () {
            function n(e, t) {
                var n, r = {};
                for (n in e) n !== t && (r[n] = e[n]);
                if (t in e)
                    for (n in e[t]) r[n] = e[t][n];
                return r
            }

            function i(e, t) {
                function r(e, t) {
                    for (var n = 0; n < t.length; n += 1)
                        if (t[n] === e) return !0;
                    return !1
                }

                function o(e) {
                    return e.replace(/^\s+|\s+$/g, "")
                }

                function i(e) {
                    return e.replace(/^\s+/g, "")
                }

                function a(e) {
                    e = e || p.before_newline;
                    var t = Object.values(p);
                    if (!r(e, t)) throw new Error("Invalid Option Value: The option 'operator_position' must be one of the following values\n" + t + "\nYou passed in: '" + e + "'");
                    return e
                }

                function s(e, t) {
                    "use strict";

                    function s(e, t) {
                        var n = 0;
                        e && (n = e.indentation_level, !X.just_added_newline() && e.line_indent_level > n && (n = e.line_indent_level));
                        var r = {
                            mode: t,
                            parent: e,
                            last_text: e ? e.last_text : "",
                            last_word: e ? e.last_word : "",
                            declaration_statement: !1,
                            declaration_assignment: !1,
                            multiline_frame: !1,
                            inline_frame: !1,
                            if_block: !1,
                            else_block: !1,
                            do_block: !1,
                            do_while: !1,
                            import_block: !1,
                            in_case_statement: !1,
                            in_case: !1,
                            case_body: !1,
                            indentation_level: n,
                            line_indent_level: e ? e.line_indent_level : n,
                            start_line_index: X.get_line_number(),
                            ternary_depth: 0
                        };
                        return r
                    }

                    function u(e, t) {
                        for (var n = e.newlines, r = ie.keep_array_indentation && E(ee.mode), o = $, i = 0; i < e.comments_before.length; i++) $ = e.comments_before[i], u($, t), oe[$.type](t);
                        if ($ = o, r)
                            for (var a = 0; a < n; a += 1) m(a > 0, t);
                        else if (ie.max_preserve_newlines && n > ie.max_preserve_newlines && (n = ie.max_preserve_newlines), ie.preserve_newlines && e.newlines > 1) {
                            m(!1, t);
                            for (var s = 1; s < n; s += 1) m(!0, t)
                        }
                    }

                    function h(e) {
                        e = e.replace(d.allLineBreaks, "\n");
                        for (var t = [], n = e.indexOf("\n"); n !== -1;) t.push(e.substring(0, n)), e = e.substring(n + 1), n = e.indexOf("\n");
                        return e.length && t.push(e), t
                    }

                    function v(e) {
                        if (e = void 0 !== e && e, !X.just_added_newline()) {
                            var t = ie.preserve_newlines && $.wanted_newline || e,
                                n = r(ee.last_text, J.positionable_operators) || r($.text, J.positionable_operators);
                            if (n) {
                                var o = r(ee.last_text, J.positionable_operators) && r(ie.operator_position, f) || r($.text, J.positionable_operators);
                                t = t && o
                            }
                            if (t) m(!1, !0);
                            else if (ie.wrap_line_length) {
                                if ("TK_RESERVED" === Y && r(ee.last_text, ce)) return;
                                var i = X.current_line.get_character_count() + $.text.length + (X.space_before_token ? 1 : 0);
                                i >= ie.wrap_line_length && m(!1, !0)
                            }
                        }
                    }

                    function m(e, t) {
                        if (!t && ";" !== ee.last_text && "," !== ee.last_text && "=" !== ee.last_text && "TK_OPERATOR" !== Y)
                            for (var n = C(1); !(ee.mode !== _.Statement || ee.if_block && n && "TK_RESERVED" === n.type && "else" === n.text || ee.do_block);) S();
                        X.add_new_line(e) && (ee.multiline_frame = !0)
                    }

                    function y() {
                        X.just_added_newline() && (ie.keep_array_indentation && E(ee.mode) && $.wanted_newline ? (X.current_line.push($.whitespace_before), X.space_before_token = !1) : X.set_indent(ee.indentation_level) && (ee.line_indent_level = ee.indentation_level))
                    }

                    function g(e) {
                        if (X.raw) return void X.add_raw_token($);
                        if (ie.comma_first && "TK_COMMA" === Y && X.just_added_newline() && "," === X.previous_line.last()) {
                            var t = X.previous_line.pop();
                            X.previous_line.is_empty() && (X.previous_line.push(t), X.trim(!0), X.current_line.pop(), X.trim()), y(), X.add_token(","), X.space_before_token = !0
                        }
                        e = e || $.text, y(), X.add_token(e)
                    }

                    function w() {
                        ee.indentation_level += 1
                    }

                    function T() {
                        ee.indentation_level > 0 && (!ee.parent || ee.indentation_level > ee.parent.indentation_level) && (ee.indentation_level -= 1)
                    }

                    function b(e) {
                        ee ? (ne.push(ee), te = ee) : te = s(null, e), ee = s(te, e)
                    }

                    function E(e) {
                        return e === _.ArrayLiteral
                    }

                    function x(e) {
                        return r(e, [_.Expression, _.ForInitializer, _.Conditional])
                    }

                    function S() {
                        ne.length > 0 && (te = ee, ee = ne.pop(), te.mode === _.Statement && X.remove_redundant_indentation(te))
                    }

                    function A() {
                        return ee.parent.mode === _.ObjectLiteral && ee.mode === _.Statement && (":" === ee.last_text && 0 === ee.ternary_depth || "TK_RESERVED" === Y && r(ee.last_text, ["get", "set"]))
                    }

                    function k() {
                        return !!("TK_RESERVED" === Y && r(ee.last_text, ["var", "let", "const"]) && "TK_WORD" === $.type || "TK_RESERVED" === Y && "do" === ee.last_text || "TK_RESERVED" === Y && r(ee.last_text, ["return", "throw"]) && !$.wanted_newline || "TK_RESERVED" === Y && "else" === ee.last_text && ("TK_RESERVED" !== $.type || "if" !== $.text || $.comments_before.length) || "TK_END_EXPR" === Y && (te.mode === _.ForInitializer || te.mode === _.Conditional) || "TK_WORD" === Y && ee.mode === _.BlockStatement && !ee.in_case && "--" !== $.text && "++" !== $.text && "function" !== Z && "TK_WORD" !== $.type && "TK_RESERVED" !== $.type || ee.mode === _.ObjectLiteral && (":" === ee.last_text && 0 === ee.ternary_depth || "TK_RESERVED" === Y && r(ee.last_text, ["get", "set"]))) && (b(_.Statement), w(), u($, !0), A() || v("TK_RESERVED" === $.type && r($.text, ["do", "for", "if", "while"])), !0)
                    }

                    function O(e, t) {
                        for (var n = 0; n < e.length; n++) {
                            var r = o(e[n]);
                            if (r.charAt(0) !== t) return !1
                        }
                        return !0
                    }

                    function R(e, t) {
                        for (var n, r = 0, o = e.length; r < o; r++)
                            if (n = e[r], n && 0 !== n.indexOf(t)) return !1;
                        return !0
                    }

                    function N(e) {
                        return r(e, ["case", "return", "do", "if", "throw", "else"])
                    }

                    function C(e) {
                        var t = q + (e || 0);
                        return t < 0 || t >= ae.length ? null : ae[t]
                    }

                    function I() {
                        k() || u($);
                        var e = _.Expression;
                        if ("[" === $.text) {
                            if ("TK_WORD" === Y || ")" === ee.last_text) return "TK_RESERVED" === Y && r(ee.last_text, J.line_starters) && (X.space_before_token = !0), b(e), g(), w(), void (ie.space_in_paren && (X.space_before_token = !0));
                            e = _.ArrayLiteral, E(ee.mode) && ("[" !== ee.last_text && ("," !== ee.last_text || "]" !== Z && "}" !== Z) || ie.keep_array_indentation || m())
                        } else "TK_RESERVED" === Y && "for" === ee.last_text ? e = _.ForInitializer : "TK_RESERVED" === Y && r(ee.last_text, ["if", "while"]) && (e = _.Conditional);
                        ";" === ee.last_text || "TK_START_BLOCK" === Y ? m() : "TK_END_EXPR" === Y || "TK_START_EXPR" === Y || "TK_END_BLOCK" === Y || "." === ee.last_text ? v($.wanted_newline) : "TK_RESERVED" === Y && "(" === $.text || "TK_WORD" === Y || "TK_OPERATOR" === Y ? "TK_RESERVED" === Y && ("function" === ee.last_word || "typeof" === ee.last_word) || "*" === ee.last_text && (r(Z, ["function", "yield"]) || ee.mode === _.ObjectLiteral && r(Z, ["{", ","])) ? ie.space_after_anon_function && (X.space_before_token = !0) : "TK_RESERVED" !== Y || !r(ee.last_text, J.line_starters) && "catch" !== ee.last_text || ie.space_before_conditional && (X.space_before_token = !0) : X.space_before_token = !0, "(" === $.text && "TK_RESERVED" === Y && "await" === ee.last_word && (X.space_before_token = !0), "(" === $.text && ("TK_EQUALS" !== Y && "TK_OPERATOR" !== Y || A() || v()), "(" === $.text && "TK_WORD" !== Y && "TK_RESERVED" !== Y && v(), b(e), g(), ie.space_in_paren && (X.space_before_token = !0), w()
                    }

                    function P() {
                        for (; ee.mode === _.Statement;) S();
                        u($), ee.multiline_frame && v("]" === $.text && E(ee.mode) && !ie.keep_array_indentation), ie.space_in_paren && ("TK_START_EXPR" !== Y || ie.space_in_empty_paren ? X.space_before_token = !0 : (X.trim(), X.space_before_token = !1)), "]" === $.text && ie.keep_array_indentation ? (g(), S()) : (S(), g()), X.remove_redundant_indentation(te), ee.do_while && te.mode === _.Conditional && (te.mode = _.Expression, ee.do_block = !1, ee.do_while = !1)
                    }

                    function L() {
                        u($);
                        var e = C(1),
                            t = C(2);
                        b(t && (r(t.text, [":", ","]) && r(e.type, ["TK_STRING", "TK_WORD", "TK_RESERVED"]) || r(e.text, ["get", "set", "..."]) && r(t.type, ["TK_WORD", "TK_RESERVED"])) ? r(Z, ["class", "interface"]) ? _.BlockStatement : _.ObjectLiteral : "TK_OPERATOR" === Y && "=>" === ee.last_text ? _.BlockStatement : r(Y, ["TK_EQUALS", "TK_START_EXPR", "TK_COMMA", "TK_OPERATOR"]) || "TK_RESERVED" === Y && r(ee.last_text, ["return", "throw", "import", "default"]) ? _.ObjectLiteral : _.BlockStatement);
                        var n = !e.comments_before.length && "}" === e.text,
                            o = n && "function" === ee.last_word && "TK_END_EXPR" === Y;
                        if (ie.brace_preserve_inline) {
                            var i = 0,
                                a = null;
                            ee.inline_frame = !0;
                            do
                                if (i += 1, a = C(i), a.wanted_newline) {
                                    ee.inline_frame = !1;
                                    break
                                }
                            while ("TK_EOF" !== a.type && ("TK_END_BLOCK" !== a.type || a.opened !== $))
                        } ("expand" === ie.brace_style || "none" === ie.brace_style && $.wanted_newline) && !ee.inline_frame ? "TK_OPERATOR" !== Y && (o || "TK_EQUALS" === Y || "TK_RESERVED" === Y && N(ee.last_text) && "else" !== ee.last_text) ? X.space_before_token = !0 : m(!1, !0) : (!E(te.mode) || "TK_START_EXPR" !== Y && "TK_COMMA" !== Y || (("TK_COMMA" === Y || ie.space_in_paren) && (X.space_before_token = !0), ("TK_COMMA" === Y || "TK_START_EXPR" === Y && ee.inline_frame) && (v(), te.multiline_frame = te.multiline_frame || ee.multiline_frame, ee.multiline_frame = !1)), "TK_OPERATOR" !== Y && "TK_START_EXPR" !== Y && ("TK_START_BLOCK" !== Y || ee.inline_frame ? X.space_before_token = !0 : m())), g(), w()
                    }

                    function U() {
                        for (u($); ee.mode === _.Statement;) S();
                        var e = "TK_START_BLOCK" === Y;
                        ee.inline_frame && !e ? X.space_before_token = !0 : "expand" === ie.brace_style ? e || m() : e || (E(ee.mode) && ie.keep_array_indentation ? (ie.keep_array_indentation = !1, m(), ie.keep_array_indentation = !0) : m()), S(), g()
                    }

                    function D() {
                        if ("TK_RESERVED" === $.type)
                            if (r($.text, ["set", "get"]) && ee.mode !== _.ObjectLiteral) $.type = "TK_WORD";
                            else if (r($.text, ["as", "from"]) && !ee.import_block) $.type = "TK_WORD";
                            else if (ee.mode === _.ObjectLiteral) {
                                var e = C(1);
                                ":" === e.text && ($.type = "TK_WORD")
                            }
                        if (k() ? "TK_RESERVED" === Y && r(ee.last_text, ["var", "let", "const"]) && "TK_WORD" === $.type && (ee.declaration_statement = !0) : !$.wanted_newline || x(ee.mode) || "TK_OPERATOR" === Y && "--" !== ee.last_text && "++" !== ee.last_text || "TK_EQUALS" === Y || !ie.preserve_newlines && "TK_RESERVED" === Y && r(ee.last_text, ["var", "let", "const", "set", "get"]) ? u($) : (u($), m()), ee.do_block && !ee.do_while) {
                            if ("TK_RESERVED" === $.type && "while" === $.text) return X.space_before_token = !0, g(), X.space_before_token = !0, void (ee.do_while = !0);
                            m(), ee.do_block = !1
                        }
                        if (ee.if_block)
                            if (ee.else_block || "TK_RESERVED" !== $.type || "else" !== $.text) {
                                for (; ee.mode === _.Statement;) S();
                                ee.if_block = !1, ee.else_block = !1
                            } else ee.else_block = !0;
                        if ("TK_RESERVED" === $.type && ("case" === $.text || "default" === $.text && ee.in_case_statement)) return m(), (ee.case_body || ie.jslint_happy) && (T(), ee.case_body = !1), g(), ee.in_case = !0, void (ee.in_case_statement = !0);
                        if ("TK_COMMA" !== Y && "TK_START_EXPR" !== Y && "TK_EQUALS" !== Y && "TK_OPERATOR" !== Y || A() || v(), "TK_RESERVED" === $.type && "function" === $.text) return (r(ee.last_text, ["}", ";"]) || X.just_added_newline() && !r(ee.last_text, ["(", "[", "{", ":", "=", ","]) && "TK_OPERATOR" !== Y) && (X.just_added_blankline() || $.comments_before.length || (m(), m(!0))), "TK_RESERVED" === Y || "TK_WORD" === Y ? "TK_RESERVED" === Y && r(ee.last_text, ["get", "set", "new", "return", "export", "async"]) ? X.space_before_token = !0 : "TK_RESERVED" === Y && "default" === ee.last_text && "export" === Z ? X.space_before_token = !0 : m() : "TK_OPERATOR" === Y || "=" === ee.last_text ? X.space_before_token = !0 : (ee.multiline_frame || !x(ee.mode) && !E(ee.mode)) && m(), g(), void (ee.last_word = $.text);
                        if (re = "NONE", "TK_END_BLOCK" === Y ? te.inline_frame ? re = "SPACE" : "TK_RESERVED" === $.type && r($.text, ["else", "catch", "finally", "from"]) ? "expand" === ie.brace_style || "end-expand" === ie.brace_style || "none" === ie.brace_style && $.wanted_newline ? re = "NEWLINE" : (re = "SPACE", X.space_before_token = !0) : re = "NEWLINE" : "TK_SEMICOLON" === Y && ee.mode === _.BlockStatement ? re = "NEWLINE" : "TK_SEMICOLON" === Y && x(ee.mode) ? re = "SPACE" : "TK_STRING" === Y ? re = "NEWLINE" : "TK_RESERVED" === Y || "TK_WORD" === Y || "*" === ee.last_text && (r(Z, ["function", "yield"]) || ee.mode === _.ObjectLiteral && r(Z, ["{", ","])) ? re = "SPACE" : "TK_START_BLOCK" === Y ? re = ee.inline_frame ? "SPACE" : "NEWLINE" : "TK_END_EXPR" === Y && (X.space_before_token = !0, re = "NEWLINE"), "TK_RESERVED" === $.type && r($.text, J.line_starters) && ")" !== ee.last_text && (re = ee.inline_frame || "else" === ee.last_text || "export" === ee.last_text ? "SPACE" : "NEWLINE"), "TK_RESERVED" === $.type && r($.text, ["else", "catch", "finally"]))
                            if (("TK_END_BLOCK" !== Y || te.mode !== _.BlockStatement || "expand" === ie.brace_style || "end-expand" === ie.brace_style || "none" === ie.brace_style && $.wanted_newline) && !ee.inline_frame) m();
                            else {
                                X.trim(!0);
                                var t = X.current_line;
                                "}" !== t.last() && m(), X.space_before_token = !0
                            } else "NEWLINE" === re ? "TK_RESERVED" === Y && N(ee.last_text) ? X.space_before_token = !0 : "TK_END_EXPR" !== Y ? "TK_START_EXPR" === Y && "TK_RESERVED" === $.type && r($.text, ["var", "let", "const"]) || ":" === ee.last_text || ("TK_RESERVED" === $.type && "if" === $.text && "else" === ee.last_text ? X.space_before_token = !0 : m()) : "TK_RESERVED" === $.type && r($.text, J.line_starters) && ")" !== ee.last_text && m() : ee.multiline_frame && E(ee.mode) && "," === ee.last_text && "}" === Z ? m() : "SPACE" === re && (X.space_before_token = !0);
                        g(), ee.last_word = $.text, "TK_RESERVED" === $.type && ("do" === $.text ? ee.do_block = !0 : "if" === $.text ? ee.if_block = !0 : "import" === $.text ? ee.import_block = !0 : ee.import_block && "TK_RESERVED" === $.type && "from" === $.text && (ee.import_block = !1))
                    }

                    function K() {
                        k() ? X.space_before_token = !1 : u($);
                        for (var e = C(1); !(ee.mode !== _.Statement || ee.if_block && e && "TK_RESERVED" === e.type && "else" === e.text || ee.do_block);) S();
                        ee.import_block && (ee.import_block = !1), g()
                    }

                    function M() {
                        k() ? X.space_before_token = !0 : (u($), "TK_RESERVED" === Y || "TK_WORD" === Y || ee.inline_frame ? X.space_before_token = !0 : "TK_COMMA" === Y || "TK_START_EXPR" === Y || "TK_EQUALS" === Y || "TK_OPERATOR" === Y ? A() || v() : m()), g()
                    }

                    function j() {
                        k() || u($), ee.declaration_statement && (ee.declaration_assignment = !0), X.space_before_token = !0, g(), X.space_before_token = !0
                    }

                    function W() {
                        u($, !0), g(), X.space_before_token = !0, ee.declaration_statement ? (x(ee.parent.mode) && (ee.declaration_assignment = !1), ee.declaration_assignment ? (ee.declaration_assignment = !1, m(!1, !0)) : ie.comma_first && v()) : ee.mode === _.ObjectLiteral || ee.mode === _.Statement && ee.parent.mode === _.ObjectLiteral ? (ee.mode === _.Statement && S(), ee.inline_frame || m()) : ie.comma_first && v()
                    }

                    function F() {
                        var e = "*" === $.text && ("TK_RESERVED" === Y && r(ee.last_text, ["function", "yield"]) || r(Y, ["TK_START_BLOCK", "TK_COMMA", "TK_END_BLOCK", "TK_SEMICOLON"])),
                            t = r($.text, ["-", "+"]) && (r(Y, ["TK_START_BLOCK", "TK_START_EXPR", "TK_EQUALS", "TK_OPERATOR"]) || r(ee.last_text, J.line_starters) || "," === ee.last_text);
                        if (k());
                        else {
                            var n = !e;
                            u($, n)
                        }
                        if ("TK_RESERVED" === Y && N(ee.last_text)) return X.space_before_token = !0, void g();
                        if ("*" === $.text && "TK_DOT" === Y) return void g();
                        if ("::" === $.text) return void g();
                        if ("TK_OPERATOR" === Y && r(ie.operator_position, f) && v(), ":" === $.text && ee.in_case) return ee.case_body = !0, w(), g(), m(), void (ee.in_case = !1);
                        var o = !0,
                            i = !0,
                            a = !1;
                        if (":" === $.text ? 0 === ee.ternary_depth ? o = !1 : (ee.ternary_depth -= 1, a = !0) : "?" === $.text && (ee.ternary_depth += 1), !t && !e && ie.preserve_newlines && r($.text, J.positionable_operators)) {
                            var s = ":" === $.text,
                                l = s && a,
                                c = s && !a;
                            switch (ie.operator_position) {
                                case p.before_newline:
                                    return X.space_before_token = !c, g(), s && !l || v(), void (X.space_before_token = !0);
                                case p.after_newline:
                                    return X.space_before_token = !0, !s || l ? C(1).wanted_newline ? m(!1, !0) : v() : X.space_before_token = !1, g(), void (X.space_before_token = !0);
                                case p.preserve_newline:
                                    return c || v(), o = !(X.just_added_newline() || c), X.space_before_token = o, g(), void (X.space_before_token = !0)
                            }
                        }
                        if (e) {
                            v(), o = !1;
                            var d = C(1);
                            i = d && r(d.type, ["TK_WORD", "TK_RESERVED"])
                        } else "..." === $.text ? (v(), o = "TK_START_BLOCK" === Y, i = !1) : (r($.text, ["--", "++", "!", "~"]) || t) && (o = !1, i = !1, !$.wanted_newline || "--" !== $.text && "++" !== $.text || m(!1, !0), ";" === ee.last_text && x(ee.mode) && (o = !0), "TK_RESERVED" === Y ? o = !0 : "TK_END_EXPR" === Y ? o = !("]" === ee.last_text && ("--" === $.text || "++" === $.text)) : "TK_OPERATOR" === Y && (o = r($.text, ["--", "-", "++", "+"]) && r(ee.last_text, ["--", "-", "++", "+"]), r($.text, ["+", "-"]) && r(ee.last_text, ["--", "++"]) && (i = !0)), (ee.mode !== _.BlockStatement || ee.inline_frame) && ee.mode !== _.Statement || "{" !== ee.last_text && ";" !== ee.last_text || m());
                        X.space_before_token = X.space_before_token || o, g(), X.space_before_token = i
                    }

                    function B(e) {
                        if (X.raw) return X.add_raw_token($), void ($.directives && "end" === $.directives.preserve && (X.raw = ie.test_output_raw));
                        if ($.directives) return m(!1, e), g(), "start" === $.directives.preserve && (X.raw = !0), void m(!1, !0);
                        if (!d.newline.test($.text) && !$.wanted_newline) return X.space_before_token = !0, g(), void (X.space_before_token = !0);
                        var t, n = h($.text),
                            r = !1,
                            o = !1,
                            a = $.whitespace_before,
                            s = a.length;
                        for (m(!1, e), n.length > 1 && (r = O(n.slice(1), "*"), o = R(n.slice(1), a)), g(n[0]), t = 1; t < n.length; t++) m(!1, !0), r ? g(" " + i(n[t])) : o && n[t].length > s ? g(n[t].substring(s)) : X.add_token(n[t]);
                        m(!1, e)
                    }

                    function G(e) {
                        $.wanted_newline ? m(!1, e) : X.trim(!0), X.space_before_token = !0, g(), m(!1, e)
                    }

                    function V() {
                        k() || u($, !0), "TK_RESERVED" === Y && N(ee.last_text) ? X.space_before_token = !0 : v(")" === ee.last_text && ie.break_chained_methods), g()
                    }

                    function H(e) {
                        g(), "\n" === $.text[$.text.length - 1] && m(!1, e)
                    }

                    function z() {
                        for (; ee.mode === _.Statement;) S();
                        u($)
                    }
                    var X, q, J, $, Y, Z, Q, ee, te, ne, re, oe, ie, ae = [],
                        se = "";
                    oe = {
                        TK_START_EXPR: I,
                        TK_END_EXPR: P,
                        TK_START_BLOCK: L,
                        TK_END_BLOCK: U,
                        TK_WORD: D,
                        TK_RESERVED: D,
                        TK_SEMICOLON: K,
                        TK_STRING: M,
                        TK_EQUALS: j,
                        TK_OPERATOR: F,
                        TK_COMMA: W,
                        TK_BLOCK_COMMENT: B,
                        TK_COMMENT: G,
                        TK_DOT: V,
                        TK_UNKNOWN: H,
                        TK_EOF: z
                    }, t = t ? t : {}, t = n(t, "js"), ie = {}, "expand-strict" === t.brace_style ? t.brace_style = "expand" : "collapse-preserve-inline" === t.brace_style ? t.brace_style = "collapse,preserve-inline" : void 0 !== t.braces_on_own_line ? t.brace_style = t.braces_on_own_line ? "expand" : "collapse" : t.brace_style || (t.brace_style = "collapse");
                    var ue = t.brace_style.split(/[^a-zA-Z0-9_\-]+/);
                    for (ie.brace_style = ue[0], ie.brace_preserve_inline = !!ue[1] && ue[1], ie.indent_size = t.indent_size ? parseInt(t.indent_size, 10) : 4, ie.indent_char = t.indent_char ? t.indent_char : " ", ie.eol = t.eol ? t.eol : "auto", ie.preserve_newlines = void 0 === t.preserve_newlines || t.preserve_newlines, ie.break_chained_methods = void 0 !== t.break_chained_methods && t.break_chained_methods, ie.max_preserve_newlines = void 0 === t.max_preserve_newlines ? 0 : parseInt(t.max_preserve_newlines, 10), ie.space_in_paren = void 0 !== t.space_in_paren && t.space_in_paren, ie.space_in_empty_paren = void 0 !== t.space_in_empty_paren && t.space_in_empty_paren, ie.jslint_happy = void 0 !== t.jslint_happy && t.jslint_happy, ie.space_after_anon_function = void 0 !== t.space_after_anon_function && t.space_after_anon_function, ie.keep_array_indentation = void 0 !== t.keep_array_indentation && t.keep_array_indentation, ie.space_before_conditional = void 0 === t.space_before_conditional || t.space_before_conditional, ie.unescape_strings = void 0 !== t.unescape_strings && t.unescape_strings, ie.wrap_line_length = void 0 === t.wrap_line_length ? 0 : parseInt(t.wrap_line_length, 10), ie.e4x = void 0 !== t.e4x && t.e4x, ie.end_with_newline = void 0 !== t.end_with_newline && t.end_with_newline, ie.comma_first = void 0 !== t.comma_first && t.comma_first, ie.operator_position = a(t.operator_position), ie.test_output_raw = void 0 !== t.test_output_raw && t.test_output_raw, ie.jslint_happy && (ie.space_after_anon_function = !0), t.indent_with_tabs && (ie.indent_char = "\t", ie.indent_size = 1), "auto" === ie.eol && (ie.eol = "\n", e && d.lineBreak.test(e || "") && (ie.eol = e.match(d.lineBreak)[0])), ie.eol = ie.eol.replace(/\\r/, "\r").replace(/\\n/, "\n"), Q = ""; ie.indent_size > 0;) Q += ie.indent_char, ie.indent_size -= 1;
                    var le = 0;
                    if (e && e.length) {
                        for (;
                            " " === e.charAt(le) || "\t" === e.charAt(le);) se += e.charAt(le), le += 1;
                        e = e.substring(le)
                    }
                    Y = "TK_START_BLOCK", Z = "", X = new l(Q, se), X.raw = ie.test_output_raw, ne = [], b(_.BlockStatement), this.beautify = function () {
                        var t;
                        for (J = new c(e, ie, Q), ae = J.tokenize(), q = 0, $ = C(); $;) oe[$.type](), Z = ee.last_text, Y = $.type, ee.last_text = $.text, q += 1, $ = C();
                        return t = X.get_code(), ie.end_with_newline && (t += "\n"), "\n" !== ie.eol && (t = t.replace(/[\n]/g, ie.eol)), t
                    };
                    var ce = ["break", "continue", "return", "throw"]
                }

                function u(e) {
                    var t = 0,
                        n = -1,
                        r = [],
                        o = !0;
                    this.set_indent = function (r) {
                        t = e.baseIndentLength + r * e.indent_length, n = r
                    }, this.get_character_count = function () {
                        return t
                    }, this.is_empty = function () {
                        return o
                    }, this.last = function () {
                        return this._empty ? null : r[r.length - 1]
                    }, this.push = function (e) {
                        r.push(e), t += e.length, o = !1
                    }, this.pop = function () {
                        var e = null;
                        return o || (e = r.pop(), t -= e.length, o = 0 === r.length), e
                    }, this.remove_indent = function () {
                        n > 0 && (n -= 1, t -= e.indent_length)
                    }, this.trim = function () {
                        for (;
                            " " === this.last();) r.pop(), t -= 1;
                        o = 0 === r.length
                    }, this.toString = function () {
                        var t = "";
                        return this._empty || (n >= 0 && (t = e.indent_cache[n]), t += r.join("")), t
                    }
                }

                function l(e, t) {
                    t = t || "", this.indent_cache = [t], this.baseIndentLength = t.length, this.indent_length = e.length, this.raw = !1;
                    var n = [];
                    this.baseIndentString = t, this.indent_string = e, this.previous_line = null, this.current_line = null, this.space_before_token = !1, this.add_outputline = function () {
                        this.previous_line = this.current_line, this.current_line = new u(this), n.push(this.current_line)
                    }, this.add_outputline(), this.get_line_number = function () {
                        return n.length
                    }, this.add_new_line = function (e) {
                        return (1 !== this.get_line_number() || !this.just_added_newline()) && (!(!e && this.just_added_newline()) && (this.raw || this.add_outputline(), !0))
                    }, this.get_code = function () {
                        var e = n.join("\n").replace(/[\r\n\t ]+$/, "");
                        return e
                    }, this.set_indent = function (e) {
                        if (n.length > 1) {
                            for (; e >= this.indent_cache.length;) this.indent_cache.push(this.indent_cache[this.indent_cache.length - 1] + this.indent_string);
                            return this.current_line.set_indent(e), !0
                        }
                        return this.current_line.set_indent(0), !1
                    }, this.add_raw_token = function (e) {
                        for (var t = 0; t < e.newlines; t++) this.add_outputline();
                        this.current_line.push(e.whitespace_before), this.current_line.push(e.text), this.space_before_token = !1
                    }, this.add_token = function (e) {
                        this.add_space_before_token(), this.current_line.push(e)
                    }, this.add_space_before_token = function () {
                        this.space_before_token && !this.just_added_newline() && this.current_line.push(" "), this.space_before_token = !1
                    }, this.remove_redundant_indentation = function (e) {
                        if (!e.multiline_frame && e.mode !== _.ForInitializer && e.mode !== _.Conditional)
                            for (var t = e.start_line_index, r = n.length; t < r;) n[t].remove_indent(), t++
                    }, this.trim = function (r) {
                        for (r = void 0 !== r && r, this.current_line.trim(e, t); r && n.length > 1 && this.current_line.is_empty();) n.pop(), this.current_line = n[n.length - 1], this.current_line.trim();
                        this.previous_line = n.length > 1 ? n[n.length - 2] : null
                    }, this.just_added_newline = function () {
                        return this.current_line.is_empty()
                    }, this.just_added_blankline = function () {
                        if (this.just_added_newline()) {
                            if (1 === n.length) return !0;
                            var e = n[n.length - 2];
                            return e.is_empty()
                        }
                        return !1
                    }
                }

                function c(e, t) {
                    function n(e) {
                        if (!e.match(x)) return null;
                        var t = {};
                        S.lastIndex = 0;
                        for (var n = S.exec(e); n;) t[n[1]] = n[2], n = S.exec(e);
                        return t
                    }

                    function i() {
                        var e, i = [];
                        _ = 0, m = "";
                        var h = w.next();
                        if (null === h) return ["", "TK_EOF"];
                        var x;
                        for (x = g.length ? g[g.length - 1] : new v("TK_START_BLOCK", "{"); r(h, s);)
                            if (d.newline.test(h) ? "\n" === h && "\r" === w.peek(-2) || (_ += 1, i = []) : i.push(h), h = w.next(), null === h) return ["", "TK_EOF"];
                        if (i.length && (m = i.join("")), u.test(h) || "." === h && w.testChar(u)) {
                            var S = !0,
                                O = !0,
                                R = u;
                            for ("0" === h && w.testChar(/[XxOoBb]/) ? (S = !1, O = !1, R = w.testChar(/[Bb]/) ? l : w.testChar(/[Oo]/) ? c : p, h += w.next()) : "." === h ? S = !1 : (h = "", w.back()); w.testChar(R);) h += w.next(), S && "." === w.peek() && (h += w.next(), S = !1), O && w.testChar(/[Ee]/) && (h += w.next(), w.testChar(/[+-]/) && (h += w.next()), O = !1, S = !1);
                            return [h, "TK_WORD"]
                        }
                        if (d.isIdentifierStart(w.peekCharCode(-1))) {
                            if (w.hasNext())
                                for (; d.isIdentifierChar(w.peekCharCode()) && (h += w.next(), w.hasNext()););
                            return "TK_DOT" === x.type || "TK_RESERVED" === x.type && r(x.text, ["set", "get"]) || !r(h, T) ? [h, "TK_WORD"] : "in" === h || "of" === h ? [h, "TK_OPERATOR"] : [h, "TK_RESERVED"]
                        }
                        if ("(" === h || "[" === h) return [h, "TK_START_EXPR"];
                        if (")" === h || "]" === h) return [h, "TK_END_EXPR"];
                        if ("{" === h) return [h, "TK_START_BLOCK"];
                        if ("}" === h) return [h, "TK_END_BLOCK"];
                        if (";" === h) return [h, "TK_SEMICOLON"];
                        if ("/" === h) {
                            var N, C = "";
                            if ("*" === w.peek()) {
                                w.next(), N = w.match(b), C = "/*" + N[0];
                                var I = n(C);
                                return I && "start" === I.ignore && (N = w.match(A), C += N[0]), C = C.replace(d.allLineBreaks, "\n"), [C, "TK_BLOCK_COMMENT", I]
                            }
                            if ("/" === w.peek()) return w.next(), N = w.match(E), C = "//" + N[0], [C, "TK_COMMENT"]
                        }
                        var P = /<()([-a-zA-Z:0-9_.]+|{[\s\S]+?}|!\[CDATA\[[\s\S]*?\]\])(\s+{[\s\S]+?}|\s+[-a-zA-Z:0-9_.]+|\s+[-a-zA-Z:0-9_.]+\s*=\s*('[^']*'|"[^"]*"|{[\s\S]+?}))*\s*(\/?)\s*>/g;
                        if ("`" === h || "'" === h || '"' === h || ("/" === h || t.e4x && "<" === h && w.test(P, -1)) && ("TK_RESERVED" === x.type && r(x.text, ["return", "case", "throw", "else", "do", "typeof", "yield"]) || "TK_END_EXPR" === x.type && ")" === x.text && x.parent && "TK_RESERVED" === x.parent.type && r(x.parent.text, ["if", "while", "for"]) || r(x.type, ["TK_COMMENT", "TK_START_EXPR", "TK_START_BLOCK", "TK_END_BLOCK", "TK_OPERATOR", "TK_EQUALS", "TK_EOF", "TK_SEMICOLON", "TK_COMMA"]))) {
                            var L = h,
                                U = !1,
                                D = !1;
                            if (e = h, "/" === L)
                                for (var K = !1; w.hasNext() && (U || K || w.peek() !== L) && !w.testChar(d.newline);) e += w.peek(), U ? U = !1 : (U = "\\" === w.peek(), "[" === w.peek() ? K = !0 : "]" === w.peek() && (K = !1)), w.next();
                            else if (t.e4x && "<" === L) {
                                var M = /[\s\S]*?<(\/?)([-a-zA-Z:0-9_.]+|{[\s\S]+?}|!\[CDATA\[[\s\S]*?\]\])(\s+{[\s\S]+?}|\s+[-a-zA-Z:0-9_.]+|\s+[-a-zA-Z:0-9_.]+\s*=\s*('[^']*'|"[^"]*"|{[\s\S]+?}))*\s*(\/?)\s*>/g;
                                w.back();
                                var j = "",
                                    W = w.match(P);
                                if (W) {
                                    for (var F = W[2].replace(/^{\s+/, "{").replace(/\s+}$/, "}"), B = 0 === F.indexOf("{"), G = 0; W;) {
                                        var V = !!W[1],
                                            H = W[2],
                                            z = !!W[W.length - 1] || "![CDATA[" === H.slice(0, 8);
                                        if (!z && (H === F || B && H.replace(/^{\s+/, "{").replace(/\s+}$/, "}")) && (V ? --G : ++G), j += W[0], G <= 0) break;
                                        W = w.match(M)
                                    }
                                    return W || (j += w.match(/[\s\S]*/g)[0]), j = j.replace(d.allLineBreaks, "\n"), [j, "TK_STRING"]
                                }
                            } else {
                                var X = function (t, n, r) {
                                    for (var o; w.hasNext() && (o = w.peek(), U || o !== t && (n || !d.newline.test(o)));)(U || n) && d.newline.test(o) ? ("\r" === o && "\n" === w.peek(1) && (w.next(), o = w.peek()), e += "\n") : e += o, U ? ("x" !== o && "u" !== o || (D = !0), U = !1) : U = "\\" === o, w.next(), r && e.indexOf(r, e.length - r.length) !== -1 && ("`" === t ? X("}", n, "`") : X("`", n, "${"), w.hasNext() && (e += w.next()))
                                };
                                "`" === L ? X("`", !0, "${") : X(L)
                            }
                            if (D && t.unescape_strings && (e = a(e)), w.peek() === L && (e += L, w.next(), "/" === L))
                                for (; w.hasNext() && d.isIdentifierStart(w.peekCharCode());) e += w.next();
                            return [e, "TK_STRING"]
                        }
                        if ("#" === h) {
                            if (0 === g.length && "!" === w.peek()) {
                                for (e = h; w.hasNext() && "\n" !== h;) h = w.next(), e += h;
                                return [o(e) + "\n", "TK_UNKNOWN"]
                            }
                            var q = "#";
                            if (w.hasNext() && w.testChar(u)) {
                                do h = w.next(), q += h; while (w.hasNext() && "#" !== h && "=" !== h);
                                return "#" === h || ("[" === w.peek() && "]" === w.peek(1) ? (q += "[]", w.next(), w.next()) : "{" === w.peek() && "}" === w.peek(1) && (q += "{}", w.next(), w.next())), [q, "TK_WORD"]
                            }
                        }
                        if ("<" === h && ("?" === w.peek() || "%" === w.peek())) {
                            w.back();
                            var J = w.match(k);
                            if (J) return h = J[0], h = h.replace(d.allLineBreaks, "\n"), [h, "TK_STRING"]
                        }
                        if ("<" === h && w.match(/\!--/g)) {
                            for (h = "<!--"; w.hasNext() && !w.testChar(d.newline);) h += w.next();
                            return y = !0, [h, "TK_COMMENT"]
                        }
                        if ("-" === h && y && w.match(/->/g)) return y = !1, ["-->", "TK_COMMENT"];
                        if ("." === h) return "." === w.peek() && "." === w.peek(1) ? (h += w.next() + w.next(), [h, "TK_OPERATOR"]) : [h, "TK_DOT"];
                        if (r(h, f)) {
                            for (; w.hasNext() && r(h + w.peek(), f) && (h += w.next(), w.hasNext()););
                            return "," === h ? [h, "TK_COMMA"] : "=" === h ? [h, "TK_EQUALS"] : [h, "TK_OPERATOR"]
                        }
                        return [h, "TK_UNKNOWN"]
                    }

                    function a(e) {
                        for (var t = "", n = 0, r = new h(e), o = null; r.hasNext();)
                            if (o = r.match(/([\s]|[^\\]|\\\\)+/g), o && (t += o[0]), "\\" === r.peek()) {
                                if (r.next(), "x" === r.peek()) o = r.match(/x([0-9A-Fa-f]{2})/g);
                                else {
                                    if ("u" !== r.peek()) {
                                        t += "\\", r.hasNext() && (t += r.next());
                                        continue
                                    }
                                    o = r.match(/u([0-9A-Fa-f]{4})/g)
                                }
                                if (!o) return e;
                                if (n = parseInt(o[1], 16), n > 126 && n <= 255 && 0 === o[0].indexOf("x")) return e;
                                if (n >= 0 && n < 32) {
                                    t += "\\" + o[0];
                                    continue
                                }
                                t += 34 === n || 39 === n || 92 === n ? "\\" + String.fromCharCode(n) : String.fromCharCode(n)
                            }
                        return t
                    }
                    var s = "\n\r\t ".split(""),
                        u = /[0-9]/,
                        l = /[01]/,
                        c = /[01234567]/,
                        p = /[0123456789abcdefABCDEF]/;
                    this.positionable_operators = "!= !== % & && * ** + - / : < << <= == === > >= >> >>> ? ^ | ||".split(" ");
                    var f = this.positionable_operators.concat("! %= &= *= **= ++ += , -- -= /= :: <<= = => >>= >>>= ^= |= ~ ...".split(" "));
                    this.line_starters = "continue,try,throw,return,var,let,const,if,switch,case,default,for,while,break,function,import,export".split(",");
                    var _, m, y, g, w, T = this.line_starters.concat(["do", "in", "of", "else", "get", "set", "new", "catch", "finally", "typeof", "yield", "async", "await", "from", "as"]),
                        b = /([\s\S]*?)((?:\*\/)|$)/g,
                        E = /([^\n\r\u2028\u2029]*)/g,
                        x = /\/\* beautify( \w+[:]\w+)+ \*\//g,
                        S = / (\w+)[:](\w+)/g,
                        A = /([\s\S]*?)((?:\/\*\sbeautify\signore:end\s\*\/)|$)/g,
                        k = /((<\?php|<\?=)[\s\S]*?\?>)|(<%[\s\S]*?%>)/g;
                    this.tokenize = function () {
                        w = new h(e), y = !1, g = [];
                        for (var t, n, r, o = null, a = [], s = []; !n || "TK_EOF" !== n.type;) {
                            for (r = i(), t = new v(r[1], r[0], _, m);
                                "TK_COMMENT" === t.type || "TK_BLOCK_COMMENT" === t.type || "TK_UNKNOWN" === t.type;) "TK_BLOCK_COMMENT" === t.type && (t.directives = r[2]), s.push(t), r = i(), t = new v(r[1], r[0], _, m);
                            s.length && (t.comments_before = s, s = []), "TK_START_BLOCK" === t.type || "TK_START_EXPR" === t.type ? (t.parent = n, a.push(o), o = t) : ("TK_END_BLOCK" === t.type || "TK_END_EXPR" === t.type) && o && ("]" === t.text && "[" === o.text || ")" === t.text && "(" === o.text || "}" === t.text && "{" === o.text) && (t.parent = o.parent, t.opened = o, o = a.pop()), g.push(t), n = t
                        }
                        return g
                    }
                }
                var d = {};
                ! function (e) {
                    var t = "ªµºÀ-ÖØ-öø-ˁˆ-ˑˠ-ˤˬˮͰ-ʹͶͷͺ-ͽΆΈ-ΊΌΎ-ΡΣ-ϵϷ-ҁҊ-ԧԱ-Ֆՙա-ևא-תװ-ײؠ-يٮٯٱ-ۓەۥۦۮۯۺ-ۼۿܐܒ-ܯݍ-ޥޱߊ-ߪߴߵߺࠀ-ࠕࠚࠤࠨࡀ-ࡘࢠࢢ-ࢬऄ-हऽॐक़-ॡॱ-ॷॹ-ॿঅ-ঌএঐও-নপ-রলশ-হঽৎড়ঢ়য়-ৡৰৱਅ-ਊਏਐਓ-ਨਪ-ਰਲਲ਼ਵਸ਼ਸਹਖ਼-ੜਫ਼ੲ-ੴઅ-ઍએ-ઑઓ-નપ-રલળવ-હઽૐૠૡଅ-ଌଏଐଓ-ନପ-ରଲଳଵ-ହଽଡ଼ଢ଼ୟ-ୡୱஃஅ-ஊஎ-ஐஒ-கஙசஜஞடணதந-பம-ஹௐఅ-ఌఎ-ఐఒ-నప-ళవ-హఽౘౙౠౡಅ-ಌಎ-ಐಒ-ನಪ-ಳವ-ಹಽೞೠೡೱೲഅ-ഌഎ-ഐഒ-ഺഽൎൠൡൺ-ൿඅ-ඖක-නඳ-රලව-ෆก-ะาำเ-ๆກຂຄງຈຊຍດ-ທນ-ຟມ-ຣລວສຫອ-ະາຳຽເ-ໄໆໜ-ໟༀཀ-ཇཉ-ཬྈ-ྌက-ဪဿၐ-ၕၚ-ၝၡၥၦၮ-ၰၵ-ႁႎႠ-ჅჇჍა-ჺჼ-ቈቊ-ቍቐ-ቖቘቚ-ቝበ-ኈኊ-ኍነ-ኰኲ-ኵኸ-ኾዀዂ-ዅወ-ዖዘ-ጐጒ-ጕጘ-ፚᎀ-ᎏᎠ-Ᏼᐁ-ᙬᙯ-ᙿᚁ-ᚚᚠ-ᛪᛮ-ᛰᜀ-ᜌᜎ-ᜑᜠ-ᜱᝀ-ᝑᝠ-ᝬᝮ-ᝰក-ឳៗៜᠠ-ᡷᢀ-ᢨᢪᢰ-ᣵᤀ-ᤜᥐ-ᥭᥰ-ᥴᦀ-ᦫᧁ-ᧇᨀ-ᨖᨠ-ᩔᪧᬅ-ᬳᭅ-ᭋᮃ-ᮠᮮᮯᮺ-ᯥᰀ-ᰣᱍ-ᱏᱚ-ᱽᳩ-ᳬᳮ-ᳱᳵᳶᴀ-ᶿḀ-ἕἘ-Ἕἠ-ὅὈ-Ὅὐ-ὗὙὛὝὟ-ώᾀ-ᾴᾶ-ᾼιῂ-ῄῆ-ῌῐ-ΐῖ-Ίῠ-Ῥῲ-ῴῶ-ῼⁱⁿₐ-ₜℂℇℊ-ℓℕℙ-ℝℤΩℨK-ℭℯ-ℹℼ-ℿⅅ-ⅉⅎⅠ-ↈⰀ-Ⱞⰰ-ⱞⱠ-ⳤⳫ-ⳮⳲⳳⴀ-ⴥⴧⴭⴰ-ⵧⵯⶀ-ⶖⶠ-ⶦⶨ-ⶮⶰ-ⶶⶸ-ⶾⷀ-ⷆⷈ-ⷎⷐ-ⷖⷘ-ⷞⸯ々-〇〡-〩〱-〵〸-〼ぁ-ゖゝ-ゟァ-ヺー-ヿㄅ-ㄭㄱ-ㆎㆠ-ㆺㇰ-ㇿ㐀-䶵一-鿌ꀀ-ꒌꓐ-ꓽꔀ-ꘌꘐ-ꘟꘪꘫꙀ-ꙮꙿ-ꚗꚠ-ꛯꜗ-ꜟꜢ-ꞈꞋ-ꞎꞐ-ꞓꞠ-Ɦꟸ-ꠁꠃ-ꠅꠇ-ꠊꠌ-ꠢꡀ-ꡳꢂ-ꢳꣲ-ꣷꣻꤊ-ꤥꤰ-ꥆꥠ-ꥼꦄ-ꦲꧏꨀ-ꨨꩀ-ꩂꩄ-ꩋꩠ-ꩶꩺꪀ-ꪯꪱꪵꪶꪹ-ꪽꫀꫂꫛ-ꫝꫠ-ꫪꫲ-ꫴꬁ-ꬆꬉ-ꬎꬑ-ꬖꬠ-ꬦꬨ-ꬮꯀ-ꯢ가-힣ힰ-ퟆퟋ-ퟻ豈-舘並-龎ﬀ-ﬆﬓ-ﬗיִײַ-ﬨשׁ-זּטּ-לּמּנּסּףּפּצּ-ﮱﯓ-ﴽﵐ-ﶏﶒ-ﷇﷰ-ﷻﹰ-ﹴﹶ-ﻼＡ-Ｚａ-ｚｦ-ﾾￂ-ￇￊ-ￏￒ-ￗￚ-ￜ",
                        n = "̀-ͯ҃-֑҇-ׇֽֿׁׂׅׄؐ-ؚؠ-ىٲ-ۓۧ-ۨۻ-ۼܰ-݊ࠀ-ࠔࠛ-ࠣࠥ-ࠧࠩ-࠭ࡀ-ࡗࣤ-ࣾऀ-ःऺ-़ा-ॏ॑-ॗॢ-ॣ०-९ঁ-ঃ়া-ৄেৈৗয়-ৠਁ-ਃ਼ਾ-ੂੇੈੋ-੍ੑ੦-ੱੵઁ-ઃ઼ા-ૅે-ૉો-્ૢ-ૣ૦-૯ଁ-ଃ଼ା-ୄେୈୋ-୍ୖୗୟ-ୠ୦-୯ஂா-ூெ-ைொ-்ௗ௦-௯ఁ-ఃె-ైొ-్ౕౖౢ-ౣ౦-౯ಂಃ಼ಾ-ೄೆ-ೈೊ-್ೕೖೢ-ೣ೦-೯ംഃെ-ൈൗൢ-ൣ൦-൯ංඃ්ා-ුූෘ-ෟෲෳิ-ฺเ-ๅ๐-๙ິ-ູ່-ໍ໐-໙༘༙༠-༩༹༵༷ཁ-ཇཱ-྄྆-྇ྍ-ྗྙ-ྼ࿆က-ဩ၀-၉ၧ-ၭၱ-ၴႂ-ႍႏ-ႝ፝-፟ᜎ-ᜐᜠ-ᜰᝀ-ᝐᝲᝳក-ឲ៝០-៩᠋-᠍᠐-᠙ᤠ-ᤫᤰ-᤻ᥑ-ᥭᦰ-ᧀᧈ-ᧉ᧐-᧙ᨀ-ᨕᨠ-ᩓ᩠-᩿᩼-᪉᪐-᪙ᭆ-ᭋ᭐-᭙᭫-᭳᮰-᮹᯦-᯳ᰀ-ᰢ᱀-᱉ᱛ-ᱽ᳐-᳒ᴀ-ᶾḁ-ἕ‌‍‿⁀⁔⃐-⃥⃜⃡-⃰ⶁ-ⶖⷠ-ⷿ〡-〨゙゚Ꙁ-ꙭꙴ-꙽ꚟ꛰-꛱ꟸ-ꠀ꠆ꠋꠣ-ꠧꢀ-ꢁꢴ-꣄꣐-꣙ꣳ-ꣷ꤀-꤉ꤦ-꤭ꤰ-ꥅꦀ-ꦃ꦳-꧀ꨀ-ꨧꩀ-ꩁꩌ-ꩍ꩐-꩙ꩻꫠ-ꫩꫲ-ꫳꯀ-ꯡ꯬꯭꯰-꯹ﬠ-ﬨ︀-️︠-︦︳︴﹍-﹏０-９＿",
                        r = new RegExp("[" + t + "]"),
                        o = new RegExp("[" + t + n + "]");
                    e.newline = /[\n\r\u2028\u2029]/, e.lineBreak = new RegExp("\r\n|" + e.newline.source), e.allLineBreaks = new RegExp(e.lineBreak.source, "g"), e.isIdentifierStart = function (e) {
                        return e < 65 ? 36 === e || 64 === e : e < 91 || (e < 97 ? 95 === e : e < 123 || e >= 170 && r.test(String.fromCharCode(e)))
                    }, e.isIdentifierChar = function (e) {
                        return e < 48 ? 36 === e : e < 58 || !(e < 65) && (e < 91 || (e < 97 ? 95 === e : e < 123 || e >= 170 && o.test(String.fromCharCode(e))))
                    }
                } (d);
                var p = {
                    before_newline: "before-newline",
                    after_newline: "after-newline",
                    preserve_newline: "preserve-newline"
                },
                    f = [p.before_newline, p.preserve_newline],
                    _ = {
                        BlockStatement: "BlockStatement",
                        Statement: "Statement",
                        ObjectLiteral: "ObjectLiteral",
                        ArrayLiteral: "ArrayLiteral",
                        ForInitializer: "ForInitializer",
                        Conditional: "Conditional",
                        Expression: "Expression"
                    },
                    h = function (e) {
                        var t = e,
                            n = t.length,
                            r = 0;
                        this.back = function () {
                            r -= 1
                        }, this.hasNext = function () {
                            return r < n
                        }, this.next = function () {
                            var e = null;
                            return this.hasNext() && (e = t.charAt(r), r += 1), e
                        }, this.peek = function (e) {
                            var o = null;
                            return e = e || 0, e += r, e >= 0 && e < n && (o = t.charAt(e)), o
                        }, this.peekCharCode = function (e) {
                            var o = 0;
                            return e = e || 0, e += r, e >= 0 && e < n && (o = t.charCodeAt(e)), o
                        }, this.test = function (e, n) {
                            return n = n || 0, e.lastIndex = r + n, e.test(t)
                        }, this.testChar = function (e, t) {
                            var n = this.peek(t);
                            return null !== n && e.test(n)
                        }, this.match = function (e) {
                            e.lastIndex = r;
                            var n = e.exec(t);
                            return n && n.index === r ? r += n[0].length : n = null, n
                        }
                    },
                    v = function (e, t, n, r, o) {
                        this.type = e, this.text = t, this.comments_before = [], this.comments_after = [], this.newlines = n || 0, this.wanted_newline = n > 0, this.whitespace_before = r || "", this.parent = o || null, this.opened = null, this.directives = null
                    },
                    m = new s(e, t);
                return m.beautify()
            }
            r = [], o = function () {
                return {
                    js_beautify: i
                }
            }.apply(t, r), !(void 0 !== o && (e.exports = o))
        } ()
}, function (e, t) {
    "use strict";

    function n(e) {
        for (var t, n = /([^=?&]+)=?([^&]*)/g, r = {}; t = n.exec(e); r[decodeURIComponent(t[1])] = decodeURIComponent(t[2]));
        return r
    }

    function r(e, t) {
        t = t || "";
        var n = [];
        "string" != typeof t && (t = "?");
        for (var r in e) o.call(e, r) && n.push(encodeURIComponent(r) + "=" + encodeURIComponent(e[r]));
        return n.length ? t + n.join("&") : ""
    }
    var o = Object.prototype.hasOwnProperty;
    t.stringify = r, t.parse = n
}, , function (e, t) {
    "use strict";
    e.exports = function (e, t) {
        if (t = t.split(":")[0], e = +e, !e) return !1;
        switch (t) {
            case "http":
            case "ws":
                return 80 !== e;
            case "https":
            case "wss":
                return 443 !== e;
            case "ftp":
                return 21 !== e;
            case "gopher":
                return 70 !== e;
            case "file":
                return !1
        }
        return 0 !== e
    }
}, , , , function (e, t, n) {
    (function (t) {
        "use strict";
        var r, o = /^[A-Za-z][A-Za-z0-9+-.]*:\/\//,
            i = {
                hash: 1,
                query: 1
            };
        e.exports = function (e) {
            e = e || t.location || {}, r = r || n(238);
            var a, s = {},
                u = typeof e;
            if ("blob:" === e.protocol) s = new r(unescape(e.pathname), {});
            else if ("string" === u) {
                s = new r(e, {});
                for (a in i) delete s[a]
            } else if ("object" === u) {
                for (a in e) a in i || (s[a] = e[a]);
                void 0 === s.slashes && (s.slashes = o.test(e.href))
            }
            return s
        }
    }).call(t, function () {
        return this
    } ())
}, , function (e, t, n) {
    e.exports = n(325)["default"]
}, function (e, t, n) {
    function r(e) {
        return "string" == typeof e || !i(e) && a(e) && o(e) == s
    }
    var o = n(24),
        i = n(12),
        a = n(25),
        s = "[object String]";
    e.exports = r
}, , , , , function (e, t, n) {
    "use strict";

    function r(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function o(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function i(e) {
        var t = b[b.length - 1];
        if (t && e.id === t.id) return E.push((0, g["default"])(e)), b.pop()
    }

    function a(e) {
        var t = E[E.length - 1];
        if (t && e.id === t.id) return b.push((0, g["default"])(e)), E.pop()
    }

    function s(e) {
        b.push((0, g["default"])(e)), E = [], b.length > T && b.shift()
    }

    function u(e, t) {
        var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : e.actions.length;
        s(e), e.actions.splice(n, 0, t)
    }

    function l(e, t) {
        s(e);
        var n = (0, h["default"])(e.actions, {
            id: t.id
        });
        n !== -1 && e.actions.splice(n, 1, t)
    }

    function c(e, t) {
        s(e), e.actions = e.actions.filter(function (e) {
            return t.indexOf(e.id) === -1
        })
    }

    function d(e, t) {
        s(e);
        var n = (0, h["default"])(e.actions, {
            id: t
        });
        return n !== -1 && e.actions.splice(n, 1), e
    }

    function p(e, t) {
        var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : e.actions.length,
            r = e.actions;
        if (t.type === m.POPSTATE && (s(e), t = f(t, n, r), r.splice(n, 0, new m.UrlChangeIndicatorAction(t.value)), r.splice(n, 0, t)), t.type === m.BACK && (s(e), r.splice(n, 0, t)), t.type === m.PUSHSTATE && (s(e), t = f(t, n, r), r.splice(n, 0, t)), t.type === m.URL_CHANGE_INDICATOR && (s(e), t = f(t, n, r), r.splice(n, 0, t)), t.type === m.PAGELOAD && (s(e), t = f(t, n, r), r.splice(n, 0, new m.UrlChangeIndicatorAction(t.value)), r.splice(n + 1, 0, t)), t.type === m.FULL_PAGELOAD && (s(e), t = f(t, n, r), r.splice(n, 0, new m.UrlChangeIndicatorAction(t.value)), r.splice(n + 1, 0, t)), t.type === m.MOUSEDOWN && (s(e), r.splice(n, 0, t)), t.type === m.SUBMIT && (s(e), r.splice(n, 0, t)), t.type === m.KEYDOWN) switch (t.keyValue) {
            case "Enter":
                s(e), r.splice(n, 0, t);
                break;
            case "Escape":
                s(e), r.splice(n, 0, t)
        }
        t.type === m.INPUT && "SELECT" === t.inputType && (s(e), r.splice(n, 0, t)), t.type === m.INPUT && (r[n - 1] && r[n - 1].type === m.INPUT && r[n - 1].selector === t.selector ? r[n - 1].value = t.value : (s(e), r.splice(n, 0, t))), t.type === m.TEXT_ASSERT && (s(e), r.splice(n, 0, t)), t.type === m.VALUE_ASSERT && (s(e), r.splice(n, 0, t)), t.type === m.EL_PRESENT_ASSERT && (s(e), r.splice(n, 0, t))
    }

    function f(e, t, n) {
        if (t > 0) {
            var r = new w(e.value),
                o = new w(n[0].value);
            r.origin === o.origin && (e.value = e.value.replace(r.origin, ""))
        }
        return e
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var _ = n(56),
        h = o(_);
    t.getPriorTestState = i, t.getLaterTestState = a, t.addAction = u, t.updateAction = l, t.removeActions = c, t.removeAction = d, t.addRecordedAction = p;
    var v = n(28),
        m = r(v),
        y = n(309),
        g = o(y),
        w = n(238),
        T = 20,
        b = [],
        E = []
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function o(e, t, n, r, o) {
        h = o, h.accountForms.isProcessingLogin = !0, h.accountForms.loginError = null;
        var i;
        return t !== n ? (h.accountForms.isProcessingLogin = !1, void (h.accountForms.loginError = "Passwords do not match.")) : r ? void fetch("https://www.snaptest.io/api/join", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: (0, _["default"])({
                email: e,
                password: t,
                betacode: "letmeinplease",
                agreedtoterms: r ? "on" : "off",
                first: e
            })
        }).then(function (e) {
            return e.json()
        }).then(function (e) {
            if (e.success) return i = e.user, (0, y.loadItems)(i.apiKey);
            throw new Error(e.detail)
        }).then(function (e) {
            u(i, h), h.tests = d(i), h.directory = p(), h.accountForms.isProcessingLogin = !1, h.accountForms.loginError = null, h.tutorialActivated = !0, (0, y.saveItemToDB)({
                type: "directory",
                tree: h.directory
            }, h), h.tests.forEach(function (e) {
                (0, y.saveItemToDB)(e, h)
            }), m["default"].toAll("stateChange", h)
        })["catch"](function (e) {
            h.accountForms.isProcessingLogin = !1, h.accountForms.loginError = e instanceof Error ? e.message : e, m["default"].toAll("stateChange", h)
        }) : (h.accountForms.isProcessingLogin = !1, void (h.accountForms.loginError = "Accepting terms of service is required."))
    }

    function i(e, t, n) {
        h = n, h.accountForms.isProcessingLogin = !0, h.accountForms.loginError = null;
        var r;
        fetch("https://www.snaptest.io/api/signin", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: (0, _["default"])({
                email: e,
                password: t
            })
        }).then(function (e) {
            return e.json()
        }).then(function (e) {
            if (e.success) return r = e.user, (0, y.loadItems)(r.apiKey);
            throw new Error(e.error)
        }).then(function (e) {
            u(r, h), l(e.tests, h), c(e.components, h), e.directory ? h.directory = e.directory.tree : (0, y.saveItemToDB)({
                tree: h.directory,
                type: "directory"
            }, h), h.accountForms.isProcessingLogin = !1, h.accountForms.loginError = null, m["default"].toAll("stateChange", h)
        })["catch"](function (e) {
            h.accountForms.isProcessingLogin = !1, h.accountForms.loginError = e instanceof Error ? e.message : e, m["default"].toAll("stateChange", h)
        })
    }

    function a(e, t) {
        h = t, h.accountForms.isProcessingLogin = !0, h.accountForms.loginError = null, fetch("https://www.snaptest.io/api/forgot-password", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: (0, _["default"])({
                email: e
            })
        }).then(function (e) {
            if (200 !== e.status) throw new Error("Something went wrong. Please try again soon.");
            return e.json()
        }).then(function () {
            h.accountForms.isProcessingLogin = !1, h.accountForms.loginError = null, h.accountForms.type = "RESET_MODE", m["default"].toAll("stateChange", h)
        })["catch"](function (e) {
            h.accountForms.isProcessingLogin = !1, h.accountForms.loginError = e instanceof Error ? e.message : e, m["default"].toAll("stateChange", h)
        })
    }

    function s(e, t, n, r) {
        return h = r, h.accountForms.isProcessingLogin = !0, h.accountForms.loginError = null, t !== n ? (h.accountForms.isProcessingLogin = !1, void (h.accountForms.loginError = "Passwords do not match.")) : void fetch("https://www.snaptest.io/api/reset-password/" + e, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: (0, _["default"])({
                password: t
            })
        }).then(function (e) {
            if (200 !== e.status) throw new Error("Something went wrong. Please try again soon.");
            return e.json()
        }).then(function (e) {
            if (e.error) throw new Error(e.error);
            h.accountForms.isProcessingLogin = !1, h.accountForms.loginError = null, h.accountForms.successMessage = null, h.accountForms.type = "LOGIN_MODE", m["default"].toAll("stateChange", h)
        })["catch"](function (e) {
            h.accountForms.isProcessingLogin = !1, h.accountForms.loginError = e instanceof Error ? e.message : e, m["default"].toAll("stateChange", h)
        })
    }

    function u(e, t) {
        return t.user = e, chrome.storage.sync.set({
            user: t.user
        }), t
    }

    function l(e, t) {
        t.tests = e
    }

    function c(e, t) {
        t.components = e
    }

    function d(e) {
        w = [];
        var t = [{
            lastUpdated: "2017-06-15T22:57:36.397Z",
            user: "58d0be5c807ae4fc114dff2f",
            variables: [],
            type: "test",
            actions: [{
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "S1thjKxmW",
                value: "http://devexpress.github.io/testcafe/example/",
                index: null
            }, {
                type: "FULL_PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "BkK3jFl7b",
                value: "http://devexpress.github.io/testcafe/example/",
                isInitialLoad: !1,
                width: 832,
                height: 726
            }, {
                type: "PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "SyAJhYemZ",
                value: "/testcafe/example/"
            }, {
                type: "INPUT",
                selector: "[name=name]",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "ByZs3oFe7b",
                value: "Bob Hope",
                inputType: "text"
            }, {
                type: "MOUSEDOWN",
                selector: "div > fieldset:nth-of-type(2) > p > label",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [8],
                suggestions: [],
                id: "HJGpoYxmZ",
                textContent: "Support for testing on remote devices",
                textAsserted: !1,
                x: 90,
                y: 288
            }, {
                type: "MOUSEDOWN",
                selector: "div > fieldset:nth-of-type(2) > p:nth-of-type(3) > label",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "rym6oYx7b",
                textContent: "Running tests in background and/or in parallel in multiple browsers",
                textAsserted: !1,
                x: 101,
                y: 366
            }, {
                type: "MOUSEDOWN",
                selector: "div > fieldset:nth-of-type(2) > p:nth-of-type(5) > label",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [8],
                suggestions: [],
                id: "H1VasKe7-",
                textContent: "Advanced traffic and markup analysis",
                textAsserted: !1,
                x: 116,
                y: 454
            }, {
                type: "MOUSEDOWN",
                selector: ".main-content .form-bottom label",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "rkNCitxmb",
                textContent: "I have tried TestCafe",
                textAsserted: !1,
                x: 106,
                y: 307
            }, {
                type: "INPUT",
                selector: "[name=comments]",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "HkW80jKlmb",
                value: "SnapTest.io wants to integrate with TestCafe!",
                inputType: "textarea"
            }, {
                type: "MOUSEDOWN",
                selector: "div:nth-of-type(2) > fieldset > p:nth-of-type(2) > label",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [8],
                suggestions: [],
                id: "rysRiYg7Z",
                textContent: "MacOS",
                textAsserted: !1,
                x: 644,
                y: 286
            }, {
                type: "MOUSEDOWN",
                selector: "body form button",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "B100iYl7W",
                textContent: "Submit",
                textAsserted: !1,
                x: 128,
                y: 682
            }, {
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "SkRRoYlmZ",
                value: "/testcafe/example/thank-you.html",
                index: null
            }, {
                type: "PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "ByR0stl7-",
                value: "/testcafe/example/thank-you.html"
            }, {
                type: "TEXT_ASSERT",
                selector: "h1",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "S1ly3KgQb",
                value: "Thank you, Bob Hope!"
            }, {
                type: "TEXT_ASSERT",
                selector: "p",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "HJMyhFe7Z",
                value: "To learn more about TestCafe, please visit:"
            }, {
                type: "TEXT_ASSERT",
                selector: "a",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "rkm12YlX-",
                value: "devexpress.github.io/testcafe"
            }, {
                type: "MOUSEDOWN",
                selector: "a",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "S1Ly3KxQZ",
                textContent: "devexpress.github.io/testcafe",
                textAsserted: !1,
                x: 471,
                y: 424
            }, {
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "Bkt12YgmW",
                value: "/testcafe/",
                index: null
            }, {
                type: "PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                description: null,
                timeout: null,
                warnings: [],
                suggestions: [],
                id: "r1Y13Kg7W",
                value: "/testcafe/"
            }],
            name: "TestCafe's sample review",
            id: "BkY5stxQ-"
        }, {
            user: "58d0be5c807ae4fc114dff2f",
            variables: [],
            type: "test",
            actions: [{
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "SyeuNGC93x",
                value: "http://bootswatch.com/cerulean/#forms",
                index: null
            }, {
                type: "FULL_PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "S1KwdC5Tx",
                value: "http://bootswatch.com/cerulean/#forms",
                isInitialLoad: !1,
                width: 500,
                height: 500
            }, {
                type: "INPUT",
                selector: "div > form > fieldset > div > div > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "BJQfHzCqng",
                value: "testwow",
                inputType: "text"
            }, {
                type: "VALUE_ASSERT",
                selector: "div > form > fieldset > div > div > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "H1qLGRc2e",
                value: "testwow"
            }, {
                type: "MOUSEDOWN",
                selector: "div > form > fieldset > div:nth-of-type(2) > div > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "H1Owz053e",
                textContent: null,
                textAsserted: !1,
                x: 287,
                y: 350
            }, {
                type: "INPUT",
                selector: "div > form > fieldset > div:nth-of-type(2) > div > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "SkeYDGR9nx",
                value: "password",
                inputType: "password"
            }, {
                type: "VALUE_ASSERT",
                selector: "div > form > fieldset > div:nth-of-type(2) > div > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "rJ-uf09nl",
                value: "password"
            }, {
                type: "MOUSEDOWN",
                selector: "form > fieldset > div:nth-of-type(2) > div > div > label > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "HkoOfAq2x",
                textContent: null,
                textAsserted: !1,
                x: 245,
                y: 387
            }, {
                type: "VALUE_ASSERT",
                selector: "form > fieldset > div:nth-of-type(2) > div > div > label > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "S1eYzC9hl",
                value: "true"
            }, {
                type: "MOUSEDOWN",
                selector: "form > fieldset > div:nth-of-type(2) > div > div > label",
                nodeName: "Unknown",
                description: null,
                warnings: [],
                suggestions: [],
                id: "Hkdbca7ZW",
                textContent: null,
                textAsserted: !1,
                x: null,
                y: null
            }, {
                type: "MOUSEDOWN",
                selector: "textArea",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "Byitf05hg",
                textContent: null,
                textAsserted: !1,
                x: 335,
                y: 435
            }, {
                type: "INPUT",
                selector: "textArea",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "ByXoKzA93x",
                value: "test",
                inputType: "textarea"
            }, {
                type: "VALUE_ASSERT",
                selector: "textArea",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "SkJcf0chx",
                value: "test"
            }, {
                type: "MOUSEDOWN",
                selector: "form > fieldset > div:nth-of-type(4) > div > div:nth-of-type(2) > label",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "SJLcz0qnx",
                textContent: "Option two can be something else",
                textAsserted: !1,
                x: 271,
                y: 426
            }, {
                type: "TEXT_ASSERT",
                selector: "form > fieldset > div:nth-of-type(4) > div > div:nth-of-type(2) > label",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "Byx_qf0q3x",
                value: "Option two can be something else"
            }, {
                type: "MOUSEDOWN",
                selector: "form > fieldset > div:nth-of-type(4) > div > div > label",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "r1p9fRqhg",
                textContent: "Option one is this",
                textAsserted: !1,
                x: 301,
                y: 398
            }, {
                type: "VALUE_ASSERT",
                selector: "form > fieldset > div:nth-of-type(4) > div > div > label > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "B1ljzCq2g",
                value: "true"
            }, {
                type: "MOUSEDOWN",
                selector: "div > form > fieldset > div:nth-of-type(5) > div > select",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "rJojMRq2x",
                textContent: null,
                textAsserted: !1,
                x: 290,
                y: 356
            }, {
                type: "INPUT",
                selector: "div > form > fieldset > div:nth-of-type(5) > div > select",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "r1noGR5hg",
                value: "3",
                inputType: "select-one"
            }, {
                type: "VALUE_ASSERT",
                selector: "div > form > fieldset > div:nth-of-type(5) > div > select",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "H1ehzAqhx",
                value: "3"
            }, {
                type: "MOUSEDOWN",
                selector: "form > fieldset > div:nth-of-type(5) > div > select:nth-of-type(2) > option:nth-of-type(3)",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "Hy8nf0q2e",
                textContent: "3",
                textAsserted: !1,
                x: 264,
                y: 433
            }, {
                type: "INPUT",
                selector: "div > form > fieldset > div:nth-of-type(5) > div > select:nth-of-type(2)",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "H1-L3MCchg",
                value: "3",
                inputType: "select-multiple"
            }, {
                type: "VALUE_ASSERT",
                selector: "div > form > fieldset > div:nth-of-type(5) > div > select:nth-of-type(2)",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "Bk32fCc2x",
                value: "3"
            }, {
                type: "MOUSEDOWN",
                selector: "div:nth-of-type(2) > form > div:nth-of-type(9) > div > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "Sk-PX09hg",
                textContent: null,
                textAsserted: !1,
                x: 911,
                y: 596
            }, {
                type: "INPUT",
                selector: "div:nth-of-type(2) > form > div:nth-of-type(9) > div > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "HJefwQCqhx",
                value: "asdfm",
                inputType: "text"
            }, {
                type: "VALUE_ASSERT",
                selector: "div:nth-of-type(2) > form > div:nth-of-type(9) > div > input",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "Hy6wmAqne",
                value: "asdfm"
            }],
            name: "Form inputs",
            lastUpdated: "2017-06-15T22:32:39.203Z",
            id: "r1uNfCchg"
        }, {
            lastUpdated: "2017-06-15T22:53:03.288Z",
            user: "58d0be5c807ae4fc114dff2f",
            variables: [],
            type: "test",
            actions: [{
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "H1dki1dRx",
                value: "https://www.google.com/",
                index: null
            }, {
                type: "FULL_PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "rJu1s1uAe",
                value: "https://www.google.com/",
                isInitialLoad: !1,
                width: 1200,
                height: 1200,
                description: "Go to google.com"
            }, {
                type: "INPUT",
                selector: "[name=q]",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "BJbqyiJ_0l",
                value: "snaptest io",
                inputType: "text",
                description: 'enter "snaptest io" into the search bar'
            }, {
                type: "MOUSEDOWN",
                selector: "svg",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "ryggi1uCl",
                textContent: null,
                textAsserted: !1,
                x: 761,
                y: 47,
                description: "click the search button"
            }, {
                type: "TEXT_ASSERT",
                selector: "div > div > div > div > div > div > div > div > h3 > a",
                nodeName: "Unknown",
                description: "sees SnapTest.io as the first result",
                timeout: null,
                warnings: [8],
                suggestions: [],
                id: "H1lNctx7b",
                value: "SnapTest - Generate & maintain automated QA tests easily"
            }],
            name: "Google Search",
            id: "ByO1jJ_Cl"
        }, {
            user: "58d0be5c807ae4fc114dff2f",
            variables: [],
            type: "test",
            actions: [{
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "rJeU39Znpl",
                value: "https://www.snaptest.io/",
                index: null
            }, {
                type: "FULL_PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "HyLhc-hpl",
                value: "https://www.snaptest.io/",
                isInitialLoad: !1,
                width: 500,
                height: 500
            }, {
                type: "MOUSEDOWN",
                selector: "[name=email]",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "rJENsZhTx",
                textContent: null,
                textAsserted: !1,
                x: 302,
                y: 384
            }, {
                type: "INPUT",
                selector: "[name=email]",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "SJgSVo-nax",
                value: "hello@all.com",
                inputType: "email"
            }, {
                type: "MOUSEDOWN",
                selector: "div:nth-of-type(2) > div > ul > li > a",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "HJR00wn0e",
                textContent: "Blog",
                textAsserted: !1,
                x: 585,
                y: 24
            }, {
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "SkyyJuhRx",
                value: "https://www.snaptest.io/blog",
                index: null
            }, {
                type: "PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "Sky1yuhCx",
                value: "/blog"
            }, {
                type: "TEXT_ASSERT",
                selector: ".tag-line",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "rkle1yd2Al",
                value: "Blog"
            }, {
                type: "TEXT_ASSERT",
                selector: "body > div > div > div > div > a",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector, but was able to clean it up a bit. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "ryWJ1_3Cl",
                value: "Devblog"
            }, {
                type: "TEXT_ASSERT",
                selector: "div > div > a:nth-of-type(2)",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "B1fkJOnRe",
                value: "Guides"
            }, {
                type: "TEXT_ASSERT",
                selector: "div > div > a:nth-of-type(3)",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "SyeG1kOn0x",
                value: "QA General"
            }, {
                type: "TEXT_ASSERT",
                selector: "div > div > a:nth-of-type(4)",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "rJXk1_2Cx",
                value: "Updates"
            }, {
                type: "MOUSEDOWN",
                selector: "div:nth-of-type(2) > div > ul > li:nth-of-type(2) > a",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "BySk1OnRx",
                textContent: "Updates",
                textAsserted: !1,
                x: 667,
                y: 30
            }, {
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "rJ8Jk_2Ae",
                value: "https://www.snaptest.io/product-updates",
                index: null
            }, {
                type: "PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "r1IJJ_hRl",
                value: "/product-updates"
            }, {
                type: "TEXT_ASSERT",
                selector: ".tag-line",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "ByuyJO20e",
                value: "Updates"
            }, {
                type: "MOUSEDOWN",
                selector: "div:nth-of-type(2) > div > ul > li:nth-of-type(3) > a",
                nodeName: "Unknown",
                warnings: ["Couldn't find good selector. Please manually add otherwise this will break very easily."],
                suggestions: [],
                id: "S1tJ1OhRl",
                textContent: "Roadmap",
                textAsserted: !1,
                x: 758,
                y: 29
            }, {
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "BJY1yu20e",
                value: "https://www.snaptest.io/product-roadmap",
                index: null
            }, {
                type: "PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "HkFyyOhAx",
                value: "/product-roadmap"
            }, {
                type: "TEXT_ASSERT",
                selector: ".tag-line",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "Bk9kk_2Ce",
                value: "Road Map"
            }],
            name: "Basic Snaptest Walkthrough",
            createdAt: "2017-04-18T00:39:22.465Z",
            lastUpdated: "2017-06-15T04:35:21.644Z",
            id: "S1-rw15Tg"
        }, {
            lastUpdated: "2017-06-15T22:41:50.363Z",
            user: "58d0be5c807ae4fc114dff2f",
            variables: [],
            type: "test",
            actions: [{
                type: "URL_CHANGE_INDICATOR",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "Hy0GY4GxZ",
                value: "http://www.google.com",
                index: null
            }, {
                type: "FULL_PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                warnings: [],
                suggestions: [],
                id: "H1-CfY4GlW",
                value: "http://www.google.com",
                isInitialLoad: !1,
                width: 1130,
                height: 896,
                description: "Hit google.com"
            }, {
                type: "PAGELOAD",
                selector: "Unknown",
                nodeName: "Unknown",
                description: "wait for the page to load",
                warnings: [],
                suggestions: [],
                id: "BJlIYhLplZ",
                value: "/"
            }, {
                type: "EXECUTE_SCRIPT",
                selector: "form > :nth-child(1) input",
                nodeName: "Unknown",
                description: "Turn the background red baby!",
                warnings: [],
                suggestions: [],
                id: "H1lsyJBfeb",
                script: 'document.querySelector("body").style.background = "red";'
            }, {
                type: "PAUSE",
                selector: "Unknown",
                nodeName: "Unknown",
                description: "Wait a bit...",
                warnings: [],
                suggestions: [],
                id: "S17y7SMg-",
                value: "1000"
            }, {
                type: "EXECUTE_SCRIPT",
                selector: "Unknown",
                nodeName: "Unknown",
                description: "Turn the background green baby!",
                warnings: [],
                suggestions: [],
                id: "HJ--mBzlW",
                script: 'document.querySelector("body").style.background = "green";'
            }, {
                type: "PAUSE",
                selector: "Unknown",
                nodeName: "Unknown",
                description: "Wait a bit...",
                warnings: [],
                suggestions: [],
                id: "S121QBfgW",
                value: "1000"
            }, {
                type: "EXECUTE_SCRIPT",
                selector: "Unknown",
                nodeName: "Unknown",
                description: "Turn the background blue baby!",
                warnings: [],
                suggestions: [],
                id: "HJ6bmSzeZ",
                script: 'document.querySelector("body").style.background = "blue";'
            }, {
                type: "PAUSE",
                selector: "Unknown",
                nodeName: "Unknown",
                description: "Wait a bit...",
                warnings: [],
                suggestions: [],
                id: "S1akQSzxb",
                value: "1000"
            }, {
                type: "EXECUTE_SCRIPT",
                selector: "Unknown",
                nodeName: "Unknown",
                description: "Turn the background yellow baby!",
                warnings: [],
                suggestions: [],
                id: "r1jGmHGe-",
                script: 'document.querySelector("body").style.background = "yellow";'
            }],
            name: "Fun with colors",
            id: "ryAGYEMgW"
        }];
        return t.forEach(function (t) {
            t.id = (0, g.generate)(), t.user = e.id, w.push(t.id)
        }), t
    }

    function p() {
        return {
            module: "Tests",
            topLevel: !0,
            showGen: !0,
            children: [{
                collapsed: !1,
                module: "Your Project",
                id: "HyN3D2bQb",
                children: [{
                    collapsed: !1,
                    module: "staging",
                    id: "rJ22Dn-X-",
                    children: []
                }, {
                    collapsed: !1,
                    module: "production",
                    id: "SkmTv2bQW"
                }]
            }, {
                collapsed: !1,
                module: "Sample Tests",
                id: "HJOL5YgmW",
                children: [{
                    module: "Unnamed test",
                    testId: w[0],
                    leaf: !0,
                    id: "SJubi1uCx",
                    type: "test"
                }, {
                    module: "Unnamed test",
                    testId: w[1],
                    leaf: !0,
                    id: "HyWykrzxb",
                    type: "test"
                }, {
                    module: "SnapTEST",
                    testId: w[2],
                    leaf: !0,
                    id: "Bk0WSeJm0g",
                    type: "test",
                    collapsed: !1
                }, {
                    module: "TestCafe's sample",
                    testId: w[3],
                    leaf: !0,
                    id: "HJ4ojKe7W",
                    type: "test"
                }, {
                    module: "Bootswatch basic form inputs",
                    testId: w[4],
                    leaf: !0,
                    id: "HyZxWHgkXAl",
                    type: "test"
                }]
            }],
            collapsed: !1
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var f = n(139),
        _ = r(f);
    t.registerProcess = o, t.loginProcess = i, t.forgotProcess = a, t.resetProcess = s, t.setUser = u, t.setTests = l, t.setComponents = c;
    var h, v = n(7),
        m = r(v),
        y = n(237),
        g = n(32),
        w = []
}, function (e, t, n) {
    "use strict";

    function r(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function o(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function i(e, t) {
        function n(e) {
            var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [],
                r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null,
                c = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : null;
            return e.forEach(function (e) {
                if (e.type === f.COMPONENT) {
                    var p = (0, d["default"])(t, {
                        id: e.componentId
                    });
                    p && n(p.actions, t, p, e)
                } else r && e.value && (c && c.variables.forEach(function (t, n) {
                    var o = (0, d["default"])(r.variables, {
                        id: t.id
                    });
                    e.value = e.value.replace("${" + o.name + "}", t.value)
                }), r.variables.forEach(function (t) {
                    e.value = e.value.replace("${" + t.name + "}", t.defaultValue)
                })), (0, l["default"])(e.value) && (e.value = e.value.replace("${random}", parseInt(1e5 * Math.random())), e.value = e.value.replace("${random1}", i), e.value = e.value.replace("${random2}", a), e.value = e.value.replace("${random3}", u)), o.push((0, s["default"])({}, e))
            })
        }
        var r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null,
            o = (arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : null, []),
            i = parseInt(1e7 * Math.random()),
            a = parseInt(1e7 * Math.random()),
            u = parseInt(1e7 * Math.random());
        return n(e, t, r), o
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var a = n(55),
        s = o(a),
        u = n(310),
        l = o(u),
        c = n(93),
        d = o(c);
    t.flattenActionsForPlayback = i;
    var p = n(28),
        f = r(p)
}, , , , function (e, t, n) {
    "use strict";

    function r(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function o(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function i(e, t) {
        var n = "";
        return e.forEach(function (r, o) {
            var i = r.selector,
                s = r.description || p(r);
            if (r.type === y.COMPONENT) {
                var c = (0, v["default"])(t, {
                    id: r.componentId
                }),
                    f = "";
                if (!c) return;
                f = l(r, c), n += "\n      .components." + w.camelback(c.name) + "(" + d(f) + ")"
            }
            r.type !== y.POPSTATE && r.type !== y.BACK || (n += "\n      .back(`" + s + "`)"), r.type === y.FORWARD && (n += "\n      .forward(`" + s + "`)"), r.type === y.REFRESH && (n += "\n      .refresh(`" + s + "`)"), r.type === y.SCREENSHOT && (n += "\n      .saveScreenshot(`screenshots/" + r.value + "`, `" + s + "`)"), r.type === y.MOUSEOVER && (n += '\n      .moveToElement("' + r.selector + '", 1, 1, `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.FOCUS && (n += '\n      .focusOnEl("' + r.selector + '", `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.BLUR && (n += '\n      .blurOffEl("' + r.selector + '", `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.SUBMIT && (n += '\n      .formSubmit("' + r.selector + '", `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.EXECUTE_SCRIPT && (n += '\n      .executeScript("' + s + '", `' + r.script + "`)"), r.type === y.SCROLL_WINDOW && (n += "\n      .scrollWindow(" + r.x + ", " + r.y + ", `" + s + "`)"), r.type === y.SCROLL_ELEMENT && (n += '\n      .scrollElement("' + r.selector + '", ' + r.x + ", " + r.y + ", `" + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.SCROLL_WINDOW_ELEMENT && (n += '\n      .scrollWindowToElement("' + r.selector + '", `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.PAGELOAD && (0 === o || y.PAGELOAD && 1 === o) ? n += '\n      .url(baseUrl, "' + a(e).path + '", ' + r.width + ", " + r.height + ", `" + s + "`)" : r.type === y.PAGELOAD && (n += '\n      .initNewPage("' + r.value + '", `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.FULL_PAGELOAD && (0 === o || y.PAGELOAD && 1 === o) ? n += '\n      .url(baseUrl, "' + a(e).path + '", ' + r.width + ", " + r.height + ", `" + s + "`)" : r.type === y.FULL_PAGELOAD && (n += '\n      .url(baseUrl, "' + r.value + '", ' + r.width + ", " + r.height + ", `" + s + "`, `" + s + "`)"), r.type !== y.CHANGE_WINDOW && r.type !== y.CHANGE_WINDOW_AUTO || (n += "\n      .switchToWindow(" + r.value + ", `" + s + "`)"), r.type === y.MOUSEDOWN && (n += '\n      .click("' + i + '", `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.PAUSE && (n += "\n      .pause(" + r.value + ")"), r.type === y.EL_PRESENT_ASSERT && (n += '\n      .elementPresent("' + i + '", `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.EL_NOT_PRESENT_ASSERT && (n += '\n      .elementNotPresent("' + i + '", `' + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.KEYDOWN && (n += '\n      .sendKeys("' + i + '", ' + u(r.keyValue) + ", `" + s + "`)"), r.type === y.TEXT_ASSERT && (n += '\n      .elTextIs("' + i + '", `' + r.value + "`, `" + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.TEXT_REGEX_ASSERT && (n += '\n      .elTextRegex("' + i + '", `' + r.value + "`, `" + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.VALUE_ASSERT && (n += '\n      .inputValueAssert("' + i + '", `' + r.value + "`, `" + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")"), r.type === y.INPUT && (n += '\n      .changeInput("' + i + '", `' + r.value + "`, `" + s + "`" + (r.timeout ? ", " + r.timeout : "") + ")")
        }), n
    }

    function a(e) {
        var t, n;
        return e.forEach(function (e, r) {
            if (!(0 !== r && 1 != r || e.type !== y.FULL_PAGELOAD && e.type !== y.PAGELOAD)) {
                var o = new T(e.value);
                t = o.origin, n = e.value.replace(o.origin, "")
            }
        }), {
                launchUrl: t,
                path: n
            }
    }

    function s(e, t, n) {
        var r = i(e, n),
            o = "",
            s = a(e);
        n.forEach(function (e) {
            var t = i(e.actions, n),
                r = c(e);
            o += "\n    \n    browser.components." + w.camelback(e.name) + " = function(" + d(r) + ") {\n      return browser" + t + "\n    };"
        });
        var u = 'const TIMEOUT = 5000; \n  \n  module.exports = {\n      "' + t.replace(/['"]/g, "\\'") + '" : function (browser) {\n        \n        bindHelpers(browser);\n        bindComponents(browser);\n       \n        var baseUrl = browser.launchUrl || "' + s.launchUrl + '"\n\n        browser' + r + "\n          .end();\n      }\n    };\n\n/*\n * Components \n*/\n\nfunction bindComponents(browser) {\n  \n  browser.components = {};\n  " + o + '\n  \n}\n\n/*\n * Auto-Generated helper code \n*/\n\nconst random = "" + parseInt(Math.random() * 1000000);\nconst random1 = "" + parseInt(Math.random() * 1000000); \nconst random2 = "" + parseInt(Math.random() * 1000000);\nconst random3 = "" + parseInt(Math.random() * 1000000);\n\nfunction bindHelpers(browser) {\n\n  var oldUrl = browser.url;\n  var oldBack = browser.back;\n\n  browser.url = function(baseUrl, pathname, width, height, description) {\n    oldUrl(baseUrl + pathname);\n    browser.resizeWindow(width, height);\n    return this;\n  };\n\n  browser.back = function(description) {\n    browser.perform(() => comment(description));\n    browser.pause(5);\n    oldBack();\n    return this;\n  };\n\n  browser.initNewPage = function(pathname, description, timeout) {\n\n    browser.perform(() => comment(description + " (navigating to " + pathname + ")"));\n\n    var attempts = parseInt((timeout || TIMEOUT) / 1000);\n    var currentAttempt = 0;\n\n    function checkForPageLoadWithPathname(pathname) {\n      browser.execute(function() {\n        return {\n          pathname: window.location.pathname,\n          readyState: document.readyState\n        };\n      }, [], function(result) {\n        if (result.value.pathname !== pathname || result.value.readyState !== "complete") {\n          if (currentAttempt < attempts) {\n            currentAttempt++;\n            browser.pause(1000);\n            checkForPageLoadWithPathname(pathname);\n          } else {\n            this.assert.ok(false, description)\n          }\n        }\n      });\n    }\n\n    checkForPageLoadWithPathname(pathname);\n\n    browser.execute(function() {\n      window.alert = function() {};\n      window.confirm = function() {\n        return true;\n      };\n    }, []);\n\n    return this;\n\n  };\n\n  browser.executeScript = function(description, script) {\n    browser.perform(() => comment(description));\n    browser.execute(function(script) {\n      eval(script);\n    }, [script], function(result) {});\n\n    return this;\n  };\n\n  browser.switchToWindow = function(windowIndex, description) {\n    browser.perform(() => comment(description));\n\n    browser.windowHandles(function(result) {\n      this.switchWindow(result.value[windowIndex]);\n    });\n\n    return this;\n  };\n\n  browser.scrollWindow = function(x, y, description) {\n    browser.perform(() => comment(description));\n    browser.execute(function(x, y) {\n      window.scrollTo(x, y);\n    }, [x, y], function(result) {});\n\n    return this;\n  };\n\n  browser.scrollElement = function(selector, x, y, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n    browser.execute(function(selector, x, y) {\n      (function(el, x, y) {\n        el.scrollLeft = x;\n        el.scrollTop = y;\n      })(document.querySelector(selector), x, y);\n    }, [selector, x, y], function(result) {});\n\n    return this;\n  };\n\n  browser.scrollWindowToElement = function(selector, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n    browser.execute(function(selector, value) {\n      (function(el) {\n        if (el) {\n          var elsScrollY = el.getBoundingClientRect().top + window.scrollY - el.offsetHeight;\n          window.scrollTo(0, elsScrollY);\n        }\n      })(document.querySelector(selector), value);\n    }, [selector]);\n\n    return this;\n  };\n\n  browser.click = function(selector, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description)}, timeout);\n    browser.execute(function(selector) {\n\n      (function(element) {\n\n        function triggerMouseEvent(node, eventType) {\n          var clickEvent = document.createEvent(\'MouseEvents\');\n          clickEvent.initEvent(eventType, true, true);\n          node.dispatchEvent(clickEvent);\n        }\n\n        triggerMouseEvent(element, "mouseover");\n        triggerMouseEvent(element, "mousedown");\n        triggerMouseEvent(element, "mouseup");\n        triggerMouseEvent(element, "click");\n\n      })(document.querySelector(selector));\n\n    }, [selector]);\n\n    return this;\n\n  };\n\n  browser.changeInput = function(selector, value, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n    browser.execute(function(selector, value) {\n\n      (function(el) {\n        function triggerKeyEvent(node, eventType) {\n          var keydownEvent = document.createEvent( \'KeyboardEvent\' );\n          keydownEvent.initEvent( eventType, true, false, null, 0, false, 0, false, 66, 0 );\n          node.dispatchEvent( keydownEvent );\n        }\n\n        if (el) {\n          triggerKeyEvent(el, "keydown");\n          el.focus();\n          el.value = value;\n          el.dispatchEvent(new Event(\'change\', {bubbles: true}));\n          el.dispatchEvent(new Event(\'input\', {bubbles: true}));\n          triggerKeyEvent(el, "keyup");\n          triggerKeyEvent(el, "keypress");\n        }\n      })(document.querySelector(selector), value);\n\n    }, [selector, value], function(result) {});\n\n    return this;\n  };\n\n  browser.inputValueAssert = function(selector, value, description, timeout) {\n\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n\n    var attempts = parseInt((timeout || TIMEOUT) / 1000);\n    var currentAttempt = 0;\n\n    function checkforValue(selector, value) {\n      browser.execute(function(selector) {\n        var el = document.querySelector(selector);\n        if (el) {\n          if (el.type === \'checkbox\' || el.type === \'radio\') {\n            return el.checked ? "true" : "false";\n          } else {\n            return el.value;\n          }\n        } else return null;\n      }, [selector], function(result) {\n        if (result.value !== value) {\n          if (currentAttempt < attempts) {\n            currentAttempt++;\n            browser.pause(1000);\n            checkforValue(selector, value);\n          } else {\n            this.assert.ok(false, description);\n          }\n        }\n      });\n    }\n\n    checkforValue(selector, value);\n\n    return this;\n\n  };\n\n  browser.elementPresent = function(selector, description, onFail, timeout) {\n\n    browser.perform(() => comment(description));\n\n    var attempts = parseInt((timeout || TIMEOUT) / 1000);\n    var currentAttempt = 0;\n\n    function checkforEl(selector) {\n      browser.execute(function(selector) {\n        return !!document.querySelector(selector);\n      }, [selector], function(result) {\n        if (!result.value && currentAttempt < attempts) {\n          currentAttempt++;\n          browser.pause(1000);\n          checkforEl(selector);\n        } else if (!result.value) {\n          if (typeof onFail === "function") {\n            onFail();\n          } else {\n            this.assert.ok(false, description);\n          }\n        }\n      });\n    }\n\n    checkforEl(selector);\n\n    return this;\n\n  };\n\n  browser.elementNotPresent = function(selector, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.waitForElementNotPresent(selector, timeout || TIMEOUT);\n    return this;\n  };\n\n  browser.focusOnEl = function(selector, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n\n    browser.execute(function(selector) {\n      (function(el) {\n        var event = new FocusEvent(\'focus\');\n        el.dispatchEvent(event);\n      })(document.querySelector(selector));\n    }, [selector]);\n\n    return this;\n  };\n\n  browser.formSubmit = function(selector, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n    browser.execute(function(selector) {\n\n      (function(el) {\n        var event = new Event(\'submit\');\n        el.dispatchEvent(event);\n      })(document.querySelector(selector));\n\n    }, [selector]);\n\n    return this;\n  };\n\n  browser.blurOffEl = function(selector, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n    browser.execute(function(selector) {\n\n      (function(el) {\n        var event = new FocusEvent(\'blur\');\n        el.dispatchEvent(event);\n      })(document.querySelector(selector));\n\n    }, [selector]);\n\n    return this;\n  };\n\n  browser.getElText = function(selector, onSuccess, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n\n    var attempts = parseInt(TIMEOUT / 1000);\n    var currentAttempt = 0;\n\n    function checkforText(selector) {\n      browser.execute(function(selector) {\n        return (function(element) {\n          if (!element) return null;\n          var text = "";\n          for (var i = 0; i < element.childNodes.length; ++i)\n            if (element.childNodes[i].nodeType === 3)\n              if (element.childNodes[i].textContent)\n                text += element.childNodes[i].textContent;\n          text = text.replace(/(\\r\\n|\\n|\\r)/gm, "");\n          return text.trim();\n        })(document.querySelector(selector));\n      }, [selector], function(result) {\n        if (result.value === "" && currentAttempt < attempts) {\n          if (currentAttempt < attempts) {\n            currentAttempt++;\n            browser.pause(1000);\n            checkforText(selector);\n          } else {\n            this.assert.ok(false, description);\n          }\n        } else {\n          if (typeof onSuccess === "function") onSuccess.call(browser, result.value);\n        }\n      });\n    }\n\n    checkforText(selector);\n\n    return this;\n\n  };\n\n  browser.elTextRegex = function(selector, regex, description, timeout) {\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n    return browser.getElText(selector, function(elsText) {\n      var assertRegEx = new RegExp(regex);\n      if (!assertRegEx.test(elsText)) {\n        this.assert.ok(false, description);\n      }\n    })\n  };\n\n  browser.elTextIs = function(selector, assertText, description, timeout) {\n\n    browser.perform(() => comment(description));\n    browser.elementPresent(selector, null, () => { browser.assert.ok(false, description) }, timeout);\n\n    var attempts = parseInt((timeout || TIMEOUT) / 1000);\n    var currentAttempt = 0;\n\n    function checkforText(selector, assertText) {\n      browser.getElText(selector, function(elsText) {\n        if (elsText !== assertText) {\n          if (currentAttempt < attempts) {\n            currentAttempt++;\n            browser.pause(1000);\n            checkforText(selector, assertText);\n          } else {\n            this.assert.ok(false, description);\n          }\n        }\n      });\n    }\n\n    checkforText(selector, assertText);\n\n    return this;\n\n  };\n\n  function comment(description) { if (description) { console.log(`${description}`); } }\n\n}\n\n';
        return g(u, {
            indent_size: 2
        })
    }

    function u(e) {
        switch (e) {
            case "Enter":
                return "browser.Keys.ENTER";
            case "Escape":
                return "browser.Keys.ESCAPE";
            default:
                return "unknown"
        }
    }

    function l(e, t) {
        var n = [];
        return t.variables.forEach(function (t) {
            var r = (0, v["default"])(e.variables, {
                id: t.id
            });
            r ? n.push('"' + r.value + '"') : n.push('"' + t.defaultValue + '"')
        }), n
    }

    function c(e) {
        var t = [];
        return e.variables.forEach(function (e) {
            t.push(e.name)
        }), t
    }

    function d(e) {
        var t = "";
        return e.forEach(function (n, r) {
            t += r !== e.length - 1 ? n + ", " : n
        }), t
    }

    function p(e) {
        var t, n = (0, v["default"])(b, {
            value: e.type
        });
        return t = n ? n.name : e.type, (0, _["default"])(e.value) && (t += ' "' + e.value + '"'), t
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var f = n(310),
        _ = o(f),
        h = n(93),
        v = o(h);
    t.generateNWCode = s;
    var m = n(28),
        y = r(m),
        g = n(326).js_beautify,
        w = n(329),
        T = n(238),
        b = [{
            name: "...",
            value: "",
            isDefault: !0
        }, {
            name: "Click element",
            value: y.MOUSEDOWN
        }, {
            name: "Press key...",
            value: y.KEYDOWN
        }, {
            name: "Change input to...",
            value: y.INPUT
        }, {
            name: "Form submit",
            value: y.SUBMIT
        }, {
            name: "Mouse over",
            value: y.MOUSEOVER
        }, {
            name: "Focus",
            value: y.FOCUS
        }, {
            name: "Blur",
            value: y.BLUR
        }, {
            name: "Pause",
            value: y.PAUSE
        }, {
            name: "Execute script",
            value: y.EXECUTE_SCRIPT
        }, {
            name: "Drag and drop",
            value: "",
            disabled: !0
        }, {
            name: "Scroll window to...",
            value: y.SCROLL_WINDOW
        }, {
            name: "Scroll window to el",
            value: y.SCROLL_WINDOW_ELEMENT
        }, {
            name: "Scroll element to...",
            value: y.SCROLL_ELEMENT
        }, {
            name: "El text is...",
            value: y.TEXT_ASSERT
        }, {
            name: "El text matches regex...",
            value: y.TEXT_REGEX_ASSERT
        }, {
            name: "Input value is...",
            value: y.VALUE_ASSERT
        }, {
            name: "El is present",
            value: y.EL_PRESENT_ASSERT
        }, {
            name: "El is not present",
            value: y.EL_NOT_PRESENT_ASSERT
        }, {
            name: "Load page...",
            value: y.FULL_PAGELOAD
        }, {
            name: "Back",
            value: y.BACK
        }, {
            name: "Forward",
            value: y.FORWARD
        }, {
            name: "Refresh",
            value: y.REFRESH
        }, {
            name: "DOM ready & path is...",
            value: y.PAGELOAD
        }, {
            name: "Focus on window",
            value: y.CHANGE_WINDOW
        }, {
            name: "Component",
            value: y.COMPONENT
        }, {
            name: "url change indicator",
            value: y.URL_CHANGE_INDICATOR
        }, {
            name: "Take screenshot",
            value: y.SCREENSHOT
        }, {
            name: "SEO meta scan",
            value: y.META_SCAN
        }]
}, , , , function (e, t) {
    "use strict";

    function n(e, t) {
        var r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : new Map,
            o = r.get(e);
        if (o) return o;
        if (Array.isArray(e)) {
            var i = [];
            r.set(e, i);
            for (var a = 0; a < e.length; a++) i[a] = n(e[a], t, r);
            return i
        }
        if (e instanceof Date) return new Date(e.valueOf());
        if (!(e instanceof Object)) return e;
        var s = {};
        r.set(e, s);
        for (var u = Object.keys(e), l = 0; l < u.length; l++) {
            var c = t ? t(u[l]) : u[l];
            s[c] = n(e[u[l]], t, r)
        }
        return s
    }

    function r(e) {
        return function (t) {
            return n(t, e)
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t["default"] = n, t.formatKeys = r, n.formatKeys = r
}, function (e, t, n) {
    function r(e, t, n) {
        var r = function (t, n) {
            return e.js_beautify(t, n)
        };
        return r.js = e.js_beautify, r.css = t.css_beautify, r.html = n.html_beautify, r.js_beautify = e.js_beautify, r.css_beautify = t.css_beautify, r.html_beautify = n.html_beautify, r
    }
    var o, i;
    o = [n(300), n(299), n(327)], i = function (e, t, n) {
        return r(e, t, n)
    }.apply(t, o), !(void 0 !== i && (e.exports = i))
}, function (e, t, n) {
    var r, o;
    ! function () {
        function i(e) {
            return e.replace(/^\s+/g, "")
        }

        function a(e) {
            return e.replace(/\s+$/g, "")
        }

        function s(e, t) {
            var n, r = {};
            for (n in e) n !== t && (r[n] = e[n]);
            if (t in e)
                for (n in e[t]) r[n] = e[t][n];
            return r
        }

        function u(e, t, n, r) {
            function o() {
                function e(e) {
                    var t = "",
                        n = function (n) {
                            var r = t + n.toLowerCase();
                            t = r.length <= e.length ? r : r.substr(r.length - e.length, e.length)
                        },
                        r = function () {
                            return t.indexOf(e) === -1
                        };
                    return {
                        add: n,
                        doesNotMatch: r
                    }
                }
                return this.pos = 0, this.token = "", this.current_mode = "CONTENT", this.tags = {
                    parent: "parent1",
                    parentcount: 1,
                    parent1: ""
                }, this.tag_type = "", this.token_text = this.last_token = this.last_text = this.token_type = "", this.newlines = 0, this.indent_content = d, this.indent_body_inner_html = p, this.indent_head_inner_html = f, this.Utils = {
                    whitespace: "\n\r\t ".split(""),
                    single_token: ["area", "base", "br", "col", "embed", "hr", "img", "input", "keygen", "link", "menuitem", "meta", "param", "source", "track", "wbr", "!doctype", "?xml", "?php", "basefont", "isindex"],
                    extra_liners: R,
                    in_array: function (e, t) {
                        for (var n = 0; n < t.length; n++)
                            if (e === t[n]) return !0;
                        return !1
                    }
                }, this.is_whitespace = function (e) {
                    for (var t = 0; t < e.length; t++)
                        if (!this.Utils.in_array(e.charAt(t), this.Utils.whitespace)) return !1;
                    return !0
                }, this.traverse_whitespace = function () {
                    var e = "";
                    if (e = this.input.charAt(this.pos), this.Utils.in_array(e, this.Utils.whitespace)) {
                        for (this.newlines = 0; this.Utils.in_array(e, this.Utils.whitespace);) w && "\n" === e && this.newlines <= T && (this.newlines += 1), this.pos++ , e = this.input.charAt(this.pos);
                        return !0
                    }
                    return !1
                }, this.space_or_wrap = function (e) {
                    return this.line_char_count >= this.wrap_line_length ? (this.print_newline(!1, e), this.print_indentation(e), !0) : (this.line_char_count++ , e.push(" "), !1)
                }, this.get_content = function () {
                    for (var e = "", t = [], n = 0;
                        "<" !== this.input.charAt(this.pos) || 2 === n;) {
                        if (this.pos >= this.input.length) return t.length ? t.join("") : ["", "TK_EOF"];
                        if (n < 2 && this.traverse_whitespace()) this.space_or_wrap(t);
                        else {
                            if (e = this.input.charAt(this.pos), b) {
                                if ("{" === e ? n += 1 : n < 2 && (n = 0), "}" === e && n > 0 && 0 === n--) break;
                                var r = this.input.substr(this.pos, 3);
                                if ("{{#" === r || "{{/" === r) break;
                                if ("{{!" === r) return [this.get_tag(), "TK_TAG_HANDLEBARS_COMMENT"];
                                if ("{{" === this.input.substr(this.pos, 2) && "{{else}}" === this.get_tag(!0)) break
                            }
                            this.pos++ , this.line_char_count++ , t.push(e)
                        }
                    }
                    return t.length ? t.join("") : ""
                }, this.get_contents_to = function (e) {
                    if (this.pos === this.input.length) return ["", "TK_EOF"];
                    var t = "",
                        n = new RegExp("</" + e + "\\s*>", "igm");
                    n.lastIndex = this.pos;
                    var r = n.exec(this.input),
                        o = r ? r.index : this.input.length;
                    return this.pos < o && (t = this.input.substring(this.pos, o), this.pos = o), t
                }, this.record_tag = function (e) {
                    this.tags[e + "count"] ? (this.tags[e + "count"]++ , this.tags[e + this.tags[e + "count"]] = this.indent_level) : (this.tags[e + "count"] = 1, this.tags[e + this.tags[e + "count"]] = this.indent_level), this.tags[e + this.tags[e + "count"] + "parent"] = this.tags.parent, this.tags.parent = e + this.tags[e + "count"]
                }, this.retrieve_tag = function (e) {
                    if (this.tags[e + "count"]) {
                        for (var t = this.tags.parent; t && e + this.tags[e + "count"] !== t;) t = this.tags[t + "parent"];
                        t && (this.indent_level = this.tags[e + this.tags[e + "count"]], this.tags.parent = this.tags[t + "parent"]), delete this.tags[e + this.tags[e + "count"] + "parent"], delete this.tags[e + this.tags[e + "count"]], 1 === this.tags[e + "count"] ? delete this.tags[e + "count"] : this.tags[e + "count"]--;
                    }
                }, this.indent_to_tag = function (e) {
                    if (this.tags[e + "count"]) {
                        for (var t = this.tags.parent; t && e + this.tags[e + "count"] !== t;) t = this.tags[t + "parent"];
                        t && (this.indent_level = this.tags[e + this.tags[e + "count"]])
                    }
                }, this.get_tag = function (e) {
                    var t, n, r, o, i = "",
                        a = [],
                        s = "",
                        u = !1,
                        l = !0,
                        c = !1,
                        d = this.pos,
                        p = this.line_char_count,
                        f = !1;
                    e = void 0 !== e && e;
                    do {
                        if (this.pos >= this.input.length) return e && (this.pos = d, this.line_char_count = p), a.length ? a.join("") : ["", "TK_EOF"];
                        if (i = this.input.charAt(this.pos), this.pos++ , this.Utils.in_array(i, this.Utils.whitespace)) u = !0;
                        else {
                            if ("'" !== i && '"' !== i || (i += this.get_unformatted(i), u = !0), "=" === i && (u = !1), o = this.input.substr(this.pos - 1), !A || !c || f || ">" !== i && "/" !== i || o.match(/^\/?\s*>/) && (u = !1, f = !0, this.print_newline(!1, a), this.print_indentation(a)), a.length && "=" !== a[a.length - 1] && ">" !== i && u) {
                                var _ = this.space_or_wrap(a),
                                    h = _ && "/" !== i && !S;
                                if (u = !1, S && "/" !== i) {
                                    var v = !1;
                                    if (A && l) {
                                        var m = null !== o.match(/^\S*(="([^"]|\\")*")?\s*\/?\s*>/);
                                        v = !m
                                    }
                                    l && !v || (this.print_newline(!1, a), this.print_indentation(a), h = !0)
                                }
                                if (h) {
                                    c = !0;
                                    var w = x;
                                    k && (w = a.indexOf(" ") + 1);
                                    for (var T = 0; T < w; T++) a.push(" ")
                                }
                                if (l)
                                    for (var E = 0; E < a.length; E++)
                                        if (" " === a[E]) {
                                            l = !1;
                                            break
                                        }
                            }
                            if (b && "<" === r && i + this.input.charAt(this.pos) === "{{" && (i += this.get_unformatted("}}"), a.length && " " !== a[a.length - 1] && "<" !== a[a.length - 1] && (i = " " + i), u = !0), "<" !== i || r || (t = this.pos - 1, r = "<"), b && !r && a.length >= 2 && "{" === a[a.length - 1] && "{" === a[a.length - 2] && (t = "#" === i || "/" === i || "!" === i ? this.pos - 3 : this.pos - 2, r = "{"), this.line_char_count++ , a.push(i), a[1] && ("!" === a[1] || "?" === a[1] || "%" === a[1])) {
                                a = [this.get_comment(t)];
                                break
                            }
                            if (b && a[1] && "{" === a[1] && a[2] && "!" === a[2]) {
                                a = [this.get_comment(t)];
                                break
                            }
                            if (b && "{" === r && a.length > 2 && "}" === a[a.length - 2] && "}" === a[a.length - 1]) break
                        }
                    } while (">" !== i);
                    var O, R, N = a.join("");
                    O = N.indexOf(" ") !== -1 ? N.indexOf(" ") : N.indexOf("\n") !== -1 ? N.indexOf("\n") : "{" === N.charAt(0) ? N.indexOf("}") : N.indexOf(">"), R = "<" !== N.charAt(0) && b ? "#" === N.charAt(2) ? 3 : 2 : 1;
                    var C = N.substring(R, O).toLowerCase();
                    return "/" === N.charAt(N.length - 2) || this.Utils.in_array(C, this.Utils.single_token) ? e || (this.tag_type = "SINGLE") : b && "{" === N.charAt(0) && "else" === C ? e || (this.indent_to_tag("if"), this.tag_type = "HANDLEBARS_ELSE", this.indent_content = !0, this.traverse_whitespace()) : this.is_unformatted(C, y) || this.is_unformatted(C, g) ? (s = this.get_unformatted("</" + C + ">", N), a.push(s), n = this.pos - 1, this.tag_type = "SINGLE") : "script" === C && (N.search("type") === -1 || N.search("type") > -1 && N.search(/\b(text|application|dojo)\/(x-)?(javascript|ecmascript|jscript|livescript|(ld\+)?json|method|aspect)/) > -1) ? e || (this.record_tag(C), this.tag_type = "SCRIPT") : "style" === C && (N.search("type") === -1 || N.search("type") > -1 && N.search("text/css") > -1) ? e || (this.record_tag(C), this.tag_type = "STYLE") : "!" === C.charAt(0) ? e || (this.tag_type = "SINGLE", this.traverse_whitespace()) : e || ("/" === C.charAt(0) ? (this.retrieve_tag(C.substring(1)), this.tag_type = "END") : (this.record_tag(C), "html" !== C.toLowerCase() && (this.indent_content = !0), this.tag_type = "START"), this.traverse_whitespace() && this.space_or_wrap(a), this.Utils.in_array(C, this.Utils.extra_liners) && (this.print_newline(!1, this.output), this.output.length && "\n" !== this.output[this.output.length - 2] && this.print_newline(!0, this.output))), e && (this.pos = d, this.line_char_count = p), a.join("")
                }, this.get_comment = function (e) {
                    var t = "",
                        n = ">",
                        r = !1;
                    this.pos = e;
                    var o = this.input.charAt(this.pos);
                    for (this.pos++; this.pos <= this.input.length && (t += o, t.charAt(t.length - 1) !== n.charAt(n.length - 1) || t.indexOf(n) === -1);) !r && t.length < 10 && (0 === t.indexOf("<![if") ? (n = "<![endif]>", r = !0) : 0 === t.indexOf("<![cdata[") ? (n = "]]>", r = !0) : 0 === t.indexOf("<![") ? (n = "]>", r = !0) : 0 === t.indexOf("<!--") ? (n = "-->", r = !0) : 0 === t.indexOf("{{!--") ? (n = "--}}", r = !0) : 0 === t.indexOf("{{!") ? 5 === t.length && t.indexOf("{{!--") === -1 && (n = "}}", r = !0) : 0 === t.indexOf("<?") ? (n = "?>", r = !0) : 0 === t.indexOf("<%") && (n = "%>", r = !0)), o = this.input.charAt(this.pos), this.pos++;
                    return t
                }, this.get_unformatted = function (t, n) {
                    if (n && n.toLowerCase().indexOf(t) !== -1) return "";
                    var r = "",
                        o = "",
                        i = !0,
                        a = e(t);
                    do {
                        if (this.pos >= this.input.length) return o;
                        if (r = this.input.charAt(this.pos), this.pos++ , this.Utils.in_array(r, this.Utils.whitespace)) {
                            if (!i) {
                                this.line_char_count--;
                                continue
                            }
                            if ("\n" === r || "\r" === r) {
                                o += "\n", this.line_char_count = 0;
                                continue
                            }
                        }
                        o += r, a.add(r), this.line_char_count++ , i = !0, b && "{" === r && o.length && "{" === o.charAt(o.length - 2) && (o += this.get_unformatted("}}"))
                    } while (a.doesNotMatch());
                    return o
                }, this.get_token = function () {
                    var e;
                    if ("TK_TAG_SCRIPT" === this.last_token || "TK_TAG_STYLE" === this.last_token) {
                        var t = this.last_token.substr(7);
                        return e = this.get_contents_to(t), "string" != typeof e ? e : [e, "TK_" + t]
                    }
                    if ("CONTENT" === this.current_mode) return e = this.get_content(), "string" != typeof e ? e : [e, "TK_CONTENT"];
                    if ("TAG" === this.current_mode) {
                        if (e = this.get_tag(), "string" != typeof e) return e;
                        var n = "TK_TAG_" + this.tag_type;
                        return [e, n]
                    }
                }, this.get_full_indent = function (e) {
                    return e = this.indent_level + e || 0, e < 1 ? "" : Array(e + 1).join(this.indent_string)
                }, this.is_unformatted = function (e, t) {
                    if (!this.Utils.in_array(e, t)) return !1;
                    if ("a" !== e.toLowerCase() || !this.Utils.in_array("a", t)) return !0;
                    var n = this.get_tag(!0),
                        r = (n || "").match(/^\s*<\s*\/?([a-z]*)\s*[^>]*>\s*$/);
                    return !(r && !this.Utils.in_array(r, t))
                }, this.printer = function (e, t, n, r, o) {
                    this.input = e || "", this.input = this.input.replace(/\r\n|[\r\u2028\u2029]/g, "\n"), this.output = [], this.indent_character = t, this.indent_string = "", this.indent_size = n, this.brace_style = o, this.indent_level = 0, this.wrap_line_length = r, this.line_char_count = 0;
                    for (var s = 0; s < this.indent_size; s++) this.indent_string += this.indent_character;
                    this.print_newline = function (e, t) {
                        this.line_char_count = 0, t && t.length && (e || "\n" !== t[t.length - 1]) && ("\n" !== t[t.length - 1] && (t[t.length - 1] = a(t[t.length - 1])), t.push("\n"))
                    }, this.print_indentation = function (e) {
                        for (var t = 0; t < this.indent_level; t++) e.push(this.indent_string), this.line_char_count += this.indent_string.length
                    }, this.print_token = function (e) {
                        this.is_whitespace(e) && !this.output.length || ((e || "" !== e) && this.output.length && "\n" === this.output[this.output.length - 1] && (this.print_indentation(this.output), e = i(e)), this.print_token_raw(e))
                    }, this.print_token_raw = function (e) {
                        this.newlines > 0 && (e = a(e)), e && "" !== e && (e.length > 1 && "\n" === e.charAt(e.length - 1) ? (this.output.push(e.slice(0, -1)), this.print_newline(!1, this.output)) : this.output.push(e));
                        for (var t = 0; t < this.newlines; t++) this.print_newline(t > 0, this.output);
                        this.newlines = 0
                    }, this.indent = function () {
                        this.indent_level++
                    }, this.unindent = function () {
                        this.indent_level > 0 && this.indent_level--
                    }
                }, this
            }
            var u, d, p, f, _, h, v, m, y, g, w, T, b, E, x, S, A, k, O, R, N;
            for (t = t || {}, t = s(t, "html"), void 0 !== t.wrap_line_length && 0 !== parseInt(t.wrap_line_length, 10) || void 0 === t.max_char || 0 === parseInt(t.max_char, 10) || (t.wrap_line_length = t.max_char), d = void 0 !== t.indent_inner_html && t.indent_inner_html, p = void 0 === t.indent_body_inner_html || t.indent_body_inner_html, f = void 0 === t.indent_head_inner_html || t.indent_head_inner_html, _ = void 0 === t.indent_size ? 4 : parseInt(t.indent_size, 10), h = void 0 === t.indent_char ? " " : t.indent_char, m = void 0 === t.brace_style ? "collapse" : t.brace_style, v = 0 === parseInt(t.wrap_line_length, 10) ? 32786 : parseInt(t.wrap_line_length || 250, 10), y = t.unformatted || ["a", "abbr", "area", "audio", "b", "bdi", "bdo", "br", "button", "canvas", "cite", "code", "data", "datalist", "del", "dfn", "em", "embed", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "map", "mark", "math", "meter", "noscript", "object", "output", "progress", "q", "ruby", "s", "samp", "select", "small", "span", "strong", "sub", "sup", "svg", "template", "textarea", "time", "u", "var", "video", "wbr", "text", "acronym", "address", "big", "dt", "ins", "strike", "tt"], g = t.content_unformatted || ["pre"], w = void 0 === t.preserve_newlines || t.preserve_newlines, T = w ? isNaN(parseInt(t.max_preserve_newlines, 10)) ? 32786 : parseInt(t.max_preserve_newlines, 10) : 0, b = void 0 !== t.indent_handlebars && t.indent_handlebars, E = void 0 === t.wrap_attributes ? "auto" : t.wrap_attributes, x = isNaN(parseInt(t.wrap_attributes_indent_size, 10)) ? _ : parseInt(t.wrap_attributes_indent_size, 10), S = "force" === E.substr(0, "force".length), A = "force-expand-multiline" === E, k = "force-aligned" === E, O = void 0 !== t.end_with_newline && t.end_with_newline, R = "object" == typeof t.extra_liners && t.extra_liners ? t.extra_liners.concat() : "string" == typeof t.extra_liners ? t.extra_liners.split(",") : "head,body,/html".split(","), N = t.eol ? t.eol : "auto", t.indent_with_tabs && (h = "\t", _ = 1), "auto" === N && (N = "\n", e && l.test(e || "") && (N = e.match(l)[0])), N = N.replace(/\\r/, "\r").replace(/\\n/, "\n"), e = e.replace(c, "\n"), u = new o, u.printer(e, h, _, v, m); ;) {
                var C = u.get_token();
                if (u.token_text = C[0], u.token_type = C[1], "TK_EOF" === u.token_type) break;
                switch (u.token_type) {
                    case "TK_TAG_START":
                        u.print_newline(!1, u.output), u.print_token(u.token_text), u.indent_content && (!u.indent_body_inner_html && u.token_text.match(/<body(?:.*)>/) || !u.indent_head_inner_html && u.token_text.match(/<head(?:.*)>/) || u.indent(), u.indent_content = !1), u.current_mode = "CONTENT";
                        break;
                    case "TK_TAG_STYLE":
                    case "TK_TAG_SCRIPT":
                        u.print_newline(!1, u.output), u.print_token(u.token_text), u.current_mode = "CONTENT";
                        break;
                    case "TK_TAG_END":
                        if ("TK_CONTENT" === u.last_token && "" === u.last_text) {
                            var I = (u.token_text.match(/\w+/) || [])[0],
                                P = null;
                            u.output.length && (P = u.output[u.output.length - 1].match(/(?:<|{{#)\s*(\w+)/)), (null === P || P[1] !== I && !u.Utils.in_array(P[1], y)) && u.print_newline(!1, u.output)
                        }
                        u.print_token(u.token_text), u.current_mode = "CONTENT";
                        break;
                    case "TK_TAG_SINGLE":
                        var L = u.token_text.match(/^\s*<([a-z-]+)/i);
                        L && u.Utils.in_array(L[1], y) || u.print_newline(!1, u.output), u.print_token(u.token_text), u.current_mode = "CONTENT";
                        break;
                    case "TK_TAG_HANDLEBARS_ELSE":
                        for (var U = !1, D = u.output.length - 1; D >= 0 && "\n" !== u.output[D]; D--)
                            if (u.output[D].match(/{{#if/)) {
                                U = !0;
                                break
                            }
                        U || u.print_newline(!1, u.output), u.print_token(u.token_text), u.indent_content && (u.indent(), u.indent_content = !1), u.current_mode = "CONTENT";
                        break;
                    case "TK_TAG_HANDLEBARS_COMMENT":
                        u.print_token(u.token_text), u.current_mode = "TAG";
                        break;
                    case "TK_CONTENT":
                        u.print_token(u.token_text), u.current_mode = "TAG";
                        break;
                    case "TK_STYLE":
                    case "TK_SCRIPT":
                        if ("" !== u.token_text) {
                            u.print_newline(!1, u.output);
                            var K, M = u.token_text,
                                j = 1;
                            "TK_SCRIPT" === u.token_type ? K = "function" == typeof n && n : "TK_STYLE" === u.token_type && (K = "function" == typeof r && r), "keep" === t.indent_scripts ? j = 0 : "separate" === t.indent_scripts && (j = -u.indent_level);
                            var W = u.get_full_indent(j);
                            if (K) {
                                var F = function () {
                                    this.eol = "\n"
                                };
                                F.prototype = t;
                                var B = new F;
                                M = K(M.replace(/^\s*/, W), B)
                            } else {
                                var G = M.match(/^\s*/)[0],
                                    V = G.match(/[^\n\r]*$/)[0].split(u.indent_string).length - 1,
                                    H = u.get_full_indent(j - V);
                                M = M.replace(/^\s*/, W).replace(/\r\n|\r|\n/g, "\n" + H).replace(/\s+$/, "")
                            }
                            M && (u.print_token_raw(M), u.print_newline(!0, u.output))
                        }
                        u.current_mode = "TAG";
                        break;
                    default:
                        "" !== u.token_text && u.print_token(u.token_text)
                }
                u.last_token = u.token_type, u.last_text = u.token_text
            }
            var z = u.output.join("").replace(/[\r\n\t ]+$/, "");
            return O && (z += "\n"), "\n" !== N && (z = z.replace(/[\n]/g, N)), z
        }
        var l = /\r\n|[\n\r\u2028\u2029]/,
            c = new RegExp(l.source, "g");
        r = [n, n(300), n(299)], o = function (e) {
            var t = n(300),
                r = n(299);
            return {
                html_beautify: function (e, n) {
                    return u(e, n, t.js_beautify, r.css_beautify)
                }
            }
        }.apply(t, r), !(void 0 !== o && (e.exports = o))
    } ()
}, , function (e, t) {
    "use strict";

    function n(e) {
        var t = l.split(e);
        return t.shift() + u(t).join("")
    }

    function r(e) {
        var t = l.split(e);
        return u(t).join("")
    }

    function o(e) {
        return l.split(e).join("-")
    }

    function i(e) {
        return l.split(e).join("_")
    }

    function a(e) {
        return e = e.replace(/[^a-z0-9]+/gi, " ").replace(/([A-Z0-9]+)([A-Z][a-z])/g, "$1 $2").replace(/([a-z0-9])([A-Z])/g, "$1 $2").toLowerCase(), s(e).split(/\s+/)
    }

    function s(e) {
        return e.replace(/^\s+|\s+$/g, "")
    }

    function u(e) {
        for (var t = [], n = 0; n < e.length; n++) t.push(e[n].charAt(0).toUpperCase() + e[n].substr(1));
        return t
    }
    var l = e.exports = {
        camelback: n,
        camelcase: r,
        dash: o,
        underscore: i,
        split: a
    }
}]);