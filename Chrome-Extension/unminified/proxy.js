
"use strict";

/*
Load Tests from api.
*/
function loadTests(e) {
    return fetch("https://www.snaptest.io/api/load", {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            apikey: e
        }
    }).then(function(e) {
        return e.json()
    }).then(function(e) {
        return {
            tests: e.tests,
            components: e.components,
            directory: e.directory
        }
    })
}

/*
save Tests with api.
*/
function saveTests() {
    fetch("https://www.snaptest.io/api/save", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            apikey: b.user.apiKey
        },
        body: (0, f["default"])(S)
    }).then(function(e) {
        return e.json()
    })["catch"](function() {})
}