! function (e) {
    function t(a) {
        if (n[a]) return n[a].exports;
        var r = n[a] = {
            exports: {},
            id: a,
            loaded: !1
        };
        return e[a].call(r.exports, r, r.exports, t), r.loaded = !0, r.exports
    }
    var n = {};
    return t.m = e, t.c = n, t.p = "", t(0)
} ([function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    var r = n(1),
        o = a(r),
        i = n(1),
        l = a(i),
        u = n(149),
        c = a(u),
        s = n(297),
        d = a(s),
        f = n(7),
        p = a(f);
    n(260), p["default"].to(p["default"].SESSION, "getState", {}, function (e) {
        (0, c["default"])(function () {
            var t = document.createElement("div");
            t.setAttribute("id", "snpt-sandboxed"), document.querySelector("body").appendChild(t), o["default"].render(l["default"].createElement(d["default"], {
                initialState: e,
                isWindow: !0
            }), t)
        })
    })
}, function (e, t) {
    /*!
     * react-lite.js v0.15.32
     * (c) 2017 Jade Gu
     * Released under the MIT License.
     */
    "use strict";

    function n(e, t, n, a, r) {
        var o = {
            vtype: e,
            type: t,
            props: n,
            refs: Xe,
            key: a,
            ref: r
        };
        return e !== qe && e !== Be || (o.uid = ie()), o
    }

    function a(e, t, n) {
        var a = e.vtype,
            r = null;
        return a ? a === Ve ? r = s(e, t, n) : a === Be ? r = N(e, t, n) : a === qe ? r = g(e, t, n) : a === He && (r = document.createComment("react-text: " + (e.uid || ie()))) : r = document.createTextNode(e), r
    }

    function r(e, t, n, a) {
        var r = e.vtype;
        if (r === Be) return b(e, t, n, a);
        if (r === qe) return E(e, t, n, a);
        if (r !== Ve) return n;
        var i = e.props[je] && e.props[je].__html;
        return null != i ? (h(e, t, n, a), d(t, n, a)) : (o(e, t, n, a), h(e, t, n, a)), n
    }

    function o(e, t, n, a) {
        var r = {
            removes: [],
            updates: [],
            creates: []
        };
        m(r, e, t, n, a), re(r.removes, l), re(r.updates, i), re(r.creates, u)
    }

    function i(e) {
        if (e) {
            var t = e.vnode,
                n = e.node;
            e.shouldIgnore || (t.vtype ? t.vtype === Ve ? h(t, e.newVnode, n, e.parentContext) : t.vtype === qe ? n = E(t, e.newVnode, n, e.parentContext) : t.vtype === Be && (n = b(t, e.newVnode, n, e.parentContext)) : n.replaceData(0, n.length, e.newVnode));
            var a = n.parentNode.childNodes[e.index];
            return a !== n && n.parentNode.insertBefore(n, a), n
        }
    }

    function l(e) {
        c(e.vnode, e.node), e.node.parentNode.removeChild(e.node)
    }

    function u(e) {
        var t = a(e.vnode, e.parentContext, e.parentNode.namespaceURI);
        e.parentNode.insertBefore(t, e.parentNode.childNodes[e.index])
    }

    function c(e, t) {
        var n = e.vtype;
        n === Ve ? v(e, t) : n === Be ? w(e, t) : n === qe && y(e, t)
    }

    function s(e, t, n) {
        var a = e.type,
            r = e.props,
            o = null;
        "svg" === a || n === We ? (o = document.createElementNS(We, a), n = We) : o = document.createElement(a), d(e, o, t);
        var i = a.indexOf("-") >= 0 || null != r.is;
        return se(o, r, i), null != e.ref && (ae($e, e), ae($e, o)), o
    }

    function d(e, t, n) {
        for (var r = t.vchildren = f(e), o = t.namespaceURI, i = 0, l = r.length; i < l; i++) t.appendChild(a(r[i], n, o))
    }

    function f(e) {
        var t = e.props.children,
            n = [];
        return bt(t) ? re(t, p, n) : p(t, n), n
    }

    function p(e, t) {
        if (null != e && "boolean" != typeof e) {
            if (!e.vtype) {
                if (e.toJS) return e = e.toJS(), void (bt(e) ? re(e, p, t) : p(e, t));
                e = "" + e
            }
            t[t.length] = e
        }
    }

    function m(e, t, n, a, r) {
        var o = a.childNodes,
            i = a.vchildren,
            l = a.vchildren = f(n),
            u = i.length,
            c = l.length;
        if (0 !== u)
            if (0 !== c) {
                for (var s = Array(c), d = null, p = null, h = 0; h < u; h++)
                    for (var v = i[h], g = 0; g < c; g++)
                        if (!s[g]) {
                            var E = l[g];
                            if (v === E) {
                                var y = !0;
                                r && (v.vtype !== Be && v.vtype !== qe || v.type.contextTypes && (y = !1)), s[g] = {
                                    shouldIgnore: y,
                                    vnode: v,
                                    newVnode: E,
                                    node: o[h],
                                    parentContext: r,
                                    index: g
                                }, i[h] = null;
                                break
                            }
                        }
                for (var h = 0; h < u; h++) {
                    var S = i[h];
                    if (null !== S) {
                        for (var N = !0, g = 0; g < c; g++)
                            if (!s[g]) {
                                var b = l[g];
                                if (b.type === S.type && b.key === S.key && b.refs === S.refs) {
                                    s[g] = {
                                        vnode: S,
                                        newVnode: b,
                                        node: o[h],
                                        parentContext: r,
                                        index: g
                                    }, N = !1;
                                    break
                                }
                            }
                        N && (d || (d = []), ae(d, {
                            vnode: S,
                            node: o[h]
                        }))
                    }
                }
                for (var h = 0; h < c; h++) {
                    var w = s[h];
                    w ? w.vnode.vtype === Ve && m(e, w.vnode, w.newVnode, w.node, w.parentContext) : (p || (p = []), ae(p, {
                        vnode: l[h],
                        parentNode: a,
                        parentContext: r,
                        index: h
                    }))
                }
                d && ae(e.removes, d), p && ae(e.creates, p), ae(e.updates, s)
            } else
                for (var h = 0; h < u; h++) ae(e.removes, {
                    vnode: i[h],
                    node: o[h]
                });
        else if (c > 0)
            for (var h = 0; h < c; h++) ae(e.creates, {
                vnode: l[h],
                parentNode: a,
                parentContext: r,
                index: h
            })
    }

    function h(e, t, n) {
        var a = e.type.indexOf("-") >= 0 || null != e.props.is;
        return de(n, e.props, t.props, a), e.ref !== t.ref && (P(e.refs, e.ref, n), R(t.refs, t.ref, n)), n
    }

    function v(e, t) {
        var n = (e.props, t.vchildren),
            a = t.childNodes;
        if (n)
            for (var r = 0, o = n.length; r < o; r++) c(n[r], a[r]);
        P(e.refs, e.ref, t), t.eventStore = t.vchildren = null
    }

    function g(e, t, n) {
        var r = S(e, t),
            o = a(r, t, n);
        return o.cache = o.cache || {}, o.cache[e.uid] = r, o
    }

    function E(e, t, n, a) {
        var r = e.uid,
            o = n.cache[r];
        delete n.cache[r];
        var i = S(t, a),
            l = x(o, i, n, a);
        return l.cache = l.cache || {}, l.cache[t.uid] = i, l !== n && M(l.cache, n.cache, l), l
    }

    function y(e, t) {
        var n = e.uid,
            a = t.cache[n];
        delete t.cache[n], c(a, t)
    }

    function S(e, t) {
        var a = e.type,
            r = e.props,
            o = _(t, a.contextTypes),
            i = a(r, o);
        if (i && i.render && (i = i.render()), null === i || i === !1) i = n(He);
        else if (!i || !i.vtype) throw new Error("@" + a.name + "#render:You may have returned undefined, an array or some other invalid object");
        return i
    }

    function N(e, t, n) {
        var r = e.type,
            o = e.props,
            i = e.uid,
            l = _(t, r.contextTypes),
            u = new r(o, l),
            c = u.$updater,
            s = u.$cache;
        s.parentContext = t, c.isPending = !0, u.props = u.props || o, u.context = u.context || l, u.componentWillMount && (u.componentWillMount(), u.state = c.getState());
        var d = k(u),
            f = a(d, C(u, t), n);
        return f.cache = f.cache || {}, f.cache[i] = u, s.vnode = d, s.node = f, s.isMounted = !0, ae(Ye, u), null != e.ref && (ae($e, e), ae($e, u)), f
    }

    function b(e, t, n, a) {
        var r = e.uid,
            o = n.cache[r],
            i = o.$updater,
            l = o.$cache,
            u = t.type,
            c = t.props,
            s = _(a, u.contextTypes);
        if (delete n.cache[r], n.cache[t.uid] = o, l.parentContext = a, o.componentWillReceiveProps) {
            var d = !i.isPending;
            d && (i.isPending = !0), o.componentWillReceiveProps(c, s), d && (i.isPending = !1)
        }
        return e.ref !== t.ref && (P(e.refs, e.ref, o), R(t.refs, t.ref, o)), i.emitUpdate(c, s), l.node
    }

    function w(e, t) {
        var n = e.uid,
            a = t.cache[n],
            r = a.$cache;
        delete t.cache[n], P(e.refs, e.ref, a), a.setState = a.forceUpdate = ee, a.componentWillUnmount && a.componentWillUnmount(), c(r.vnode, t), delete a.setState, r.isMounted = !1, r.node = r.parentContext = r.vnode = a.refs = a.context = null
    }

    function _(e, t) {
        var n = {};
        if (!t || !e) return n;
        for (var a in t) t.hasOwnProperty(a) && (n[a] = e[a]);
        return n
    }

    function k(e, t) {
        Xe = e.refs;
        var a = e.render();
        if (null === a || a === !1) a = n(He);
        else if (!a || !a.vtype) throw new Error("@" + e.constructor.name + "#render:You may have returned undefined, an array or some other invalid object");
        return Xe = null, a
    }

    function C(e, t) {
        if (e.getChildContext) {
            var n = e.getChildContext();
            n && (t = oe(oe({}, t), n))
        }
        return t
    }

    function T() {
        var e = Ye.length;
        if (e) {
            var t = Ye;
            Ye = [];
            for (var n = -1; e--;) {
                var a = t[++n],
                    r = a.$updater;
                a.componentDidMount && a.componentDidMount(), r.isPending = !1, r.emitUpdate()
            }
        }
    }

    function O() {
        var e = $e.length;
        if (e) {
            var t = $e;
            $e = [];
            for (var n = 0; n < e; n += 2) {
                var a = t[n],
                    r = t[n + 1];
                R(a.refs, a.ref, r)
            }
        }
    }

    function A() {
        O(), T()
    }

    function x(e, t, n, o) {
        var i = n;
        return null == t ? (c(e, n), n.parentNode.removeChild(n)) : e.type !== t.type || e.key !== t.key ? (c(e, n), i = a(t, o, n.namespaceURI), n.parentNode.replaceChild(i, n)) : (e !== t || o) && (i = r(e, t, n, o)), i
    }

    function I() {
        return this
    }

    function R(e, t, n) {
        null != t && n && (n.nodeName && !n.getDOMNode && (n.getDOMNode = I), Z(t) ? t(n) : e && (e[t] = n))
    }

    function P(e, t, n) {
        null != t && (Z(t) ? t(null) : e && e[t] === n && delete e[t])
    }

    function M(e, t, n) {
        for (var a in t)
            if (t.hasOwnProperty(a)) {
                var r = t[a];
                e[a] = r, r.forceUpdate && (r.$cache.node = n)
            }
    }

    function L(e) {
        this.instance = e, this.pendingStates = [], this.pendingCallbacks = [], this.isPending = !1, this.nextProps = this.nextContext = null, this.clearCallbacks = this.clearCallbacks.bind(this)
    }

    function D(e, t) {
        this.$updater = new L(this), this.$cache = {
            isMounted: !1
        }, this.props = e, this.state = {}, this.refs = {}, this.context = t
    }

    function U(e, t, n, a, r) {
        var o = !0;
        if (e.shouldComponentUpdate && (o = e.shouldComponentUpdate(t, n, a)), o === !1) return e.props = t, e.state = n, void (e.context = a || {});
        var i = e.$cache;
        i.props = t, i.state = n, i.context = a || {}, e.forceUpdate(r)
    }

    function j(e) {
        return "onDoubleClick" === e ? e = "ondblclick" : "onTouchTap" === e && (e = "onclick"), e.toLowerCase()
    }

    function W(e, t, n) {
        t = j(t);
        var a = e.eventStore || (e.eventStore = {});
        if (a[t] = n, 1 === Ze[t]) return void (e[t] = q);
        if (at[t] || (document.addEventListener(t.substr(2), V, !1), at[t] = !0), et && t === nt) return void e.addEventListener("click", tt, !1);
        e.nodeName;
        "onchange" === t && W(e, "oninput", n)
    }

    function F(e, t) {
        t = j(t);
        var n = e.eventStore || (e.eventStore = {});
        if (delete n[t], 1 === Ze[t]) return void (e[t] = null);
        if (et && t === nt) return void e.removeEventListener("click", tt, !1);
        e.nodeName;
        "onchange" === t && delete n.oninput
    }

    function V(e) {
        var t = e.target,
            n = e.type,
            a = "on" + n,
            r = void 0;
        for (Je.isPending = !0; t;) {
            var o = t,
                i = o.eventStore,
                l = i && i[a];
            if (l) {
                if (r || (r = B(e)), r.currentTarget = t, l.call(t, r), r.$cancelBubble) break;
                t = t.parentNode
            } else t = t.parentNode
        }
        Je.isPending = !1, Je.batchUpdate()
    }

    function q(e) {
        var t = e.currentTarget || e.target,
            n = "on" + e.type,
            a = B(e);
        a.currentTarget = t, Je.isPending = !0;
        var r = t.eventStore,
            o = r && r[n];
        o && o.call(t, a), Je.isPending = !1, Je.batchUpdate()
    }

    function B(e) {
        var t = {},
            n = function () {
                return t.$cancelBubble = !0
            };
        t.nativeEvent = e, t.persist = ee;
        for (var a in e) "function" != typeof e[a] ? t[a] = e[a] : "stopPropagation" === a || "stopImmediatePropagation" === a ? t[a] = n : t[a] = e[a].bind(e);
        return t
    }

    function H(e, t) {
        for (var n in t) t.hasOwnProperty(n) && X(e, n, t[n])
    }

    function z(e, t) {
        for (var n in t) t.hasOwnProperty(n) && (e[n] = "")
    }

    function G(e, t, n) {
        if (t !== n) {
            if (!n && t) return void z(e, t);
            if (n && !t) return void H(e, n);
            for (var a in t) n.hasOwnProperty(a) ? n[a] !== t[a] && X(e, a, n[a]) : e[a] = "";
            for (var a in n) t.hasOwnProperty(a) || X(e, a, n[a])
        }
    }

    function K(e, t) {
        return e + t.charAt(0).toUpperCase() + t.substring(1)
    }

    function X(e, t, n) {
        return !rt[t] && it.test(n) ? void (e[t] = n + "px") : ("float" === t && (t = "cssFloat"), null != n && "boolean" != typeof n || (n = ""), void (e[t] = n))
    }

    function Y(e) {
        var t = e.props,
            n = e.attrNS,
            a = e.domAttrs,
            r = e.domProps;
        for (var o in t)
            if (t.hasOwnProperty(o)) {
                var i = t[o];
                dt[o] = {
                    attributeName: a.hasOwnProperty(o) ? a[o] : o.toLowerCase(),
                    propertyName: r.hasOwnProperty(o) ? r[o] : o,
                    attributeNamespace: n.hasOwnProperty(o) ? n[o] : null,
                    mustUseProperty: $(i, ft),
                    hasBooleanValue: $(i, pt),
                    hasNumericValue: $(i, mt),
                    hasPositiveNumericValue: $(i, ht),
                    hasOverloadedBooleanValue: $(i, vt)
                }
            }
    }

    function $(e, t) {
        return (e & t) === t
    }

    function J(e, t, n) {
        var a = dt.hasOwnProperty(t) && dt[t];
        if (a)
            if (null == n || a.hasBooleanValue && !n || a.hasNumericValue && isNaN(n) || a.hasPositiveNumericValue && n < 1 || a.hasOverloadedBooleanValue && n === !1) Q(e, t);
            else if (a.mustUseProperty) {
                var r = a.propertyName;
                "value" === r && "" + e[r] == "" + n || (e[r] = n)
            } else {
                var o = a.attributeName,
                    i = a.attributeNamespace;
                i ? e.setAttributeNS(i, o, "" + n) : a.hasBooleanValue || a.hasOverloadedBooleanValue && n === !0 ? e.setAttribute(o, "") : e.setAttribute(o, "" + n)
            } else st(t) && ct.test(t) && (null == n ? e.removeAttribute(t) : e.setAttribute(t, "" + n))
    }

    function Q(e, t) {
        var n = dt.hasOwnProperty(t) && dt[t];
        if (n)
            if (n.mustUseProperty) {
                var a = n.propertyName;
                n.hasBooleanValue ? e[a] = !1 : "value" === a && "" + e[a] == "" || (e[a] = "")
            } else e.removeAttribute(n.attributeName);
        else st(t) && e.removeAttribute(t)
    }

    function Z(e) {
        return "function" == typeof e
    }

    function ee() { }

    function te(e) {
        return e
    }

    function ne(e, t) {
        return function () {
            return e.apply(this, arguments), t.apply(this, arguments)
        }
    }

    function ae(e, t) {
        e[e.length] = t
    }

    function re(e, t, n) {
        for (var a = e.length, r = -1; a--;) {
            var o = e[++r];
            bt(o) ? re(o, t, n) : t(o, n)
        }
    }

    function oe(e, t) {
        if (!t) return e;
        for (var n = Object.keys(t), a = n.length; a--;) e[n[a]] = t[n[a]];
        return e
    }

    function ie() {
        return ++wt
    }

    function le(e, t, n, a) {
        _t.test(t) ? W(e, t, n) : "style" === t ? H(e.style, n) : t === je ? n && null != n.__html && (e.innerHTML = n.__html) : a ? null == n ? e.removeAttribute(t) : e.setAttribute(t, "" + n) : J(e, t, n)
    }

    function ue(e, t, n, a) {
        _t.test(t) ? F(e, t) : "style" === t ? z(e.style, n) : t === je ? e.innerHTML = "" : a ? e.removeAttribute(t) : Q(e, t)
    }

    function ce(e, t, n, a, r) {
        if ("value" !== t && "checked" !== t || (a = e[t]), n !== a) return void 0 === n ? void ue(e, t, a, r) : void ("style" === t ? G(e.style, a, n) : le(e, t, n, r))
    }

    function se(e, t, n) {
        for (var a in t) "children" !== a && le(e, a, t[a], n)
    }

    function de(e, t, n, a) {
        for (var r in t) "children" !== r && (n.hasOwnProperty(r) ? ce(e, r, n[r], t[r], a) : ue(e, r, t[r], a));
        for (var r in n) "children" === r || t.hasOwnProperty(r) || le(e, r, n[r], a)
    }

    function fe(e) {
        return !(!e || e.nodeType !== ze && e.nodeType !== Ge && e.nodeType !== Ke)
    }

    function pe(e, t, n, r) {
        if (!e.vtype) throw new Error("cannot render " + e + " to container");
        if (!fe(t)) throw new Error("container " + t + " is not a DOM element");
        var o = t[Fe] || (t[Fe] = ie()),
            i = kt[o];
        if (i) return void (i === !0 ? kt[o] = i = {
            vnode: e,
            callback: n,
            parentContext: r
        } : (i.vnode = e, i.parentContext = r, i.callback = i.callback ? ne(i.callback, n) : n));
        kt[o] = !0;
        var l = null,
            u = null;
        if (l = Ct[o]) u = x(l, e, t.firstChild, r);
        else {
            u = a(e, r, t.namespaceURI);
            for (var c = null; c = t.lastChild;) t.removeChild(c);
            t.appendChild(u)
        }
        Ct[o] = e;
        var s = Je.isPending;
        Je.isPending = !0, A(), i = kt[o], delete kt[o];
        var d = null;
        return "object" == typeof i ? d = pe(i.vnode, t, i.callback, i.parentContext) : e.vtype === Ve ? d = u : e.vtype === Be && (d = u.cache[e.uid]), s || (Je.isPending = !1, Je.batchUpdate()), n && n.call(d), d
    }

    function me(e, t, n) {
        return pe(e, t, n)
    }

    function he(e, t, n, a) {
        var r = e.$cache.parentContext;
        return pe(t, n, a, r)
    }

    function ve(e) {
        if (!e.nodeName) throw new Error("expect node");
        var t = e[Fe],
            n = null;
        return !!(n = Ct[t]) && (c(n, e.firstChild), e.removeChild(e.firstChild), delete Ct[t], !0)
    }

    function ge(e) {
        if (null == e) return null;
        if (e.nodeName) return e;
        var t = e;
        if (t.getDOMNode && t.$cache.isMounted) return t.getDOMNode();
        throw new Error("findDOMNode can not find Node")
    }

    function Ee(e, t, a) {
        var r = null;
        if ("string" == typeof e) r = Ve;
        else {
            if ("function" != typeof e) throw new Error("React.createElement: unexpect type [ " + e + " ]");
            r = e.prototype && e.prototype.isReactComponent ? Be : qe
        }
        var o = null,
            i = null,
            l = {};
        if (null != t)
            for (var u in t) t.hasOwnProperty(u) && ("key" === u ? void 0 !== t.key && (o = "" + t.key) : "ref" === u ? void 0 !== t.ref && (i = t.ref) : l[u] = t[u]);
        var c = e.defaultProps;
        if (c)
            for (var u in c) void 0 === l[u] && (l[u] = c[u]);
        var s = arguments.length,
            d = a;
        if (s > 3) {
            d = Array(s - 2);
            for (var f = 2; f < s; f++) d[f - 2] = arguments[f]
        }
        return void 0 !== d && (l.children = d), n(r, e, l, o, i)
    }

    function ye(e) {
        return null != e && !!e.vtype
    }

    function Se(e, t) {
        for (var n = e.type, a = e.key, r = e.ref, o = oe(oe({
            key: a,
            ref: r
        }, e.props), t), i = arguments.length, l = Array(i > 2 ? i - 2 : 0), u = 2; u < i; u++) l[u - 2] = arguments[u];
        var c = Ee.apply(void 0, [n, o].concat(l));
        return c.ref === e.ref && (c.refs = e.refs), c
    }

    function Ne(e) {
        var t = function () {
            for (var t = arguments.length, n = Array(t), a = 0; a < t; a++) n[a] = arguments[a];
            return Ee.apply(void 0, [e].concat(n))
        };
        return t.type = e, t
    }

    function be(e) {
        if (ye(e)) return e;
        throw new Error("expect only one child")
    }

    function we(e, t, n) {
        if (null == e) return e;
        var a = 0;
        bt(e) ? re(e, function (e) {
            t.call(n, e, a++)
        }) : t.call(n, e, a)
    }

    function _e(e, t, n) {
        if (null == e) return e;
        var a = [],
            r = {};
        we(e, function (e, o) {
            var i = {};
            i.child = t.call(n, e, o) || e, i.isEqual = i.child === e;
            var l = i.key = Te(e, o);
            r.hasOwnProperty(l) ? r[l] += 1 : r[l] = 0, i.index = r[l], ae(a, i)
        });
        var o = [];
        return a.forEach(function (e) {
            var t = e.child,
                n = e.key,
                a = e.index,
                i = e.isEqual;
            if (null != t && "boolean" != typeof t) {
                if (!ye(t) || null == n) return void ae(o, t);
                0 !== r[n] && (n += ":" + a), i || (n = Oe(t.key || "") + "/" + n), t = Se(t, {
                    key: n
                }), ae(o, t)
            }
        }), o
    }

    function ke(e) {
        var t = 0;
        return we(e, function () {
            t++
        }), t
    }

    function Ce(e) {
        return _e(e, te) || []
    }

    function Te(e, t) {
        var n = void 0;
        return n = ye(e) && "string" == typeof e.key ? ".$" + e.key : "." + t.toString(36)
    }

    function Oe(e) {
        return ("" + e).replace(Rt, "//")
    }

    function Ae(e, t) {
        e.forEach(function (e) {
            e && (bt(e.mixins) && Ae(e.mixins, t), t(e))
        })
    }

    function xe(e, t) {
        for (var n in t)
            if (t.hasOwnProperty(n)) {
                var a = t[n];
                if ("getInitialState" !== n) {
                    var r = e[n];
                    Z(r) && Z(a) ? e[n] = ne(r, a) : e[n] = a
                } else ae(e.$getInitialStates, a)
            }
    }

    function Ie(e, t) {
        t.propTypes && (e.propTypes = e.propTypes || {}, oe(e.propTypes, t.propTypes)), t.contextTypes && (e.contextTypes = e.contextTypes || {}, oe(e.contextTypes, t.contextTypes)), oe(e, t.statics), Z(t.getDefaultProps) && (e.defaultProps = e.defaultProps || {}, oe(e.defaultProps, t.getDefaultProps()))
    }

    function Re(e, t) {
        for (var n in t) t.hasOwnProperty(n) && Z(t[n]) && (e[n] = t[n].bind(e))
    }

    function Pe() {
        var e = this,
            t = {},
            n = this.setState;
        return this.setState = Mt, this.$getInitialStates.forEach(function (n) {
            Z(n) && oe(t, n.call(e))
        }), this.setState = n, t
    }

    function Me(e) {
        function t(n, a) {
            D.call(this, n, a), this.constructor = t, e.autobind !== !1 && Re(this, t.prototype), this.state = this.getInitialState() || this.state
        }
        if (!Z(e.render)) throw new Error("createClass: spec.render is not function");
        var n = e.mixins || [],
            a = n.concat(e);
        e.mixins = null, t.displayName = e.displayName;
        var r = t.prototype = new Mt;
        return r.$getInitialStates = [], Ae(a, function (e) {
            xe(r, e), Ie(t, e)
        }), r.getInitialState = Pe, e.mixins = n, t
    }

    function Le(e, t) {
        if (e === t) return !0;
        if ("object" != typeof e || null === e || "object" != typeof t || null === t) return !1;
        var n = Object.keys(e),
            a = Object.keys(t);
        if (n.length !== a.length) return !1;
        for (var r = 0; r < n.length; r++)
            if (!t.hasOwnProperty(n[r]) || e[n[r]] !== t[n[r]]) return !1;
        return !0
    }

    function De(e, t) {
        D.call(this, e, t)
    }

    function Ue(e, t) {
        return !Le(this.props, e) || !Le(this.state, t)
    }
    var je = "dangerouslySetInnerHTML",
        We = "http://www.w3.org/2000/svg",
        Fe = "liteid",
        Ve = 2,
        qe = 3,
        Be = 4,
        He = 5,
        ze = 1,
        Ge = 9,
        Ke = 11,
        Xe = null,
        Ye = [],
        $e = [],
        Je = {
            updaters: [],
            isPending: !1,
            add: function (e) {
                ae(this.updaters, e)
            },
            batchUpdate: function () {
                if (!this.isPending) {
                    this.isPending = !0;
                    for (var e = this.updaters, t = void 0; t = e.pop();) t.updateComponent();
                    this.isPending = !1
                }
            }
        };
    L.prototype = {
        emitUpdate: function (e, t) {
            this.nextProps = e, this.nextContext = t, e || !Je.isPending ? this.updateComponent() : Je.add(this)
        },
        updateComponent: function () {
            var e = this.instance,
                t = this.pendingStates,
                n = this.nextProps,
                a = this.nextContext;
            (n || t.length > 0) && (n = n || e.props, a = a || e.context, this.nextProps = this.nextContext = null, U(e, n, this.getState(), a, this.clearCallbacks))
        },
        addState: function (e) {
            e && (ae(this.pendingStates, e), this.isPending || this.emitUpdate())
        },
        replaceState: function (e) {
            var t = this.pendingStates;
            t.pop(), ae(t, [e])
        },
        getState: function () {
            var e = this.instance,
                t = this.pendingStates,
                n = e.state,
                a = e.props;
            return t.length && (n = oe({}, n), t.forEach(function (t) {
                var r = bt(t);
                r && (t = t[0]), Z(t) && (t = t.call(e, n, a)), r ? n = oe({}, t) : oe(n, t)
            }), t.length = 0), n
        },
        clearCallbacks: function () {
            var e = this.pendingCallbacks,
                t = this.instance;
            e.length > 0 && (this.pendingCallbacks = [], e.forEach(function (e) {
                return e.call(t)
            }))
        },
        addCallback: function (e) {
            Z(e) && ae(this.pendingCallbacks, e)
        }
    };
    var Qe = {};
    D.prototype = {
        constructor: D,
        isReactComponent: Qe,
        forceUpdate: function (e) {
            var t = this.$updater,
                n = this.$cache,
                a = this.props,
                r = this.state,
                o = this.context;
            if (n.isMounted) {
                if (t.isPending) return void t.addState(r);
                var i = n.props || a,
                    l = n.state || r,
                    u = n.context || o,
                    c = n.parentContext,
                    s = n.node,
                    d = n.vnode;
                n.props = n.state = n.context = null, t.isPending = !0, this.componentWillUpdate && this.componentWillUpdate(i, l, u), this.state = l, this.props = i, this.context = u;
                var f = k(this),
                    p = x(d, f, s, C(this, c));
                p !== s && (p.cache = p.cache || {}, M(p.cache, s.cache, p)), n.vnode = f, n.node = p, A(), this.componentDidUpdate && this.componentDidUpdate(a, r, o), e && e.call(this), t.isPending = !1, t.emitUpdate()
            }
        },
        setState: function (e, t) {
            var n = this.$updater;
            n.addCallback(t), n.addState(e)
        },
        replaceState: function (e, t) {
            var n = this.$updater;
            n.addCallback(t), n.replaceState(e)
        },
        getDOMNode: function () {
            var e = this.$cache.node;
            return e && "#comment" === e.nodeName ? null : e
        },
        isMounted: function () {
            return this.$cache.isMounted
        }
    };
    var Ze = {
        onmousemove: 1,
        onmouseleave: 1,
        onmouseenter: 1,
        onload: 1,
        onunload: 1,
        onscroll: 1,
        onfocus: 1,
        onblur: 1,
        onrowexit: 1,
        onbeforeunload: 1,
        onstop: 1,
        ondragdrop: 1,
        ondragenter: 1,
        ondragexit: 1,
        ondraggesture: 1,
        ondragover: 1,
        oncontextmenu: 1
    },
        et = "ontouchstart" in document,
        tt = function () { },
        nt = "onclick",
        at = {},
        rt = {
            animationIterationCount: 1,
            borderImageOutset: 1,
            borderImageSlice: 1,
            borderImageWidth: 1,
            boxFlex: 1,
            boxFlexGroup: 1,
            boxOrdinalGroup: 1,
            columnCount: 1,
            flex: 1,
            flexGrow: 1,
            flexPositive: 1,
            flexShrink: 1,
            flexNegative: 1,
            flexOrder: 1,
            gridRow: 1,
            gridColumn: 1,
            fontWeight: 1,
            lineClamp: 1,
            lineHeight: 1,
            opacity: 1,
            order: 1,
            orphans: 1,
            tabSize: 1,
            widows: 1,
            zIndex: 1,
            zoom: 1,
            fillOpacity: 1,
            floodOpacity: 1,
            stopOpacity: 1,
            strokeDasharray: 1,
            strokeDashoffset: 1,
            strokeMiterlimit: 1,
            strokeOpacity: 1,
            strokeWidth: 1
        },
        ot = ["Webkit", "ms", "Moz", "O"];
    Object.keys(rt).forEach(function (e) {
        ot.forEach(function (t) {
            rt[K(t, e)] = 1
        })
    });
    var it = /^-?\d+(\.\d+)?$/,
        lt = ":A-Z_a-z\\u00C0-\\u00D6\\u00D8-\\u00F6\\u00F8-\\u02FF\\u0370-\\u037D\\u037F-\\u1FFF\\u200C-\\u200D\\u2070-\\u218F\\u2C00-\\u2FEF\\u3001-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFFD",
        ut = lt + "\\-.0-9\\uB7\\u0300-\\u036F\\u203F-\\u2040",
        ct = new RegExp("^[" + lt + "][" + ut + "]*$"),
        st = RegExp.prototype.test.bind(new RegExp("^(data|aria)-[" + ut + "]*$")),
        dt = {},
        ft = 1,
        pt = 4,
        mt = 8,
        ht = 24,
        vt = 32,
        gt = {
            props: {
                accept: 0,
                acceptCharset: 0,
                accessKey: 0,
                action: 0,
                allowFullScreen: pt,
                allowTransparency: 0,
                alt: 0,
                async: pt,
                autoComplete: 0,
                autoFocus: pt,
                autoPlay: pt,
                capture: pt,
                cellPadding: 0,
                cellSpacing: 0,
                charSet: 0,
                challenge: 0,
                checked: ft | pt,
                cite: 0,
                classID: 0,
                className: 0,
                cols: ht,
                colSpan: 0,
                content: 0,
                contentEditable: 0,
                contextMenu: 0,
                controls: pt,
                coords: 0,
                crossOrigin: 0,
                data: 0,
                dateTime: 0,
                "default": pt,
                defaultValue: ft,
                defaultChecked: ft | pt,
                defer: pt,
                dir: 0,
                disabled: pt,
                download: vt,
                draggable: 0,
                encType: 0,
                form: 0,
                formAction: 0,
                formEncType: 0,
                formMethod: 0,
                formNoValidate: pt,
                formTarget: 0,
                frameBorder: 0,
                headers: 0,
                height: 0,
                hidden: pt,
                high: 0,
                href: 0,
                hrefLang: 0,
                htmlFor: 0,
                httpEquiv: 0,
                icon: 0,
                id: 0,
                inputMode: 0,
                integrity: 0,
                is: 0,
                keyParams: 0,
                keyType: 0,
                kind: 0,
                label: 0,
                lang: 0,
                list: 0,
                loop: pt,
                low: 0,
                manifest: 0,
                marginHeight: 0,
                marginWidth: 0,
                max: 0,
                maxLength: 0,
                media: 0,
                mediaGroup: 0,
                method: 0,
                min: 0,
                minLength: 0,
                multiple: ft | pt,
                muted: ft | pt,
                name: 0,
                nonce: 0,
                noValidate: pt,
                open: pt,
                optimum: 0,
                pattern: 0,
                placeholder: 0,
                poster: 0,
                preload: 0,
                profile: 0,
                radioGroup: 0,
                readOnly: pt,
                referrerPolicy: 0,
                rel: 0,
                required: pt,
                reversed: pt,
                role: 0,
                rows: ht,
                rowSpan: mt,
                sandbox: 0,
                scope: 0,
                scoped: pt,
                scrolling: 0,
                seamless: pt,
                selected: ft | pt,
                shape: 0,
                size: ht,
                sizes: 0,
                span: ht,
                spellCheck: 0,
                src: 0,
                srcDoc: 0,
                srcLang: 0,
                srcSet: 0,
                start: mt,
                step: 0,
                style: 0,
                summary: 0,
                tabIndex: 0,
                target: 0,
                title: 0,
                type: 0,
                useMap: 0,
                value: ft,
                width: 0,
                wmode: 0,
                wrap: 0,
                about: 0,
                datatype: 0,
                inlist: 0,
                prefix: 0,
                property: 0,
                resource: 0,
                "typeof": 0,
                vocab: 0,
                autoCapitalize: 0,
                autoCorrect: 0,
                autoSave: 0,
                color: 0,
                itemProp: 0,
                itemScope: pt,
                itemType: 0,
                itemID: 0,
                itemRef: 0,
                results: 0,
                security: 0,
                unselectable: 0
            },
            attrNS: {},
            domAttrs: {
                acceptCharset: "accept-charset",
                className: "class",
                htmlFor: "for",
                httpEquiv: "http-equiv"
            },
            domProps: {}
        },
        Et = "http://www.w3.org/1999/xlink",
        yt = "http://www.w3.org/XML/1998/namespace",
        St = {
            accentHeight: "accent-height",
            accumulate: 0,
            additive: 0,
            alignmentBaseline: "alignment-baseline",
            allowReorder: "allowReorder",
            alphabetic: 0,
            amplitude: 0,
            arabicForm: "arabic-form",
            ascent: 0,
            attributeName: "attributeName",
            attributeType: "attributeType",
            autoReverse: "autoReverse",
            azimuth: 0,
            baseFrequency: "baseFrequency",
            baseProfile: "baseProfile",
            baselineShift: "baseline-shift",
            bbox: 0,
            begin: 0,
            bias: 0,
            by: 0,
            calcMode: "calcMode",
            capHeight: "cap-height",
            clip: 0,
            clipPath: "clip-path",
            clipRule: "clip-rule",
            clipPathUnits: "clipPathUnits",
            colorInterpolation: "color-interpolation",
            colorInterpolationFilters: "color-interpolation-filters",
            colorProfile: "color-profile",
            colorRendering: "color-rendering",
            contentScriptType: "contentScriptType",
            contentStyleType: "contentStyleType",
            cursor: 0,
            cx: 0,
            cy: 0,
            d: 0,
            decelerate: 0,
            descent: 0,
            diffuseConstant: "diffuseConstant",
            direction: 0,
            display: 0,
            divisor: 0,
            dominantBaseline: "dominant-baseline",
            dur: 0,
            dx: 0,
            dy: 0,
            edgeMode: "edgeMode",
            elevation: 0,
            enableBackground: "enable-background",
            end: 0,
            exponent: 0,
            externalResourcesRequired: "externalResourcesRequired",
            fill: 0,
            fillOpacity: "fill-opacity",
            fillRule: "fill-rule",
            filter: 0,
            filterRes: "filterRes",
            filterUnits: "filterUnits",
            floodColor: "flood-color",
            floodOpacity: "flood-opacity",
            focusable: 0,
            fontFamily: "font-family",
            fontSize: "font-size",
            fontSizeAdjust: "font-size-adjust",
            fontStretch: "font-stretch",
            fontStyle: "font-style",
            fontVariant: "font-variant",
            fontWeight: "font-weight",
            format: 0,
            from: 0,
            fx: 0,
            fy: 0,
            g1: 0,
            g2: 0,
            glyphName: "glyph-name",
            glyphOrientationHorizontal: "glyph-orientation-horizontal",
            glyphOrientationVertical: "glyph-orientation-vertical",
            glyphRef: "glyphRef",
            gradientTransform: "gradientTransform",
            gradientUnits: "gradientUnits",
            hanging: 0,
            horizAdvX: "horiz-adv-x",
            horizOriginX: "horiz-origin-x",
            ideographic: 0,
            imageRendering: "image-rendering",
            "in": 0,
            in2: 0,
            intercept: 0,
            k: 0,
            k1: 0,
            k2: 0,
            k3: 0,
            k4: 0,
            kernelMatrix: "kernelMatrix",
            kernelUnitLength: "kernelUnitLength",
            kerning: 0,
            keyPoints: "keyPoints",
            keySplines: "keySplines",
            keyTimes: "keyTimes",
            lengthAdjust: "lengthAdjust",
            letterSpacing: "letter-spacing",
            lightingColor: "lighting-color",
            limitingConeAngle: "limitingConeAngle",
            local: 0,
            markerEnd: "marker-end",
            markerMid: "marker-mid",
            markerStart: "marker-start",
            markerHeight: "markerHeight",
            markerUnits: "markerUnits",
            markerWidth: "markerWidth",
            mask: 0,
            maskContentUnits: "maskContentUnits",
            maskUnits: "maskUnits",
            mathematical: 0,
            mode: 0,
            numOctaves: "numOctaves",
            offset: 0,
            opacity: 0,
            operator: 0,
            order: 0,
            orient: 0,
            orientation: 0,
            origin: 0,
            overflow: 0,
            overlinePosition: "overline-position",
            overlineThickness: "overline-thickness",
            paintOrder: "paint-order",
            panose1: "panose-1",
            pathLength: "pathLength",
            patternContentUnits: "patternContentUnits",
            patternTransform: "patternTransform",
            patternUnits: "patternUnits",
            pointerEvents: "pointer-events",
            points: 0,
            pointsAtX: "pointsAtX",
            pointsAtY: "pointsAtY",
            pointsAtZ: "pointsAtZ",
            preserveAlpha: "preserveAlpha",
            preserveAspectRatio: "preserveAspectRatio",
            primitiveUnits: "primitiveUnits",
            r: 0,
            radius: 0,
            refX: "refX",
            refY: "refY",
            renderingIntent: "rendering-intent",
            repeatCount: "repeatCount",
            repeatDur: "repeatDur",
            requiredExtensions: "requiredExtensions",
            requiredFeatures: "requiredFeatures",
            restart: 0,
            result: 0,
            rotate: 0,
            rx: 0,
            ry: 0,
            scale: 0,
            seed: 0,
            shapeRendering: "shape-rendering",
            slope: 0,
            spacing: 0,
            specularConstant: "specularConstant",
            specularExponent: "specularExponent",
            speed: 0,
            spreadMethod: "spreadMethod",
            startOffset: "startOffset",
            stdDeviation: "stdDeviation",
            stemh: 0,
            stemv: 0,
            stitchTiles: "stitchTiles",
            stopColor: "stop-color",
            stopOpacity: "stop-opacity",
            strikethroughPosition: "strikethrough-position",
            strikethroughThickness: "strikethrough-thickness",
            string: 0,
            stroke: 0,
            strokeDasharray: "stroke-dasharray",
            strokeDashoffset: "stroke-dashoffset",
            strokeLinecap: "stroke-linecap",
            strokeLinejoin: "stroke-linejoin",
            strokeMiterlimit: "stroke-miterlimit",
            strokeOpacity: "stroke-opacity",
            strokeWidth: "stroke-width",
            surfaceScale: "surfaceScale",
            systemLanguage: "systemLanguage",
            tableValues: "tableValues",
            targetX: "targetX",
            targetY: "targetY",
            textAnchor: "text-anchor",
            textDecoration: "text-decoration",
            textRendering: "text-rendering",
            textLength: "textLength",
            to: 0,
            transform: 0,
            u1: 0,
            u2: 0,
            underlinePosition: "underline-position",
            underlineThickness: "underline-thickness",
            unicode: 0,
            unicodeBidi: "unicode-bidi",
            unicodeRange: "unicode-range",
            unitsPerEm: "units-per-em",
            vAlphabetic: "v-alphabetic",
            vHanging: "v-hanging",
            vIdeographic: "v-ideographic",
            vMathematical: "v-mathematical",
            values: 0,
            vectorEffect: "vector-effect",
            version: 0,
            vertAdvY: "vert-adv-y",
            vertOriginX: "vert-origin-x",
            vertOriginY: "vert-origin-y",
            viewBox: "viewBox",
            viewTarget: "viewTarget",
            visibility: 0,
            widths: 0,
            wordSpacing: "word-spacing",
            writingMode: "writing-mode",
            x: 0,
            xHeight: "x-height",
            x1: 0,
            x2: 0,
            xChannelSelector: "xChannelSelector",
            xlinkActuate: "xlink:actuate",
            xlinkArcrole: "xlink:arcrole",
            xlinkHref: "xlink:href",
            xlinkRole: "xlink:role",
            xlinkShow: "xlink:show",
            xlinkTitle: "xlink:title",
            xlinkType: "xlink:type",
            xmlBase: "xml:base",
            xmlns: 0,
            xmlnsXlink: "xmlns:xlink",
            xmlLang: "xml:lang",
            xmlSpace: "xml:space",
            y: 0,
            y1: 0,
            y2: 0,
            yChannelSelector: "yChannelSelector",
            z: 0,
            zoomAndPan: "zoomAndPan"
        },
        Nt = {
            props: {},
            attrNS: {
                xlinkActuate: Et,
                xlinkArcrole: Et,
                xlinkHref: Et,
                xlinkRole: Et,
                xlinkShow: Et,
                xlinkTitle: Et,
                xlinkType: Et,
                xmlBase: yt,
                xmlLang: yt,
                xmlSpace: yt
            },
            domAttrs: {},
            domProps: {}
        };
    Object.keys(St).map(function (e) {
        Nt.props[e] = 0, St[e] && (Nt.domAttrs[e] = St[e])
    }), Y(gt), Y(Nt);
    var bt = Array.isArray,
        wt = 0,
        _t = /^on/i;
    Object.freeze || (Object.freeze = te);
    var kt = {},
        Ct = {},
        Tt = Object.freeze({
            render: me,
            unstable_renderSubtreeIntoContainer: he,
            unmountComponentAtNode: ve,
            findDOMNode: ge
        }),
        Ot = "a|abbr|address|area|article|aside|audio|b|base|bdi|bdo|big|blockquote|body|br|button|canvas|caption|cite|code|col|colgroup|data|datalist|dd|del|details|dfn|dialog|div|dl|dt|em|embed|fieldset|figcaption|figure|footer|form|h1|h2|h3|h4|h5|h6|head|header|hgroup|hr|html|i|iframe|img|input|ins|kbd|keygen|label|legend|li|link|main|map|mark|menu|menuitem|meta|meter|nav|noscript|object|ol|optgroup|option|output|p|param|picture|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|source|span|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|u|ul|var|video|wbr|circle|clipPath|defs|ellipse|g|image|line|linearGradient|mask|path|pattern|polygon|polyline|radialGradient|rect|stop|svg|text|tspan",
        At = {};
    Ot.split("|").forEach(function (e) {
        At[e] = Ne(e)
    });
    var xt = function Dt() {
        return Dt
    };
    xt.isRequired = xt;
    var It = {
        array: xt,
        bool: xt,
        func: xt,
        number: xt,
        object: xt,
        string: xt,
        any: xt,
        arrayOf: xt,
        element: xt,
        instanceOf: xt,
        node: xt,
        objectOf: xt,
        oneOf: xt,
        oneOfType: xt,
        shape: xt
    },
        Rt = /\/(?!\/)/g,
        Pt = Object.freeze({
            only: be,
            forEach: we,
            map: _e,
            count: ke,
            toArray: Ce
        }),
        Mt = function () { };
    Mt.prototype = D.prototype, De.prototype = Object.create(D.prototype), De.prototype.constructor = De, De.prototype.isPureReactComponent = !0, De.prototype.shouldComponentUpdate = Ue;
    var Lt = oe({
        version: "0.15.1",
        cloneElement: Se,
        isValidElement: ye,
        createElement: Ee,
        createFactory: Ne,
        Component: D,
        PureComponent: De,
        createClass: Me,
        Children: Pt,
        PropTypes: It,
        DOM: At
    }, Tt);
    Lt.__SECRET_DOM_DO_NOT_USE_OR_YOU_WILL_BE_FIRED = Tt, e.exports = Lt
}, function (e, t) {
    "use strict";
    t.__esModule = !0, t["default"] = function (e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(105),
        __esModule: !0
    }
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var r = n(101),
        o = a(r),
        i = n(100),
        l = a(i),
        u = n(63),
        c = a(u);
    t["default"] = function (e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + ("undefined" == typeof t ? "undefined" : (0, c["default"])(t)));
        e.prototype = (0, l["default"])(t && t.prototype, {
            constructor: {
                value: e,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), t && (o["default"] ? (0, o["default"])(e, t) : e.__proto__ = t)
    }
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var r = n(63),
        o = a(r);
    t["default"] = function (e, t) {
        if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !t || "object" !== ("undefined" == typeof t ? "undefined" : (0, o["default"])(t)) && "function" != typeof t ? e : t
    }
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var r = n(143),
        o = a(r);
    t["default"] = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var a = t[n];
                a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), (0, o["default"])(e, a.key, a)
            }
        }
        return function (t, n, a) {
            return n && e(t.prototype, n), a && e(t, a), t
        }
    } ()
}, function (e, t, n) {
    "use strict";
    var a = [],
        r = {
            SESSION: "SESSION",
            POPUP: "POPUP",
            PANEL: "PANEL",
            TAB: "TAB",
            NOOP: function () { },
            to: function (e, t) {
                var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {},
                    a = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : this.NOOP;
                e === this.TAB ? chrome.tabs.query({
                    active: !0,
                    currentWindow: !0
                }, function (r) {
                    chrome.tabs.sendMessage(r[0].id, {
                        action: t,
                        payload: n,
                        destination: e
                    }, function (e) {
                        a(e)
                    })
                }) : chrome.runtime.sendMessage({
                    action: t,
                    payload: n,
                    destination: e
                }, function (e) {
                    a(e)
                })
            },
            toAll: function (e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                    n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : this.NOOP;
                chrome.runtime.sendMessage({
                    action: e,
                    payload: t,
                    destination: "all"
                }, function (e) {
                    n(e)
                }), "undefined" != typeof chrome.tabs && chrome.tabs.query({
                    active: !0
                }, function (a) {
                    a.forEach(function (a) {
                        chrome.tabs.sendMessage(a.id, {
                            action: e,
                            payload: t,
                            destination: "all"
                        }, function (e) {
                            n(e)
                        })
                    })
                }), a.length > 0 && a.forEach(function (n) {
                    n.postMessage({
                        action: e,
                        payload: t
                    })
                })
            },
            onMessageFor: function (e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : this.NOOP;
                chrome.runtime.onMessage.addListener(function (n, a, r) {
                    n.destination !== e && "all" !== n.destination || t(n, a, r)
                })
            },
            addDevToolWindow: function (e) {
                a.push(e)
            },
            removeDevToolWindow: function (e) {
                var t = a.indexOf(e);
                t !== -1 && a.splice(t, 1)
            }
        };
    e.exports = r
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.Tree = t.Modal = t.Icon = t.Dropdown = t.EditableLabel = t.Window = void 0;
    var r = n(254),
        o = a(r),
        i = n(242),
        l = a(i),
        u = n(241),
        c = a(u),
        s = n(243),
        d = a(s),
        f = n(244),
        p = a(f),
        m = n(252),
        h = a(m);
    t.Window = o["default"], t.EditableLabel = l["default"], t.Dropdown = c["default"], t.Icon = d["default"], t.Modal = p["default"], t.Tree = h["default"]
}, function (e, t) {
    var n = e.exports = {
        version: "2.4.0"
    };
    "number" == typeof __e && (__e = n)
}, function (e, t) {
    var n = e.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")();
    "number" == typeof __g && (__g = n)
}, function (e, t, n) {
    var a = n(87),
        r = "object" == typeof self && self && self.Object === Object && self,
        o = a || r || Function("return this")();
    e.exports = o
}, function (e, t) {
    var n = Array.isArray;
    e.exports = n
}, function (e, t) {
    var n = {}.hasOwnProperty;
    e.exports = function (e, t) {
        return n.call(e, t)
    }
}, function (e, t, n) {
    var a = n(76),
        r = n(34);
    e.exports = function (e) {
        return a(r(e))
    }
}, function (e, t, n) {
    e.exports = !n(20)(function () {
        return 7 != Object.defineProperty({}, "a", {
            get: function () {
                return 7
            }
        }).a
    })
}, function (e, t, n) {
    var a = n(21),
        r = n(67),
        o = n(43),
        i = Object.defineProperty;
    t.f = n(15) ? Object.defineProperty : function (e, t, n) {
        if (a(e), t = o(t, !0), a(n), r) try {
            return i(e, t, n)
        } catch (l) { }
        if ("get" in n || "set" in n) throw TypeError("Accessors not supported!");
        return "value" in n && (e[t] = n.value), e
    }
}, function (e, t, n) {
    var a = n(10),
        r = n(9),
        o = n(65),
        i = n(18),
        l = "prototype",
        u = function (e, t, n) {
            var c, s, d, f = e & u.F,
                p = e & u.G,
                m = e & u.S,
                h = e & u.P,
                v = e & u.B,
                g = e & u.W,
                E = p ? r : r[t] || (r[t] = {}),
                y = E[l],
                S = p ? a : m ? a[t] : (a[t] || {})[l];
            p && (n = t);
            for (c in n) s = !f && S && void 0 !== S[c], s && c in E || (d = s ? S[c] : n[c], E[c] = p && "function" != typeof S[c] ? n[c] : v && s ? o(d, a) : g && S[c] == d ? function (e) {
                var t = function (t, n, a) {
                    if (this instanceof e) {
                        switch (arguments.length) {
                            case 0:
                                return new e;
                            case 1:
                                return new e(t);
                            case 2:
                                return new e(t, n)
                        }
                        return new e(t, n, a)
                    }
                    return e.apply(this, arguments)
                };
                return t[l] = e[l], t
            } (d) : h && "function" == typeof d ? o(Function.call, d) : d, h && ((E.virtual || (E.virtual = {}))[c] = d, e & u.R && y && !y[c] && i(y, c, d)))
        };
    u.F = 1, u.G = 2, u.S = 4, u.P = 8, u.B = 16, u.W = 32, u.U = 64, u.R = 128, e.exports = u
}, function (e, t, n) {
    var a = n(16),
        r = n(29);
    e.exports = n(15) ? function (e, t, n) {
        return a.f(e, t, r(1, n))
    } : function (e, t, n) {
        return e[t] = n, e
    }
}, function (e, t, n) {
    var a = n(41)("wks"),
        r = n(30),
        o = n(10).Symbol,
        i = "function" == typeof o,
        l = e.exports = function (e) {
            return a[e] || (a[e] = i && o[e] || (i ? o : r)("Symbol." + e))
        };
    l.store = a
}, function (e, t) {
    e.exports = function (e) {
        try {
            return !!e()
        } catch (t) {
            return !0
        }
    }
}, function (e, t, n) {
    var a = n(22);
    e.exports = function (e) {
        if (!a(e)) throw TypeError(e + " is not an object!");
        return e
    }
}, function (e, t) {
    e.exports = function (e) {
        return "object" == typeof e ? null !== e : "function" == typeof e
    }
}, function (e, t, n) {
    var a = n(72),
        r = n(35);
    e.exports = Object.keys || function (e) {
        return a(e, r)
    }
}, function (e, t, n) {
    function a(e) {
        return null == e ? void 0 === e ? u : l : c && c in Object(e) ? o(e) : i(e)
    }
    var r = n(47),
        o = n(183),
        i = n(208),
        l = "[object Null]",
        u = "[object Undefined]",
        c = r ? r.toStringTag : void 0;
    e.exports = a
}, function (e, t) {
    function n(e) {
        return null != e && "object" == typeof e
    }
    e.exports = n
}, function (e, t, n) {
    function a(e, t) {
        var n = o(e, t);
        return r(n) ? n : void 0
    }
    var r = n(168),
        o = n(185);
    e.exports = a
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(2),
        o = a(r),
        i = n(32),
        l = function u(e, t) {
            (0, o["default"])(this, u), this.name = "404", this.data = {}, this.id = (0, i.generate)(), e && (this.name = e), t && (this.data = t)
        };
    t["default"] = l
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.MetaScanAction = t.ScreenshotAction = t.PauseAction = t.RefreshAction = t.ForwardAction = t.BackAction = t.BlankAction = t.ElNotPresentAssertAction = t.ElPresentAssertAction = t.ValueAssertAction = t.TextRegexAssertAction = t.TextAssertAction = t.UrlChangeIndicatorAction = t.ComponentAction = t.PageloadAction = t.FullPageloadAction = t.AutoChangeWindowAction = t.ChangeWindowAction = t.PushstateAction = t.PopstateAction = t.ScrollElement = t.ScrollWindowToElement = t.ScrollWindow = t.ExecuteScriptAction = t.InputAction = t.BlurAction = t.FocusAction = t.MouseoverAction = t.MousedownAction = t.SubmitAction = t.KeydownAction = t.Action = t.META_SCAN = t.EXECUTE_SCRIPT = t.SCROLL_ELEMENT = t.SCROLL_WINDOW_ELEMENT = t.SCROLL_WINDOW = t.SUBMIT = t.BLUR = t.FOCUS = t.URL_CHANGE_INDICATOR = t.SCREENSHOT = t.PAUSE = t.COMPONENT = t.REFRESH = t.FORWARD = t.BACK = t.EL_NOT_PRESENT_ASSERT = t.EL_PRESENT_ASSERT = t.VALUE_ASSERT = t.TEXT_REGEX_ASSERT = t.PATH_ASSERT = t.TEXT_ASSERT = t.PAGELOAD = t.CHANGE_WINDOW_AUTO = t.CHANGE_WINDOW = t.FULL_PAGELOAD = t.PUSHSTATE = t.POPSTATE = t.INPUT = t.MOUSEOVER = t.MOUSEDOWN = t.KEYDOWN = t.BLANK = void 0;
    var r = n(3),
        o = a(r),
        i = n(5),
        l = a(i),
        u = n(4),
        c = a(u),
        s = n(2),
        d = a(s),
        f = n(32),
        p = t.BLANK = "BLANK",
        m = t.KEYDOWN = "KEYDOWN",
        h = t.MOUSEDOWN = "MOUSEDOWN",
        v = t.MOUSEOVER = "MOUSEOVER",
        g = t.INPUT = "INPUT",
        E = t.POPSTATE = "POPSTATE",
        y = t.PUSHSTATE = "PUSHSTATE",
        S = t.FULL_PAGELOAD = "FULL_PAGELOAD",
        N = t.CHANGE_WINDOW = "CHANGE_WINDOW",
        b = t.CHANGE_WINDOW_AUTO = "CHANGE_WINDOW_AUTO",
        w = t.PAGELOAD = "PAGELOAD",
        _ = t.TEXT_ASSERT = "TEXT_ASSERT",
        k = (t.PATH_ASSERT = "PATH_ASSERT", t.TEXT_REGEX_ASSERT = "TEXT_REGEX_ASSERT"),
        C = t.VALUE_ASSERT = "VALUE_ASSERT",
        T = t.EL_PRESENT_ASSERT = "EL_PRESENT_ASSERT",
        O = t.EL_NOT_PRESENT_ASSERT = "EL_NOT_PRESENT_ASSERT",
        A = t.BACK = "BACK",
        x = t.FORWARD = "FORWARD",
        I = t.REFRESH = "REFRESH",
        R = t.COMPONENT = "COMPONENT",
        P = t.PAUSE = "PAUSE",
        M = t.SCREENSHOT = "SCREENSHOT",
        L = t.URL_CHANGE_INDICATOR = "URL_CHANGE_INDICATOR",
        D = t.FOCUS = "FOCUS",
        U = t.BLUR = "BLUR",
        j = t.SUBMIT = "SUBMIT",
        W = t.SCROLL_WINDOW = "SCROLL_WINDOW",
        F = t.SCROLL_WINDOW_ELEMENT = "SCROLL_WINDOW_ELEMENT",
        V = t.SCROLL_ELEMENT = "SCROLL_ELEMENT",
        q = t.EXECUTE_SCRIPT = "EXECUTE_SCRIPT",
        B = t.META_SCAN = "META_SCAN",
        H = t.Action = function z(e, t, n) {
            (0, d["default"])(this, z), this.type = null, this.selector = "Unknown", this.nodeName = "Unknown", this.description = null, this.timeout = null, this.warnings = [], this.suggestions = [], this.id = (0, f.generate)(), "string" == typeof e && (this.selector = e), t && (this.warnings = t), n && (this.suggestions = n)
        };
    t.KeydownAction = function (e) {
        function t(e, n, a, r, i, u) {
            (0, d["default"])(this, t);
            var c = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, i, u));
            return c.keyValue = "", c.keyCode = "", c.inputValue = "", c.type = m, c.keyValue = n, c.keyCode = a, c.inputValue = r, c
        }
        return (0, c["default"])(t, e), t
    } (H), t.SubmitAction = function (e) {
        function t(e, n, a) {
            (0, d["default"])(this, t);
            var r = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, n, a));
            return r.type = j, r
        }
        return (0, c["default"])(t, e), t
    } (H), t.MousedownAction = function (e) {
        function t(e, n, a, r, i, u) {
            (0, d["default"])(this, t);
            var c = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, i, u));
            return c.textContent = null, c.textAsserted = !1, c.x = null, c.y = null, c.type = h, n && (c.textContent = n), a && (c.x = a), r && (c.y = r), c
        }
        return (0, c["default"])(t, e), t
    } (H), t.MouseoverAction = function (e) {
        function t(e, n, a) {
            (0, d["default"])(this, t);
            var r = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, n, a));
            return r.type = v, r
        }
        return (0, c["default"])(t, e), t
    } (H), t.FocusAction = function (e) {
        function t(e, n, a) {
            (0, d["default"])(this, t);
            var r = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, n, a));
            return r.type = D, r
        }
        return (0, c["default"])(t, e), t
    } (H), t.BlurAction = function (e) {
        function t(e, n, a) {
            (0, d["default"])(this, t);
            var r = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, n, a));
            return r.type = U, r
        }
        return (0, c["default"])(t, e), t
    } (H), t.InputAction = function (e) {
        function t(e, n, a, r, i) {
            (0, d["default"])(this, t);
            var u = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, r, i));
            return u.value = null, u.inputType = null, u.type = g, u.inputType = n, u.value = a, u
        }
        return (0, c["default"])(t, e), t
    } (H), t.ExecuteScriptAction = function (e) {
        function t() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
                n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
            (0, d["default"])(this, t);
            var a = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return a.script = null, a.description = "", a.type = q, a.script = e, a.description = n, a
        }
        return (0, c["default"])(t, e), t
    } (H), t.ScrollWindow = function (e) {
        function t() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
            arguments[2], arguments[3];
            (0, d["default"])(this, t);
            var a = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return a.x = 0, a.y = 0, a.type = W, a.x = e, a.y = n, a
        }
        return (0, c["default"])(t, e), t
    } (H), t.ScrollWindowToElement = function (e) {
        function t(e, n, a) {
            (0, d["default"])(this, t);
            var r = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, n, a));
            return r.type = F, r
        }
        return (0, c["default"])(t, e), t
    } (H), t.ScrollElement = function (e) {
        function t() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0,
                n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
            (0, d["default"])(this, t);
            var a = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return a.x = 0, a.y = 0, a.type = V, a.x = e, a.y = n, a
        }
        return (0, c["default"])(t, e), t
    } (H), t.PopstateAction = function (e) {
        function t(e, n) {
            (0, d["default"])(this, t);
            var a = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return a.value = "", a.index = null, a.type = E, a.value = e, a.index = n, a
        }
        return (0, c["default"])(t, e), t
    } (H), t.PushstateAction = function (e) {
        function t(e, n) {
            (0, d["default"])(this, t);
            var a = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return a.value = "", a.index = null, a.type = y, a.value = e, a.index = n, a
        }
        return (0, c["default"])(t, e), t
    } (H), t.ChangeWindowAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return n.value = 0, n.value = e, n.type = N, n
        }
        return (0, c["default"])(t, e), t
    } (H), t.AutoChangeWindowAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return n.value = 0, n.value = e, n.type = b, n
        }
        return (0, c["default"])(t, e), t
    } (H), t.FullPageloadAction = function (e) {
        function t() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "Set url...",
                n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 500,
                a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 500;
            (0, d["default"])(this, t);
            var r = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return r.value = "", r.isInitialLoad = !1, r.width = 500, r.height = 500, r.type = S, r.value = e, r.width = n, r.height = a, r
        }
        return (0, c["default"])(t, e), t
    } (H), t.PageloadAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return n.value = "", n.type = w, e && (n.value = e), n
        }
        return (0, c["default"])(t, e), t
    } (H), t.ComponentAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return n.componentId = null, n.variables = [], n.type = R, e && (n.componentId = e), n
        }
        return (0, c["default"])(t, e), t
    } (H), t.UrlChangeIndicatorAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return n.value = "", n.index = null, n.value = e, n.type = L, n
        }
        return (0, c["default"])(t, e), t
    } (H), t.TextAssertAction = function (e) {
        function t(e, n, a, r) {
            (0, d["default"])(this, t);
            var i = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, a, r));
            return i.value = "", i.type = _, i.value = n, i
        }
        return (0, c["default"])(t, e), t
    } (H), t.TextRegexAssertAction = function (e) {
        function t(e, n, a, r) {
            (0, d["default"])(this, t);
            var i = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, a, r));
            return i.value = ".+", i.type = k, i.value = n, i
        }
        return (0, c["default"])(t, e), t
    } (H), t.ValueAssertAction = function (e) {
        function t(e, n, a, r) {
            (0, d["default"])(this, t);
            var i = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, a, r));
            return i.value = "", i.type = C, i.value = n, i
        }
        return (0, c["default"])(t, e), t
    } (H), t.ElPresentAssertAction = function (e) {
        function t(e, n, a) {
            (0, d["default"])(this, t);
            var r = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, n, a));
            return r.type = T, r
        }
        return (0, c["default"])(t, e), t
    } (H), t.ElNotPresentAssertAction = function (e) {
        function t(e, n, a) {
            (0, d["default"])(this, t);
            var r = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e, n, a));
            return r.type = O, r
        }
        return (0, c["default"])(t, e), t
    } (H), t.BlankAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return e.type = p, e
        }
        return (0, c["default"])(t, e), t
    } (H), t.BackAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return e.type = A, e
        }
        return (0, c["default"])(t, e), t
    } (H), t.ForwardAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return e.type = x, e
        }
        return (0, c["default"])(t, e), t
    } (H), t.RefreshAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return e.type = I, e
        }
        return (0, c["default"])(t, e), t
    } (H), t.PauseAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return n.value = 2e3, n.type = P, e && (n.value = e), n
        }
        return (0, c["default"])(t, e), t
    } (H), t.ScreenshotAction = function (e) {
        function t(e) {
            (0, d["default"])(this, t);
            var n = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return n.value = "<filename>", n.type = M, e && (n.value = e), n
        }
        return (0, c["default"])(t, e), t
    } (H), t.MetaScanAction = function (e) {
        function t() {
            (0, d["default"])(this, t);
            var e = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this));
            return e.pTitle = null, e.pDescription = null, e.type = B, e
        }
        return (0, c["default"])(t, e), t
    } (H)
}, function (e, t) {
    e.exports = function (e, t) {
        return {
            enumerable: !(1 & e),
            configurable: !(2 & e),
            writable: !(4 & e),
            value: t
        }
    }
}, function (e, t) {
    var n = 0,
        a = Math.random();
    e.exports = function (e) {
        return "Symbol(".concat(void 0 === e ? "" : e, ")_", (++n + a).toString(36))
    }
}, function (e, t) {
    t.f = {}.propertyIsEnumerable
}, function (e, t, n) {
    "use strict";
    e.exports = n(232)
}, function (e, t) {
    function n(e) {
        var t = typeof e;
        return null != e && ("object" == t || "function" == t)
    }
    e.exports = n
}, function (e, t) {
    e.exports = function (e) {
        if (void 0 == e) throw TypeError("Can't call method on  " + e);
        return e
    }
}, function (e, t) {
    e.exports = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")
}, function (e, t) {
    e.exports = {}
}, function (e, t) {
    e.exports = !0
}, function (e, t, n) {
    var a = n(21),
        r = n(119),
        o = n(35),
        i = n(40)("IE_PROTO"),
        l = function () { },
        u = "prototype",
        c = function () {
            var e, t = n(66)("iframe"),
                a = o.length,
                r = "<",
                i = ">";
            for (t.style.display = "none", n(113).appendChild(t), t.src = "javascript:", e = t.contentWindow.document, e.open(), e.write(r + "script" + i + "document.F=Object" + r + "/script" + i), e.close(), c = e.F; a--;) delete c[u][o[a]];
            return c()
        };
    e.exports = Object.create || function (e, t) {
        var n;
        return null !== e ? (l[u] = a(e), n = new l, l[u] = null, n[i] = e) : n = c(), void 0 === t ? n : r(n, t)
    }
}, function (e, t, n) {
    var a = n(16).f,
        r = n(13),
        o = n(19)("toStringTag");
    e.exports = function (e, t, n) {
        e && !r(e = n ? e : e.prototype, o) && a(e, o, {
            configurable: !0,
            value: t
        })
    }
}, function (e, t, n) {
    var a = n(41)("keys"),
        r = n(30);
    e.exports = function (e) {
        return a[e] || (a[e] = r(e))
    }
}, function (e, t, n) {
    var a = n(10),
        r = "__core-js_shared__",
        o = a[r] || (a[r] = {});
    e.exports = function (e) {
        return o[e] || (o[e] = {})
    }
}, function (e, t) {
    var n = Math.ceil,
        a = Math.floor;
    e.exports = function (e) {
        return isNaN(e = +e) ? 0 : (e > 0 ? a : n)(e)
    }
}, function (e, t, n) {
    var a = n(22);
    e.exports = function (e, t) {
        if (!a(e)) return e;
        var n, r;
        if (t && "function" == typeof (n = e.toString) && !a(r = n.call(e))) return r;
        if ("function" == typeof (n = e.valueOf) && !a(r = n.call(e))) return r;
        if (!t && "function" == typeof (n = e.toString) && !a(r = n.call(e))) return r;
        throw TypeError("Can't convert object to primitive value")
    }
}, function (e, t, n) {
    var a = n(10),
        r = n(9),
        o = n(37),
        i = n(45),
        l = n(16).f;
    e.exports = function (e) {
        var t = r.Symbol || (r.Symbol = o ? {} : a.Symbol || {});
        "_" == e.charAt(0) || e in t || l(t, e, {
            value: i.f(e)
        })
    }
}, function (e, t, n) {
    t.f = n(19)
}, function (e, t, n) {
    function a(e) {
        var t = -1,
            n = null == e ? 0 : e.length;
        for (this.clear(); ++t < n;) {
            var a = e[t];
            this.set(a[0], a[1])
        }
    }
    var r = n(194),
        o = n(195),
        i = n(196),
        l = n(197),
        u = n(198);
    a.prototype.clear = r, a.prototype["delete"] = o, a.prototype.get = i, a.prototype.has = l, a.prototype.set = u, e.exports = a
}, function (e, t, n) {
    var a = n(11),
        r = a.Symbol;
    e.exports = r
}, function (e, t, n) {
    function a(e, t) {
        for (var n = e.length; n--;)
            if (r(e[n][0], t)) return n;
        return -1
    }
    var r = n(92);
    e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        var n = e.__data__;
        return r(t) ? n["string" == typeof t ? "string" : "hash"] : n.map
    }
    var r = n(192);
    e.exports = a
}, function (e, t, n) {
    var a = n(26),
        r = a(Object, "create");
    e.exports = r
}, function (e, t, n) {
    function a(e) {
        if ("string" == typeof e || r(e)) return e;
        var t = e + "";
        return "0" == t && 1 / e == -o ? "-0" : t
    }
    var r = n(52),
        o = 1 / 0;
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        return "symbol" == typeof e || o(e) && r(e) == i
    }
    var r = n(24),
        o = n(25),
        i = "[object Symbol]";
    e.exports = a
}, function (e, t) {
    t.f = Object.getOwnPropertySymbols
}, function (e, t, n) {
    var a = n(34);
    e.exports = function (e) {
        return Object(a(e))
    }
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var r = n(96),
        o = a(r);
    t["default"] = o["default"] || function (e) {
        for (var t = 1; t < arguments.length; t++) {
            var n = arguments[t];
            for (var a in n) Object.prototype.hasOwnProperty.call(n, a) && (e[a] = n[a])
        }
        return e
    }
}, function (e, t, n) {
    function a(e, t, n) {
        var a = null == e ? 0 : e.length;
        if (!a) return -1;
        var u = null == n ? 0 : i(n);
        return u < 0 && (u = l(a + u, 0)), r(e, o(t, 3), u)
    }
    var r = n(162),
        o = n(97),
        i = n(228),
        l = Math.max;
    e.exports = a
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = (n(8), function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props,
                        t = e.isAutoSaving,
                        n = e.isDevtool;
                    return h["default"].createElement("div", {
                        className: "view-mode-dd"
                    }, n && h["default"].createElement("button", {
                        className: "btn btn-primary",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "openWindow", !0)
                        }
                    }, "popout"), t && h["default"].createElement("div", {
                        className: "auto-saving"
                    }, "Autosaving..."))
                }
            }]), t
        } (h["default"].PureComponent));
    t["default"] = E
}, function (e, t, n) {
    var a = n(26),
        r = n(11),
        o = a(r, "Map");
    e.exports = o
}, function (e, t, n) {
    function a(e) {
        var t = -1,
            n = null == e ? 0 : e.length;
        for (this.clear(); ++t < n;) {
            var a = e[t];
            this.set(a[0], a[1])
        }
    }
    var r = n(199),
        o = n(200),
        i = n(201),
        l = n(202),
        u = n(203);
    a.prototype.clear = r, a.prototype["delete"] = o, a.prototype.get = i, a.prototype.has = l, a.prototype.set = u, e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        if (r(e)) return !1;
        var n = typeof e;
        return !("number" != n && "symbol" != n && "boolean" != n && null != e && !o(e)) || (l.test(e) || !i.test(e) || null != t && e in Object(t))
    }
    var r = n(12),
        o = n(52),
        i = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
        l = /^\w*$/;
    e.exports = a
}, function (e, t) {
    function n(e) {
        return "number" == typeof e && e > -1 && e % 1 == 0 && e <= a
    }
    var a = 9007199254740991;
    e.exports = n
}, function (e, t, n) {
    "use strict";

    function a() {
        f = !1
    }

    function r(e) {
        if (!e) return void (s !== m && (s = m, a()));
        if (e !== s) {
            if (e.length !== m.length) throw new Error("Custom alphabet for shortid must be " + m.length + " unique characters. You submitted " + e.length + " characters: " + e);
            var t = e.split("").filter(function (e, t, n) {
                return t !== n.lastIndexOf(e)
            });
            if (t.length) throw new Error("Custom alphabet for shortid must be " + m.length + " unique characters. These characters were not unique: " + t.join(", "));
            s = e, a()
        }
    }

    function o(e) {
        return r(e), s
    }

    function i(e) {
        p.seed(e), d !== e && (a(), d = e)
    }

    function l() {
        s || r(m);
        for (var e, t = s.split(""), n = [], a = p.nextValue(); t.length > 0;) a = p.nextValue(), e = Math.floor(a * t.length), n.push(t.splice(e, 1)[0]);
        return n.join("")
    }

    function u() {
        return f ? f : f = l()
    }

    function c(e) {
        var t = u();
        return t[e]
    }
    var s, d, f, p = n(235),
        m = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
    e.exports = {
        characters: o,
        seed: i,
        lookup: c,
        shuffled: u
    }
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    t.__esModule = !0;
    var r = n(103),
        o = a(r),
        i = n(102),
        l = a(i),
        u = "function" == typeof l["default"] && "symbol" == typeof o["default"] ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof l["default"] && e.constructor === l["default"] && e !== l["default"].prototype ? "symbol" : typeof e
        };
    t["default"] = "function" == typeof l["default"] && "symbol" === u(o["default"]) ? function (e) {
        return "undefined" == typeof e ? "undefined" : u(e)
    } : function (e) {
        return e && "function" == typeof l["default"] && e.constructor === l["default"] && e !== l["default"].prototype ? "symbol" : "undefined" == typeof e ? "undefined" : u(e)
    }
}, function (e, t) {
    var n = {}.toString;
    e.exports = function (e) {
        return n.call(e).slice(8, -1)
    }
}, function (e, t, n) {
    var a = n(109);
    e.exports = function (e, t, n) {
        if (a(e), void 0 === t) return e;
        switch (n) {
            case 1:
                return function (n) {
                    return e.call(t, n)
                };
            case 2:
                return function (n, a) {
                    return e.call(t, n, a)
                };
            case 3:
                return function (n, a, r) {
                    return e.call(t, n, a, r)
                }
        }
        return function () {
            return e.apply(t, arguments)
        }
    }
}, function (e, t, n) {
    var a = n(22),
        r = n(10).document,
        o = a(r) && a(r.createElement);
    e.exports = function (e) {
        return o ? r.createElement(e) : {}
    }
}, function (e, t, n) {
    e.exports = !n(15) && !n(20)(function () {
        return 7 != Object.defineProperty(n(66)("div"), "a", {
            get: function () {
                return 7
            }
        }).a
    })
}, function (e, t, n) {
    "use strict";
    var a = n(37),
        r = n(17),
        o = n(73),
        i = n(18),
        l = n(13),
        u = n(36),
        c = n(115),
        s = n(39),
        d = n(71),
        f = n(19)("iterator"),
        p = !([].keys && "next" in [].keys()),
        m = "@@iterator",
        h = "keys",
        v = "values",
        g = function () {
            return this
        };
    e.exports = function (e, t, n, E, y, S, N) {
        c(n, t, E);
        var b, w, _, k = function (e) {
            if (!p && e in A) return A[e];
            switch (e) {
                case h:
                    return function () {
                        return new n(this, e)
                    };
                case v:
                    return function () {
                        return new n(this, e)
                    }
            }
            return function () {
                return new n(this, e)
            }
        },
            C = t + " Iterator",
            T = y == v,
            O = !1,
            A = e.prototype,
            x = A[f] || A[m] || y && A[y],
            I = x || k(y),
            R = y ? T ? k("entries") : I : void 0,
            P = "Array" == t ? A.entries || x : x;
        if (P && (_ = d(P.call(new e)), _ !== Object.prototype && (s(_, C, !0), a || l(_, f) || i(_, f, g))), T && x && x.name !== v && (O = !0, I = function () {
            return x.call(this)
        }), a && !N || !p && !O && A[f] || i(A, f, I), u[t] = I, u[C] = g, y)
            if (b = {
                values: T ? I : k(v),
                keys: S ? I : k(h),
                entries: R
            }, N)
                for (w in b) w in A || o(A, w, b[w]);
            else r(r.P + r.F * (p || O), t, b);
        return b
    }
}, function (e, t, n) {
    var a = n(31),
        r = n(29),
        o = n(14),
        i = n(43),
        l = n(13),
        u = n(67),
        c = Object.getOwnPropertyDescriptor;
    t.f = n(15) ? c : function (e, t) {
        if (e = o(e), t = i(t, !0), u) try {
            return c(e, t)
        } catch (n) { }
        if (l(e, t)) return r(!a.f.call(e, t), e[t])
    }
}, function (e, t, n) {
    var a = n(72),
        r = n(35).concat("length", "prototype");
    t.f = Object.getOwnPropertyNames || function (e) {
        return a(e, r)
    }
}, function (e, t, n) {
    var a = n(13),
        r = n(54),
        o = n(40)("IE_PROTO"),
        i = Object.prototype;
    e.exports = Object.getPrototypeOf || function (e) {
        return e = r(e), a(e, o) ? e[o] : "function" == typeof e.constructor && e instanceof e.constructor ? e.constructor.prototype : e instanceof Object ? i : null
    }
}, function (e, t, n) {
    var a = n(13),
        r = n(14),
        o = n(111)(!1),
        i = n(40)("IE_PROTO");
    e.exports = function (e, t) {
        var n, l = r(e),
            u = 0,
            c = [];
        for (n in l) n != i && a(l, n) && c.push(n);
        for (; t.length > u;) a(l, n = t[u++]) && (~o(c, n) || c.push(n));
        return c
    }
}, function (e, t, n) {
    e.exports = n(18)
}, function (e, t, n) {
    function a(e) {
        if (!o(e)) return !1;
        var t = r(e);
        return t == l || t == u || t == i || t == c
    }
    var r = n(24),
        o = n(33),
        i = "[object AsyncFunction]",
        l = "[object Function]",
        u = "[object GeneratorFunction]",
        c = "[object Proxy]";
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        return i(e) ? r(e) : o(e)
    }
    var r = n(158),
        o = n(136),
        i = n(81);
    e.exports = a
}, function (e, t, n) {
    var a = n(64);
    e.exports = Object("z").propertyIsEnumerable(0) ? Object : function (e) {
        return "String" == a(e) ? e.split("") : Object(e)
    }
}, function (e, t, n) {
    var a = n(165),
        r = n(25),
        o = Object.prototype,
        i = o.hasOwnProperty,
        l = o.propertyIsEnumerable,
        u = a(function () {
            return arguments
        } ()) ? a : function (e) {
            return r(e) && i.call(e, "callee") && !l.call(e, "callee")
        };
    e.exports = u
}, function (e, t, n) {
    (function (e) {
        var a = n(11),
            r = n(226),
            o = "object" == typeof t && t && !t.nodeType && t,
            i = o && "object" == typeof e && e && !e.nodeType && e,
            l = i && i.exports === o,
            u = l ? a.Buffer : void 0,
            c = u ? u.isBuffer : void 0,
            s = c || r;
        e.exports = s
    }).call(t, n(95)(e))
}, function (e, t, n) {
    var a = n(169),
        r = n(176),
        o = n(207),
        i = o && o.isTypedArray,
        l = i ? r(i) : a;
    e.exports = l
}, function (e, t, n) {
    var a, r;
    /*!
    	  Copyright (c) 2016 Jed Watson.
    	  Licensed under the MIT License (MIT), see
    	  http://jedwatson.github.io/classnames
    	*/
    ! function () {
        "use strict";

        function n() {
            for (var e = [], t = 0; t < arguments.length; t++) {
                var a = arguments[t];
                if (a) {
                    var r = typeof a;
                    if ("string" === r || "number" === r) e.push(a);
                    else if (Array.isArray(a)) e.push(n.apply(null, a));
                    else if ("object" === r)
                        for (var i in a) o.call(a, i) && a[i] && e.push(i)
                }
            }
            return e.join(" ")
        }
        var o = {}.hasOwnProperty;
        "undefined" != typeof e && e.exports ? e.exports = n : (a = [], r = function () {
            return n
        }.apply(t, a), !(void 0 !== r && (e.exports = r)))
    } ()
}, function (e, t, n) {
    function a(e) {
        return null != e && o(e.length) && !r(e)
    }
    var r = n(74),
        o = n(61);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        var t = this.__data__ = new r(e);
        this.size = t.size
    }
    var r = n(46),
        o = n(213),
        i = n(214),
        l = n(215),
        u = n(216),
        c = n(217);
    a.prototype.clear = o, a.prototype["delete"] = i, a.prototype.get = l, a.prototype.has = u, a.prototype.set = c, e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        t = r(t, e);
        for (var n = 0, a = t.length; null != e && n < a;) e = e[o(t[n++])];
        return n && n == a ? e : void 0
    }
    var r = n(85),
        o = n(51);
    e.exports = a
}, function (e, t, n) {
    function a(e, t, n, i, l) {
        return e === t || (null == e || null == t || !o(e) && !o(t) ? e !== e && t !== t : r(e, t, n, i, a, l))
    }
    var r = n(166),
        o = n(25);
    e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        return r(e) ? e : o(e, t) ? [e] : i(l(e))
    }
    var r = n(12),
        o = n(60),
        i = n(218),
        l = n(229);
    e.exports = a
}, function (e, t, n) {
    function a(e, t, n, a, c, s) {
        var d = n & l,
            f = e.length,
            p = t.length;
        if (f != p && !(d && p > f)) return !1;
        var m = s.get(e);
        if (m && s.get(t)) return m == t;
        var h = -1,
            v = !0,
            g = n & u ? new r : void 0;
        for (s.set(e, t), s.set(t, e); ++h < f;) {
            var E = e[h],
                y = t[h];
            if (a) var S = d ? a(y, E, h, t, e, s) : a(E, y, h, e, t, s);
            if (void 0 !== S) {
                if (S) continue;
                v = !1;
                break
            }
            if (g) {
                if (!o(t, function (e, t) {
                    if (!i(g, t) && (E === e || c(E, e, n, a, s))) return g.push(t)
                })) {
                    v = !1;
                    break
                }
            } else if (E !== y && !c(E, y, n, a, s)) {
                v = !1;
                break
            }
        }
        return s["delete"](e), s["delete"](t), v
    }
    var r = n(154),
        o = n(161),
        i = n(177),
        l = 1,
        u = 2;
    e.exports = a
}, function (e, t) {
    (function (t) {
        var n = "object" == typeof t && t && t.Object === Object && t;
        e.exports = n
    }).call(t, function () {
        return this
    } ())
}, function (e, t) {
    function n(e, t) {
        return t = null == t ? a : t, !!t && ("number" == typeof e || r.test(e)) && e > -1 && e % 1 == 0 && e < t
    }
    var a = 9007199254740991,
        r = /^(?:0|[1-9]\d*)$/;
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        return e === e && !r(e)
    }
    var r = n(33);
    e.exports = a
}, function (e, t) {
    function n(e, t) {
        return function (n) {
            return null != n && (n[e] === t && (void 0 !== t || e in Object(n)))
        }
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        if (null != e) {
            try {
                return r.call(e)
            } catch (t) { }
            try {
                return e + ""
            } catch (t) { }
        }
        return ""
    }
    var a = Function.prototype,
        r = a.toString;
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        return e === t || e !== e && t !== t
    }
    e.exports = n
}, function (e, t, n) {
    var a = n(262),
        r = n(56),
        o = a(r);
    e.exports = o
}, function (e, t) {
    function n() { }
    e.exports = n
}, function (e, t) {
    e.exports = function (e) {
        return e.webpackPolyfill || (e.deprecate = function () { }, e.paths = [], e.children = [], e.webpackPolyfill = 1), e
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(144),
        __esModule: !0
    }
}, function (e, t, n) {
    function a(e) {
        return "function" == typeof e ? e : null == e ? i : "object" == typeof e ? l(e) ? o(e[0], e[1]) : r(e) : u(e)
    }
    var r = n(170),
        o = n(171),
        i = n(221),
        l = n(12),
        u = n(224);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        if ("number" == typeof e) return e;
        if (o(e)) return i;
        if (r(e)) {
            var t = "function" == typeof e.valueOf ? e.valueOf() : e;
            e = r(t) ? t + "" : t
        }
        if ("string" != typeof e) return 0 === e ? e : +e;
        e = e.replace(l, "");
        var n = c.test(e);
        return n || s.test(e) ? d(e.slice(2), n ? 2 : 8) : u.test(e) ? i : +e
    }
    var r = n(33),
        o = n(52),
        i = NaN,
        l = /^\s+|\s+$/g,
        u = /^[-+]0x[0-9a-f]+$/i,
        c = /^0b[01]+$/i,
        s = /^0o[0-7]+$/i,
        d = parseInt;
    e.exports = a
}, , function (e, t, n) {
    e.exports = {
        "default": n(104),
        __esModule: !0
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(106),
        __esModule: !0
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(107),
        __esModule: !0
    }
}, function (e, t, n) {
    e.exports = {
        "default": n(108),
        __esModule: !0
    }
}, function (e, t, n) {
    n(127);
    var a = n(9).Object;
    e.exports = function (e, t) {
        return a.create(e, t)
    }
}, function (e, t, n) {
    n(128), e.exports = n(9).Object.getPrototypeOf
}, function (e, t, n) {
    n(129), e.exports = n(9).Object.setPrototypeOf
}, function (e, t, n) {
    n(132), n(130), n(133), n(134), e.exports = n(9).Symbol
}, function (e, t, n) {
    n(131), n(135), e.exports = n(45).f("iterator")
}, function (e, t) {
    e.exports = function (e) {
        if ("function" != typeof e) throw TypeError(e + " is not a function!");
        return e
    }
}, function (e, t) {
    e.exports = function () { }
}, function (e, t, n) {
    var a = n(14),
        r = n(125),
        o = n(124);
    e.exports = function (e) {
        return function (t, n, i) {
            var l, u = a(t),
                c = r(u.length),
                s = o(i, c);
            if (e && n != n) {
                for (; c > s;)
                    if (l = u[s++], l != l) return !0
            } else
                for (; c > s; s++)
                    if ((e || s in u) && u[s] === n) return e || s || 0; return !e && -1
        }
    }
}, function (e, t, n) {
    var a = n(23),
        r = n(53),
        o = n(31);
    e.exports = function (e) {
        var t = a(e),
            n = r.f;
        if (n)
            for (var i, l = n(e), u = o.f, c = 0; l.length > c;) u.call(e, i = l[c++]) && t.push(i);
        return t
    }
}, function (e, t, n) {
    e.exports = n(10).document && document.documentElement
}, function (e, t, n) {
    var a = n(64);
    e.exports = Array.isArray || function (e) {
        return "Array" == a(e)
    }
}, function (e, t, n) {
    "use strict";
    var a = n(38),
        r = n(29),
        o = n(39),
        i = {};
    n(18)(i, n(19)("iterator"), function () {
        return this
    }), e.exports = function (e, t, n) {
        e.prototype = a(i, {
            next: r(1, n)
        }), o(e, t + " Iterator")
    }
}, function (e, t) {
    e.exports = function (e, t) {
        return {
            value: t,
            done: !!e
        }
    }
}, function (e, t, n) {
    var a = n(23),
        r = n(14);
    e.exports = function (e, t) {
        for (var n, o = r(e), i = a(o), l = i.length, u = 0; l > u;)
            if (o[n = i[u++]] === t) return n
    }
}, function (e, t, n) {
    var a = n(30)("meta"),
        r = n(22),
        o = n(13),
        i = n(16).f,
        l = 0,
        u = Object.isExtensible || function () {
            return !0
        },
        c = !n(20)(function () {
            return u(Object.preventExtensions({}))
        }),
        s = function (e) {
            i(e, a, {
                value: {
                    i: "O" + ++l,
                    w: {}
                }
            })
        },
        d = function (e, t) {
            if (!r(e)) return "symbol" == typeof e ? e : ("string" == typeof e ? "S" : "P") + e;
            if (!o(e, a)) {
                if (!u(e)) return "F";
                if (!t) return "E";
                s(e)
            }
            return e[a].i
        },
        f = function (e, t) {
            if (!o(e, a)) {
                if (!u(e)) return !0;
                if (!t) return !1;
                s(e)
            }
            return e[a].w
        },
        p = function (e) {
            return c && m.NEED && u(e) && !o(e, a) && s(e), e
        },
        m = e.exports = {
            KEY: a,
            NEED: !1,
            fastKey: d,
            getWeak: f,
            onFreeze: p
        }
}, function (e, t, n) {
    var a = n(16),
        r = n(21),
        o = n(23);
    e.exports = n(15) ? Object.defineProperties : function (e, t) {
        r(e);
        for (var n, i = o(t), l = i.length, u = 0; l > u;) a.f(e, n = i[u++], t[n]);
        return e
    }
}, function (e, t, n) {
    var a = n(14),
        r = n(70).f,
        o = {}.toString,
        i = "object" == typeof window && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [],
        l = function (e) {
            try {
                return r(e)
            } catch (t) {
                return i.slice()
            }
        };
    e.exports.f = function (e) {
        return i && "[object Window]" == o.call(e) ? l(e) : r(a(e))
    }
}, function (e, t, n) {
    var a = n(17),
        r = n(9),
        o = n(20);
    e.exports = function (e, t) {
        var n = (r.Object || {})[e] || Object[e],
            i = {};
        i[e] = t(n), a(a.S + a.F * o(function () {
            n(1)
        }), "Object", i)
    }
}, function (e, t, n) {
    var a = n(22),
        r = n(21),
        o = function (e, t) {
            if (r(e), !a(t) && null !== t) throw TypeError(t + ": can't set as prototype!")
        };
    e.exports = {
        set: Object.setPrototypeOf || ("__proto__" in {} ? function (e, t, a) {
            try {
                a = n(65)(Function.call, n(69).f(Object.prototype, "__proto__").set, 2), a(e, []), t = !(e instanceof Array)
            } catch (r) {
                t = !0
            }
            return function (e, n) {
                return o(e, n), t ? e.__proto__ = n : a(e, n), e
            }
        } ({}, !1) : void 0),
        check: o
    }
}, function (e, t, n) {
    var a = n(42),
        r = n(34);
    e.exports = function (e) {
        return function (t, n) {
            var o, i, l = String(r(t)),
                u = a(n),
                c = l.length;
            return u < 0 || u >= c ? e ? "" : void 0 : (o = l.charCodeAt(u), o < 55296 || o > 56319 || u + 1 === c || (i = l.charCodeAt(u + 1)) < 56320 || i > 57343 ? e ? l.charAt(u) : o : e ? l.slice(u, u + 2) : (o - 55296 << 10) + (i - 56320) + 65536)
        }
    }
}, function (e, t, n) {
    var a = n(42),
        r = Math.max,
        o = Math.min;
    e.exports = function (e, t) {
        return e = a(e), e < 0 ? r(e + t, 0) : o(e, t)
    }
}, function (e, t, n) {
    var a = n(42),
        r = Math.min;
    e.exports = function (e) {
        return e > 0 ? r(a(e), 9007199254740991) : 0
    }
}, function (e, t, n) {
    "use strict";
    var a = n(110),
        r = n(116),
        o = n(36),
        i = n(14);
    e.exports = n(68)(Array, "Array", function (e, t) {
        this._t = i(e), this._i = 0, this._k = t
    }, function () {
        var e = this._t,
            t = this._k,
            n = this._i++;
        return !e || n >= e.length ? (this._t = void 0, r(1)) : "keys" == t ? r(0, n) : "values" == t ? r(0, e[n]) : r(0, [n, e[n]])
    }, "values"), o.Arguments = o.Array, a("keys"), a("values"), a("entries")
}, function (e, t, n) {
    var a = n(17);
    a(a.S, "Object", {
        create: n(38)
    })
}, function (e, t, n) {
    var a = n(54),
        r = n(71);
    n(121)("getPrototypeOf", function () {
        return function (e) {
            return r(a(e))
        }
    })
}, function (e, t, n) {
    var a = n(17);
    a(a.S, "Object", {
        setPrototypeOf: n(122).set
    })
}, function (e, t) { }, function (e, t, n) {
    "use strict";
    var a = n(123)(!0);
    n(68)(String, "String", function (e) {
        this._t = String(e), this._i = 0
    }, function () {
        var e, t = this._t,
            n = this._i;
        return n >= t.length ? {
            value: void 0,
            done: !0
        } : (e = a(t, n), this._i += e.length, {
            value: e,
            done: !1
        })
    })
}, function (e, t, n) {
    "use strict";
    var a = n(10),
        r = n(13),
        o = n(15),
        i = n(17),
        l = n(73),
        u = n(118).KEY,
        c = n(20),
        s = n(41),
        d = n(39),
        f = n(30),
        p = n(19),
        m = n(45),
        h = n(44),
        v = n(117),
        g = n(112),
        E = n(114),
        y = n(21),
        S = n(14),
        N = n(43),
        b = n(29),
        w = n(38),
        _ = n(120),
        k = n(69),
        C = n(16),
        T = n(23),
        O = k.f,
        A = C.f,
        x = _.f,
        I = a.Symbol,
        R = a.JSON,
        P = R && R.stringify,
        M = "prototype",
        L = p("_hidden"),
        D = p("toPrimitive"),
        U = {}.propertyIsEnumerable,
        j = s("symbol-registry"),
        W = s("symbols"),
        F = s("op-symbols"),
        V = Object[M],
        q = "function" == typeof I,
        B = a.QObject,
        H = !B || !B[M] || !B[M].findChild,
        z = o && c(function () {
            return 7 != w(A({}, "a", {
                get: function () {
                    return A(this, "a", {
                        value: 7
                    }).a
                }
            })).a
        }) ? function (e, t, n) {
            var a = O(V, t);
            a && delete V[t], A(e, t, n), a && e !== V && A(V, t, a)
        } : A,
        G = function (e) {
            var t = W[e] = w(I[M]);
            return t._k = e, t
        },
        K = q && "symbol" == typeof I.iterator ? function (e) {
            return "symbol" == typeof e
        } : function (e) {
            return e instanceof I
        },
        X = function (e, t, n) {
            return e === V && X(F, t, n), y(e), t = N(t, !0), y(n), r(W, t) ? (n.enumerable ? (r(e, L) && e[L][t] && (e[L][t] = !1), n = w(n, {
                enumerable: b(0, !1)
            })) : (r(e, L) || A(e, L, b(1, {})), e[L][t] = !0), z(e, t, n)) : A(e, t, n)
        },
        Y = function (e, t) {
            y(e);
            for (var n, a = g(t = S(t)), r = 0, o = a.length; o > r;) X(e, n = a[r++], t[n]);
            return e
        },
        $ = function (e, t) {
            return void 0 === t ? w(e) : Y(w(e), t)
        },
        J = function (e) {
            var t = U.call(this, e = N(e, !0));
            return !(this === V && r(W, e) && !r(F, e)) && (!(t || !r(this, e) || !r(W, e) || r(this, L) && this[L][e]) || t)
        },
        Q = function (e, t) {
            if (e = S(e), t = N(t, !0), e !== V || !r(W, t) || r(F, t)) {
                var n = O(e, t);
                return !n || !r(W, t) || r(e, L) && e[L][t] || (n.enumerable = !0), n
            }
        },
        Z = function (e) {
            for (var t, n = x(S(e)), a = [], o = 0; n.length > o;) r(W, t = n[o++]) || t == L || t == u || a.push(t);
            return a
        },
        ee = function (e) {
            for (var t, n = e === V, a = x(n ? F : S(e)), o = [], i = 0; a.length > i;) !r(W, t = a[i++]) || n && !r(V, t) || o.push(W[t]);
            return o
        };
    q || (I = function () {
        if (this instanceof I) throw TypeError("Symbol is not a constructor!");
        var e = f(arguments.length > 0 ? arguments[0] : void 0),
            t = function (n) {
                this === V && t.call(F, n), r(this, L) && r(this[L], e) && (this[L][e] = !1), z(this, e, b(1, n))
            };
        return o && H && z(V, e, {
            configurable: !0,
            set: t
        }), G(e)
    }, l(I[M], "toString", function () {
        return this._k
    }), k.f = Q, C.f = X, n(70).f = _.f = Z, n(31).f = J, n(53).f = ee, o && !n(37) && l(V, "propertyIsEnumerable", J, !0), m.f = function (e) {
        return G(p(e))
    }), i(i.G + i.W + i.F * !q, {
        Symbol: I
    });
    for (var te = "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","), ne = 0; te.length > ne;) p(te[ne++]);
    for (var te = T(p.store), ne = 0; te.length > ne;) h(te[ne++]);
    i(i.S + i.F * !q, "Symbol", {
        "for": function (e) {
            return r(j, e += "") ? j[e] : j[e] = I(e)
        },
        keyFor: function (e) {
            if (K(e)) return v(j, e);
            throw TypeError(e + " is not a symbol!")
        },
        useSetter: function () {
            H = !0
        },
        useSimple: function () {
            H = !1
        }
    }), i(i.S + i.F * !q, "Object", {
        create: $,
        defineProperty: X,
        defineProperties: Y,
        getOwnPropertyDescriptor: Q,
        getOwnPropertyNames: Z,
        getOwnPropertySymbols: ee
    }), R && i(i.S + i.F * (!q || c(function () {
        var e = I();
        return "[null]" != P([e]) || "{}" != P({
            a: e
        }) || "{}" != P(Object(e))
    })), "JSON", {
            stringify: function (e) {
                if (void 0 !== e && !K(e)) {
                    for (var t, n, a = [e], r = 1; arguments.length > r;) a.push(arguments[r++]);
                    return t = a[1], "function" == typeof t && (n = t), !n && E(t) || (t = function (e, t) {
                        if (n && (t = n.call(this, e, t)), !K(t)) return t
                    }), a[1] = t, P.apply(R, a)
                }
            }
        }), I[M][D] || n(18)(I[M], D, I[M].valueOf), d(I, "Symbol"), d(Math, "Math", !0), d(a.JSON, "JSON", !0)
}, function (e, t, n) {
    n(44)("asyncIterator")
}, function (e, t, n) {
    n(44)("observable")
}, function (e, t, n) {
    n(126);
    for (var a = n(10), r = n(18), o = n(36), i = n(19)("toStringTag"), l = ["NodeList", "DOMTokenList", "MediaList", "StyleSheetList", "CSSRuleList"], u = 0; u < 5; u++) {
        var c = l[u],
            s = a[c],
            d = s && s.prototype;
        d && !d[i] && r(d, i, c), o[c] = o.Array
    }
}, function (e, t, n) {
    function a(e) {
        if (!r(e)) return o(e);
        var t = [];
        for (var n in Object(e)) l.call(e, n) && "constructor" != n && t.push(n);
        return t
    }
    var r = n(138),
        o = n(206),
        i = Object.prototype,
        l = i.hasOwnProperty;
    e.exports = a
}, function (e, t, n) {
    var a = n(150),
        r = n(58),
        o = n(152),
        i = n(153),
        l = n(156),
        u = n(24),
        c = n(91),
        s = "[object Map]",
        d = "[object Object]",
        f = "[object Promise]",
        p = "[object Set]",
        m = "[object WeakMap]",
        h = "[object DataView]",
        v = c(a),
        g = c(r),
        E = c(o),
        y = c(i),
        S = c(l),
        N = u;
    (a && N(new a(new ArrayBuffer(1))) != h || r && N(new r) != s || o && N(o.resolve()) != f || i && N(new i) != p || l && N(new l) != m) && (N = function (e) {
        var t = u(e),
            n = t == d ? e.constructor : void 0,
            a = n ? c(n) : "";
        if (a) switch (a) {
            case v:
                return h;
            case g:
                return s;
            case E:
                return f;
            case y:
                return p;
            case S:
                return m
        }
        return t
    }), e.exports = N
}, function (e, t) {
    function n(e) {
        var t = e && e.constructor,
            n = "function" == typeof t && t.prototype || a;
        return e === n
    }
    var a = Object.prototype;
    e.exports = n
}, function (e, t, n) {
    e.exports = {
        "default": n(259),
        __esModule: !0
    }
}, function (e, t, n) {
    var a, r;
    ! function (o) {
        function i(e, t, n) {
            return function (e, a) {
                var r = t.createClass({
                    statics: {
                        getClass: function () {
                            return e.getClass ? e.getClass() : e
                        }
                    },
                    getInstance: function () {
                        return e.prototype.isReactComponent ? this.refs.instance : this
                    },
                    __outsideClickHandler: function () { },
                    componentDidMount: function () {
                        if ("undefined" != typeof document && document.createElement) {
                            var e, r = this.getInstance();
                            if (a && "function" == typeof a.handleClickOutside) {
                                if (e = a.handleClickOutside(r), "function" != typeof e) throw new Error("Component lacks a function for processing outside click events specified by the handleClickOutside config option.")
                            } else if ("function" == typeof r.handleClickOutside) e = t.Component.prototype.isPrototypeOf(r) ? r.handleClickOutside.bind(r) : r.handleClickOutside;
                            else {
                                if ("function" != typeof r.props.handleClickOutside) throw new Error("Component lacks a handleClickOutside(event) function for processing outside click events.");
                                e = r.props.handleClickOutside
                            }
                            var o = n.findDOMNode(r);
                            null === o && (console.warn("Antipattern warning: there was no DOM node associated with the component that is being wrapped by outsideClick."), console.warn(["This is typically caused by having a component that starts life with a render function that", "returns `null` (due to a state or props value), so that the component 'exist' in the React", "chain of components, but not in the DOM.\n\nInstead, you need to refactor your code so that the", "decision of whether or not to show your component is handled by the parent, in their render()", "function.\n\nIn code, rather than:\n\n  A{render(){return check? <.../> : null;}\n  B{render(){<A check=... />}\n\nmake sure that you", "use:\n\n  A{render(){return <.../>}\n  B{render(){return <...>{ check ? <A/> : null }<...>}}\n\nThat is:", "the parent is always responsible for deciding whether or not to render any of its children.", "It is not the child's responsibility to decide whether a render instruction from above should", "get ignored or not by returning `null`.\n\nWhen any component gets its render() function called,", "that is the signal that it should be rendering its part of the UI. It may in turn decide not to", "render all of *its* children, but it should never return `null` for itself. It is not responsible", "for that decision."].join(" ")));
                            var i = this.__outsideClickHandler = h(o, r, e, this.props.outsideClickIgnoreClass || s, this.props.excludeScrollbar || !1, this.props.preventDefault || !1, this.props.stopPropagation || !1),
                                l = u.length;
                            u.push(this), c[l] = i, this.props.disableOnClickOutside || this.enableOnClickOutside()
                        }
                    },
                    componentWillReceiveProps: function (e) {
                        this.props.disableOnClickOutside && !e.disableOnClickOutside ? this.enableOnClickOutside() : !this.props.disableOnClickOutside && e.disableOnClickOutside && this.disableOnClickOutside()
                    },
                    componentWillUnmount: function () {
                        this.disableOnClickOutside(), this.__outsideClickHandler = !1;
                        var e = u.indexOf(this);
                        e > -1 && (c[e] && c.splice(e, 1), u.splice(e, 1))
                    },
                    enableOnClickOutside: function () {
                        var e = this.__outsideClickHandler;
                        if ("undefined" != typeof document) {
                            var t = this.props.eventTypes || d;
                            t.forEach || (t = [t]), t.forEach(function (t) {
                                document.addEventListener(t, e)
                            })
                        }
                    },
                    disableOnClickOutside: function () {
                        var e = this.__outsideClickHandler;
                        if ("undefined" != typeof document) {
                            var t = this.props.eventTypes || d;
                            t.forEach || (t = [t]), t.forEach(function (t) {
                                document.removeEventListener(t, e)
                            })
                        }
                    },
                    render: function () {
                        var n = this.props,
                            a = {};
                        return Object.keys(this.props).forEach(function (e) {
                            "excludeScrollbar" !== e && (a[e] = n[e])
                        }), e.prototype.isReactComponent && (a.ref = "instance"), a.disableOnClickOutside = this.disableOnClickOutside, a.enableOnClickOutside = this.enableOnClickOutside, t.createElement(e, a)
                    }
                });
                return function (e, t) {
                    var n = e.displayName || e.name || "Component";
                    t.displayName = "OnClickOutside(" + n + ")"
                } (e, r), r
            }
        }

        function l(o, i) {
            a = [n(1), n(1)], r = function (e, t) {
                return i(o, e, t)
            }.apply(t, a), !(void 0 !== r && (e.exports = r))
        }
        var u = [],
            c = [],
            s = "ignore-react-onclickoutside",
            d = ["mousedown", "touchstart"],
            f = function (e, t, n) {
                return e === t || (e.correspondingElement ? e.correspondingElement.classList.contains(n) : e.classList.contains(n))
            },
            p = function (e, t, n) {
                if (e === t) return !0;
                for (; e.parentNode;) {
                    if (f(e, t, n)) return !0;
                    e = e.parentNode
                }
                return e
            },
            m = function (e) {
                return document.documentElement.clientWidth <= e.clientX
            },
            h = function (e, t, n, a, r, o, i) {
                return function (t) {
                    o && t.preventDefault(), i && t.stopPropagation();
                    var l = t.target;
                    r && m(t) || p(l, e, a) !== document || n(t)
                }
            };
        l(o, i)
    } (this)
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(2),
        o = a(r),
        i = n(32),
        l = function u(e) {
            (0, o["default"])(this, u), this.name = "Unnamed test", this.actions = [], this.id = (0, i.generate)(), this.type = "test", this.variables = [], e && (this.name = e)
        };
    t["default"] = l
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function r(e, t) {
        var n = null;
        return o(e, function (e) {
            e.id === t && (n = e)
        }), n
    }

    function o(e, t) {
        function n(e, a) {
            (0, s["default"])(e) && e.forEach(function (e, r) {
                t(e, a, r), n(e.children, e)
            })
        }
        t(e, null, null), n(e.children, e)
    }

    function i(e) {
        var t = 0;
        return o(e, function (n) {
            e !== n && t++
        }), t
    }

    function l(e, t) {
        var n;
        return o(e, function (e) {
            for (var a in t) e[a] === t[a] && (n = e)
        }), n
    }

    function u(e, t) {
        return o(e, function (e, n, a) {
            e.id === t && n.children.splice(a, 1)
        }), e
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var c = n(12),
        s = a(c);
    t.findNodeById = r, t.walkThroughTreeNodes = o, t.countNodesChildren = i, t.findNode = l, t.removeNodeFromTree = u
}, function (e, t, n) {
    e.exports = {
        "default": n(145),
        __esModule: !0
    }
}, function (e, t, n) {
    n(147), e.exports = n(9).Object.assign
}, function (e, t, n) {
    n(148);
    var a = n(9).Object;
    e.exports = function (e, t, n) {
        return a.defineProperty(e, t, n)
    }
}, function (e, t, n) {
    "use strict";
    var a = n(23),
        r = n(53),
        o = n(31),
        i = n(54),
        l = n(76),
        u = Object.assign;
    e.exports = !u || n(20)(function () {
        var e = {},
            t = {},
            n = Symbol(),
            a = "abcdefghijklmnopqrst";
        return e[n] = 7, a.split("").forEach(function (e) {
            t[e] = e
        }), 7 != u({}, e)[n] || Object.keys(u({}, t)).join("") != a
    }) ? function (e, t) {
        for (var n = i(e), u = arguments.length, c = 1, s = r.f, d = o.f; u > c;)
            for (var f, p = l(arguments[c++]), m = s ? a(p).concat(s(p)) : a(p), h = m.length, v = 0; h > v;) d.call(p, f = m[v++]) && (n[f] = p[f]);
        return n
    } : u
}, function (e, t, n) {
    var a = n(17);
    a(a.S + a.F, "Object", {
        assign: n(146)
    })
}, function (e, t, n) {
    var a = n(17);
    a(a.S + a.F * !n(15), "Object", {
        defineProperty: n(16).f
    })
}, function (e, t, n) {
    /*!
     * domready (c) Dustin Diaz 2014 - License MIT
     */
    ! function (t, n) {
        e.exports = n()
    } ("domready", function () {
        var e, t = [],
            n = document,
            a = n.documentElement.doScroll,
            r = "DOMContentLoaded",
            o = (a ? /^loaded|^c/ : /^loaded|^i|^c/).test(n.readyState);
        return o || n.addEventListener(r, e = function () {
            for (n.removeEventListener(r, e), o = 1; e = t.shift();) e()
        }),
            function (e) {
                o ? setTimeout(e, 0) : t.push(e)
            }
    })
}, function (e, t, n) {
    var a = n(26),
        r = n(11),
        o = a(r, "DataView");
    e.exports = o
}, function (e, t, n) {
    function a(e) {
        var t = -1,
            n = null == e ? 0 : e.length;
        for (this.clear(); ++t < n;) {
            var a = e[t];
            this.set(a[0], a[1])
        }
    }
    var r = n(187),
        o = n(188),
        i = n(189),
        l = n(190),
        u = n(191);
    a.prototype.clear = r, a.prototype["delete"] = o, a.prototype.get = i, a.prototype.has = l, a.prototype.set = u, e.exports = a
}, function (e, t, n) {
    var a = n(26),
        r = n(11),
        o = a(r, "Promise");
    e.exports = o
}, function (e, t, n) {
    var a = n(26),
        r = n(11),
        o = a(r, "Set");
    e.exports = o
}, function (e, t, n) {
    function a(e) {
        var t = -1,
            n = null == e ? 0 : e.length;
        for (this.__data__ = new r; ++t < n;) this.add(e[t])
    }
    var r = n(59),
        o = n(210),
        i = n(211);
    a.prototype.add = a.prototype.push = o, a.prototype.has = i, e.exports = a
}, function (e, t, n) {
    var a = n(11),
        r = a.Uint8Array;
    e.exports = r
}, function (e, t, n) {
    var a = n(26),
        r = n(11),
        o = a(r, "WeakMap");
    e.exports = o
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, a = null == e ? 0 : e.length, r = 0, o = []; ++n < a;) {
            var i = e[n];
            t(i, n, e) && (o[r++] = i)
        }
        return o
    }
    e.exports = n
}, function (e, t, n) {
    function a(e, t) {
        var n = i(e),
            a = !n && o(e),
            s = !n && !a && l(e),
            f = !n && !a && !s && c(e),
            p = n || a || s || f,
            m = p ? r(e.length, String) : [],
            h = m.length;
        for (var v in e) !t && !d.call(e, v) || p && ("length" == v || s && ("offset" == v || "parent" == v) || f && ("buffer" == v || "byteLength" == v || "byteOffset" == v) || u(v, h)) || m.push(v);
        return m
    }
    var r = n(174),
        o = n(77),
        i = n(12),
        l = n(78),
        u = n(88),
        c = n(79),
        s = Object.prototype,
        d = s.hasOwnProperty;
    e.exports = a
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, a = null == e ? 0 : e.length, r = Array(a); ++n < a;) r[n] = t(e[n], n, e);
        return r
    }
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, a = t.length, r = e.length; ++n < a;) e[r + n] = t[n];
        return e
    }
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, a = null == e ? 0 : e.length; ++n < a;)
            if (t(e[n], n, e)) return !0;
        return !1
    }
    e.exports = n
}, function (e, t) {
    function n(e, t, n, a) {
        for (var r = e.length, o = n + (a ? 1 : -1); a ? o-- : ++o < r;)
            if (t(e[o], o, e)) return o;
        return -1
    }
    e.exports = n
}, function (e, t, n) {
    function a(e, t, n) {
        var a = t(e);
        return o(e) ? a : r(a, n(e))
    }
    var r = n(160),
        o = n(12);
    e.exports = a
}, function (e, t) {
    function n(e, t) {
        return null != e && t in Object(e)
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        return o(e) && r(e) == i
    }
    var r = n(24),
        o = n(25),
        i = "[object Arguments]";
    e.exports = a
}, function (e, t, n) {
    function a(e, t, n, a, v, E) {
        var y = c(e),
            S = c(t),
            N = y ? m : u(e),
            b = S ? m : u(t);
        N = N == p ? h : N, b = b == p ? h : b;
        var w = N == h,
            _ = b == h,
            k = N == b;
        if (k && s(e)) {
            if (!s(t)) return !1;
            y = !0, w = !1
        }
        if (k && !w) return E || (E = new r), y || d(e) ? o(e, t, n, a, v, E) : i(e, t, N, n, a, v, E);
        if (!(n & f)) {
            var C = w && g.call(e, "__wrapped__"),
                T = _ && g.call(t, "__wrapped__");
            if (C || T) {
                var O = C ? e.value() : e,
                    A = T ? t.value() : t;
                return E || (E = new r), v(O, A, n, a, E)
            }
        }
        return !!k && (E || (E = new r), l(e, t, n, a, v, E))
    }
    var r = n(82),
        o = n(86),
        i = n(179),
        l = n(180),
        u = n(137),
        c = n(12),
        s = n(78),
        d = n(79),
        f = 1,
        p = "[object Arguments]",
        m = "[object Array]",
        h = "[object Object]",
        v = Object.prototype,
        g = v.hasOwnProperty;
    e.exports = a
}, function (e, t, n) {
    function a(e, t, n, a) {
        var u = n.length,
            c = u,
            s = !a;
        if (null == e) return !c;
        for (e = Object(e); u--;) {
            var d = n[u];
            if (s && d[2] ? d[1] !== e[d[0]] : !(d[0] in e)) return !1
        }
        for (; ++u < c;) {
            d = n[u];
            var f = d[0],
                p = e[f],
                m = d[1];
            if (s && d[2]) {
                if (void 0 === p && !(f in e)) return !1
            } else {
                var h = new r;
                if (a) var v = a(p, m, f, e, t, h);
                if (!(void 0 === v ? o(m, p, i | l, a, h) : v)) return !1
            }
        }
        return !0
    }
    var r = n(82),
        o = n(84),
        i = 1,
        l = 2;
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        if (!i(e) || o(e)) return !1;
        var t = r(e) ? m : c;
        return t.test(l(e))
    }
    var r = n(74),
        o = n(193),
        i = n(33),
        l = n(91),
        u = /[\\^$.*+?()[\]{}|]/g,
        c = /^\[object .+?Constructor\]$/,
        s = Function.prototype,
        d = Object.prototype,
        f = s.toString,
        p = d.hasOwnProperty,
        m = RegExp("^" + f.call(p).replace(u, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$");
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        return i(e) && o(e.length) && !!I[r(e)]
    }
    var r = n(24),
        o = n(61),
        i = n(25),
        l = "[object Arguments]",
        u = "[object Array]",
        c = "[object Boolean]",
        s = "[object Date]",
        d = "[object Error]",
        f = "[object Function]",
        p = "[object Map]",
        m = "[object Number]",
        h = "[object Object]",
        v = "[object RegExp]",
        g = "[object Set]",
        E = "[object String]",
        y = "[object WeakMap]",
        S = "[object ArrayBuffer]",
        N = "[object DataView]",
        b = "[object Float32Array]",
        w = "[object Float64Array]",
        _ = "[object Int8Array]",
        k = "[object Int16Array]",
        C = "[object Int32Array]",
        T = "[object Uint8Array]",
        O = "[object Uint8ClampedArray]",
        A = "[object Uint16Array]",
        x = "[object Uint32Array]",
        I = {};
    I[b] = I[w] = I[_] = I[k] = I[C] = I[T] = I[O] = I[A] = I[x] = !0, I[l] = I[u] = I[S] = I[c] = I[N] = I[s] = I[d] = I[f] = I[p] = I[m] = I[h] = I[v] = I[g] = I[E] = I[y] = !1, e.exports = a
}, function (e, t, n) {
    function a(e) {
        var t = o(e);
        return 1 == t.length && t[0][2] ? i(t[0][0], t[0][1]) : function (n) {
            return n === e || r(n, e, t)
        }
    }
    var r = n(167),
        o = n(182),
        i = n(90);
    e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        return l(e) && u(t) ? c(s(e), t) : function (n) {
            var a = o(n, e);
            return void 0 === a && a === t ? i(n, e) : r(t, a, d | f)
        }
    }
    var r = n(84),
        o = n(219),
        i = n(220),
        l = n(60),
        u = n(89),
        c = n(90),
        s = n(51),
        d = 1,
        f = 2;
    e.exports = a
}, function (e, t) {
    function n(e) {
        return function (t) {
            return null == t ? void 0 : t[e]
        }
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        return function (t) {
            return r(t, e)
        }
    }
    var r = n(83);
    e.exports = a
}, function (e, t) {
    function n(e, t) {
        for (var n = -1, a = Array(e); ++n < e;) a[n] = t(n);
        return a
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        if ("string" == typeof e) return e;
        if (i(e)) return o(e, a) + "";
        if (l(e)) return s ? s.call(e) : "";
        var t = e + "";
        return "0" == t && 1 / e == -u ? "-0" : t
    }
    var r = n(47),
        o = n(159),
        i = n(12),
        l = n(52),
        u = 1 / 0,
        c = r ? r.prototype : void 0,
        s = c ? c.toString : void 0;
    e.exports = a
}, function (e, t) {
    function n(e) {
        return function (t) {
            return e(t)
        }
    }
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        return e.has(t)
    }
    e.exports = n
}, function (e, t, n) {
    var a = n(11),
        r = a["__core-js_shared__"];
    e.exports = r
}, function (e, t, n) {
    function a(e, t, n, a, r, w, k) {
        switch (n) {
            case b:
                if (e.byteLength != t.byteLength || e.byteOffset != t.byteOffset) return !1;
                e = e.buffer, t = t.buffer;
            case N:
                return !(e.byteLength != t.byteLength || !w(new o(e), new o(t)));
            case f:
            case p:
            case v:
                return i(+e, +t);
            case m:
                return e.name == t.name && e.message == t.message;
            case g:
            case y:
                return e == t + "";
            case h:
                var C = u;
            case E:
                var T = a & s;
                if (C || (C = c), e.size != t.size && !T) return !1;
                var O = k.get(e);
                if (O) return O == t;
                a |= d, k.set(e, t);
                var A = l(C(e), C(t), a, r, w, k);
                return k["delete"](e), A;
            case S:
                if (_) return _.call(e) == _.call(t)
        }
        return !1
    }
    var r = n(47),
        o = n(155),
        i = n(92),
        l = n(86),
        u = n(204),
        c = n(212),
        s = 1,
        d = 2,
        f = "[object Boolean]",
        p = "[object Date]",
        m = "[object Error]",
        h = "[object Map]",
        v = "[object Number]",
        g = "[object RegExp]",
        E = "[object Set]",
        y = "[object String]",
        S = "[object Symbol]",
        N = "[object ArrayBuffer]",
        b = "[object DataView]",
        w = r ? r.prototype : void 0,
        _ = w ? w.valueOf : void 0;
    e.exports = a
}, function (e, t, n) {
    function a(e, t, n, a, i, u) {
        var c = n & o,
            s = r(e),
            d = s.length,
            f = r(t),
            p = f.length;
        if (d != p && !c) return !1;
        for (var m = d; m--;) {
            var h = s[m];
            if (!(c ? h in t : l.call(t, h))) return !1
        }
        var v = u.get(e);
        if (v && u.get(t)) return v == t;
        var g = !0;
        u.set(e, t), u.set(t, e);
        for (var E = c; ++m < d;) {
            h = s[m];
            var y = e[h],
                S = t[h];
            if (a) var N = c ? a(S, y, h, t, e, u) : a(y, S, h, e, t, u);
            if (!(void 0 === N ? y === S || i(y, S, n, a, u) : N)) {
                g = !1;
                break
            }
            E || (E = "constructor" == h)
        }
        if (g && !E) {
            var b = e.constructor,
                w = t.constructor;
            b != w && "constructor" in e && "constructor" in t && !("function" == typeof b && b instanceof b && "function" == typeof w && w instanceof w) && (g = !1)
        }
        return u["delete"](e), u["delete"](t), g
    }
    var r = n(181),
        o = 1,
        i = Object.prototype,
        l = i.hasOwnProperty;
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        return r(e, i, o)
    }
    var r = n(163),
        o = n(184),
        i = n(75);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        for (var t = o(e), n = t.length; n--;) {
            var a = t[n],
                i = e[a];
            t[n] = [a, i, r(i)]
        }
        return t
    }
    var r = n(89),
        o = n(75);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        var t = i.call(e, u),
            n = e[u];
        try {
            e[u] = void 0;
            var a = !0
        } catch (r) { }
        var o = l.call(e);
        return a && (t ? e[u] = n : delete e[u]), o
    }
    var r = n(47),
        o = Object.prototype,
        i = o.hasOwnProperty,
        l = o.toString,
        u = r ? r.toStringTag : void 0;
    e.exports = a
}, function (e, t, n) {
    var a = n(157),
        r = n(225),
        o = Object.prototype,
        i = o.propertyIsEnumerable,
        l = Object.getOwnPropertySymbols,
        u = l ? function (e) {
            return null == e ? [] : (e = Object(e), a(l(e), function (t) {
                return i.call(e, t)
            }))
        } : r;
    e.exports = u
}, function (e, t) {
    function n(e, t) {
        return null == e ? void 0 : e[t]
    }
    e.exports = n
}, function (e, t, n) {
    function a(e, t, n) {
        t = r(t, e);
        for (var a = -1, s = t.length, d = !1; ++a < s;) {
            var f = c(t[a]);
            if (!(d = null != e && n(e, f))) break;
            e = e[f]
        }
        return d || ++a != s ? d : (s = null == e ? 0 : e.length, !!s && u(s) && l(f, s) && (i(e) || o(e)))
    }
    var r = n(85),
        o = n(77),
        i = n(12),
        l = n(88),
        u = n(61),
        c = n(51);
    e.exports = a
}, function (e, t, n) {
    function a() {
        this.__data__ = r ? r(null) : {}, this.size = 0
    }
    var r = n(50);
    e.exports = a
}, function (e, t) {
    function n(e) {
        var t = this.has(e) && delete this.__data__[e];
        return this.size -= t ? 1 : 0, t
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        var t = this.__data__;
        if (r) {
            var n = t[e];
            return n === o ? void 0 : n
        }
        return l.call(t, e) ? t[e] : void 0
    }
    var r = n(50),
        o = "__lodash_hash_undefined__",
        i = Object.prototype,
        l = i.hasOwnProperty;
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        var t = this.__data__;
        return r ? void 0 !== t[e] : i.call(t, e)
    }
    var r = n(50),
        o = Object.prototype,
        i = o.hasOwnProperty;
    e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        var n = this.__data__;
        return this.size += this.has(e) ? 0 : 1, n[e] = r && void 0 === t ? o : t, this
    }
    var r = n(50),
        o = "__lodash_hash_undefined__";
    e.exports = a
}, function (e, t) {
    function n(e) {
        var t = typeof e;
        return "string" == t || "number" == t || "symbol" == t || "boolean" == t ? "__proto__" !== e : null === e
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        return !!o && o in e
    }
    var r = n(178),
        o = function () {
            var e = /[^.]+$/.exec(r && r.keys && r.keys.IE_PROTO || "");
            return e ? "Symbol(src)_1." + e : ""
        } ();
    e.exports = a
}, function (e, t) {
    function n() {
        this.__data__ = [], this.size = 0
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        var t = this.__data__,
            n = r(t, e);
        if (n < 0) return !1;
        var a = t.length - 1;
        return n == a ? t.pop() : i.call(t, n, 1), --this.size, !0
    }
    var r = n(48),
        o = Array.prototype,
        i = o.splice;
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        var t = this.__data__,
            n = r(t, e);
        return n < 0 ? void 0 : t[n][1]
    }
    var r = n(48);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        return r(this.__data__, e) > -1
    }
    var r = n(48);
    e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        var n = this.__data__,
            a = r(n, e);
        return a < 0 ? (++this.size, n.push([e, t])) : n[a][1] = t, this
    }
    var r = n(48);
    e.exports = a
}, function (e, t, n) {
    function a() {
        this.size = 0, this.__data__ = {
            hash: new r,
            map: new (i || o),
            string: new r
        }
    }
    var r = n(151),
        o = n(46),
        i = n(58);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        var t = r(this, e)["delete"](e);
        return this.size -= t ? 1 : 0, t
    }
    var r = n(49);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        return r(this, e).get(e)
    }
    var r = n(49);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        return r(this, e).has(e)
    }
    var r = n(49);
    e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        var n = r(this, e),
            a = n.size;
        return n.set(e, t), this.size += n.size == a ? 0 : 1, this
    }
    var r = n(49);
    e.exports = a
}, function (e, t) {
    function n(e) {
        var t = -1,
            n = Array(e.size);
        return e.forEach(function (e, a) {
            n[++t] = [a, e]
        }), n
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        var t = r(e, function (e) {
            return n.size === o && n.clear(), e
        }),
            n = t.cache;
        return t
    }
    var r = n(223),
        o = 500;
    e.exports = a
}, function (e, t, n) {
    var a = n(209),
        r = a(Object.keys, Object);
    e.exports = r
}, function (e, t, n) {
    (function (e) {
        var a = n(87),
            r = "object" == typeof t && t && !t.nodeType && t,
            o = r && "object" == typeof e && e && !e.nodeType && e,
            i = o && o.exports === r,
            l = i && a.process,
            u = function () {
                try {
                    return l && l.binding && l.binding("util")
                } catch (e) { }
            } ();
        e.exports = u
    }).call(t, n(95)(e))
}, function (e, t) {
    function n(e) {
        return r.call(e)
    }
    var a = Object.prototype,
        r = a.toString;
    e.exports = n
}, function (e, t) {
    function n(e, t) {
        return function (n) {
            return e(t(n))
        }
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        return this.__data__.set(e, a), this
    }
    var a = "__lodash_hash_undefined__";
    e.exports = n
}, function (e, t) {
    function n(e) {
        return this.__data__.has(e)
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        var t = -1,
            n = Array(e.size);
        return e.forEach(function (e) {
            n[++t] = e
        }), n
    }
    e.exports = n
}, function (e, t, n) {
    function a() {
        this.__data__ = new r, this.size = 0
    }
    var r = n(46);
    e.exports = a
}, function (e, t) {
    function n(e) {
        var t = this.__data__,
            n = t["delete"](e);
        return this.size = t.size, n
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        return this.__data__.get(e)
    }
    e.exports = n
}, function (e, t) {
    function n(e) {
        return this.__data__.has(e)
    }
    e.exports = n
}, function (e, t, n) {
    function a(e, t) {
        var n = this.__data__;
        if (n instanceof r) {
            var a = n.__data__;
            if (!o || a.length < l - 1) return a.push([e, t]), this.size = ++n.size, this;
            n = this.__data__ = new i(a)
        }
        return n.set(e, t), this.size = n.size, this
    }
    var r = n(46),
        o = n(58),
        i = n(59),
        l = 200;
    e.exports = a
}, function (e, t, n) {
    var a = n(205),
        r = /^\./,
        o = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
        i = /\\(\\)?/g,
        l = a(function (e) {
            var t = [];
            return r.test(e) && t.push(""), e.replace(o, function (e, n, a, r) {
                t.push(a ? r.replace(i, "$1") : n || e)
            }), t
        });
    e.exports = l
}, function (e, t, n) {
    function a(e, t, n) {
        var a = null == e ? void 0 : r(e, t);
        return void 0 === a ? n : a
    }
    var r = n(83);
    e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        return null != e && o(e, t, r)
    }
    var r = n(164),
        o = n(186);
    e.exports = a
}, function (e, t) {
    function n(e) {
        return e
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        return "number" == typeof e || o(e) && r(e) == i
    }
    var r = n(24),
        o = n(25),
        i = "[object Number]";
    e.exports = a
}, function (e, t, n) {
    function a(e, t) {
        if ("function" != typeof e || null != t && "function" != typeof t) throw new TypeError(o);
        var n = function () {
            var a = arguments,
                r = t ? t.apply(this, a) : a[0],
                o = n.cache;
            if (o.has(r)) return o.get(r);
            var i = e.apply(this, a);
            return n.cache = o.set(r, i) || o, i
        };
        return n.cache = new (a.Cache || r), n
    }
    var r = n(59),
        o = "Expected a function";
    a.Cache = r, e.exports = a
}, function (e, t, n) {
    function a(e) {
        return i(e) ? r(l(e)) : o(e)
    }
    var r = n(172),
        o = n(173),
        i = n(60),
        l = n(51);
    e.exports = a
}, function (e, t) {
    function n() {
        return []
    }
    e.exports = n
}, function (e, t) {
    function n() {
        return !1
    }
    e.exports = n
}, function (e, t, n) {
    function a(e) {
        if (!e) return 0 === e ? e : 0;
        if (e = r(e), e === o || e === -o) {
            var t = e < 0 ? -1 : 1;
            return t * i
        }
        return e === e ? e : 0
    }
    var r = n(98),
        o = 1 / 0,
        i = 1.7976931348623157e308;
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        var t = r(e),
            n = t % 1;
        return t === t ? n ? t - n : t : 0
    }
    var r = n(227);
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        return null == e ? "" : r(e)
    }
    var r = n(175);
    e.exports = a
}, function (e, t, n) {
    "use strict";

    function a(e) {
        var t = r.shuffled();
        return {
            version: 15 & t.indexOf(e.substr(0, 1)),
            worker: 15 & t.indexOf(e.substr(1, 1))
        }
    }
    var r = n(62);
    e.exports = a
}, function (e, t, n) {
    "use strict";

    function a(e, t) {
        for (var n, a = 0, o = ""; !n;) o += e(t >> 4 * a & 15 | r()), n = t < Math.pow(16, a + 1), a++;
        return o
    }
    var r = n(234);
    e.exports = a
}, function (e, t, n) {
    "use strict";

    function a() {
        var e = "",
            t = Math.floor(.001 * (Date.now() - p));
        return t === u ? l++ : (l = 0, u = t), e += s(c.lookup, m), e += s(c.lookup, h), l > 0 && (e += s(c.lookup, l)), e += s(c.lookup, t)
    }

    function r(t) {
        return c.seed(t), e.exports
    }

    function o(t) {
        return h = t, e.exports
    }

    function i(e) {
        return void 0 !== e && c.characters(e), c.shuffled()
    }
    var l, u, c = n(62),
        s = n(231),
        d = n(230),
        f = n(233),
        p = 1459707606518,
        m = 6,
        h = n(236) || 0;
    e.exports = a, e.exports.generate = a, e.exports.seed = r, e.exports.worker = o, e.exports.characters = i, e.exports.decode = d, e.exports.isValid = f
}, function (e, t, n) {
    "use strict";

    function a(e) {
        if (!e || "string" != typeof e || e.length < 6) return !1;
        for (var t = r.characters(), n = e.length, a = 0; a < n; a++)
            if (t.indexOf(e[a]) === -1) return !1;
        return !0
    }
    var r = n(62);
    e.exports = a
}, function (e, t) {
    "use strict";

    function n() {
        if (!a || !a.getRandomValues) return 48 & Math.floor(256 * Math.random());
        var e = new Uint8Array(1);
        return a.getRandomValues(e), 48 & e[0]
    }
    var a = "object" == typeof window && (window.crypto || window.msCrypto);
    e.exports = n
}, function (e, t) {
    "use strict";

    function n() {
        return r = (9301 * r + 49297) % 233280, r / 233280
    }

    function a(e) {
        r = e
    }
    var r = 1;
    e.exports = {
        nextValue: n,
        seed: a
    }
}, function (e, t) {
    "use strict";
    e.exports = 0
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function r(e, t) {
        var n = (0, m["default"])({}, e),
            a = (0, E["default"])(S, {
                id: n.id
            });
        b = t, a === -1 ? S.push(n) : S.splice(a, 1, n), w()
    }

    function o(e, t) {
        var n = (0, m["default"])({}, e),
            a = (0, E["default"])(S, {
                id: n.id
            });
        b = t, n.toDelete = !0, a === -1 ? S.push(n) : S.splice(a, 1, n), w()
    }

    function loadTests(e) {
        return fetch("https://www.snaptest.io/api/load", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                apikey: e
            }
        }).then(function (e) {
            return e.json()
        }).then(function (e) {
            return {
                tests: e.tests,
                components: e.components,
                directory: e.directory
            }
        })
    }

    function l() {
        S = S.map(function (e) {
            return "component" === e.type || "test" === e.type ? (e.actions = (0, f["default"])(e.actions), e.variables = (0, f["default"])(e.variables), (0, s["default"])({}, e)) : (0, s["default"])({}, e)
        })
    }

    function saveTests() {
        fetch("https://www.snaptest.io/api/save", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                apikey: b.user.apiKey
            },
            body: (0, f["default"])(S)
        }).then(function (e) {
            return e.json()
        })["catch"](function () { })
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var c = n(55),
        s = a(c),
        d = n(139),
        f = a(d),
        p = n(96),
        m = a(p),
        h = n(263),
        v = a(h),
        g = n(56),
        E = a(g);
    t.saveItemToDB = r, t.removeItemFromDB = o, t.loadItems = loadTests;
    var y = n(7),
        S = (a(y), []),
        N = !1,
        b = null,
        w = (0, v["default"])(function () {
            !N && S.length > 0 && (l(), saveTests(), S = [])
        }, 3e3)
}, , function (e, t, n) {
    "use strict";

    function a(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(3),
        i = r(o),
        l = n(2),
        u = r(l),
        c = n(6),
        s = r(c),
        d = n(5),
        f = r(d),
        p = n(4),
        m = r(p),
        h = n(94),
        v = r(h),
        g = n(1),
        E = r(g),
        y = n(7),
        S = r(y),
        N = n(28),
        b = a(N),
        w = n(80),
        _ = r(w),
        k = n(291),
        C = 5e3,
        T = function (e) {
            function t(e) {
                (0, u["default"])(this, t);
                var n = (0, f["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e));
                return n.actionTypeGroups = [{
                    label: "Actions",
                    options: [{
                        name: "...",
                        value: "",
                        isDefault: !0
                    }, {
                        name: "Click element",
                        value: b.MOUSEDOWN
                    }, {
                        name: "Press key...",
                        value: b.KEYDOWN
                    }, {
                        name: "Change input to...",
                        value: b.INPUT
                    }, {
                        name: "Form submit",
                        value: b.SUBMIT
                    }, {
                        name: "Mouse over",
                        value: b.MOUSEOVER
                    }, {
                        name: "Focus",
                        value: b.FOCUS
                    }, {
                        name: "Blur",
                        value: b.BLUR
                    }, {
                        name: "Pause",
                        value: b.PAUSE
                    }, {
                        name: "Execute script",
                        value: b.EXECUTE_SCRIPT
                    }, {
                        name: "Drag and drop",
                        value: "",
                        disabled: !0
                    }]
                }, {
                    label: "Scrolling",
                    options: [{
                        name: "Scroll window to...",
                        value: b.SCROLL_WINDOW
                    }, {
                        name: "Scroll window to el",
                        value: b.SCROLL_WINDOW_ELEMENT
                    }, {
                        name: "Scroll element to...",
                        value: b.SCROLL_ELEMENT
                    }]
                }, {
                    label: "Assertions",
                    options: [{
                        name: "El text is...",
                        value: b.TEXT_ASSERT
                    }, {
                        name: "El text matches regex...",
                        value: b.TEXT_REGEX_ASSERT
                    }, {
                        name: "Input value is...",
                        value: b.VALUE_ASSERT
                    }, {
                        name: "El is present",
                        value: b.EL_PRESENT_ASSERT
                    }, {
                        name: "El is not present",
                        value: b.EL_NOT_PRESENT_ASSERT
                    }]
                }, {
                    label: "Navigation",
                    options: [{
                        name: "Load page...",
                        value: b.FULL_PAGELOAD
                    }, {
                        name: "DOM ready & path is...",
                        value: b.PAGELOAD
                    }, {
                        name: "Back",
                        value: b.BACK
                    }, {
                        name: "Forward",
                        value: b.FORWARD
                    }, {
                        name: "Refresh",
                        value: b.REFRESH
                    }, {
                        name: "Focus on window",
                        value: b.CHANGE_WINDOW
                    }]
                }, {
                    label: "Other",
                    options: [{
                        name: "Component",
                        value: b.COMPONENT
                    }, {
                        name: "url change indicator",
                        value: b.URL_CHANGE_INDICATOR
                    }, {
                        name: "Take screenshot",
                        value: b.SCREENSHOT
                    }, {
                        name: "SEO meta scan",
                        value: b.META_SCAN,
                        disabled: !0
                    }]
                }], n
            }
            return (0, m["default"])(t, e), (0, s["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.activeTest,
                        r = t.idx,
                        o = t.onNWRowClick,
                        i = t.isLast,
                        l = (t.isFirst, t.isCursorHere),
                        u = t.isRecording,
                        c = t.isAssertMode,
                        s = t.components,
                        d = t.disAllowComponents,
                        f = void 0 !== d && d,
                        p = t.isInComponent,
                        m = t.selectingForActionId,
                        h = t.selectionCandidate,
                        v = t.playbackResult,
                        g = void 0 === v ? null : v,
                        y = t.playbackCursor,
                        N = t.playbackBreakpoints,
                        w = void 0 === N ? [] : N,
                        T = t.expandedActions,
                        O = void 0 === T ? [] : T,
                        A = this.isIndicator(),
                        x = this.isComponent(),
                        I = w.indexOf(n.id) !== -1,
                        R = {
                            action: n,
                            idx: r,
                            components: s,
                            isInComponent: p,
                            disAllowComponents: f,
                            actionTypeGroups: this.actionTypeGroups,
                            activeTest: a,
                            onActionTypeChange: this.onActionTypeChange,
                            selectingForActionId: m,
                            selectionCandidate: h,
                            playbackResult: g,
                            playbackCursor: y,
                            playbackBreakpoints: w,
                            defaultTimeout: C,
                            isExpanded: O.indexOf(n.id) !== -1
                        },
                        P = (0, _["default"])({
                            "nw-action-row-wrapper": !0,
                            "nw-indicator": A,
                            "nw-component": x,
                            "playback-processing": g[n.id] && g[n.id].processing,
                            "playback-success": g[n.id] && !g[n.id].processing && g[n.id].success,
                            "playback-error": g[n.id] && !g[n.id].processing && !g[n.id].success,
                            "playback-skipped": g[n.id] && !g[n.id].processing && g[n.id].skipped
                        });
                    return E["default"].createElement("div", {
                        className: P
                    }, E["default"].createElement("div", {
                        className: "debug-action",
                        onClick: function () {
                            return I ? S["default"].to(S["default"].SESSION, "removeBreakpoint", n.id) : S["default"].to(S["default"].SESSION, "setBreakpoint", n.id)
                        }
                    }, y === n.id && E["default"].createElement("div", {
                        className: "playback-cursor"
                    }), E["default"].createElement("div", {
                        className: "breakpoint-cursor" + (I ? " active" : "")
                    })), E["default"].createElement("div", {
                        className: "nw-action-row grid-row",
                        key: r,
                        onMouseOver: function () {
                            return e.onActionItemMouseEnter(n)
                        },
                        onMouseOut: function () {
                            return e.onActionItemMouseOut(n)
                        }
                    }, E["default"].createElement("div", {
                        className: "grid-row action-selector"
                    }, E["default"].createElement("div", {
                        className: "checkbox" + (this.isRowSelected(n) ? " active" : ""),
                        onClick: function () {
                            return o(n, r)
                        }
                    }, E["default"].createElement("div", {
                        className: "box"
                    }))), n.type === b.PAGELOAD || n.type === b.FULL_PAGELOAD ? E["default"].createElement(k.PageLoadActionItem, R) : n.type === b.MOUSEDOWN || n.type === b.EL_PRESENT_ASSERT || n.type === b.EL_NOT_PRESENT_ASSERT || n.type === b.MOUSEOVER || n.type === b.FOCUS || n.type === b.BLUR || n.type === b.SCROLL_WINDOW_ELEMENT || n.type === b.SUBMIT ? E["default"].createElement(k.MouseClickActionItem, R) : n.type === b.CHANGE_WINDOW_AUTO ? E["default"].createElement(k.AutoChangeWindowAction, R) : n.type === b.POPSTATE || n.type === b.BACK || n.type === b.FORWARD || n.type === b.REFRESH ? E["default"].createElement(k.BackActionItem, R) : n.type === b.CHANGE_WINDOW ? E["default"].createElement(k.ActionWithNumberValue, R) : n.type === b.INPUT ? E["default"].createElement(k.InputChangeActionItem, R) : n.type === b.TEXT_ASSERT || n.type === b.TEXT_REGEX_ASSERT || n.type === b.SCREENSHOT || n.type === b.VALUE_ASSERT ? E["default"].createElement(k.ValueAssertActionItem, R) : n.type === b.SCROLL_ELEMENT || n.type === b.SCROLL_WINDOW ? E["default"].createElement(k.XYCoordActionItem, R) : n.type === b.BLANK ? E["default"].createElement(k.BlankActionItem, R) : n.type === b.PUSHSTATE ? E["default"].createElement(k.LinkChangeIndicator, R) : n.type === b.KEYDOWN ? E["default"].createElement(k.KeyDownActionItem, R) : n.type === b.URL_CHANGE_INDICATOR ? E["default"].createElement(k.LinkChangeIndicator, R) : n.type === b.COMPONENT ? E["default"].createElement(k.ComponentActionItem, R) : n.type === b.PAUSE ? E["default"].createElement(k.PauseActionItem, R) : n.type === b.EXECUTE_SCRIPT ? E["default"].createElement(k.ScriptActionItem, R) : n.type === b.META_SCAN ? E["default"].createElement(k.MetaScanActionItem, R) : null), g[n.id] && !g[n.id].processing && !g[n.id].success && E["default"].createElement("div", {
                        className: "playback-error-status"
                    }, g[n.id].message), !i && E["default"].createElement("div", {
                        className: "grid-row quick-row-actions"
                    }, u ? E["default"].createElement("div", {
                        className: "cursor cursor-record grid-item",
                        onClick: function () {
                            return e.onSetRecordCursor(r)
                        }
                    }, E["default"].createElement("div", {
                        className: "dots"
                    }, "•••")) : c ? E["default"].createElement("div", {
                        className: "cursor cursor-assert grid-item",
                        onClick: function () {
                            return e.onSetAssertCursor(r)
                        }
                    }, E["default"].createElement("div", {
                        className: "dots"
                    }, "•••")) : E["default"].createElement("div", {
                        className: "add-row grid-item",
                        onClick: function () {
                            return e.onAddBlank(r)
                        }
                    }, "+")), l && E["default"].createElement("div", {
                        className: "record-cursor-container" + (c ? " is-asserting" : "")
                    }, E["default"].createElement("div", {
                        className: "record-cursor"
                    }, E["default"].createElement("div", {
                        className: "record-cursor-bar"
                    }))), i && !A && E["default"].createElement("div", {
                        className: "arrow-down"
                    }))
                }
            }, {
                key: "isIndicator",
                value: function () {
                    var e = this.props.action;
                    return e.type === b.PUSHSTATE || e.type === b.URL_CHANGE_INDICATOR
                }
            }, {
                key: "isComponent",
                value: function () {
                    return this.props.action.type === b.COMPONENT
                }
            }, {
                key: "onSetAssertCursor",
                value: function (e) {
                    var t = this.props.isAssertMode;
                    S["default"].to(S["default"].SESSION, "setCursor", e), t || S["default"].to(S["default"].SESSION, "setAssertMode", !0)
                }
            }, {
                key: "onSetRecordCursor",
                value: function (e) {
                    var t = this.props.isRecording;
                    S["default"].to(S["default"].SESSION, "setCursor", e), t || S["default"].to(S["default"].SESSION, "startRecording", {
                        initialUrl: window.location.href,
                        width: window.innerWidth,
                        height: window.innerHeight
                    })
                }
            }, {
                key: "onAddBlank",
                value: function (e) {
                    S["default"].to(S["default"].SESSION, "insertNWAction", {
                        insertAt: e + 1,
                        action: new b.BlankAction
                    })
                }
            }, {
                key: "onAddComponent",
                value: function (e) {
                    S["default"].to(S["default"].SESSION, "insertNWAction", {
                        insertAt: e + 1,
                        action: new b.BlankAction
                    })
                }
            }, {
                key: "onActionItemMouseEnter",
                value: function (e) {
                    S["default"].to(S["default"].SESSION, "setHoverIndicator", e.selector)
                }
            }, {
                key: "onActionItemMouseOut",
                value: function (e) {
                    S["default"].to(S["default"].SESSION, "setHoverIndicator", null)
                }
            }, {
                key: "isRowSelected",
                value: function () {
                    return this.props.selectedRows.indexOf(this.props.action.id) !== -1
                }
            }, {
                key: "onActionTypeChange",
                value: function (e, t) {
                    var n;
                    switch (t) {
                        case b.MOUSEDOWN:
                            n = new b.MousedownAction;
                            break;
                        case b.MOUSEOVER:
                            n = new b.MouseoverAction;
                            break;
                        case b.FOCUS:
                            n = new b.FocusAction;
                            break;
                        case b.BLUR:
                            n = new b.BlurAction;
                            break;
                        case b.SUBMIT:
                            n = new b.SubmitAction;
                            break;
                        case b.TEXT_ASSERT:
                            n = new b.TextAssertAction;
                            break;
                        case b.TEXT_REGEX_ASSERT:
                            n = new b.TextRegexAssertAction;
                            break;
                        case b.EL_PRESENT_ASSERT:
                            n = new b.ElPresentAssertAction;
                            break;
                        case b.EL_NOT_PRESENT_ASSERT:
                            n = new b.ElNotPresentAssertAction;
                            break;
                        case b.VALUE_ASSERT:
                            n = new b.ValueAssertAction;
                            break;
                        case b.SCROLL_WINDOW:
                            n = new b.ScrollWindow;
                            break;
                        case b.SCROLL_WINDOW_ELEMENT:
                            n = new b.ScrollWindowToElement;
                            break;
                        case b.SCROLL_ELEMENT:
                            n = new b.ScrollElement;
                            break;
                        case b.EXECUTE_SCRIPT:
                            n = new b.ExecuteScriptAction;
                            break;
                        case b.KEYDOWN:
                            n = new b.KeydownAction;
                            break;
                        case b.INPUT:
                            n = new b.InputAction;
                            break;
                        case b.BACK:
                            n = new b.BackAction;
                            break;
                        case b.REFRESH:
                            n = new b.RefreshAction;
                            break;
                        case b.FORWARD:
                            n = new b.ForwardAction;
                            break;
                        case b.CHANGE_WINDOW:
                            n = new b.ChangeWindowAction;
                            break;
                        case b.PAUSE:
                            n = new b.PauseAction;
                            break;
                        case b.SCREENSHOT:
                            n = new b.ScreenshotAction;
                            break;
                        case b.URL_CHANGE_INDICATOR:
                            n = new b.UrlChangeIndicatorAction;
                            break;
                        case b.COMPONENT:
                            n = new b.ComponentAction;
                            break;
                        case b.PAGELOAD:
                            n = new b.PageloadAction;
                            break;
                        case b.FULL_PAGELOAD:
                            n = new b.FullPageloadAction;
                            break;
                        case b.META_SCAN:
                            n = new b.MetaScanAction
                    }
                    n.id = e.id, n.description = e.description, n.timeout = e.timeout, n.selector && e.selector && (n.selector = e.selector), S["default"].to(S["default"].SESSION, "updateNWAction", n)
                }
            }]), t
        } (E["default"].PureComponent);
    T.propTypes = {
        action: E["default"].PropTypes.object.isRequired,
        idx: E["default"].PropTypes.number.isRequired,
        onNWRowClick: E["default"].PropTypes.func,
        isLast: E["default"].PropTypes.bool,
        isCursorHere: E["default"].PropTypes.bool,
        isRecording: E["default"].PropTypes.bool,
        isAssertMode: E["default"].PropTypes.bool,
        components: E["default"].PropTypes.array,
        selectedRows: E["default"].PropTypes.array
    }, T.defaultProps = {
        onNWRowClick: v["default"],
        isLast: !1,
        isCursorHere: !1,
        isRecording: !1,
        isAssertMode: !1,
        components: [],
        selectedRows: []
    }, t["default"] = T
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(93),
        h = a(m),
        v = n(1),
        g = a(v),
        E = n(142),
        y = n(7),
        S = a(y),
        N = n(304),
        b = [{
            name: "NightwatchJS",
            value: "nightwatch",
            styles: [{
                name: "Flat",
                value: "flat"
            }, {
                name: "Page Objects",
                value: "pageobjects",
                disabled: !0
            }]
        }, {
            name: "TestCafe",
            disabled: !0,
            value: "testcafe",
            styles: []
        }, {
            name: "Java",
            disabled: !0,
            value: "java",
            styles: []
        }, {
            name: "Cypress.io",
            disabled: !0,
            value: "cypressio",
            styles: []
        }, {
            name: "Webdriver.io",
            disabled: !0,
            value: "webdriverio",
            styles: []
        }],
        w = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e, t = this,
                        n = this.props,
                        a = n.user,
                        r = n.activeTest,
                        o = n.activeFolder,
                        i = n.generatedCode,
                        l = void 0 === i ? "" : i,
                        u = n.directory,
                        c = n.includeHarness,
                        s = n.selectedFramework,
                        d = n.selectedStyle,
                        f = n.topDirName,
                        p = a.apiKey,
                        m = !!r,
                        v = !!o && "root" !== o,
                        y = !!o && "root" === o,
                        N = (0, h["default"])(b, {
                            value: s
                        }),
                        w = N ? N.styles : [];
                    return v && (e = (0, E.findNode)(u, {
                        id: o
                    })), y && (e = u), g["default"].createElement("div", {
                        className: "grid-row grid-item grid-column grid-noshrink"
                    }, g["default"].createElement("div", {
                        className: "grid-row grid-noshrink code-options grid-column"
                    }, g["default"].createElement("div", {
                        className: "grid-item grid-row v-align"
                    }, g["default"].createElement("div", {
                        className: "option-label"
                    }, "Generating for: "), y ? "All tests" : v ? "(folder) " + e.module : "(test) " + r.name), g["default"].createElement("div", {
                        className: "grid-item grid-row v-align"
                    }, g["default"].createElement("div", {
                        className: "option-label"
                    }, "Framework:"), g["default"].createElement("div", {
                        className: "select-wrap"
                    }, g["default"].createElement("select", {
                        value: s,
                        onChange: function (e) {
                            return t.onFrameworkChange(e.currentTarget.value)
                        }
                    }, b.map(function (e) {
                        return g["default"].createElement("option", {
                            disabled: e.disabled,
                            value: e.value
                        }, e.name)
                    })))), g["default"].createElement("div", {
                        className: "grid-item grid-row v-align"
                    }, g["default"].createElement("div", {
                        className: "option-label"
                    }, "Style:"), g["default"].createElement("div", {
                        className: "select-wrap"
                    }, g["default"].createElement("select", {
                        value: d,
                        onChange: function (e) {
                            return t.onStyleChange(e.currentTarget.value)
                        }
                    }, w.map(function (e) {
                        return g["default"].createElement("option", {
                            disabled: e.disabled,
                            value: e.value
                        }, e.name)
                    })))), (v || y) && [g["default"].createElement("div", {
                        className: "grid-item grid-row v-align",
                        key: 0
                    }, g["default"].createElement("div", {
                        className: "option-label"
                    }, "Top directory name:"), g["default"].createElement("input", {
                        type: "text",
                        className: "text-input",
                        placeholder: "snaptests",
                        value: f,
                        onChange: function (e) {
                            return S["default"].to(S["default"].SESSION, "setTopDirName", e.currentTarget.value)
                        }
                    })), g["default"].createElement("div", {
                        className: "grid-item grid-row v-align",
                        key: 1
                    }, g["default"].createElement("div", {
                        className: "option-label"
                    }, g["default"].createElement("label", {
                        htmlFor: "code-harness"
                    }, "Include harness")), g["default"].createElement("input", {
                        type: "checkbox",
                        id: "code-harness",
                        checked: c,
                        onChange: function (e) {
                            return t.onIncludeHarnessChange(e.currentTarget.checked)
                        }
                    }))]), m && [g["default"].createElement("div", null, g["default"].createElement("div", {
                        className: "code-req-panel"
                    }, g["default"].createElement("p", null, "You are viewing the code for a single test.  It contains all the required dependencies in one file.  In order to run with Selenium, follow the install instructions here: ", g["default"].createElement("a", {
                        target: "_blank",
                        href: "https://github.com/ozymandias547/snaptest-harness"
                    }, "SnapTest NW Scaffold"), " "), g["default"].createElement("p", null, 'Entire project/folder generation is available by clicking the "CODE" button in the test directory view.'))), g["default"].createElement("div", {
                        className: "code-section-header"
                    }, "Code to copy & paste: "), g["default"].createElement("div", {
                        className: "code-container "
                    }, g["default"].createElement("textarea", {
                        className: "code"
                    }, l), g["default"].createElement("a", {
                        href: "https://github.com/ozymandias547/snaptest-harness",
                        target: "_blank",
                        className: "github"
                    }, g["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/github.png")
                    }), g["default"].createElement("div", null, "Get the harness")))], (v || y) && g["default"].createElement("div", null, c ? g["default"].createElement("div", {
                        className: "code-req-panel"
                    }, g["default"].createElement("p", null, "You have chosen to generate an entire harness project with required scaffolding. You will only need to do this once for your project."), g["default"].createElement("p", null, "You'll need to manually execute installation step #4 (installing chromedriver) in the ", g["default"].createElement("a", {
                        target: "_blank",
                        href: "https://github.com/ozymandias547/snaptest-harness"
                    }, "NightwatchJS harness README"), "."), g["default"].createElement("p", null, g["default"].createElement("div", null, "Requirements to generate:"), g["default"].createElement("ul", {
                        className: "req-list"
                    }, g["default"].createElement("li", null, "git VCS"), g["default"].createElement("li", null, "node and npm"), g["default"].createElement("li", null, g["default"].createElement("a", {
                        target: "_blank",
                        href: "https://www.npmjs.com/package/snaptest-cli"
                    }, "SnapTest CLI (npm package)"))))) : g["default"].createElement("div", {
                        className: "code-req-panel"
                    }, g["default"].createElement("p", null, "Generating the tests without a harness assumes you have already set up a nightwatchJS test project. "), g["default"].createElement("p", null, g["default"].createElement("div", null, "Requirements to generate:"), g["default"].createElement("ul", {
                        className: "req-list"
                    }, g["default"].createElement("li", null, g["default"].createElement("a", {
                        target: "_blank",
                        href: "https://www.npmjs.com/package/snaptest-cli"
                    }, "SnapTest CLI (npm package)"))))), g["default"].createElement("div", {
                        className: "code-section-header"
                    }, "Command: "), g["default"].createElement("div", {
                        className: "cli-container"
                    }, v ? g["default"].createElement("span", null, c && this.getHarnessCommand(y, v), "snaptest -t ", p, " -f ", o, " -r ", s, " -s ", d, " ", f ? " -o " + f : null) : g["default"].createElement("span", null, c && this.getHarnessCommand(y, v), "snaptest -t ", p, " -r ", s, " -s ", d, " ", f ? " -o " + f : null)), g["default"].createElement("div", {
                        className: "code-section-header"
                    }, "Preview - these files folders will be generated:"), g["default"].createElement("div", {
                        className: "generated-preview"
                    }, this.buildPreview(e, y))))
                }
            }, {
                key: "getHarnessCommand",
                value: function (e, t) {
                    return "git clone https://github.com/ozymandias547/snaptest-harness.git && cd snaptest-harness && npm install && "
                }
            }, {
                key: "buildPreview",
                value: function (e, t) {
                    function n(e, t) {
                        var a;
                        return e.testId && (0, h["default"])(r, {
                            id: e.testId
                        }) ? a = (0, h["default"])(r, {
                            id: e.testId
                        }).name : e && (a = e.module), g["default"].createElement("div", {
                            className: "node"
                        }, t ? o ? o : "snaptests" : N(a), t && g["default"].createElement("div", {
                            className: "boilerplate"
                        }, "common", g["default"].createElement("div", {
                            className: "boilerplate"
                        }, "snaptest-nw-driver.js"), g["default"].createElement("div", {
                            className: "boilerplate"
                        }, "components.js")), t && g["default"].createElement("div", {
                            className: "boilerplate"
                        }, "tests", e.children && e.children.length > 0 && e.children.map(function (e) {
                            return g["default"].createElement("div", null, n(e))
                        })), !t && g["default"].createElement("div", null, e.children && e.children.length > 0 && e.children.map(function (e) {
                            return g["default"].createElement("div", null, n(e))
                        })))
                    }
                    var a = this.props,
                        r = a.tests,
                        o = a.topDirName;
                    return n(e, !0)
                }
            }, {
                key: "onIncludeHarnessChange",
                value: function (e) {
                    console.log(e), S["default"].to(S["default"].SESSION, "setIncludeHarness", e)
                }
            }, {
                key: "onFrameworkChange",
                value: function (e) {
                    S["default"].to(S["default"].SESSION, "setFramework", {
                        framework: e,
                        style: (0, h["default"])(b, {
                            value: e
                        }).styles[0].value
                    })
                }
            }, {
                key: "onStyleChange",
                value: function (e) {
                    var t = this.props.selectedFramework;
                    S["default"].to(S["default"].SESSION, "setFramework", {
                        framework: t,
                        style: e
                    })
                }
            }]), t
        } (g["default"].PureComponent);
    t["default"] = w
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(94),
        h = a(m),
        v = n(1),
        g = a(v),
        E = n(140),
        y = a(E),
        S = function (e) {
            function t(e) {
                (0, l["default"])(this, t);
                var n = (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e));
                return n.state = {
                    isOpen: !1,
                    containerTop: 0
                }, n
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "handleClickOutside",
                value: function () {
                    var e = this.props.onClose,
                        t = void 0 === e ? h["default"] : e;
                    t(), this.setState({
                        isOpen: !1
                    })
                }
            }, {
                key: "componentWillMount",
                value: function () {
                    this.onBodyKeyDown = function (e) {
                        27 === e.keyCode && this.setState({
                            isOpen: !1
                        })
                    }.bind(this), document.addEventListener("keydown", this.onBodyKeyDown)
                }
            }, {
                key: "componentWillUnmount",
                value: function () {
                    document.removeEventListener("keydown", this.onBodyKeyDown)
                }
            }, {
                key: "openMenu",
                value: function () {
                    this.setState({
                        isOpen: !this.state.isOpen
                    })
                }
            }, {
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = (t.buttonClasses, t.button),
                        a = this.state.isOpen;
                    return g["default"].createElement("div", {
                        className: "dropdown",
                        ref: "dropdownbutton"
                    }, g["default"].createElement("div", {
                        className: "dropdown-trigger " + (this.state.isOpen ? " active" : ""),
                        onMouseDown: function () {
                            return e.openMenu()
                        }
                    }, n), a && g["default"].createElement("div", {
                        className: "dropdown-container" + (this.state.isOpen ? " active" : ""),
                        onClick: function () {
                            return e.handleClickOutside()
                        }
                    }, this.props.children))
                }
            }]), t
        } (g["default"].Component);
    t["default"] = (0, y["default"])(S)
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(74),
        h = a(m),
        v = n(94),
        g = a(v),
        E = n(1),
        y = a(E),
        S = n(140),
        N = a(S),
        b = function (e) {
            function t(e) {
                (0, l["default"])(this, t);
                var n = (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e));
                return n.state = {
                    isEditing: !1
                }, n
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "handleClickOutside",
                value: function () {
                    this.state.isEditing && this.onApply()
                }
            }, {
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.value,
                        a = void 0 === n ? "" : n,
                        r = t.classNames,
                        o = t.noStyles,
                        i = void 0 !== o && o,
                        l = t.showOKbutton,
                        u = void 0 !== l && l,
                        c = t.size,
                        s = void 0 === c ? "30" : c,
                        d = t.showEditButton,
                        f = void 0 === d || d,
                        p = t.onMouseDown,
                        m = void 0 === p ? g["default"] : p,
                        v = t.onClick,
                        E = t.doubleClickEdit,
                        S = void 0 === E || E,
                        N = this.state,
                        b = N.isEditing;
                    N.newValue;
                    return y["default"].createElement("span", {
                        className: (i ? "" : "editable-label") + (r ? " " + r : ""),
                        onMouseDown: m
                    }, b ? y["default"].createElement("span", null, y["default"].createElement("input", {
                        defaultValue: a,
                        ref: "editinput",
                        type: "text",
                        autoFocus: !0,
                        size: s < 50 ? s : "",
                        onKeyDown: function (t) {
                            return e.onKeyDown(t)
                        },
                        onChange: function (t) {
                            return e.setState({
                                newValue: t.currentTarget.value
                            })
                        }
                    }), u && y["default"].createElement("button", {
                        className: "accept",
                        onClick: function () {
                            e.onApply()
                        }
                    }, "ok")) : y["default"].createElement("span", {
                        className: "value",
                        onDoubleClick: function () {
                            return S ? e.setState({
                                isEditing: !0,
                                newValue: a
                            }) : null
                        },
                        onClick: function (e) {
                            return (0, h["default"])(v) ? v(e) : null
                        }
                    }, y["default"].createElement("span", {
                        className: (0, h["default"])(v) ? "underline" : ""
                    }, a), f && y["default"].createElement("button", {
                        className: "edit",
                        onClick: function (t) {
                            t.stopPropagation(), e.setState({
                                isEditing: !0,
                                newValue: a
                            })
                        }
                    }, y["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/edit.png")
                    }))))
                }
            }, {
                key: "componentDidUpdate",
                value: function (e, t) {
                    !t.isEditing && this.state.isEditing && this.refs.editinput.select()
                }
            }, {
                key: "onKeyDown",
                value: function (e) {
                    if (13 === e.keyCode) {
                        if (this.props.value === e.currentTarget.value) return void this.setState({
                            isEditing: !1
                        });
                        this.onApply()
                    }
                }
            }, {
                key: "onApply",
                value: function () {
                    this.setState({
                        isEditing: !1
                    }), this.props.onChange(this.state.newValue)
                }
            }]), t
        } (y["default"].Component);
    t["default"] = (0, N["default"])(b)
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props,
                        t = e.name,
                        n = e.classNames,
                        a = void 0 === n ? "" : n;
                    return "remove" === t ? h["default"].createElement("div", {
                        className: "icon" + (a ? " " + a : "")
                    }, h["default"].createElement("img", {
                        className: "icon-remove",
                        src: chrome.extension.getURL("assets/xbutton.png")
                    })) : h["default"].createElement("div", {
                        className: "icon icon-" + t
                    })
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = v
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(249),
        g = n(7),
        E = a(g),
        y = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props.modal;
                    return h["default"].createElement("div", {
                        className: "modal-background",
                        onClick: function (t) {
                            return e.closeModal()
                        }
                    }, h["default"].createElement("div", {
                        className: "modal-content",
                        onClick: function (e) {
                            return e.stopPropagation()
                        }
                    }, h["default"].createElement("button", {
                        className: "btn btn-primary close-modal",
                        onClick: function (t) {
                            return e.closeModal()
                        }
                    }, "Close"), "dashboard-help" === t ? h["default"].createElement(v.DashboardHelp, null) : "test-help" === t ? h["default"].createElement(v.TestHelp, null) : "component-help" === t ? h["default"].createElement(v.ComponentHelp, null) : "support" === t ? h["default"].createElement(v.Support, null) : null))
                }
            }, {
                key: "closeModal",
                value: function () {
                    E["default"].to(E["default"].SESSION, "setModal", null)
                }
            }]), t
        } (h["default"].Component);
    t["default"] = y
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    return h["default"].createElement("div", {
                        className: "dashboard-modal iframe-container"
                    }, h["default"].createElement("iframe", {
                        src: "https://www.snaptest.io/extension-help?section=inextensionhelp-component"
                    }))
                }
            }]), t
        } (h["default"].Component);
    t["default"] = v
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    return h["default"].createElement("div", {
                        className: "dashboard-modal iframe-container"
                    }, h["default"].createElement("iframe", {
                        src: "https://www.snaptest.io/extension-help?section=inextensionhelp-dashboard"
                    }))
                }
            }]), t
        } (h["default"].Component);
    t["default"] = v
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this;
                    return h["default"].createElement("div", {
                        className: "support-modal grid-row"
                    }, h["default"].createElement("div", {
                        className: "grid-row grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align support-section"
                    }, h["default"].createElement("div", null, "Need more help? ", h["default"].createElement("a", {
                        target: "_blank",
                        href: "https://www.snaptest.io"
                    }, "Visit our website for tutorials"), " or ", h["default"].createElement("a", {
                        target: "_blank",
                        href: "https://www.youtube.com/channel/UCi8nyq-b-tgmL4JU0gFU2BQ"
                    }, "check out our youtube channel"), ".")), h["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align support-section"
                    }, h["default"].createElement("div", null, "Want to do the tutorial again? ", h["default"].createElement("a", {
                        onClick: function (t) {
                            return e.onTutorialStart(t)
                        }
                    }, "Click here."))), h["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align support-section"
                    }, h["default"].createElement("div", null, "Find a bug? ", h["default"].createElement("a", {
                        target: "_blank",
                        href: "https://github.com/ozymandias547/snaptest/issues"
                    }, "Log it here."))), h["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align support-section"
                    }, h["default"].createElement("div", null, "Please take some time to ", h["default"].createElement("a", {
                        target: "_blank",
                        href: "https://www.surveymonkey.com/r/TZ2ZBWX"
                    }, "fill out the feature survey."))), h["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align support-section"
                    }, h["default"].createElement("div", null, "Want to contact us directly?  Email us at ", h["default"].createElement("a", {
                        href: "mailto:joe.snaptest.io@gmail.com"
                    }, "joe.snaptest.io@gmail.com"), " or ", h["default"].createElement("a", {
                        href: "mailto:mike.snaptest.io@gmail.com"
                    }, "mike.snaptest.io@gmail.com"), "."))))
                }
            }, {
                key: "onTutorialStart",
                value: function (e) {
                    e.preventDefault(), g["default"].to(g["default"].SESSION, "setTutStep", 0), g["default"].to(g["default"].SESSION, "setTutActive", !0)
                }
            }]), t
        } (h["default"].Component);
    t["default"] = E
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    return h["default"].createElement("div", {
                        className: "dashboard-modal iframe-container"
                    }, h["default"].createElement("iframe", {
                        src: "https://www.snaptest.io/extension-help?section=inextensionhelp-test"
                    }))
                }
            }]), t
        } (h["default"].Component);
    t["default"] = v
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.Support = t.ComponentHelp = t.TestHelp = t.DashboardHelp = void 0;
    var r = n(246),
        o = a(r),
        i = n(248),
        l = a(i),
        u = n(245),
        c = a(u),
        s = n(247),
        d = a(s);
    t.DashboardHelp = o["default"], t.TestHelp = l["default"], t.ComponentHelp = c["default"], t.Support = d["default"]
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = "LOGIN_MODE",
        y = "REGISTER_MODE",
        S = "FORGOT_MODE",
        N = "RESET_MODE",
        b = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props.accountForms,
                        n = t.email,
                        a = t.password,
                        r = t.password2,
                        o = t.resettoken,
                        i = t.acceptTerms,
                        l = t.type,
                        u = t.isProcessingLogin,
                        c = t.successMessage,
                        s = t.loginError;
                    return h["default"].createElement("form", {
                        className: "LoginForm",
                        onSubmit: function (t) {
                            return e.onSubmit(t)
                        },
                        autoComplete: "off"
                    }, h["default"].createElement("h5", {
                        className: "form-row form-title"
                    }, l === E ? "Login" : l === y ? "Register" : l === S ? "Forgot Password" : "Change password"), (l === E || l === y || l === S) && h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("input", {
                        type: "email",
                        placeholder: "email",
                        value: n,
                        onChange: function (t) {
                            return e.setAccountForm({
                                email: t.currentTarget.value
                            })
                        },
                        autoComplete: "off"
                    })), l === N && h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("input", {
                        type: "password",
                        placeholder: "token from email",
                        value: o,
                        onChange: function (t) {
                            return e.setAccountForm({
                                resettoken: t.currentTarget.value
                            })
                        },
                        autoComplete: "off"
                    })), (l === E || l === y || l === N) && h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("input", {
                        type: "password",
                        placeholder: "password",
                        value: a,
                        onChange: function (t) {
                            return e.setAccountForm({
                                password: t.currentTarget.value
                            })
                        },
                        autoComplete: "off"
                    })), l === E && h["default"].createElement("div", {
                        className: "form-row forgot-my-password"
                    }, h["default"].createElement("a", {
                        onClick: function () {
                            return e.changeTypeto(S)
                        }
                    }, "I forgot my password.")), (l === y || l === N) && h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("input", {
                        type: "password",
                        placeholder: "password confirm",
                        value: r,
                        onChange: function (t) {
                            return e.setAccountForm({
                                password2: t.currentTarget.value
                            })
                        },
                        autoComplete: "off"
                    })), c && l === S && [h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("div", null, c)), h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("input", {
                        type: "password",
                        placeholder: "reset token",
                        value: o,
                        onChange: function (t) {
                            return e.setAccountForm({
                                resettoken: t.currentTarget.value
                            })
                        },
                        autoComplete: "off"
                    }))], l === y && h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("label", null, h["default"].createElement("input", {
                        id: "acce ptTerms",
                        type: "checkbox",
                        value: i,
                        onChange: function (t) {
                            return e.setAccountForm({
                                acceptTerms: t.currentTarget.checked
                            })
                        }
                    }), "I agree to the ", h["default"].createElement("a", {
                        target: "_blank",
                        href: "https://www.snaptest.io/terms-of-service"
                    }, "Terms of Service"), ".")), h["default"].createElement("div", {
                        className: "form-row"
                    }, u ? h["default"].createElement("button", {
                        type: "button",
                        className: "btn btn-primary"
                    }, "processing...") : h["default"].createElement("button", {
                        type: "submit",
                        className: "btn btn-primary"
                    }, l === y ? "register" : l === E ? "login" : l === S ? "Send reset email" : "Change password")), l === E && h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("a", {
                        onClick: function () {
                            return e.changeTypeto(y)
                        }
                    }, "Need an account? Register here.")), (l === y || l === S || l === N) && h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("a", {
                        onClick: function () {
                            return e.changeTypeto(E)
                        }
                    }, "Back to login.")), s && h["default"].createElement("div", {
                        className: "form-row"
                    }, h["default"].createElement("div", {
                        className: "form-server-error"
                    }, s)))
                }
            }, {
                key: "setAccountForm",
                value: function (e) {
                    g["default"].to(g["default"].SESSION, "setAccountForm", e)
                }
            }, {
                key: "changeTypeto",
                value: function (e) {
                    this.setAccountForm({
                        type: e
                    }), g["default"].to(g["default"].SESSION, "resetLogin")
                }
            }, {
                key: "onSubmit",
                value: function (e) {
                    var t = this.props.accountForms,
                        n = t.email,
                        a = t.password,
                        r = t.password2,
                        o = t.resettoken,
                        i = t.acceptTerms,
                        l = t.type;
                    e.preventDefault(), l === E && g["default"].to(g["default"].SESSION, "loginWith", {
                        email: n,
                        password: a
                    }), l === S && g["default"].to(g["default"].SESSION, "forgotWith", {
                        email: n
                    }), l === N && g["default"].to(g["default"].SESSION, "resetWith", {
                        resettoken: o,
                        password: a,
                        password2: r
                    }), l === y && g["default"].to(g["default"].SESSION, "registerWith", {
                        email: n,
                        password: a,
                        password2: r,
                        acceptTerms: i
                    })
                }
            }]), t
        } (h["default"].Component);
    t["default"] = b
}, function (e, t, n) {
    "use strict";
    var a = n(80),
        r = n(1),
        o = (n(1), r.createClass({
            displayName: "UITreeNode",
            renderCollapse: function () {
                var e = this.props.index;
                if (e.node && !e.node.leaf) {
                    var t = e.node.collapsed;
                    return r.createElement("span", {
                        className: a("collapse", t ? "caret-right" : "caret-down"),
                        onMouseDown: function (e) {
                            e.stopPropagation()
                        },
                        onClick: this.handleCollapse
                    })
                }
                return null
            },
            renderChildren: function () {
                var e = this,
                    t = this.props.index,
                    n = this.props.tree,
                    a = this.props.dragging;
                if (t.children && t.children.length) {
                    var i = {};
                    return t.node.collapsed && (i.display = "none"), i.paddingLeft = this.props.paddingLeft + "px", r.createElement("div", {
                        className: "children",
                        style: i
                    }, t.children.map(function (t) {
                        var i = n.getIndex(t);
                        return r.createElement(o, {
                            tree: n,
                            index: i,
                            key: i.id,
                            dragging: a,
                            paddingLeft: e.props.paddingLeft,
                            onCollapse: e.props.onCollapse,
                            onDragStart: e.props.onDragStart
                        })
                    }))
                }
                return null
            },
            render: function () {
                var e = this.props.tree,
                    t = this.props.index,
                    n = this.props.dragging,
                    o = t.node,
                    i = {};
                return r.createElement("div", {
                    className: a("m-node", {
                        placeholder: t.id === n
                    }),
                    style: i
                }, r.createElement("div", {
                    className: "inner",
                    ref: "inner",
                    onMouseDown: this.handleMouseDown
                }, this.renderCollapse(), e.renderNode(o)), this.renderChildren())
            },
            handleCollapse: function (e) {
                e.stopPropagation();
                var t = this.props.index.id;
                this.props.onCollapse && this.props.onCollapse(t)
            },
            handleMouseDown: function (e) {
                var t = this.props.index.id,
                    n = this.refs.inner;
                this.props.onDragStart && this.props.onDragStart(t, n, e)
            }
        }));
    e.exports = o
}, function (e, t, n) {
    "use strict";
    var a = n(1),
        r = n(253),
        o = n(251);
    e.exports = a.createClass({
        displayName: "UITree",
        propTypes: {
            tree: a.PropTypes.object.isRequired,
            paddingLeft: a.PropTypes.number,
            renderNode: a.PropTypes.func.isRequired
        },
        getDefaultProps: function () {
            return {
                paddingLeft: 20
            }
        },
        getInitialState: function () {
            return this.init(this.props)
        },
        componentWillReceiveProps: function (e) {
            this._updated ? this._updated = !1 : this.setState(this.init(e))
        },
        init: function (e) {
            var t = new r(e.tree);
            return t.isNodeCollapsed = e.isNodeCollapsed, t.renderNode = e.renderNode, t.changeNodeCollapsed = e.changeNodeCollapsed, t.updateNodesPosition(), {
                tree: t,
                dragging: {
                    id: null,
                    x: null,
                    y: null,
                    w: null,
                    h: null
                }
            }
        },
        getDraggingDom: function () {
            var e = this.state.tree,
                t = this.state.dragging;
            if (t && t.id) {
                var n = e.getIndex(t.id),
                    r = {
                        top: t.y,
                        left: t.x,
                        width: t.w
                    };
                return a.createElement("div", {
                    className: "m-draggable",
                    style: r
                }, a.createElement(o, {
                    tree: e,
                    index: n,
                    paddingLeft: this.props.paddingLeft
                }))
            }
            return null
        },
        render: function () {
            var e = this.state.tree,
                t = this.state.dragging,
                n = this.getDraggingDom();
            return a.createElement("div", {
                className: "m-tree"
            }, n, a.createElement(o, {
                tree: e,
                index: e.getIndex(1),
                key: 1,
                paddingLeft: this.props.paddingLeft,
                onDragStart: this.dragStart,
                onCollapse: this.toggleCollapse,
                dragging: t && t.id
            }))
        },
        dragStart: function (e, t, n) {
            this.dragging = {
                id: e,
                w: t.offsetWidth,
                h: t.offsetHeight,
                x: t.offsetLeft,
                y: t.offsetTop
            }, this._startX = t.offsetLeft, this._startY = t.offsetTop, this._offsetX = n.clientX, this._offsetY = n.clientY, this._start = !0, window.addEventListener("mousemove", this.drag), window.addEventListener("mouseup", this.dragEnd)
        },
        drag: function (e) {
            this._start && (this.setState({
                dragging: this.dragging
            }), this._start = !1);
            var t = this.state.tree,
                n = this.state.dragging,
                a = this.props.paddingLeft,
                r = null,
                o = t.getIndex(n.id);
            if (o) {
                var i = o.node.collapsed,
                    l = this._startX,
                    u = this._startY,
                    c = this._offsetX,
                    s = this._offsetY,
                    d = {
                        x: l + e.clientX - c,
                        y: u + e.clientY - s
                    };
                n.x = d.x, n.y = d.y;
                var f = n.x - a / 2 - (o.left - 2) * a,
                    p = n.y - n.h / 2 - (o.top - 2) * n.h;
                if (f < 0) o.parent && !o.next && (r = t.move(o.id, o.parent, "after"));
                else if (f > a && o.prev) {
                    var m = t.getIndex(o.prev).node;
                    m.collapsed || m.leaf || (r = t.move(o.id, o.prev, "append"))
                }
                if (r && (o = r, r.node.collapsed = i, n.id = r.id), p < 0) {
                    var h = t.getNodeByTop(o.top - 1);
                    r = t.move(o.id, h.id, "before")
                } else if (p > n.h)
                    if (o.next) {
                        var v = t.getIndex(o.next);
                        r = v.children && v.children.length && !v.node.collapsed ? t.move(o.id, o.next, "prepend") : t.move(o.id, o.next, "after")
                    } else {
                        var v = t.getNodeByTop(o.top + o.height);
                        v && v.parent !== o.id && (r = v.children && v.children.length ? t.move(o.id, v.id, "prepend") : t.move(o.id, v.id, "after"))
                    }
                r && (r.node.collapsed = i, n.id = r.id), this.setState({
                    tree: t,
                    dragging: n
                })
            }
        },
        dragEnd: function () {
            this.setState({
                dragging: {
                    id: null,
                    x: null,
                    y: null,
                    w: null,
                    h: null
                }
            }), this.change(this.state.tree), window.removeEventListener("mousemove", this.drag), window.removeEventListener("mouseup", this.dragEnd)
        },
        change: function (e) {
            this._updated = !0, this.props.onChange && this.props.onChange(e.obj)
        },
        toggleCollapse: function (e) {
            var t = this.state.tree,
                n = t.getIndex(e),
                a = n.node;
            a.collapsed = !a.collapsed, t.updateNodesPosition(), this.setState({
                tree: t
            }), this.change(t)
        }
    })
}, function (e, t, n) {
    "use strict";
    var a = n(261),
        r = a.prototype;
    r.updateNodesPosition = function () {
        function e(n, a, o, i) {
            var l = 1;
            return n.forEach(function (n) {
                var a = r.getIndex(n);
                i ? (a.top = null, a.left = null) : (a.top = t++ , a.left = o), a.children && a.children.length ? l += e(a.children, a, o + 1, i || a.node.collapsed) : (a.height = 1, l += 1)
            }), a.node.collapsed ? a.height = 1 : a.height = l, a.height
        }
        var t = 1,
            n = 1,
            a = this.getIndex(1),
            r = this;
        a.top = t++ , a.left = n++ , a.children && a.children.length && e(a.children, a, n, a.node.collapsed)
    }, r.move = function (e, t, n) {
        if (e !== t && 1 !== t) {
            var a = this.remove(e),
                r = null;
            return "before" === n ? r = this.insertBefore(a, t) : "after" === n ? r = this.insertAfter(a, t) : "prepend" === n ? r = this.prepend(a, t) : "append" === n && (r = this.append(a, t)), this.updateNodesPosition(), r
        }
    }, r.getNodeByTop = function (e) {
        var t = this.indexes;
        for (var n in t)
            if (t.hasOwnProperty(n) && t[n].top === e) return t[n]
    }, e.exports = a
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function r(e, t, n, a, r) {
        e.style.left = t + "px", e.style.top = n + "px", e.style.width = a + "px", e.style.height = r + "px"
    }

    function o(e) {
        h = E.getBoundingClientRect(), v = e.clientX - h.left, g = e.clientY - h.top, f = g < W, d = v < W, c = v >= h.width - W, s = g >= h.height - W, p = window.innerWidth - W, m = window.innerHeight - W
    }

    function i() {
        return v > 0 && v < h.width && g > 0 && g < h.height && g < 30
    }

    function l() {
        if (requestAnimationFrame(l), V) {
            if (V = !1, !F || !F.isResizing) return F && F.isMoving ? u ? void r(E, y.clientX - u.width / 2, y.clientY - Math.min(F.y, u.height), u.width, u.height) : (E.style.top = y.clientY - F.y + "px", void (E.style.left = y.clientX - F.x + "px")) : void (c && s || d && f ? E.style.cursor = "nwse-resize" : c && f || s && d ? E.style.cursor = "nesw-resize" : c || d ? E.style.cursor = "ew-resize" : s || f ? E.style.cursor = "ns-resize" : i() ? E.style.cursor = "move" : E.style.cursor = "default");
            if (F.onRightEdge && (E.style.width = Math.max(v, U) + "px"), F.onBottomEdge && (E.style.height = Math.max(g, j) + "px"), F.onLeftEdge) {
                var e = Math.max(F.cx - y.clientX + F.w, U);
                e > U && (E.style.width = e + "px", E.style.left = y.clientX + "px")
            }
            if (F.onTopEdge) {
                var t = Math.max(F.cy - y.clientY + F.h, j);
                t > j && (E.style.height = t + "px", E.style.top = y.clientY + "px")
            }
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var u, c, s, d, f, p, m, h, v, g, E, y, S, N = n(3),
        b = a(N),
        w = n(2),
        _ = a(w),
        k = n(6),
        C = a(k),
        T = n(5),
        O = a(T),
        A = n(4),
        x = a(A),
        I = n(1),
        R = a(I),
        P = n(7),
        M = a(P),
        L = n(80),
        D = a(L),
        U = 60,
        j = 40,
        W = 10,
        F = null,
        V = !1,
        q = function (e) {
            function t(e) {
                (0, _["default"])(this, t);
                var n = (0, O["default"])(this, (t.__proto__ || (0, b["default"])(t)).call(this, e));
                return e.disableResize && (W = 0), n
            }
            return (0, x["default"])(t, e), (0, C["default"])(t, [{
                key: "setupDragAndResize",
                value: function () {
                    E = this.refs.pane, E.addEventListener("mousedown", this.onMouseDown), document.addEventListener("mousemove", this.onMove), document.addEventListener("mouseup", this.onUp), l()
                }
            }, {
                key: "tearDownDragAndResize",
                value: function () {
                    E = this.refs.pane, E.removeEventListener("mousedown", this.onMouseDown), document.removeEventListener("mousemove", this.onMove), document.removeEventListener("mouseup", this.onUp), E.style.cursor = "default"
                }
            }, {
                key: "componentWillReceiveProps",
                value: function (e) {
                    "window" === e.viewMode && "window" !== this.props.viewMode && (S = "window", this.setupDragAndResize()), "window" !== e.viewMode && "window" === this.props.viewMode && (S = "", this.tearDownDragAndResize())
                }
            }, {
                key: "componentWillUnmount",
                value: function () {
                    S = "", this.tearDownDragAndResize()
                }
            }, {
                key: "onMouseDown",
                value: function (e) {
                    o(e);
                    var t = c || s || f || d;
                    F = {
                        x: v,
                        y: g,
                        cx: e.clientX,
                        cy: e.clientY,
                        w: h.width,
                        h: h.height,
                        isResizing: t,
                        isMoving: !t && i(),
                        onTopEdge: f,
                        onLeftEdge: d,
                        onRightEdge: c,
                        onBottomEdge: s
                    }
                }
            }, {
                key: "onUp",
                value: function (e) {
                    o(e), M["default"].to(M["default"].SESSION, "setViewModeWindowAttr", {
                        viewX: E.style.left,
                        viewY: E.style.top,
                        viewHeight: h.height,
                        viewWidth: h.width
                    }), F = null
                }
            }, {
                key: "onMove",
                value: function (e) {
                    o(e), y = e, V = !0
                }
            }, {
                key: "componentDidMount",
                value: function () {
                    "window" === this.props.viewMode && (this.setupDragAndResize(), S = "window")
                }
            }, {
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.viewMode,
                        a = t.children,
                        r = t.isRecording,
                        o = t.isAssertMode,
                        i = t.showHotkeys,
                        l = t.minimized,
                        u = (0, D["default"])({
                            window: !0,
                            "control-panel": !0,
                            "is-recording": r || o,
                            "show-hotkeys": i,
                            minimized: l
                        });
                    return l ? R["default"].createElement("div", {
                        className: u,
                        style: this.getWindowStyles(),
                        ref: "pane",
                        onClick: function () {
                            return e.onWindowClick()
                        }
                    }) : R["default"].createElement("div", {
                        className: u + " " + n,
                        style: this.getWindowStyles(),
                        ref: "pane",
                        onClick: function () {
                            return e.onWindowClick()
                        }
                    }, a)
                }
            }, {
                key: "onWindowClick",
                value: function () {
                    var e = this.props.minimized;
                    e && M["default"].to(M["default"].SESSION, "setMinimized", !1)
                }
            }, {
                key: "getWindowStyles",
                value: function () {
                    var e = this.props,
                        t = e.viewX,
                        n = e.viewY,
                        a = e.viewWidth,
                        r = e.viewHeight,
                        o = e.viewMode,
                        i = e.minimized;
                    return i ? {
                        right: 10,
                        top: 10,
                        height: "20px",
                        width: "40px"
                    } : "window" === o ? {
                        top: n,
                        left: t,
                        width: a,
                        height: r
                    } : "maximize" === o ? {
                        left: 0,
                        top: 0,
                        height: "100%",
                        width: "100%"
                    } : "right" === o ? {
                        right: 0,
                        top: 0,
                        height: "100%",
                        width: 500
                    } : "left" === o ? {
                        left: 0,
                        top: 0,
                        height: "100%",
                        width: 500
                    } : "top" === o ? {
                        left: 0,
                        top: 0,
                        height: "35%",
                        width: "100%"
                    } : "bottom" === o ? {
                        left: 0,
                        bottom: 0,
                        top: "inherit",
                        height: "35%",
                        width: "100%"
                    } : null
                }
            }]), t
        } (R["default"].Component);
    t["default"] = q
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(2),
        o = a(r),
        i = n(32),
        l = function u(e, t) {
            (0, o["default"])(this, u), this.name = "Unnamed component", this.actions = [], this.id = (0, i.generate)(), this.isBlank = !0, this.type = "component", this.variables = [], e && (this.name = e, this.isBlank = !1), t && (this.actions = t, this.isBlank = !1)
        };
    t["default"] = l
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.NumberVariable = t.StringVariable = t.CompVariable = t.Variable = void 0;
    var r = n(3),
        o = a(r),
        i = n(5),
        l = a(i),
        u = n(4),
        c = a(u),
        s = n(2),
        d = a(s),
        f = n(32),
        p = "COMP_VAR",
        m = "STRING_VAR",
        h = "NUMBER_VAR",
        v = t.Variable = function g(e) {
            (0, d["default"])(this, g), this.id = (0, f.generate)(), this.name = "var", this.name = e
        };
    t.CompVariable = function E(e, t, n, a) {
        (0, d["default"])(this, E), this.actionId = null, this.id = (0, f.generate)(), this.name = "var", this.defaultValue = "", this.varType = "string", this.type = p, e && (this.name = e), t && (this.defaultValue = t), n && (this.actionId = n), a && (this.varType = a)
    }, t.StringVariable = function (e) {
        function t(e, n) {
            (0, d["default"])(this, t);
            var a = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e));
            return a.defaultValue = "", a.type = m, n && (a.defaultValue = n), a
        }
        return (0, c["default"])(t, e), t
    } (v), t.NumberVariable = function (e) {
        function t(e, n) {
            (0, d["default"])(this, t);
            var a = (0, l["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e));
            return a.defaultValue = 0, a.type = h, n && (a.defaultValue = n), a
        }
        return (0, c["default"])(t, e), t
    } (v)
}, function (e, t) {
    "use strict";

    function n(e) {
        return k[k.indexOf(e)]
    }

    function a(e) {
        return k[e]
    }

    function r(e) {
        var t = k.indexOf(step);
        return k[t + 1]
    }

    function o(e) {
        var t = k.indexOf(step);
        return k[t - 1]
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.getStepIndex = n, t.getStepName = a, t.getNextStep = r, t.getLastStep = o;
    var i = t.INTRO_START = "INTRO_START",
        l = t.INTRO_WINDOWS = "INTRO_WINDOWS",
        u = t.FIRSTTEST_CREATE = "FIRSTTEST_CREATE",
        c = t.FIRSTTEST_EDIT = "FIRSTTEST_EDIT",
        s = (t.FIRSTTEST_PRETHOUGHT = "FIRSTTEST_PRETHOUGHT", t.FIRSTTEST_NAVTOSNAP = "FIRSTTEST_NAVTOSNAP"),
        d = t.FIRSTTEST_PRETEND = "FIRSTTEST_PRETEND",
        f = t.FIRSTTEST_STOPRECORD = "FIRSTTEST_STOPRECORD",
        p = t.FIRSTTEST_ASSERT = "FIRSTTEST_ASSERT",
        m = t.FIRSTTEST_STOPASSERT = "FIRSTTEST_STOPASSERT",
        h = t.FIRSTTEST_SIMULATE = "FIRSTTEST_SIMULATE",
        v = t.FIRSTTEST_SIMULATE2 = "FIRSTTEST_SIMULATE2",
        g = t.FIRSTTEST_NAMESTEP = "FIRSTTEST_NAMESTEP",
        E = t.FIRSTTEST_ADJUSTSELECTOR = "FIRSTTEST_ADJUSTSELECTOR",
        y = t.FIRSTTEST_MANUALADD = "FIRSTTEST_MANUALADD",
        S = t.FIRSTTEST_MANAGE = "FIRSTTEST_MANAGE",
        N = t.FIRSTTEST_MANAGE2 = "FIRSTTEST_MANAGE2",
        b = t.FIRSTTEST_MANAGE3 = "FIRSTTEST_MANAGE3",
        w = t.EXISTTEST_KICKOFF = "EXISTTEST_KICKOFF",
        _ = (t.EXISTTEST_FIX = "EXISTTEST_FIX", t.SUITE_CODE = "SUITE_CODE", t.SUITE_CLI = "SUITE_CLI", t.END = "END"),
        k = t.ORDER = [i, l, u, c, s, d, f, p, m, h, v, g, E, y, S, N, b, w, _]
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }

    function r() {
        var e = this;
        c["default"].bind(this.state.hotkeyConfig.TOGGLE_VIEW_HOTKEYS, function () {
            l["default"].to(l["default"].SESSION, "setShowHotkeys", !e.state.showHotkeys)
        }), c["default"].bind(this.state.hotkeyConfig.DELETE_ROWS, function () {
            l["default"].to(l["default"].SESSION, "removeNWActions"), l["default"].to(l["default"].SESSION, "clearSelectedRows")
        }), c["default"].bind(this.state.hotkeyConfig.START_PLAYBACK, function () {
            l["default"].to(l["default"].SESSION, "startPlayback")
        }), c["default"].bind(this.state.hotkeyConfig.UNDO, function () {
            l["default"].to(l["default"].SESSION, "undo")
        }), c["default"].bind(this.state.hotkeyConfig.REDO, function () {
            l["default"].to(l["default"].SESSION, "redo")
        }), c["default"].bind(this.state.hotkeyConfig.CLEAR_SELECTION, function () {
            l["default"].to(l["default"].SESSION, "clearSelectedRows")
        }), c["default"].bind(this.state.hotkeyConfig.ROLLUP_SELECTION, function () {
            l["default"].to(l["default"].SESSION, "createComponentFromSelectedRows")
        }), c["default"].bind(this.state.hotkeyConfig.BACK, function () {
            l["default"].to(l["default"].SESSION, "backRoute")
        }), c["default"].bind(this.state.hotkeyConfig.ASSERT_MODE, function () {
            var t = e.state.viewStack[e.state.viewStack.length - 1].name;
            "testbuilder" !== t && "componentbuilder" !== t || l["default"].to(l["default"].SESSION, "setAssertMode", !e.state.isAssertMode)
        }), c["default"].bind(this.state.hotkeyConfig.RECORD_MODE, function () {
            var t = e.state.viewStack[e.state.viewStack.length - 1].name;
            "testbuilder" !== t && "componentbuilder" !== t || (e.state.isRecording ? l["default"].to(l["default"].SESSION, "stopRecording") : l["default"].to(l["default"].SESSION, "startRecording", {
                initialUrl: window.location.href,
                width: window.innerWidth,
                height: window.innerHeight
            }))
        }), c["default"].bind(this.state.hotkeyConfig.CANCEL_MODE, function () {
            l["default"].to(l["default"].SESSION, "setAssertMode", !1), l["default"].to(l["default"].SESSION, "stopRecording")
        }), c["default"].bind(this.state.hotkeyConfig.NEW_TEST, function () {
            var t = e.state.viewStack[e.state.viewStack.length - 1].name;
            if ("dashboard" === t) {
                var n = new d["default"];
                l["default"].to(l["default"].SESSION, "addNewTest", n), l["default"].to(l["default"].SESSION, "setTestActive", n.id), l["default"].to(l["default"].SESSION, "pushRoute", new p["default"]("testbuilder", {
                    testId: n.id
                })), l["default"].to(l["default"].SESSION, "startRecording", {
                    initialUrl: window.location.href,
                    width: window.innerWidth,
                    height: window.innerHeight
                })
            }
        })
    }

    function o() { }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.bindHotkeys = r, t.unbindHotkeys = o;
    var i = n(7),
        l = a(i),
        u = n(266),
        c = a(u),
        s = n(141),
        d = a(s),
        f = n(27),
        p = a(f)
}, function (e, t, n) {
    var a = n(9),
        r = a.JSON || (a.JSON = {
            stringify: JSON.stringify
        });
    e.exports = function (e) {
        return r.stringify.apply(r, arguments)
    }
}, function (e, t) { }, function (e, t) {
    function n(e) {
        this.cnt = 1, this.obj = e || {
            children: []
        }, this.indexes = {}, this.build(this.obj)
    }
    var a = n.prototype;
    a.build = function (e) {
        function t(e, a) {
            var o = [];
            e.forEach(function (e, i) {
                var l = {};
                l.id = r.cnt, l.node = e, a && (l.parent = a.id), n[r.cnt + ""] = l, o.push(r.cnt), r.cnt++ , e.children && e.children.length && t(e.children, l)
            }), a.children = o, o.forEach(function (e, t) {
                var a = n[e + ""];
                t > 0 && (a.prev = o[t - 1]), t < o.length - 1 && (a.next = o[t + 1])
            })
        }
        var n = this.indexes,
            a = this.cnt,
            r = this,
            o = {
                id: a,
                node: e
            };
        return n[this.cnt + ""] = o, this.cnt++ , e.children && e.children.length && t(e.children, o), o
    }, a.getIndex = function (e) {
        var t = this.indexes[e + ""];
        if (t) return t
    }, a.removeIndex = function (e) {
        function t(e) {
            delete n.indexes[e.id + ""], e.children && e.children.length && e.children.forEach(function (e) {
                t(n.getIndex(e))
            })
        }
        var n = this;
        t(e)
    }, a.get = function (e) {
        var t = this.getIndex(e);
        return t && t.node ? t.node : null
    }, a.remove = function (e) {
        var t = this.getIndex(e),
            n = this.get(e),
            a = this.getIndex(t.parent),
            r = this.get(t.parent);
        return r.children.splice(r.children.indexOf(n), 1), a.children.splice(a.children.indexOf(e), 1), this.removeIndex(t), this.updateChildren(a.children), n
    }, a.updateChildren = function (e) {
        e.forEach(function (t, n) {
            var a = this.getIndex(t);
            a.prev = a.next = null, n > 0 && (a.prev = e[n - 1]), n < e.length - 1 && (a.next = e[n + 1])
        }.bind(this))
    }, a.insert = function (e, t, n) {
        var a = this.getIndex(t),
            r = this.get(t),
            o = this.build(e);
        return o.parent = t, r.children = r.children || [], a.children = a.children || [], r.children.splice(n, 0, e), a.children.splice(n, 0, o.id), this.updateChildren(a.children), a.parent && this.updateChildren(this.getIndex(a.parent).children), o
    }, a.insertBefore = function (e, t) {
        var n = this.getIndex(t),
            a = n.parent,
            r = this.getIndex(a).children.indexOf(t);
        return this.insert(e, a, r)
    }, a.insertAfter = function (e, t) {
        var n = this.getIndex(t),
            a = n.parent,
            r = this.getIndex(a).children.indexOf(t);
        return this.insert(e, a, r + 1)
    }, a.prepend = function (e, t) {
        return this.insert(e, t, 0)
    }, a.append = function (e, t) {
        var n = this.getIndex(t);
        return n.children = n.children || [], this.insert(e, t, n.children.length)
    }, e.exports = n
}, function (e, t, n) {
    function a(e) {
        return function (t, n, a) {
            var l = Object(t);
            if (!o(t)) {
                var u = r(n, 3);
                t = i(t), n = function (e) {
                    return u(l[e], e, l)
                }
            }
            var c = e(t, n, a);
            return c > -1 ? l[u ? t[c] : c] : void 0
        }
    }
    var r = n(97),
        o = n(81),
        i = n(75);
    e.exports = a
}, function (e, t, n) {
    function a(e, t, n) {
        function a(t) {
            var n = E,
                a = y;
            return E = y = void 0, _ = t, N = e.apply(a, n)
        }

        function s(e) {
            return _ = e, b = setTimeout(p, t), k ? a(e) : N
        }

        function d(e) {
            var n = e - w,
                a = e - _,
                r = t - n;
            return C ? c(r, S - a) : r
        }

        function f(e) {
            var n = e - w,
                a = e - _;
            return void 0 === w || n >= t || n < 0 || C && a >= S
        }

        function p() {
            var e = o();
            return f(e) ? m(e) : void (b = setTimeout(p, d(e)))
        }

        function m(e) {
            return b = void 0, T && E ? a(e) : (E = y = void 0, N)
        }

        function h() {
            void 0 !== b && clearTimeout(b), _ = 0, E = w = y = b = void 0
        }

        function v() {
            return void 0 === b ? N : m(o())
        }

        function g() {
            var e = o(),
                n = f(e);
            if (E = arguments, y = this, w = e, n) {
                if (void 0 === b) return s(w);
                if (C) return b = setTimeout(p, t), a(w)
            }
            return void 0 === b && (b = setTimeout(p, t)), N
        }
        var E, y, S, N, b, w, _ = 0,
            k = !1,
            C = !1,
            T = !0;
        if ("function" != typeof e) throw new TypeError(l);
        return t = i(t) || 0, r(n) && (k = !!n.leading, C = "maxWait" in n, S = C ? u(i(n.maxWait) || 0, t) : S, T = "trailing" in n ? !!n.trailing : T), g.cancel = h, g.flush = v, g
    }
    var r = n(33),
        o = n(265),
        i = n(98),
        l = "Expected a function",
        u = Math.max,
        c = Math.min;
    e.exports = a
}, function (e, t, n) {
    function a(e) {
        if (null == e) return !0;
        if (u(e) && (l(e) || "string" == typeof e || "function" == typeof e.splice || c(e) || d(e) || i(e))) return !e.length;
        var t = o(e);
        if (t == f || t == p) return !e.size;
        if (s(e)) return !r(e).length;
        for (var n in e)
            if (h.call(e, n)) return !1;
        return !0
    }
    var r = n(136),
        o = n(137),
        i = n(77),
        l = n(12),
        u = n(81),
        c = n(78),
        s = n(138),
        d = n(79),
        f = "[object Map]",
        p = "[object Set]",
        m = Object.prototype,
        h = m.hasOwnProperty;
    e.exports = a
}, function (e, t, n) {
    var a = n(11),
        r = function () {
            return a.Date.now()
        };
    e.exports = r
}, function (e, t, n) {
    var a;
    ! function (r, o, i) {
        function l(e, t, n) {
            return e.addEventListener ? void e.addEventListener(t, n, !1) : void e.attachEvent("on" + t, n)
        }

        function u(e) {
            if ("keypress" == e.type) {
                var t = String.fromCharCode(e.which);
                return e.shiftKey || (t = t.toLowerCase()), t
            }
            return N[e.which] ? N[e.which] : b[e.which] ? b[e.which] : String.fromCharCode(e.which).toLowerCase()
        }

        function c(e, t) {
            return e.sort().join(",") === t.sort().join(",")
        }

        function s(e) {
            var t = [];
            return e.shiftKey && t.push("shift"), e.altKey && t.push("alt"), e.ctrlKey && t.push("ctrl"), e.metaKey && t.push("meta"), t
        }

        function d(e) {
            return e.preventDefault ? void e.preventDefault() : void (e.returnValue = !1)
        }

        function f(e) {
            return e.stopPropagation ? void e.stopPropagation() : void (e.cancelBubble = !0)
        }

        function p(e) {
            return "shift" == e || "ctrl" == e || "alt" == e || "meta" == e
        }

        function m() {
            if (!S) {
                S = {};
                for (var e in N) e > 95 && e < 112 || N.hasOwnProperty(e) && (S[N[e]] = e)
            }
            return S
        }

        function h(e, t, n) {
            return n || (n = m()[e] ? "keydown" : "keypress"), "keypress" == n && t.length && (n = "keydown"), n
        }

        function v(e) {
            return "+" === e ? ["+"] : (e = e.replace(/\+{2}/g, "+plus"), e.split("+"))
        }

        function g(e, t) {
            var n, a, r, o = [];
            for (n = v(e), r = 0; r < n.length; ++r) a = n[r], _[a] && (a = _[a]), t && "keypress" != t && w[a] && (a = w[a], o.push("shift")), p(a) && o.push(a);
            return t = h(a, o, t), {
                key: a,
                modifiers: o,
                action: t
            }
        }

        function E(e, t) {
            return null !== e && e !== o && (e === t || E(e.parentNode, t))
        }

        function y(e) {
            function t(e) {
                e = e || {};
                var t, n = !1;
                for (t in S) e[t] ? n = !0 : S[t] = 0;
                n || (w = !1)
            }

            function n(e, t, n, a, r, o) {
                var i, l, u = [],
                    s = n.type;
                if (!v._callbacks[e]) return [];
                for ("keyup" == s && p(e) && (t = [e]), i = 0; i < v._callbacks[e].length; ++i)
                    if (l = v._callbacks[e][i], (a || !l.seq || S[l.seq] == l.level) && s == l.action && ("keypress" == s && !n.metaKey && !n.ctrlKey || c(t, l.modifiers))) {
                        var d = !a && l.combo == r,
                            f = a && l.seq == a && l.level == o;
                        (d || f) && v._callbacks[e].splice(i, 1), u.push(l)
                    }
                return u
            }

            function a(e, t, n, a) {
                v.stopCallback(t, t.target || t.srcElement, n, a) || e(t, n) === !1 && (d(t), f(t))
            }

            function r(e) {
                "number" != typeof e.which && (e.which = e.keyCode);
                var t = u(e);
                if (t) return "keyup" == e.type && N === t ? void (N = !1) : void v.handleKey(t, s(e), e)
            }

            function i() {
                clearTimeout(E), E = setTimeout(t, 1e3)
            }

            function m(e, n, r, o) {
                function l(t) {
                    return function () {
                        w = t, ++S[e], i()
                    }
                }

                function c(n) {
                    a(r, n, e), "keyup" !== o && (N = u(n)), setTimeout(t, 10)
                }
                S[e] = 0;
                for (var s = 0; s < n.length; ++s) {
                    var d = s + 1 === n.length,
                        f = d ? c : l(o || g(n[s + 1]).action);
                    h(n[s], f, o, e, s)
                }
            }

            function h(e, t, a, r, o) {
                v._directMap[e + ":" + a] = t, e = e.replace(/\s+/g, " ");
                var i, l = e.split(" ");
                return l.length > 1 ? void m(e, l, t, a) : (i = g(e, a), v._callbacks[i.key] = v._callbacks[i.key] || [], n(i.key, i.modifiers, {
                    type: i.action
                }, r, e, o), void v._callbacks[i.key][r ? "unshift" : "push"]({
                    callback: t,
                    modifiers: i.modifiers,
                    action: i.action,
                    seq: r,
                    level: o,
                    combo: e
                }))
            }
            var v = this;
            if (e = e || o, !(v instanceof y)) return new y(e);
            v.target = e, v._callbacks = {}, v._directMap = {};
            var E, S = {},
                N = !1,
                b = !1,
                w = !1;
            v._handleKey = function (e, r, o) {
                var i, l = n(e, r, o),
                    u = {},
                    c = 0,
                    s = !1;
                for (i = 0; i < l.length; ++i) l[i].seq && (c = Math.max(c, l[i].level));
                for (i = 0; i < l.length; ++i)
                    if (l[i].seq) {
                        if (l[i].level != c) continue;
                        s = !0, u[l[i].seq] = 1, a(l[i].callback, o, l[i].combo, l[i].seq)
                    } else s || a(l[i].callback, o, l[i].combo);
                var d = "keypress" == o.type && b;
                o.type != w || p(e) || d || t(u), b = s && "keydown" == o.type
            }, v._bindMultiple = function (e, t, n) {
                for (var a = 0; a < e.length; ++a) h(e[a], t, n)
            }, l(e, "keypress", r), l(e, "keydown", r), l(e, "keyup", r)
        }
        if (r) {
            for (var S, N = {
                8: "backspace",
                9: "tab",
                13: "enter",
                16: "shift",
                17: "ctrl",
                18: "alt",
                20: "capslock",
                27: "esc",
                32: "space",
                33: "pageup",
                34: "pagedown",
                35: "end",
                36: "home",
                37: "left",
                38: "up",
                39: "right",
                40: "down",
                45: "ins",
                46: "del",
                91: "meta",
                93: "meta",
                224: "meta"
            }, b = {
                106: "*",
                107: "+",
                109: "-",
                110: ".",
                111: "/",
                186: ";",
                187: "=",
                188: ",",
                189: "-",
                190: ".",
                191: "/",
                192: "`",
                219: "[",
                220: "\\",
                221: "]",
                222: "'"
            }, w = {
                "~": "`",
                "!": "1",
                "@": "2",
                "#": "3",
                $: "4",
                "%": "5",
                "^": "6",
                "&": "7",
                "*": "8",
                "(": "9",
                ")": "0",
                _: "-",
                "+": "=",
                ":": ";",
                '"': "'",
                "<": ",",
                ">": ".",
                "?": "/",
                "|": "\\"
            }, _ = {
                option: "alt",
                command: "meta",
                "return": "enter",
                escape: "esc",
                plus: "+",
                mod: /Mac|iPod|iPhone|iPad/.test(navigator.platform) ? "meta" : "ctrl"
            }, k = 1; k < 20; ++k) N[111 + k] = "f" + k;
            for (k = 0; k <= 9; ++k) N[k + 96] = k;
            y.prototype.bind = function (e, t, n) {
                var a = this;
                return e = e instanceof Array ? e : [e], a._bindMultiple.call(a, e, t, n), a
            }, y.prototype.unbind = function (e, t) {
                var n = this;
                return n.bind.call(n, e, function () { }, t)
            }, y.prototype.trigger = function (e, t) {
                var n = this;
                return n._directMap[e + ":" + t] && n._directMap[e + ":" + t]({}, e), n
            }, y.prototype.reset = function () {
                var e = this;
                return e._callbacks = {}, e._directMap = {}, e
            }, y.prototype.stopCallback = function (e, t) {
                var n = this;
                return !((" " + t.className + " ").indexOf(" mousetrap ") > -1) && (!E(t, n.target) && ("INPUT" == t.tagName || "SELECT" == t.tagName || "TEXTAREA" == t.tagName || t.isContentEditable))
            }, y.prototype.handleKey = function () {
                var e = this;
                return e._handleKey.apply(e, arguments)
            }, y.addKeycodes = function (e) {
                for (var t in e) e.hasOwnProperty(t) && (N[t] = e[t]);
                S = null
            }, y.init = function () {
                var e = y(o);
                for (var t in e) "_" !== t.charAt(0) && (y[t] = function (t) {
                    return function () {
                        return e[t].apply(e, arguments)
                    }
                } (t))
            }, y.init(), r.Mousetrap = y, "undefined" != typeof e && e.exports && (e.exports = y), a = function () {
                return y
            }.call(t, n, t, e), !(a !== i && (e.exports = a))
        }
    } ("undefined" != typeof window ? window : null, "undefined" != typeof window ? document : null)
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = (n(8), n(57)),
        g = (a(v), n(7)),
        E = a(g),
        y = n(27),
        S = (a(y), function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.user;
                    t.lastRoute;
                    return h["default"].createElement("div", null, h["default"].createElement("div", {
                        className: "account-section"
                    }, h["default"].createElement("a", {
                        onClick: function (t) {
                            return e.onLogout(t)
                        }
                    }, "Logout")), h["default"].createElement("div", {
                        className: "account-section"
                    }, "Email: ", h["default"].createElement("b", null, n.email)), h["default"].createElement("div", {
                        className: "account-section"
                    }, "API Key: ", h["default"].createElement("b", null, n.apiKey)))
                }
            }, {
                key: "onLogout",
                value: function (e) {
                    setTimeout(function () {
                        e.stopPropagation(), e.preventDefault(), E["default"].to(E["default"].SESSION, "logout")
                    }, 16)
                }
            }]), t
        } (h["default"].Component));
    t["default"] = S
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = (n(8), n(57)),
        g = a(v),
        E = n(7),
        y = a(E),
        S = n(27),
        N = a(S),
        b = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props,
                        t = e.user,
                        n = e.lastRoute;
                    return h["default"].createElement("div", {
                        className: "grid-row top-menu-container v-align"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, h["default"].createElement("button", {
                        className: "btn btn-primary",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "backRoute")
                        }
                    }, n ? n.name : "back")), h["default"].createElement("div", {
                        className: "grid-item grid-row h-align db-title"
                    }, "Account Settings"), h["default"].createElement("a", {
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "pushRoute", new N["default"]("account"))
                        }
                    }, t.email), h["default"].createElement("img", {
                        className: "help-icon",
                        src: chrome.extension.getURL("assets/help.png"),
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "setModal", "dashboard-help")
                        }
                    }), h["default"].createElement(g["default"], this.props))
                }
            }]), t
        } (h["default"].Component);
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(55),
        o = a(r),
        i = n(3),
        l = a(i),
        u = n(2),
        c = a(u),
        s = n(6),
        d = a(s),
        f = n(5),
        p = a(f),
        m = n(4),
        h = a(m),
        v = n(1),
        g = a(v),
        E = n(8),
        y = n(294),
        S = a(y),
        N = n(295),
        b = a(N),
        w = n(240),
        _ = a(w),
        k = n(293),
        C = a(k),
        T = n(268),
        O = a(T),
        A = n(267),
        x = a(A),
        I = n(272),
        R = a(I),
        P = n(273),
        M = a(P),
        L = n(271),
        D = a(L),
        U = n(270),
        j = a(U),
        W = n(292),
        F = a(W),
        V = n(7),
        q = a(V),
        B = function (e) {
            function t(e) {
                return (0, c["default"])(this, t), (0, p["default"])(this, (t.__proto__ || (0, l["default"])(t)).call(this, e))
            }
            return (0, h["default"])(t, e), (0, d["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props,
                        t = e.viewStack,
                        n = e.user,
                        a = e.primaryTabId,
                        r = e.currentTabId,
                        i = e.activeTabAlert,
                        l = e.tutorialActivated,
                        u = t[t.length - 1],
                        c = t[t.length - 2] ? t[t.length - 2] : null,
                        s = (0, o["default"])({}, this.props, {
                            currentRoute: u,
                            lastRoute: c,
                            user: n
                        });
                    return g["default"].createElement("div", {
                        className: "full-height" + (l ? " tutorial-active" : "")
                    }, s.modal && g["default"].createElement(E.Modal, {
                        modal: s.modal
                    }), a !== r && i && g["default"].createElement("div", {
                        className: "alert-panel"
                    }, g["default"].createElement("div", {
                        className: "grid-row v-align"
                    }, g["default"].createElement("div", {
                        className: "grid-item-2 active-tab-message"
                    }, g["default"].createElement("p", null, g["default"].createElement("span", {
                        className: "underline-this"
                    }, "Active tab isn't in focus."), " Either return focus, or set current tab as active using this menu:"), g["default"].createElement("p", null, g["default"].createElement("a", {
                        onClick: function () {
                            return q["default"].to(q["default"].SESSION, "setActiveTabAlert", !1)
                        }
                    }, "Dismiss this message"))), g["default"].createElement("div", {
                        className: "grid-item"
                    }, g["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/changeActiveTab.png"),
                        style: {
                            width: "100%"
                        }
                    })))), l && !!n && g["default"].createElement(M["default"], s), n ? "dashboard" === u.name ? g["default"].createElement("div", {
                        className: "content"
                    }, g["default"].createElement(D["default"], s), g["default"].createElement(j["default"], s)) : "codeviewer" === u.name ? g["default"].createElement("div", {
                        className: "content"
                    }, g["default"].createElement(C["default"], s), g["default"].createElement(_["default"], s)) : "account" === u.name ? g["default"].createElement("div", {
                        className: "content"
                    }, g["default"].createElement(O["default"], s), g["default"].createElement(x["default"], s)) : "testbuilder" === u.name || "testbuildercode" === u.name ? g["default"].createElement("div", {
                        className: "content " + u.name
                    }, g["default"].createElement(S["default"], s), g["default"].createElement(b["default"], s), g["default"].createElement("button", {
                        className: "btn btn-secondary support",
                        onClick: function () {
                            return q["default"].to(q["default"].SESSION, "setModal", "support")
                        }
                    }, "Support")) : "componentbuilder" === u.name ? g["default"].createElement("div", {
                        className: "content is-component"
                    }, g["default"].createElement(F["default"], s), g["default"].createElement(b["default"], s), g["default"].createElement("button", {
                        className: "btn btn-secondary support",
                        onClick: function () {
                            return q["default"].to(q["default"].SESSION, "setModal", "support")
                        }
                    }, "Support")) : g["default"].createElement("div", {
                        className: "content"
                    }, g["default"].createElement(D["default"], s), g["default"].createElement("button", {
                        className: "btn btn-secondary support",
                        onClick: function () {
                            return q["default"].to(q["default"].SESSION, "setModal", "support")
                        }
                    }, "Support")) : g["default"].createElement(R["default"], s))
                }
            }]), t
        } (g["default"].Component);
    t["default"] = B
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(55),
        o = a(r),
        i = n(3),
        l = a(i),
        u = n(2),
        c = a(u),
        s = n(6),
        d = a(s),
        f = n(5),
        p = a(f),
        m = n(4),
        h = a(m),
        v = n(93),
        g = a(v),
        E = n(1),
        y = a(E),
        S = n(8),
        N = n(7),
        b = a(N),
        w = n(142),
        _ = n(141),
        k = a(_),
        C = n(27),
        T = a(C),
        O = n(32),
        A = function (e) {
            function t(e) {
                return (0, c["default"])(this, t), (0, p["default"])(this, (t.__proto__ || (0, l["default"])(t)).call(this, e))
            }
            return (0, h["default"])(t, e), (0, d["default"])(t, [{
                key: "getTestTree",
                value: function () {
                    var e = this.props,
                        t = e.directory,
                        n = e.tests,
                        a = e.components,
                        r = n.concat(a),
                        i = [],
                        l = [],
                        u = (0, o["default"])({}, t);
                    return (0, w.walkThroughTreeNodes)(u, function (e) {
                        e.testId && ((0, g["default"])(r, {
                            id: e.testId
                        }) || l.push(e.id), i.push(e.testId))
                    }), l.forEach(function (e) {
                        (0, w.removeNodeFromTree)(u, e)
                    }), n.forEach(function (e) {
                        i.indexOf(e.id) === -1 && u.children.unshift({
                            module: e.name,
                            testId: e.id,
                            leaf: !0,
                            id: (0, O.generate)(),
                            type: "test"
                        })
                    }), a.forEach(function (e) {
                        i.indexOf(e.id) === -1 && u.children.unshift({
                            module: e.name,
                            testId: e.id,
                            leaf: !0,
                            id: (0, O.generate)(),
                            type: "component"
                        })
                    }), u
                }
            }, {
                key: "onAddTestTreeFolder",
                value: function () {
                    b["default"].to(b["default"].SESSION, "addTestFolder"), this.forceUpdate()
                }
            }, {
                key: "onChangeTestFolderName",
                value: function (e, t) {
                    b["default"].to(b["default"].SESSION, "changeTestFolderName", {
                        id: e.id,
                        name: t
                    }), this.forceUpdate()
                }
            }, {
                key: "componentDidMount",
                value: function () {
                    this.onTestTreeChange(this.props.directory)
                }
            }, {
                key: "onTestTreeChange",
                value: function (e) {
                    e && b["default"].to(b["default"].SESSION, "saveTree", e)
                }
            }, {
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props;
                    t.tests, t.components;
                    return y["default"].createElement("div", {
                        className: "grid-item grid-row grid-column db-content"
                    }, y["default"].createElement("div", {
                        className: "tests"
                    }, y["default"].createElement(S.Tree, {
                        paddingLeft: 18,
                        tree: this.getTestTree(),
                        onChange: function (t) {
                            return e.onTestTreeChange(t)
                        },
                        renderNode: function (t) {
                            return e.renderNode(t)
                        }
                    })))
                }
            }, {
                key: "renderNode",
                value: function (e) {
                    var t = this,
                        n = this.props,
                        a = n.tests,
                        r = n.components,
                        o = n.currentTabId,
                        i = n.primaryTabId;
                    if (e.topLevel) return y["default"].createElement("div", {
                        className: "grid-row test-row"
                    }, y["default"].createElement("div", {
                        className: "section-header grid-item all-tests grid-row"
                    }, y["default"].createElement("div", null, "All Tests"), y["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, y["default"].createElement("button", {
                        className: "quick-button",
                        title: "Generate all code",
                        onMouseDown: this.stopPropagation,
                        onClick: function () {
                            return t.onGenerateAllCode()
                        }
                    }, y["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/folder_code.png")
                    })), y["default"].createElement("button", {
                        className: "quick-button",
                        title: "View generated code",
                        onMouseDown: this.stopPropagation,
                        onClick: function () {
                            return t.onAddTestTreeFolder()
                        }
                    }, y["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/folder.png")
                    })))), y["default"].createElement("div", {
                        className: "button-group"
                    }, y["default"].createElement("button", {
                        className: "btn recording-btn" + (i !== o ? " btn-disabled btn-clickable" : ""),
                        onMouseDown: this.stopPropagation,
                        onClick: function () {
                            return t.onStartNewTest()
                        }
                    }, this.props.showHotkeys && y["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", this.props.hotkeyConfig.NEW_TEST, ")"), "new test")));
                    if (e.leaf && "test" === e.type) {
                        var l = (0, g["default"])(a, {
                            id: e.testId
                        });
                        return y["default"].createElement("div", {
                            className: "test-row"
                        }, y["default"].createElement("div", {
                            className: "item grid-row v-align",
                            onMouseDown: function (e) {
                                return t.checkForHandle(e)
                            }
                        }, y["default"].createElement("div", {
                            className: "test-move handle",
                            draggable: !1
                        }, y["default"].createElement("img", {
                            className: "handle",
                            draggable: !1,
                            src: chrome.extension.getURL("assets/order.png")
                        })), y["default"].createElement("div", {
                            className: "square handle"
                        }, "Test:"), y["default"].createElement("div", {
                            className: "grid-item"
                        }, y["default"].createElement(S.EditableLabel, {
                            value: l.name,
                            size: l.name.length,
                            onClick: function () {
                                return t.onViewTest(l)
                            },
                            onChange: function (e) {
                                return t.onTestNameChange(e, l)
                            },
                            doubleClickEdit: !1
                        })), y["default"].createElement("div", {
                            className: "action-quick-buttons"
                        }, y["default"].createElement("button", {
                            className: "quick-button",
                            title: "Edit test",
                            onMouseDown: this.stopPropagation,
                            onClick: function (e) {
                                e.stopPropagation(), t.onViewTest(l)
                            }
                        }, y["default"].createElement("img", {
                            src: chrome.extension.getURL("assets/edit_2.png")
                        })), y["default"].createElement("button", {
                            className: "quick-button",
                            title: "View generated code",
                            onMouseDown: this.stopPropagation,
                            onClick: function (e) {
                                e.stopPropagation(), t.onViewCode(l)
                            }
                        }, y["default"].createElement("img", {
                            src: chrome.extension.getURL("assets/code.png")
                        })), y["default"].createElement("button", {
                            className: "quick-button",
                            title: "Duplicate test",
                            onMouseDown: this.stopPropagation,
                            onClick: function (e) {
                                e.stopPropagation(), t.onDuplicateTest(l)
                            }
                        }, y["default"].createElement("img", {
                            src: chrome.extension.getURL("assets/duplicate.png")
                        })), y["default"].createElement("button", {
                            className: "quick-button",
                            title: "Delete test",
                            onMouseDown: this.stopPropagation,
                            onClick: function (e) {
                                e.stopPropagation(), t.onDeleteTest(l)
                            }
                        }, y["default"].createElement("img", {
                            src: chrome.extension.getURL("assets/trash.png")
                        })), y["default"].createElement("button", {
                            className: "btn btn-primary play-btn" + (o !== i ? " btn-disabled btn-clickable" : ""),
                            onMouseDown: this.stopPropagation,
                            onClick: function (e) {
                                e.stopPropagation(), t.onRunTest(l)
                            },
                            title: "Run test"
                        }, "run"))))
                    }
                    if (e.leaf && "component" === e.type) {
                        var u = (0, g["default"])(r, {
                            id: e.testId
                        });
                        return y["default"].createElement("div", {
                            className: "test-row is-component"
                        }, y["default"].createElement("div", {
                            className: "item grid-row v-align"
                        }, y["default"].createElement("div", {
                            className: "test-move handle"
                        }, y["default"].createElement("img", {
                            className: "handle",
                            src: chrome.extension.getURL("assets/order.png")
                        })), y["default"].createElement("div", {
                            className: "square handle is-comp"
                        }, "Comp:"), y["default"].createElement("div", {
                            className: "grid-item"
                        }, y["default"].createElement(S.EditableLabel, {
                            value: u.name,
                            size: u.name && u.name.length,
                            onClick: function () {
                                return t.onViewComponent(u)
                            },
                            onChange: function (e) {
                                return t.onComponentNameChange(e, u)
                            },
                            doubleClickEdit: !1
                        })), y["default"].createElement("div", {
                            className: "action-quick-buttons"
                        }, y["default"].createElement("button", {
                            className: "quick-button",
                            title: "View component",
                            onMouseDown: this.stopPropagation,
                            onClick: function () {
                                return t.onViewComponent(u)
                            }
                        }, y["default"].createElement("img", {
                            src: chrome.extension.getURL("assets/edit_2.png")
                        })), y["default"].createElement("button", {
                            className: "quick-button",
                            title: "Delete component",
                            onMouseDown: this.stopPropagation,
                            onClick: function () {
                                return t.onDeleteComponent(u)
                            }
                        }, y["default"].createElement("img", {
                            src: chrome.extension.getURL("assets/trash.png")
                        })))))
                    }
                    return y["default"].createElement("div", {
                        className: "folder-row"
                    }, y["default"].createElement("div", {
                        className: "item grid-row v-align",
                        onClick: function () {
                            return t.onToggleFolderOpenAtNode(e)
                        }
                    }, y["default"].createElement("div", {
                        className: "grid-item"
                    }, y["default"].createElement(S.EditableLabel, {
                        value: e.module,
                        size: e.module && e.module.length,
                        onChange: function (n) {
                            return t.onChangeTestFolderName(e, n)
                        },
                        doubleClickEdit: !1
                    }), y["default"].createElement("span", {
                        className: "folder-item-count"
                    }, "(", (0, w.countNodesChildren)(e), " items)")), y["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, y["default"].createElement("button", {
                        className: "quick-button",
                        title: "View folders code",
                        onMouseDown: this.stopPropagation,
                        onClick: function (n) {
                            n.stopPropagation(), t.onViewFolderCode(e.id)
                        }
                    }, y["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/folder_code.png")
                    })), y["default"].createElement("button", {
                        className: "quick-button",
                        title: "Delete folder",
                        onMouseDown: this.stopPropagation,
                        onClick: function (n) {
                            n.stopPropagation(), t.onDeleteTestFolder(e)
                        }
                    }, y["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })))))
                }
            }, {
                key: "checkForHandle",
                value: function (e) {
                    e.srcElement.className.indexOf("handle") === -1 && e.stopPropagation()
                }
            }, {
                key: "onToggleFolderOpenAtNode",
                value: function (e) {
                    var t = this.props.directory,
                        n = (0, o["default"])({}, t),
                        a = (0, w.findNode)(n, {
                            id: e.id
                        });
                    a.collapsed = !a.collapsed, this.onTestTreeChange(n)
                }
            }, {
                key: "onDeleteTestFolder",
                value: function (e) {
                    var t = this.props.directory;
                    0 === (0, w.countNodesChildren)(e) ? this.onTestTreeChange((0, w.removeNodeFromTree)(t, e.id)) : alert("Please remove all tests from this folder before deleting.")
                }
            }, {
                key: "stopPropagation",
                value: function (e) {
                    e.stopPropagation()
                }
            }, {
                key: "onDeleteComponent",
                value: function (e) {
                    var t = this.props.tests,
                        n = [];
                    if (t.forEach(function (t) {
                        t.actions.forEach(function (a) {
                            a.componentId === e.id && n.indexOf(t.id) === -1 && n.push(t)
                        })
                    }), 0 === n.length) b["default"].to(b["default"].SESSION, "deleteComponent", e.id), this.forceUpdate();
                    else {
                        var a = n.map(function (e) {
                            return e.name
                        });
                        alert("Please remove component from tests before deleting. Currently in: " + a.join(", "))
                    }
                }
            }, {
                key: "onDeleteTest",
                value: function (e) {
                    var t = this;
                    confirm("Are you sure you want to delete this test? ") && b["default"].to(b["default"].SESSION, "deleteTest", e.id, function () {
                        t.forceUpdate()
                    })
                }
            }, {
                key: "onDuplicateTest",
                value: function (e) {
                    b["default"].to(b["default"].SESSION, "duplicateTest", e.id), this.forceUpdate()
                }
            }, {
                key: "onComponentNameChange",
                value: function (e, t) {
                    t.name = e, b["default"].to(b["default"].SESSION, "updateComponent", t)
                }
            }, {
                key: "onTestNameChange",
                value: function (e, t) {
                    b["default"].to(b["default"].SESSION, "setTestName", {
                        id: t.id,
                        value: e
                    }), this.forceUpdate()
                }
            }, {
                key: "onViewComponent",
                value: function (e) {
                    b["default"].to(b["default"].SESSION, "setComponentActive", e.id), b["default"].to(b["default"].SESSION, "pushRoute", new T["default"]("componentbuilder", {
                        componentId: e.id
                    }))
                }
            }, {
                key: "onViewTest",
                value: function (e) {
                    b["default"].to(b["default"].SESSION, "setTestActive", e.id), b["default"].to(b["default"].SESSION, "pushRoute", new T["default"]("testbuilder", {
                        testId: e.id
                    }))
                }
            }, {
                key: "onRunTest",
                value: function (e) {
                    var t = this.props,
                        n = t.primaryTabId,
                        a = t.currentTabId;
                    a !== n ? b["default"].to(b["default"].SESSION, "setActiveTabAlert", !0) : (b["default"].to(b["default"].SESSION, "setTestActive", e.id), b["default"].to(b["default"].SESSION, "pushRoute", new T["default"]("testbuilder", {
                        testId: e.id
                    })), b["default"].to(b["default"].SESSION, "startPlayback"))
                }
            }, {
                key: "onViewCode",
                value: function (e) {
                    b["default"].to(b["default"].SESSION, "setTestActive", e.id), b["default"].to(b["default"].SESSION, "pushRoute", new T["default"]("codeviewer"))
                }
            }, {
                key: "onViewFolderCode",
                value: function (e) {
                    b["default"].to(b["default"].SESSION, "setFolderActive", e), b["default"].to(b["default"].SESSION, "pushRoute", new T["default"]("codeviewer"))
                }
            }, {
                key: "onGenerateAllCode",
                value: function () {
                    b["default"].to(b["default"].SESSION, "setFolderActive", "root"), b["default"].to(b["default"].SESSION, "pushRoute", new T["default"]("codeviewer"))
                }
            }, {
                key: "onStartNewTest",
                value: function () {
                    if (this.props.primaryTabId !== this.props.currentTabId) b["default"].to(b["default"].SESSION, "setActiveTabAlert", !0);
                    else {
                        var e = new k["default"];
                        b["default"].to(b["default"].SESSION, "addNewTest", e)
                    }
                }
            }]), t
        } (y["default"].Component);
    t["default"] = A
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = (n(8), n(57)),
        g = a(v),
        E = n(7),
        y = a(E),
        S = n(27),
        N = a(S),
        b = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props.user;
                    return h["default"].createElement("div", {
                        className: "grid-row top-menu-container v-align"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, h["default"].createElement("div", {
                        className: "logo",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "setModal", "support")
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/logo.png")
                    }))), h["default"].createElement("div", {
                        className: "grid-item grid-row h-align db-title"
                    }), h["default"].createElement("a", {
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "pushRoute", new N["default"]("account"))
                        }
                    }, e.email), h["default"].createElement("img", {
                        className: "help-icon",
                        src: chrome.extension.getURL("assets/help.png"),
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "setModal", "dashboard-help")
                        }
                    }), h["default"].createElement(g["default"], this.props))
                }
            }]), t
        } (h["default"].Component);
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(55),
        o = a(r),
        i = n(3),
        l = a(i),
        u = n(2),
        c = a(u),
        s = n(6),
        d = a(s),
        f = n(5),
        p = a(f),
        m = n(4),
        h = a(m),
        v = n(1),
        g = a(v),
        E = n(250),
        y = a(E),
        S = n(296),
        N = (a(S), n(57)),
        b = a(N),
        w = function (e) {
            function t(e) {
                return (0, c["default"])(this, t), (0, p["default"])(this, (t.__proto__ || (0, l["default"])(t)).call(this, e))
            }
            return (0, h["default"])(t, e), (0, d["default"])(t, [{
                key: "render",
                value: function () {
                    this.props.user;
                    return g["default"].createElement("div", {
                        className: "content"
                    }, g["default"].createElement("div", {
                        className: "grid-row top-menu-container v-align"
                    }, g["default"].createElement("div", {
                        className: "grid-row"
                    }, g["default"].createElement("div", {
                        className: "logo handle"
                    }, g["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/logo.png")
                    }))), g["default"].createElement("div", {
                        className: "grid-item"
                    }), g["default"].createElement(b["default"], this.props)), g["default"].createElement("div", {
                        className: "help-content grid-row grid-column"
                    }, g["default"].createElement("div", {
                        className: "grid-item-2"
                    }, g["default"].createElement("h3", {
                        className: "t-center"
                    }, "Take the pain out of Selenium."), g["default"].createElement("div", {
                        className: "grid-row h-align t-section"
                    }, g["default"].createElement(y["default"], (0, o["default"])({}, this.props, {
                        title: "Login"
                    })))), g["default"].createElement("div", {
                        className: "t-ruler"
                    }), g["default"].createElement("div", {
                        className: "grid-item grid-row v-align t-section"
                    }, g["default"].createElement("div", {
                        className: "grid-item-2"
                    }, "SnapTest is in your devtools as well:"), g["default"].createElement("div", {
                        className: "grid-item"
                    }, g["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/devtool.png")
                    }))), g["default"].createElement("div", {
                        className: "grid-item grid-row v-align t-section"
                    }, g["default"].createElement("div", {
                        className: "grid-item-3"
                    }, 'To remove this tool from the tab, click "Stop testing".'), g["default"].createElement("div", {
                        className: "grid-item"
                    }, g["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/stopTesting.png")
                    })))))
                }
            }]), t
        } (g["default"].Component);
    t["default"] = w
}, function (e, t, n) {
    "use strict";

    function a(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(3),
        i = r(o),
        l = n(2),
        u = r(l),
        c = n(6),
        s = r(c),
        d = n(5),
        f = r(d),
        p = n(4),
        m = r(p),
        h = n(1),
        v = r(h),
        g = n(80),
        E = (r(g), n(7)),
        y = r(E),
        S = n(257),
        N = a(S),
        b = function (e) {
            function t(e) {
                return (0, u["default"])(this, t), (0, f["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e))
            }
            return (0, m["default"])(t, e), (0, s["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props.tutorialStep;
                    return t === N.INTRO_START || t === N.INTRO_WINDOWS ? v["default"].createElement("div", {
                        className: "modal-background"
                    }, v["default"].createElement("div", {
                        className: "modal-content modal-med grid-row v-align h-align t-center",
                        onClick: function (e) {
                            return e.stopPropagation()
                        }
                    }, t === N.INTRO_START ? v["default"].createElement("div", {
                        className: "grid-item"
                    }, v["default"].createElement("h1", null, "Getting started with SnapTest."), v["default"].createElement("p", {
                        className: "button-group"
                    }, v["default"].createElement("button", {
                        className: "btn btn-primary btn-big",
                        onClick: function () {
                            return e.onTutorialStart()
                        }
                    }, "Quick tutorial")), v["default"].createElement("a", {
                        onClick: function () {
                            return e.onTutorialSkip()
                        }
                    }, "Skip tutorial.")) : v["default"].createElement("div", {
                        className: "grid-item"
                    }, v["default"].createElement("h4", null, "Before we begin, organize your windows like this:"), v["default"].createElement("div", null, "(Note the ACTIVE TAB)"), v["default"].createElement("img", {
                        className: "screen_layout_sug",
                        src: chrome.extension.getURL("assets/screen_layout_suggestion.png")
                    }), v["default"].createElement("a", {
                        className: "next-step-prompt",
                        onClick: function (t) {
                            return e.goToNextStep()
                        }
                    }, "I've configured my windows.")))) : v["default"].createElement("div", {
                        className: "modal-background tut-mode"
                    }, v["default"].createElement("div", {
                        className: "modal-content modal-med grid-row"
                    }, v["default"].createElement("button", {
                        className: "prev-tut-btn",
                        onClick: function () {
                            return e.goToPrevStep()
                        }
                    }, "last"), v["default"].createElement("div", {
                        className: "grid-item grid-row grid-column"
                    }, t === N.FIRSTTEST_CREATE ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", null, "Let's ", v["default"].createElement("span", {
                        className: "t-underline"
                    }, "create"), " your first test."), v["default"].createElement("h4", null, "Click on the ", v["default"].createElement("button", {
                        className: "btn recording-btn"
                    }, "new test"), " button in the top-right.")) : t === N.FIRSTTEST_EDIT ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h4", null, "Click on the ", v["default"].createElement("button", {
                        className: "quick-button"
                    }, v["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/edit_2.png")
                    })), " to enter the new test.")) : t === N.FIRSTTEST_PRETHOUGHT ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", null, "When doing QA tests..."), v["default"].createElement("h4", {
                        className: "tut-content-section"
                    }, "You are testing a user flow through a site or web application."), v["default"].createElement("h4", {
                        className: "tut-content-section"
                    }, "It's important to put yourself into the mindset of a user... "), v["default"].createElement("a", {
                        onClick: function () {
                            return e.goToNextStep()
                        }
                    }, "Got it...")) : t === N.FIRSTTEST_NAVTOSNAP ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h4", null, "For this tutorial, let's test as a new user of SnapTests marketing site..."), v["default"].createElement("h3", null, "Navigate the active chrome window to snaptest.io")) : t === N.FIRSTTEST_PRETEND ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h4", null, "Enter", v["default"].createElement("button", {
                        className: "btn recording-btn"
                    }, "record"), " mode and start to record some actions in the active chrome tab.")) : t === N.FIRSTTEST_STOPRECORD ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h4", null, "When you're done, hit ", v["default"].createElement("button", {
                        className: "btn recording-btn active"
                    }, "record"), " to stop recording.")) : t === N.FIRSTTEST_ASSERT ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h4", null, "Let's assert some elements. Enter ", v["default"].createElement("button", {
                        className: "btn assert-btn"
                    }, "assert"), " mode."), v["default"].createElement("h4", null, "Click around in the active tab, and notice that SnapTest will generate assertions for you.")) : t === N.FIRSTTEST_STOPASSERT ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h4", null, "When you're done exit by clicking ", v["default"].createElement("button", {
                        className: "btn assert-btn active"
                    }, "assert"), ".")) : t === N.FIRSTTEST_SIMULATE ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", null, "Let's run this test by clicking ", v["default"].createElement("button", {
                        className: "btn btn-primary play-btn"
                    }, "run"), ". "), v["default"].createElement("h4", null, "This will emulate the generated Selenium code with high accuracy, so it's useful for debugging. ")) : t === N.FIRSTTEST_SIMULATE2 ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", null, "Try adding breakpoints ", v["default"].createElement("img", {
                        className: "tut-img-openaction",
                        src: chrome.extension.getURL("assets/breakpoint2.png")
                    }), " and use the ", v["default"].createElement("button", {
                        className: "btn play-btns"
                    }, "step over"), " function. ")) : t === N.FIRSTTEST_NAMESTEP ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h4", null, "After recording, it's a good idea to give your steps more human readable descriptions. "), v["default"].createElement("h3", {
                        className: "tut-content-section grid-row v-align"
                    }, "Open a step with ", v["default"].createElement("img", {
                        className: "tut-img-openaction",
                        src: chrome.extension.getURL("assets/openaction.png")
                    }), "."), v["default"].createElement("h4", {
                        className: "tut-content-section"
                    }, 'Give it a good description from the perspective of a user, like: "clicked on the blog button in the main menu".')) : t === N.FIRSTTEST_ADJUSTSELECTOR ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h4", null, "Sometimes you need to add/change the way SnapTest finds the element (the selector)."), v["default"].createElement("h4", {
                        className: "tut-content-section"
                    }, "Click on a step's ", v["default"].createElement("img", {
                        className: "selection-icon",
                        src: chrome.extension.getURL("assets/target.png")
                    }), " icon."), v["default"].createElement("h5", {
                        className: "tut-content-section"
                    }, "It will change to ", v["default"].createElement("img", {
                        className: "tut-img-openaction",
                        src: chrome.extension.getURL("assets/selectormode.png")
                    }), " which indicates quick selector mode. Find and click desired target element in active tab. ")) : t === N.FIRSTTEST_MANUALADD ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", null, "Manually adding steps is easy... click ", v["default"].createElement("img", {
                        className: "tut-img-openaction",
                        src: chrome.extension.getURL("assets/manual_add.png")
                    })), v["default"].createElement("h3", {
                        className: "tut-content-section"
                    }, "Try adding an action manually, maybe a 'pause' step...")) : t === N.FIRSTTEST_MANAGE ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", null, "Let's go back to the dashboard and organize the new test."), v["default"].createElement("h4", {
                        className: "tut-content-section"
                    }, "Click on ", v["default"].createElement("button", {
                        className: "btn btn-primary"
                    }, "dashboard"), " to get back to your tests.")) : t === N.FIRSTTEST_MANAGE2 ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", {
                        className: "tut-content-section"
                    }, 'Rename your test from "Unnamed test" to something like "new user walkthrough".')) : t === N.FIRSTTEST_MANAGE3 ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h5", {
                        className: "tut-content-section"
                    }, "We've created some folders for you."), v["default"].createElement("h4", {
                        className: "tut-content-section"
                    }, 'Drag your new test into the "Your project -> production" folder.'), v["default"].createElement("h5", {
                        className: "tut-content-section"
                    }, "There is no magical folder structure. Feel free to configure it as you like. ")) : t === N.EXISTTEST_KICKOFF ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", null, "Try running some example tests from the dashboard"), v["default"].createElement("h4", {
                        className: "tut-content-section"
                    }, "We've provided a 'Sample Tests' folder, feel free to run any of them with the ", v["default"].createElement("button", {
                        className: "btn btn-primary play-btn"
                    }, "run"), " button.")) : t === N.END ? v["default"].createElement("div", {
                        className: "grid-item grid-row v-align h-align grid-column tut-content"
                    }, v["default"].createElement("h3", null, "That's a basic walkthrough of creating and emulating tests, but that's only half of it..."), v["default"].createElement("h5", {
                        className: "tut-content-section"
                    }, v["default"].createElement("a", {
                        href: "https://www.youtube.com/channel/UCi8nyq-b-tgmL4JU0gFU2BQ",
                        target: "_blank"
                    }, "Click here for more tutorials. ")), v["default"].createElement("a", {
                        onClick: function () {
                            return e.onTutorialSkip()
                        }
                    }, "Finish tutorial")) : null), v["default"].createElement("button", {
                        className: "next-tut-btn",
                        onClick: function () {
                            return e.goToNextStep()
                        }
                    }, "next")))
                }
            }, {
                key: "goToNextStep",
                value: function () {
                    var e = this.props.tutorialStepNumber;
                    y["default"].to(y["default"].SESSION, "setTutStep", e + 1)
                }
            }, {
                key: "goToPrevStep",
                value: function () {
                    var e = this.props.tutorialStepNumber;
                    y["default"].to(y["default"].SESSION, "setTutStep", e - 1)
                }
            }, {
                key: "onTutorialStart",
                value: function () {
                    y["default"].to(y["default"].SESSION, "setTutStep", 1)
                }
            }, {
                key: "onTutorialSkip",
                value: function () {
                    y["default"].to(y["default"].SESSION, "setTutActive", !1)
                }
            }]), t
        } (v["default"].Component);
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(222),
        h = a(m),
        v = n(1),
        g = a(v),
        E = n(8),
        y = n(7),
        S = a(y),
        N = function (e) {
            function t(e) {
                (0, l["default"])(this, t);
                var n = (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e));
                return n.state = {
                    isViewingMore: !1
                }, n
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = this.state.isViewingMore,
                        i = g["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, g["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return g["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return g["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        }))),
                        l = g["default"].createElement("div", {
                            className: "grid-item"
                        }, g["default"].createElement("div", {
                            className: "value-tag-label"
                        }, "Window/tab #"), g["default"].createElement("div", {
                            className: "value-tag"
                        }, "#", g["default"].createElement(E.EditableLabel, {
                            value: n.value + "",
                            onChange: function (t) {
                                return e.onValueChange(t)
                            }
                        })));
                    return g["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, g["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? g["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return e.setState({
                                isViewingMore: !o
                            })
                        }
                    }, g["default"].createElement("div", null, n.description)) : [i, l], g["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, g["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return S["default"].to(S["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, g["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), g["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return e.setState({
                                isViewingMore: !e.state.isViewingMore
                            })
                        }
                    }, g["default"].createElement("img", {
                        src: chrome.extension.getURL(o ? "assets/info_cancel.png" : "assets/info.png")
                    })))), o && g["default"].createElement("div", {
                        className: "details-section"
                    }, g["default"].createElement("div", {
                        className: "details-row"
                    }, g["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), i), g["default"].createElement("div", {
                        className: "details-row"
                    }, g["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Window/tab:"), l), g["default"].createElement("div", {
                        className: "details-row"
                    }, g["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), g["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onSelectorWarningClick",
                value: function () {
                    this.setState({
                        isViewingMore: !this.state.isViewingMore
                    })
                }
            }, {
                key: "onValueChange",
                value: function (e) {
                    var t = this.props.action;
                    e = parseInt(e), (0, h["default"])(e) && (t.value = e, S["default"].to(S["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, S["default"].to(S["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (g["default"].PureComponent);
    t["default"] = N
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props.action;
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-row v-align"
                    }, h["default"].createElement("div", {
                        className: "action-type"
                    }, "(auto) Focus on window:"), h["default"].createElement("div", {
                        className: "grid-item"
                    }, "#", e.value), h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "removeNWAction", e.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button disabled"
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/info.png")
                    })))))
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = E
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = t.isExpanded,
                        i = h["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, h["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return h["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return h["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        })));
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? h["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("div", null, n.description)) : [i, h["default"].createElement("div", {
                        className: "grid-item"
                    })], h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL(o ? "assets/info_cancel.png" : "assets/info.png")
                    })))), o && h["default"].createElement("div", {
                        className: "details-section"
                    }, h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), i), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), h["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, g["default"].to(g["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = E
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props,
                        t = e.action,
                        n = e.actionTypeGroups,
                        a = e.onActionTypeChange;
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row v-align nw-action-con"
                    }, h["default"].createElement("div", {
                        className: "action-type select-wrap"
                    }, h["default"].createElement("select", {
                        onChange: function (e) {
                            return a(t, e.currentTarget.value)
                        }
                    }, n.map(function (e) {
                        return h["default"].createElement("optgroup", {
                            label: e.label
                        }, e.options.map(function (e) {
                            return h["default"].createElement("option", {
                                disabled: e.disabled,
                                value: e.value
                            }, e.name)
                        }))
                    }))), h["default"].createElement("div", {
                        className: "grid-item"
                    }), h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "removeNWAction", t.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button disabled"
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/info.png")
                    }))))
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = E
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(56),
        h = a(m),
        v = n(264),
        g = a(v),
        E = n(93),
        y = a(E),
        S = n(1),
        N = a(S),
        b = n(8),
        w = n(7),
        _ = a(w),
        k = n(239),
        C = a(k),
        T = n(27),
        O = a(T),
        A = n(255),
        x = (a(A), function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.components,
                        r = t.playbackResult,
                        o = t.playbackCursor,
                        i = t.playbackBreakpoints,
                        l = t.isExpanded,
                        u = (0, y["default"])(a, {
                            id: n.componentId
                        });
                    return N["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, N["default"].createElement("div", {
                        className: "grid-row"
                    }, N["default"].createElement("div", {
                        className: "action-type select-wrap"
                    }, N["default"].createElement("select", {
                        value: n.componentId,
                        onChange: function (t) {
                            return e.onComponentChange(t.currentTarget.value)
                        }
                    }, N["default"].createElement("option", {
                        value: ""
                    }, "..."), a.map(function (e) {
                        return N["default"].createElement("option", {
                            value: e.id
                        }, e.name)
                    }))), u && u.variables.length > 0 ? N["default"].createElement("div", {
                        className: "grid-item grid-row grid-flexwrap"
                    }, u.variables.map(function (t, n) {
                        return N["default"].createElement("div", {
                            className: "variable",
                            key: n
                        }, N["default"].createElement("div", {
                            className: "key"
                        }, t.name), N["default"].createElement("div", {
                            className: "default"
                        }, N["default"].createElement(b.EditableLabel, {
                            value: e.getVarValue(t),
                            size: e.getVarValue(t).length,
                            onChange: function (n) {
                                return e.onVariableValueChange(t, n)
                            }
                        })))
                    })) : N["default"].createElement("div", {
                        className: "grid-item"
                    }), u && N["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, N["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return e.onEditComponentClick(u)
                        }
                    }, N["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/edit_2.png")
                    })), N["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return _["default"].to(_["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, N["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), N["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return _["default"].to(_["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, N["default"].createElement("img", {
                        src: chrome.extension.getURL(l ? "assets/info_cancel.png" : "assets/info.png")
                    })))), u && (l || o || !(0, g["default"])(r)) && N["default"].createElement("div", {
                        className: "grid-row grid-column component-rows"
                    }, u.actions.map(function (e, t) {
                        return N["default"].createElement(C["default"], {
                            action: e,
                            idx: t,
                            components: a,
                            playbackResult: r,
                            playbackCursor: o,
                            playbackBreakpoints: i
                        })
                    })))
                }
            }, {
                key: "getVarValue",
                value: function (e) {
                    var t = this.props.action,
                        n = (0, y["default"])(t.variables, {
                            id: e.id
                        });
                    return n ? n.value : e.defaultValue
                }
            }, {
                key: "onVariableValueChange",
                value: function (e, t) {
                    var n = this.props.action,
                        a = (0, h["default"])(n.variables, {
                            id: e.id
                        });
                    a !== -1 ? n.variables.splice(a, 1, {
                        id: e.id,
                        value: t
                    }) : n.variables.push({
                        id: e.id,
                        value: t
                    }), _["default"].to(_["default"].SESSION, "updateNWAction", n)
                }
            }, {
                key: "onComponentChange",
                value: function (e) {
                    var t = this.props.action;
                    t.componentId = e, _["default"].to(_["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onEditComponentClick",
                value: function (e) {
                    _["default"].to(_["default"].SESSION, "setComponentActive", e.id), _["default"].to(_["default"].SESSION, "pushRoute", new O["default"]("componentbuilder", {
                        componentId: e.id
                    }))
                }
            }, {
                key: "onComponentNameChange",
                value: function (e, t) {
                    t.name = e, _["default"].to(_["default"].SESSION, "updateComponent", t)
                }
            }]), t
        } (N["default"].PureComponent));
    t["default"] = x
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(8),
        g = n(7),
        E = a(g),
        y = function (e) {
            function t(e) {
                (0, l["default"])(this, t);
                var n = (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e));
                return n.state = {
                    isViewingMore: !1
                }, n
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = this.state.isViewingMore;
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, h["default"].createElement("div", {
                        className: "action-type"
                    }, h["default"].createElement("select", {
                        value: n.type,
                        onChange: function (e) {
                            return r(n, e.currentTarget.value)
                        }
                    }, a.map(function (e) {
                        return h["default"].createElement("optgroup", {
                            label: e.label
                        }, e.options.map(function (e) {
                            return h["default"].createElement("option", {
                                disabled: e.disabled,
                                value: e.value
                            }, e.name)
                        }))
                    }))), h["default"].createElement("div", {
                        className: "grid-item"
                    }), h["default"].createElement("div", {
                        className: "selector"
                    }, h["default"].createElement(v.EditableLabel, {
                        value: n.selector,
                        size: n.selector.length,
                        onChange: function (t) {
                            return e.onSelectorChange(t, "selector")
                        }
                    })), n.warnings.length > 0 && h["default"].createElement("img", {
                        className: "warning-icon",
                        src: chrome.extension.getURL("assets/warning.png"),
                        onClick: function () {
                            return e.onSelectorWarningClick()
                        }
                    })), o && n.warnings.length > 0 && h["default"].createElement("div", {
                        className: "grid-row grid-column warnings"
                    }, n.warnings.map(function (e) {
                        return h["default"].createElement("div", null, e)
                    })))
                }
            }, {
                key: "onSelectorWarningClick",
                value: function () {
                    this.setState({
                        isViewingMore: !this.state.isViewingMore
                    })
                }
            }, {
                key: "onSelectorChange",
                value: function (e, t) {
                    var n = this.props.action;
                    n[t] = e, E["default"].to(E["default"].SESSION, "updateNWAction", n)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = y
}, function (e, t, n) {
    "use strict";

    function a(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(3),
        i = r(o),
        l = n(2),
        u = r(l),
        c = n(6),
        s = r(c),
        d = n(5),
        f = r(d),
        p = n(4),
        m = r(p),
        h = n(1),
        v = r(h),
        g = n(28),
        E = a(g),
        y = n(8),
        S = n(7),
        N = r(S),
        b = function (e) {
            function t(e) {
                return (0, u["default"])(this, t), (0, f["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e))
            }
            return (0, m["default"])(t, e), (0, s["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypes,
                        r = t.onActionTypeChange;
                    return v["default"].createElement("div", {
                        className: "grid-item grid-row v-align"
                    }, v["default"].createElement("div", {
                        className: "action-type"
                    }, n.type === E.POPSTATE ? "Back" : n.type === E.PAGELOAD ? v["default"].createElement("select", {
                        value: n.type,
                        onChange: function (e) {
                            return r(n, e.currentTarget.value)
                        }
                    }, a.map(function (e) {
                        return v["default"].createElement("option", {
                            value: e.value
                        }, e.name)
                    })) : n.type === E.MOUSEDOWN ? v["default"].createElement("select", {
                        value: n.type,
                        onChange: function (e) {
                            return r(n, e.currentTarget.value)
                        }
                    }, a.map(function (e) {
                        return v["default"].createElement("option", {
                            value: e.value
                        }, e.name)
                    })) : n.type === E.INPUT ? v["default"].createElement("select", {
                        value: n.type,
                        onChange: function (e) {
                            return r(n, e.currentTarget.value)
                        }
                    }, a.map(function (e) {
                        return v["default"].createElement("option", {
                            value: e.value
                        }, e.name)
                    })) : n.type === E.TEXT_ASSERT ? v["default"].createElement("select", {
                        value: n.type,
                        onChange: function (e) {
                            return r(n, e.currentTarget.value)
                        }
                    }, a.map(function (e) {
                        return v["default"].createElement("option", {
                            value: e.value
                        }, e.name)
                    })) : n.type === E.BLANK ? v["default"].createElement("select", {
                        onChange: function (e) {
                            return r(n, e.currentTarget.value)
                        }
                    }, a.map(function (e) {
                        return v["default"].createElement("option", {
                            value: e.value
                        }, e.name)
                    })) : n.type === E.PUSHSTATE ? "Url change" : v["default"].createElement("span", null, n.type)), v["default"].createElement("div", {
                        className: "grid-item grid-row v-align nw-action-con"
                    }, v["default"].createElement("div", {
                        className: "grid-item"
                    }, n.value ? v["default"].createElement("div", {
                        className: "value-tag"
                    }, v["default"].createElement(y.EditableLabel, {
                        value: n.value,
                        onChange: function (t) {
                            return e.onValueChange(t)
                        }
                    })) : n.textContent && !n.textAsserted ? v["default"].createElement("div", {
                        className: "assert-tag"
                    }, v["default"].createElement("span", {
                        className: "assert",
                        onClick: function () {
                            e.onAddNWTextAssertion(n)
                        }
                    }, "auto: "), v["default"].createElement("span", {
                        className: "value"
                    }, " assert text: ", n.textContent.substring(0, 20), "...")) : n.text ? v["default"].createElement("div", {
                        className: "value-tag"
                    }, v["default"].createElement(y.EditableLabel, {
                        value: n.text,
                        onChange: function (t) {
                            return e.onTextAssertionChange(t)
                        }
                    })) : null), v["default"].createElement("div", {
                        className: "selector"
                    }, n.url ? v["default"].createElement(y.EditableLabel, {
                        value: n.url,
                        size: n.url.length,
                        onChange: function (t) {
                            return e.onUrlChange(t)
                        }
                    }) : n.selector ? v["default"].createElement(y.EditableLabel, {
                        value: n.selector,
                        size: n.selector.length,
                        onChange: function (t) {
                            return e.onSelectorChange(t, "selector")
                        }
                    }) : n.prettySelector ? v["default"].createElement(y.EditableLabel, {
                        value: n.prettySelector,
                        size: n.prettySelector.length,
                        onChange: function (t) {
                            return e.onSelectorChange(t, "prettySelector")
                        }
                    }) : null)))
                }
            }, {
                key: "onValueChange",
                value: function (e) {
                    var t = this.props.action;
                    t.value = e, N["default"].to(N["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onTextAssertionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.text = e, N["default"].to(N["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onSelectorChange",
                value: function (e, t) {
                    var n = this.props.action;
                    n[t] = e, N["default"].to(N["default"].SESSION, "updateNWAction", n)
                }
            }, {
                key: "onUrlChange",
                value: function (e) {
                    var t = this.props.action;
                    t.url = e, N["default"].to(N["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onAddNWTextAssertion",
                value: function (e) {
                    var t = new E.TextAssertAction(e.prettySelector || e.selector, e.textContent);
                    N["default"].to(N["default"].SESSION, "insertNWActionBefore", {
                        insertBefore: e.id,
                        action: t
                    })
                }
            }]), t
        } (v["default"].PureComponent);
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(8),
        g = n(7),
        E = a(g),
        y = n(256),
        S = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = t.isInComponent,
                        i = t.selectingForActionId,
                        l = t.selectionCandidate,
                        u = void 0 === l ? "select element..." : l,
                        c = t.isExpanded,
                        s = t.defaultTimeout,
                        d = h["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, h["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return h["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return h["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        }))),
                        f = h["default"].createElement("div", {
                            className: "grid-item grid-row"
                        }, h["default"].createElement("div", {
                            className: "value-tag"
                        }, h["default"].createElement(v.EditableLabel, {
                            value: n.value,
                            onChange: function (t) {
                                return e.onValueChange(t)
                            }
                        })), o && h["default"].createElement("img", {
                            className: "add-var-icon",
                            src: chrome.extension.getURL("assets/target.png"),
                            onClick: function () {
                                return e.onMakeCompVar()
                            }
                        })),
                        p = h["default"].createElement("div", {
                            className: "selector" + (n.warnings.length > 0 ? " warning" : "")
                        }, i === n.id ? h["default"].createElement("div", {
                            className: "selecting-el"
                        }, u || "click element...") : h["default"].createElement(v.EditableLabel, {
                            value: n.selector,
                            size: n.selector && n.selector.length,
                            onChange: function (t) {
                                return e.onSelectorChange(t)
                            }
                        }), h["default"].createElement("div", {
                            className: "action-quick-buttons"
                        }, i === n.id ? h["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return E["default"].to(E["default"].SESSION, "cancelSelection")
                            }
                        }, h["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        })) : h["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return E["default"].to(E["default"].SESSION, "startSelection", n.id)
                            }
                        }, h["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        }))));
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? h["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("div", null, n.description)) : [d, f, p], h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL(c ? "assets/info_cancel.png" : "assets/info.png")
                    })))), c && h["default"].createElement("div", {
                        className: "details-section"
                    }, h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), d), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Value:"), f), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Selector:"), p), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Timeout:"), h["default"].createElement("input", {
                        className: "text-input",
                        type: "text",
                        value: n.timeout || s,
                        onChange: function (t) {
                            return e.onTimeoutChange(t.currentTarget.value)
                        }
                    }), "milliseconds"), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), h["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onTimeoutChange",
                value: function (e) {
                    var t = this.props.action;
                    "" !== e && parseInt(e) && (t.timeout = parseInt(e),
                        E["default"].to(E["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onMakeCompVar",
                value: function () {
                    var e = this.props,
                        t = e.action,
                        n = e.activeTest,
                        a = new y.CompVariable("var" + n.variables.length, t.value, t.id);
                    E["default"].to(E["default"].SESSION, "addVarToComp", {
                        id: n.id,
                        compVar: a
                    }), t.value = "${" + a.name + "}", E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onValueChange",
                value: function (e) {
                    var t = this.props.action;
                    t.value = e, E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onSelectorChange",
                value: function (e) {
                    var t = this.props.action;
                    t.selector = e, t.warnings = [], E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = S
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(8),
        g = n(7),
        E = a(g),
        y = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = t.selectingForActionId,
                        i = t.selectionCandidate,
                        l = void 0 === i ? "select element..." : i,
                        u = t.isExpanded,
                        c = t.defaultTimeout,
                        s = h["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, h["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return h["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return h["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        }))),
                        d = h["default"].createElement("div", {
                            className: "selector" + (n.warnings.length > 0 ? " warning" : "")
                        }, o === n.id ? h["default"].createElement("div", {
                            className: "selecting-el"
                        }, l || "click element...") : h["default"].createElement(v.EditableLabel, {
                            value: n.selector,
                            size: n.selector.length,
                            onChange: function (t) {
                                return e.onSelectorChange(t)
                            }
                        }), h["default"].createElement("div", {
                            className: "action-quick-buttons"
                        }, o === n.id ? h["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return E["default"].to(E["default"].SESSION, "cancelSelection")
                            }
                        }, h["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        })) : h["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return E["default"].to(E["default"].SESSION, "startSelection", n.id)
                            }
                        }, h["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        })))),
                        f = h["default"].createElement("div", {
                            className: "grid-item"
                        }, h["default"].createElement("div", {
                            className: "value-tag"
                        }, h["default"].createElement("select", {
                            value: n.keyValue,
                            onChange: function (t) {
                                return e.onValueChange(t.currentTarget.value)
                            }
                        }, h["default"].createElement("option", {
                            value: ""
                        }, "..."), h["default"].createElement("option", {
                            value: "Enter"
                        }, "Enter"), h["default"].createElement("option", {
                            value: "Escape"
                        }, "Escape"))));
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? h["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("div", null, n.description)) : [s, f, d], h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL(u ? "assets/info_cancel.png" : "assets/info.png")
                    })))), u && h["default"].createElement("div", {
                        className: "details-section"
                    }, h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), s), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Value:"), f), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Selector:"), d), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Timeout:"), h["default"].createElement("input", {
                        className: "text-input",
                        type: "text",
                        value: n.timeout || c,
                        onChange: function (t) {
                            return e.onTimeoutChange(t.currentTarget.value)
                        }
                    }), "milliseconds"), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), h["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onTimeoutChange",
                value: function (e) {
                    var t = this.props.action;
                    "" !== e && parseInt(e) && (t.timeout = parseInt(e), E["default"].to(E["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onValueChange",
                value: function (e) {
                    var t = this.props.action;
                    t.keyValue = e, E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onSelectorChange",
                value: function (e) {
                    var t = this.props.action;
                    t.selector = e, t.warnings = [], E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = y
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = n(8),
        y = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props.action;
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con"
                    }, h["default"].createElement("div", {
                        className: "grid-item"
                    }, h["default"].createElement(E.EditableLabel, {
                        value: t.value,
                        onChange: function (t) {
                            return e.onValueChange(t)
                        }
                    })))
                }
            }, {
                key: "onRemoveRow",
                value: function () {
                    var e = this.props.action;
                    g["default"].to(g["default"].SESSION, "removeNWActions", [e.id])
                }
            }, {
                key: "onValueChange",
                value: function (e) {
                    var t = this.props.action;
                    t.value = e, g["default"].to(g["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = y
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = (n(8), n(7)),
        g = a(v),
        E = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = (t.selectingForActionId, t.selectionCandidate, t.isExpanded),
                        i = h["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, h["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return h["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return h["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        })));
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? h["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("div", null, n.description)) : [i, h["default"].createElement("div", {
                        className: "grid-item"
                    })], h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL(o ? "assets/info_cancel.png" : "assets/info.png")
                    })))), o && h["default"].createElement("div", {
                        className: "details-section"
                    }, h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), i), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), h["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, g["default"].to(g["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onSelectorChange",
                value: function (e) {
                    var t = this.props.action;
                    t.selector = e, t.warnings = [], g["default"].to(g["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = E
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(8),
        g = n(7),
        E = a(g),
        y = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = t.selectingForActionId,
                        i = t.selectionCandidate,
                        l = t.isExpanded,
                        u = void 0 !== l && l,
                        c = t.defaultTimeout,
                        s = h["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, h["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return h["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return h["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        }))),
                        d = h["default"].createElement("div", {
                            className: "selector" + (n.warnings.length > 0 ? " warning" : "")
                        }, o === n.id ? h["default"].createElement("div", {
                            className: "selecting-el"
                        }, i || "click element...") : h["default"].createElement(v.EditableLabel, {
                            value: n.selector,
                            size: n.selector && n.selector.length,
                            onChange: function (t) {
                                return e.onSelectorChange(t)
                            }
                        }), h["default"].createElement("div", {
                            className: "action-quick-buttons"
                        }, o === n.id ? h["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return E["default"].to(E["default"].SESSION, "cancelSelection")
                            }
                        }, h["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        })) : h["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return E["default"].to(E["default"].SESSION, "startSelection", n.id)
                            }
                        }, h["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        }))));
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? h["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("div", null, n.description)) : [s, h["default"].createElement("div", {
                        className: "grid-item"
                    }), d], h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL(u ? "assets/info_cancel.png" : "assets/info.png")
                    })))), u && h["default"].createElement("div", {
                        className: "details-section"
                    }, h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), s), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Selector:"), d), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Timeout:"), h["default"].createElement("input", {
                        className: "text-input",
                        type: "text",
                        value: n.timeout || c,
                        onChange: function (t) {
                            return e.onTimeoutChange(t.currentTarget.value)
                        }
                    }), "milliseconds"), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), h["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onTimeoutChange",
                value: function (e) {
                    var t = this.props.action;
                    "" !== e && parseInt(e) && (t.timeout = parseInt(e), E["default"].to(E["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onSelectorChange",
                value: function (e) {
                    var t = this.props.action;
                    t.selector = e, t.warnings = [], E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = y
}, function (e, t, n) {
    "use strict";

    function a(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(3),
        i = r(o),
        l = n(2),
        u = r(l),
        c = n(6),
        s = r(c),
        d = n(5),
        f = r(d),
        p = n(4),
        m = r(p),
        h = n(1),
        v = r(h),
        g = n(8),
        E = n(7),
        y = r(E),
        S = n(28),
        N = a(S),
        b = function (e) {
            function t(e) {
                return (0, u["default"])(this, t), (0, f["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e))
            }
            return (0, m["default"])(t, e), (0, s["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = t.isExpanded,
                        i = t.defaultTimeout,
                        l = v["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, v["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return v["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return v["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        }))),
                        u = v["default"].createElement("div", {
                            className: "grid-item"
                        }, v["default"].createElement("div", {
                            className: "value-tag"
                        }, v["default"].createElement(g.EditableLabel, {
                            value: n.value,
                            onChange: function (t) {
                                return e.onUrlChange(t)
                            }
                        })));
                    return v["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, v["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? v["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, v["default"].createElement("div", null, n.description)) : [l, u], v["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, v["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, v["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), v["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, v["default"].createElement("img", {
                        src: chrome.extension.getURL(o ? "assets/info_cancel.png" : "assets/info.png")
                    })))), o && v["default"].createElement("div", {
                        className: "details-section"
                    }, v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), l), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, n.type === N.PAGELOAD ? "Path" : "Url", ":"), u), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Timeout:"), v["default"].createElement("input", {
                        className: "text-input",
                        type: "text",
                        value: n.timeout || i,
                        onChange: function (t) {
                            return e.onTimeoutChange(t.currentTarget.value)
                        }
                    }), "milliseconds"), n.type === N.FULL_PAGELOAD && v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Window:"), v["default"].createElement("div", null, v["default"].createElement("div", {
                        className: "value-tag-label"
                    }, "Width:"), v["default"].createElement("div", {
                        className: "value-tag"
                    }, v["default"].createElement(g.EditableLabel, {
                        value: n.width,
                        size: 6,
                        onChange: function (t) {
                            return e.onChangeWidth(t)
                        }
                    })), v["default"].createElement("div", {
                        className: "value-tag-label"
                    }, "Height:"), v["default"].createElement("div", {
                        className: "value-tag"
                    }, v["default"].createElement(g.EditableLabel, {
                        value: n.height,
                        size: 6,
                        onChange: function (t) {
                            return e.onChangeHeight(t)
                        }
                    })))), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), v["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onTimeoutChange",
                value: function (e) {
                    var t = this.props.action;
                    "" !== e && parseInt(e) && (t.timeout = parseInt(e), y["default"].to(y["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onChangeWidth",
                value: function (e) {
                    var t = this.props.action,
                        e = parseInt(e);
                    e && (t.width = e, y["default"].to(y["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onChangeHeight",
                value: function (e) {
                    var t = this.props.action,
                        e = parseInt(e);
                    e && (t.height = e, y["default"].to(y["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, y["default"].to(y["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onUrlChange",
                value: function (e) {
                    var t = this.props.action;
                    t.value = e, y["default"].to(y["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (v["default"].PureComponent);
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(8),
        g = n(7),
        E = a(g),
        y = n(256),
        S = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = (t.isInComponent, t.isExpanded),
                        i = h["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, h["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return h["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return h["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        }))),
                        l = h["default"].createElement("div", {
                            className: "grid-item grid-row"
                        }, h["default"].createElement("div", {
                            className: "value-tag"
                        }, h["default"].createElement(v.EditableLabel, {
                            value: n.value,
                            onChange: function (t) {
                                return e.onValueChange(t)
                            }
                        })));
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? h["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("div", null, n.description)) : [i, l], h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return E["default"].to(E["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL(o ? "assets/info_cancel.png" : "assets/info.png")
                    })))), o && h["default"].createElement("div", {
                        className: "details-section"
                    }, h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), i), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Milliseconds:"), l), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), h["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onMakeCompVar",
                value: function () {
                    var e = this.props,
                        t = e.action,
                        n = e.activeTest,
                        a = new y.CompVariable("var" + n.variables.length, t.value, t.id);
                    E["default"].to(E["default"].SESSION, "addVarToComp", {
                        id: n.id,
                        compVar: a
                    }), t.value = "${" + a.name + "}", E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onValueChange",
                value: function (e) {
                    var t = this.props.action;
                    t.value = e, E["default"].to(E["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onSelectorChange",
                value: function (e, t) {
                    var n = this.props.action;
                    n[t] = e, E["default"].to(E["default"].SESSION, "updateNWAction", n)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = S
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = (n(8), n(7)),
        g = a(v),
        E = function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = t.isExpanded,
                        i = h["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, h["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return h["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return h["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        })));
                    return h["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column script-action"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? h["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("div", null, n.description)) : [i, h["default"].createElement("div", {
                        className: "grid-item"
                    }, h["default"].createElement("div", {
                        className: "description"
                    }, h["default"].createElement("div", null, n.description)))], h["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), h["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, h["default"].createElement("img", {
                        src: chrome.extension.getURL(o ? "assets/info_cancel.png" : "assets/info.png")
                    })))), o && h["default"].createElement("div", {
                        className: "details-section"
                    }, h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "warnings"
                    }, "Warning: Scripts run in SnapTest are sandboxed, and can only access the DOM - For full access, run the generated Selenium code.")), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Script:"), h["default"].createElement("textarea", {
                        className: "script-area",
                        value: n.script,
                        onChange: function (t) {
                            return e.onScriptChange(t.currentTarget.value)
                        }
                    })), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), h["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description)), h["default"].createElement("div", {
                        className: "details-row"
                    }, h["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), i)))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, g["default"].to(g["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onScriptChange",
                value: function (e) {
                    var t = this.props.action;
                    t.script = e, g["default"].to(g["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (h["default"].PureComponent);
    t["default"] = E
}, function (e, t, n) {
    "use strict";

    function a(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(3),
        i = r(o),
        l = n(2),
        u = r(l),
        c = n(6),
        s = r(c),
        d = n(5),
        f = r(d),
        p = n(4),
        m = r(p),
        h = n(1),
        v = r(h),
        g = n(8),
        E = n(7),
        y = r(E),
        S = n(28),
        N = a(S),
        b = function (e) {
            function t(e) {
                return (0, u["default"])(this, t), (0, f["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e))
            }
            return (0, m["default"])(t, e), (0, s["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = t.selectingForActionId,
                        i = t.selectionCandidate,
                        l = t.isExpanded,
                        u = t.defaultTimeout,
                        c = v["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, v["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return v["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return v["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        }))),
                        s = v["default"].createElement("div", {
                            className: "selector" + (n.warnings.length > 0 ? " warning" : "")
                        }, o === n.id ? v["default"].createElement("div", {
                            className: "selecting-el"
                        }, i || "click element...") : v["default"].createElement(g.EditableLabel, {
                            value: n.selector,
                            size: n.selector && n.selector.length,
                            onChange: function (t) {
                                return e.onSelectorChange(t)
                            }
                        }), v["default"].createElement("div", {
                            className: "action-quick-buttons"
                        }, o === n.id ? v["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return y["default"].to(y["default"].SESSION, "cancelSelection")
                            }
                        }, v["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        })) : v["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return y["default"].to(y["default"].SESSION, "startSelection", n.id)
                            }
                        }, v["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        })))),
                        d = v["default"].createElement("div", {
                            className: "grid-item"
                        }, v["default"].createElement("div", {
                            className: "value-tag"
                        }, v["default"].createElement(g.EditableLabel, {
                            value: n.value,
                            onChange: function (t) {
                                return e.onValueChange(t)
                            }
                        })));
                    return v["default"].createElement("div", {
                        className: "grid-item grid-row nw-action-con grid-column"
                    }, v["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? v["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, v["default"].createElement("div", null, n.description)) : [c, d, s], v["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, v["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, v["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), v["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, v["default"].createElement("img", {
                        src: chrome.extension.getURL(l ? "assets/info_cancel.png" : "assets/info.png")
                    })))), l && v["default"].createElement("div", {
                        className: "details-section"
                    }, v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), c), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, n.type === N.SCREENSHOT ? "Filename:" : "Assert value:", " "), d), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Selector:"), s), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Timeout:"), v["default"].createElement("input", {
                        className: "text-input",
                        type: "text",
                        value: n.timeout || u,
                        onChange: function (t) {
                            return e.onTimeoutChange(t.currentTarget.value)
                        }
                    }), "milliseconds"), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), v["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onTimeoutChange",
                value: function (e) {
                    var t = this.props.action;
                    "" !== e && parseInt(e) && (t.timeout = parseInt(e), y["default"].to(y["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, y["default"].to(y["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onValueChange",
                value: function (e) {
                    var t = this.props.action;
                    t.value = e, y["default"].to(y["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onSelectorChange",
                value: function (e) {
                    var t = this.props.action;
                    t.selector = e, t.warnings = [], y["default"].to(y["default"].SESSION, "updateNWAction", t)
                }
            }]), t
        } (v["default"].PureComponent);
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(3),
        i = r(o),
        l = n(2),
        u = r(l),
        c = n(6),
        s = r(c),
        d = n(5),
        f = r(d),
        p = n(4),
        m = r(p),
        h = n(1),
        v = r(h),
        g = n(8),
        E = n(7),
        y = r(E),
        S = n(28),
        N = a(S),
        b = function (e) {
            function t(e) {
                return (0, u["default"])(this, t), (0, f["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e))
            }
            return (0, m["default"])(t, e), (0, s["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.action,
                        a = t.actionTypeGroups,
                        r = t.onActionTypeChange,
                        o = t.selectingForActionId,
                        i = t.selectionCandidate,
                        l = t.isExpanded,
                        u = t.defaultTimeout,
                        c = v["default"].createElement("div", {
                            className: "action-type select-wrap"
                        }, v["default"].createElement("select", {
                            value: n.type,
                            onChange: function (e) {
                                return r(n, e.currentTarget.value)
                            }
                        }, a.map(function (e) {
                            return v["default"].createElement("optgroup", {
                                label: e.label
                            }, e.options.map(function (e) {
                                return v["default"].createElement("option", {
                                    disabled: e.disabled,
                                    value: e.value
                                }, e.name)
                            }))
                        }))),
                        s = v["default"].createElement("div", {
                            className: "grid-item"
                        }, v["default"].createElement("div", {
                            className: "value-tag-label"
                        }, "X:"), v["default"].createElement("div", {
                            className: "value-tag"
                        }, v["default"].createElement(g.EditableLabel, {
                            value: n.x + "",
                            onChange: function (t) {
                                return e.onXChange(t)
                            }
                        })), v["default"].createElement("div", {
                            className: "value-tag-label"
                        }, "Y:"), v["default"].createElement("div", {
                            className: "value-tag"
                        }, v["default"].createElement(g.EditableLabel, {
                            value: n.y + "",
                            onChange: function (t) {
                                return e.onYChange(t)
                            }
                        }))),
                        d = v["default"].createElement("div", {
                            className: "selector" + (n.warnings.length > 0 ? " warning" : "")
                        }, o === n.id ? v["default"].createElement("div", {
                            className: "selecting-el"
                        }, i || "click element...") : v["default"].createElement(g.EditableLabel, {
                            value: n.selector,
                            size: n.selector.length,
                            onChange: function (t) {
                                return e.onSelectorChange(t)
                            }
                        }), v["default"].createElement("div", {
                            className: "action-quick-buttons"
                        }, o === n.id ? v["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return y["default"].to(y["default"].SESSION, "cancelSelection")
                            }
                        }, v["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        })) : v["default"].createElement("div", {
                            className: "quick-button",
                            onClick: function () {
                                return y["default"].to(y["default"].SESSION, "startSelection", n.id)
                            }
                        }, v["default"].createElement("img", {
                            className: "selection-icon",
                            src: chrome.extension.getURL("assets/target.png")
                        }))));
                    return v["default"].createElement("div", {
                        className: "grid-item grid-row grid-column nw-action-con xy-coord-action"
                    }, v["default"].createElement("div", {
                        className: "grid-row"
                    }, n.description ? v["default"].createElement("div", {
                        className: "description",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, v["default"].createElement("div", null, n.description)) : [c, s, n.type === N.SCROLL_ELEMENT ? d : null], v["default"].createElement("div", {
                        className: "action-quick-buttons"
                    }, v["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "removeNWAction", n.id)
                        }
                    }, v["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/trash.png")
                    })), v["default"].createElement("div", {
                        className: "quick-button",
                        onClick: function () {
                            return y["default"].to(y["default"].SESSION, "toggleActionExpanded", n.id)
                        }
                    }, v["default"].createElement("img", {
                        src: chrome.extension.getURL(l ? "assets/info_cancel.png" : "assets/info.png")
                    })))), l && v["default"].createElement("div", {
                        className: "details-section"
                    }, v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Action:"), c), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Value:"), s), n.type === N.SCROLL_ELEMENT && v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Selector:"), d), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Timeout:"), v["default"].createElement("input", {
                        className: "text-input",
                        type: "text",
                        value: n.timeout || u,
                        onChange: function (t) {
                            return e.onTimeoutChange(t.currentTarget.value)
                        }
                    }), "milliseconds"), v["default"].createElement("div", {
                        className: "details-row"
                    }, v["default"].createElement("div", {
                        className: "details-row-title"
                    }, "Description:"), v["default"].createElement("textarea", {
                        placeholder: "Add description...",
                        onChange: function (t) {
                            return e.onDescriptionChange(t)
                        }
                    }, n.description))))
                }
            }, {
                key: "onTimeoutChange",
                value: function (e) {
                    var t = this.props.action;
                    "" !== e && parseInt(e) && (t.timeout = parseInt(e), y["default"].to(y["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onDescriptionChange",
                value: function (e) {
                    var t = this.props.action;
                    t.description = e.currentTarget.value, y["default"].to(y["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onSelectorChange",
                value: function (e) {
                    var t = this.props.action;
                    t.selector = e, t.warnings = [], y["default"].to(y["default"].SESSION, "updateNWAction", t)
                }
            }, {
                key: "onXChange",
                value: function (e) {
                    var t = this.props.action;
                    e = parseInt(e), e && (t.x = e, y["default"].to(y["default"].SESSION, "updateNWAction", t))
                }
            }, {
                key: "onYChange",
                value: function (e) {
                    var t = this.props.action;
                    e = parseInt(e), e && (t.y = e, y["default"].to(y["default"].SESSION, "updateNWAction", t))
                }
            }]), t
        } (v["default"].PureComponent);
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.MetaScanActionItem = t.AutoChangeWindowAction = t.ActionWithNumberValue = t.ScriptActionItem = t.XYCoordActionItem = t.PauseActionItem = t.ComponentActionItem = t.ElPresentAssertActionItem = t.ValueAssertActionItem = t.KeyDownActionItem = t.BackActionItem = t.LinkChangeIndicator = t.BlankActionItem = t.InputChangeActionItem = t.PageLoadActionItem = t.GenericActionItem = t.MouseClickActionItem = void 0;
    var r = n(285),
        o = a(r),
        i = n(280),
        l = a(i),
        u = n(286),
        c = a(u),
        s = n(281),
        d = a(s),
        f = n(277),
        p = a(f),
        m = n(283),
        h = a(m),
        v = n(276),
        g = a(v),
        E = n(282),
        y = a(E),
        S = n(289),
        N = a(S),
        b = n(279),
        w = a(b),
        _ = n(278),
        k = a(_),
        C = n(287),
        T = a(C),
        O = n(290),
        A = a(O),
        x = n(288),
        I = a(x),
        R = n(274),
        P = a(R),
        M = n(275),
        L = a(M),
        D = n(284),
        U = a(D);
    t.MouseClickActionItem = o["default"], t.GenericActionItem = l["default"], t.PageLoadActionItem = c["default"], t.InputChangeActionItem = d["default"], t.BlankActionItem = p["default"], t.LinkChangeIndicator = h["default"], t.BackActionItem = g["default"], t.KeyDownActionItem = y["default"], t.ValueAssertActionItem = N["default"], t.ElPresentAssertActionItem = w["default"], t.ComponentActionItem = k["default"], t.PauseActionItem = T["default"], t.XYCoordActionItem = A["default"], t.ScriptActionItem = I["default"], t.ActionWithNumberValue = P["default"], t.AutoChangeWindowAction = L["default"], t.MetaScanActionItem = U["default"]
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = n(8),
        y = n(57),
        S = a(y),
        N = n(27),
        b = (a(N), function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props,
                        t = e.activeTest,
                        n = (e.hotkeyConfig, e.showHotkeys, e.lastRoute);
                    return h["default"].createElement("div", {
                        className: "grid-row top-menu-container v-align"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, h["default"].createElement("button", {
                        className: "btn btn-primary",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "backRoute")
                        }
                    }, n ? n.name : "done")), h["default"].createElement("div", {
                        className: "grid-item grid-row test-name h-align"
                    }, h["default"].createElement(E.EditableLabel, {
                        value: t.name,
                        size: t.name.length,
                        onChange: function (e) {
                            g["default"].to(g["default"].SESSION, "setComponentName", {
                                id: t.id,
                                value: e
                            })
                        }
                    })), h["default"].createElement("img", {
                        className: "help-icon",
                        src: chrome.extension.getURL("assets/help.png"),
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "setModal", "component-help")
                        }
                    }), h["default"].createElement(S["default"], this.props))
                }
            }]), t
        } (h["default"].Component));
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = (n(8), n(57)),
        y = a(E),
        S = n(27),
        N = (a(S), function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props,
                        t = (e.activeTest, e.showHotkeys),
                        n = e.hotkeyConfig,
                        a = e.lastRoute,
                        r = e.activeFolder;
                    return h["default"].createElement("div", {
                        className: "grid-row top-menu-container v-align"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, h["default"].createElement("button", {
                        className: "btn btn-primary",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "backRoute")
                        }
                    }, t && h["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", n.BACK, ")"), a ? a.name : "back")), h["default"].createElement("div", {
                        className: "grid-item grid-row test-name h-align"
                    }, r ? "Generate for folder" : "Generate for single test"), h["default"].createElement("img", {
                        className: "help-icon",
                        src: chrome.extension.getURL("assets/help.png"),
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "setModal", "test-help")
                        }
                    }), h["default"].createElement(y["default"], this.props))
                }
            }]), t
        } (h["default"].Component));
    t["default"] = N
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(3),
        o = a(r),
        i = n(2),
        l = a(i),
        u = n(6),
        c = a(u),
        s = n(5),
        d = a(s),
        f = n(4),
        p = a(f),
        m = n(1),
        h = a(m),
        v = n(7),
        g = a(v),
        E = n(8),
        y = n(57),
        S = a(y),
        N = n(27),
        b = (a(N), function (e) {
            function t(e) {
                return (0, l["default"])(this, t), (0, d["default"])(this, (t.__proto__ || (0, o["default"])(t)).call(this, e))
            }
            return (0, p["default"])(t, e), (0, c["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this.props,
                        t = e.activeTest,
                        n = e.showHotkeys,
                        a = e.hotkeyConfig,
                        r = e.lastRoute;
                    return h["default"].createElement("div", {
                        className: "grid-row top-menu-container v-align"
                    }, h["default"].createElement("div", {
                        className: "grid-row"
                    }, h["default"].createElement("button", {
                        className: "btn btn-primary",
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "backRoute")
                        }
                    }, n && h["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", a.BACK, ")"), r ? r.name : "back")), h["default"].createElement("div", {
                        className: "grid-item grid-row test-name h-align"
                    }, h["default"].createElement(E.EditableLabel, {
                        value: t.name,
                        size: t.name && t.name.length,
                        onChange: function (e) {
                            g["default"].to(g["default"].SESSION, "setTestName", {
                                id: t.id,
                                value: e
                            })
                        }
                    })), h["default"].createElement("img", {
                        className: "help-icon",
                        src: chrome.extension.getURL("assets/help.png"),
                        onClick: function () {
                            return g["default"].to(g["default"].SESSION, "setModal", "test-help")
                        }
                    }), h["default"].createElement(S["default"], this.props))
                }
            }]), t
        } (h["default"].Component));
    t["default"] = b
}, function (e, t, n) {
    "use strict";

    function a(e) {
        if (e && e.__esModule) return e;
        var t = {};
        if (null != e)
            for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && (t[n] = e[n]);
        return t["default"] = e, t
    }

    function r(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = n(3),
        i = r(o),
        l = n(2),
        u = r(l),
        c = n(6),
        s = r(c),
        d = n(5),
        f = r(d),
        p = n(4),
        m = r(p),
        h = n(56),
        v = r(h),
        g = n(222),
        E = r(g),
        y = n(264),
        S = r(y),
        N = n(1),
        b = r(N),
        w = n(302),
        _ = r(w),
        k = n(28),
        C = a(k),
        T = n(7),
        O = r(T),
        A = n(239),
        x = r(A),
        I = n(8),
        R = n(240),
        P = (r(R), n(27)),
        M = r(P),
        L = function (e) {
            function t(e) {
                (0, u["default"])(this, t);
                var n = (0, f["default"])(this, (t.__proto__ || (0, i["default"])(t)).call(this, e));
                return n.state = {
                    isShiftSelected: !1
                }, n
            }
            return (0, m["default"])(t, e), (0, s["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.props,
                        n = t.isRecording,
                        a = void 0 !== n && n,
                        r = t.primaryTabId,
                        o = t.currentTabId,
                        i = t.isAssertMode,
                        l = void 0 !== i && i,
                        u = t.isPlayingBack,
                        c = void 0 !== u && u,
                        s = t.cursorIndex,
                        d = void 0 === s ? -1 : s,
                        f = t.components,
                        p = void 0 === f ? [] : f,
                        m = t.currentRoute,
                        h = t.activeTest,
                        v = t.selectedRows,
                        g = t.showHotkeys,
                        y = t.hotkeyConfig,
                        N = t.selectingForActionId,
                        w = t.selectionCandidate,
                        k = t.playbackResult,
                        C = t.playbackCursor,
                        T = t.playbackBreakpoints,
                        A = t.expandedActions,
                        R = h.actions,
                        P = void 0 === R ? [] : R;
                    return b["default"].createElement("div", {
                        className: "NWTestRecorder"
                    }, b["default"].createElement(_["default"], {
                        keyEventName: "keydown",
                        keyValue: "Shift",
                        onKeyHandle: function () {
                            return e.onShiftDown()
                        }
                    }), b["default"].createElement(_["default"], {
                        keyEventName: "keyup",
                        keyValue: "Shift",
                        onKeyHandle: function () {
                            return e.onShiftUp()
                        }
                    }), h.variables.length > 0 && b["default"].createElement("div", {
                        className: "variables"
                    }, b["default"].createElement("div", {
                        className: "variable-header"
                    }, "Variables with defaults: "), b["default"].createElement("div", {
                        className: "grid-row grid-flexwrap"
                    }, h.variables.map(function (t, n) {
                        return b["default"].createElement("div", {
                            className: "variable"
                        }, b["default"].createElement("div", {
                            className: "key"
                        }, b["default"].createElement(I.EditableLabel, {
                            value: t.name,
                            size: t.name.length,
                            onChange: function (n) {
                                return e.onVarNameChange(t, n)
                            }
                        })), b["default"].createElement("div", {
                            className: "default"
                        }, b["default"].createElement(I.EditableLabel, {
                            value: t.defaultValue,
                            size: t.defaultValue.length,
                            onChange: function (n) {
                                return e.onVarDefaultvalueChange(t, n)
                            }
                        }), b["default"].createElement("span", {
                            onClick: function () {
                                return e.onVarDelete(t)
                            }
                        }, b["default"].createElement(I.Icon, {
                            classNames: "remove-btn",
                            name: "remove"
                        }))))
                    }))), b["default"].createElement("div", {
                        className: "test-header"
                    }, b["default"].createElement("div", {
                        className: "grid-row v-align test-header-controls"
                    }, b["default"].createElement("div", {
                        className: "grid-row selection-actions v-align"
                    }, b["default"].createElement("button", {
                        className: "btn btn-primary play-btn" + (c ? " active" : "") + (o !== r ? " btn-disabled btn-clickable" : ""),
                        onClick: function () {
                            return c ? e.onPausePlayback() : e.onStartPlayback()
                        }
                    }, c ? "Stop" : C ? "Resume" : "Run"), C && b["default"].createElement("button", {
                        key: 0,
                        className: "btn play-btns" + (c ? " btn-disabled" : ""),
                        onClick: function () {
                            return e.onStepOver()
                        }
                    }, "Step over"), !c && !(0, S["default"])(k) && b["default"].createElement("button", {
                        className: "btn play-btns",
                        onClick: function () {
                            return e.onResetPlayback()
                        }
                    }, "Clear")), b["default"].createElement("div", {
                        className: "grid-item grid-row h-align-right test-controls"
                    }, b["default"].createElement("div", {
                        className: "grid-row h-align-right"
                    }, P.length > 0 && b["default"].createElement("button", {
                        className: "btn assert-btn" + (l ? " active" : "") + (r !== o ? " btn-disabled btn-clickable" : ""),
                        onClick: function () {
                            return e.onAssertModeChange()
                        }
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.ASSERT_MODE, ")"), "Assert"), a && !l ? b["default"].createElement("button", {
                        className: "btn recording-btn active",
                        onClick: function () {
                            return O["default"].to(O["default"].SESSION, "stopRecording")
                        }
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.RECORD_MODE, ")"), "Record") : b["default"].createElement("button", {
                        className: "btn recording-btn" + (a || r === o ? "" : " btn-disabled btn-clickable"),
                        onClick: function () {
                            return e.onStartRecording()
                        }
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.RECORD_MODE, ")"), "Record"), P.length > 0 && "test" === h.type && b["default"].createElement("button", {
                        className: "btn btn-secondary",
                        onClick: function () {
                            return e.onGetCode()
                        }
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.SHOW_CODE, ")"), "Get Code")))), b["default"].createElement("div", {
                        className: "grid-row v-align"
                    }, b["default"].createElement("div", {
                        className: "grid-item"
                    }, b["default"].createElement("button", {
                        className: "btn btn-secondary" + (v.length > 0 ? "" : " btn-disabled"),
                        onClick: function () {
                            return e.onClearSelected()
                        }
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.CLEAR_SELECTION, ")"), "Clear"), b["default"].createElement("button", {
                        className: "btn btn-secondary" + (v.length > 0 ? "" : " btn-disabled"),
                        onClick: function () {
                            e.onDeleteActions()
                        }
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.DELETE_ROWS, ")"), "Delete"), b["default"].createElement("button", {
                        className: "btn btn-secondary" + (v.length > 0 ? "" : " btn-disabled"),
                        onClick: function () {
                            return e.onRollup()
                        }
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.ROLLUP_SELECTION, ")"), "Rollup")), "test" === h.type && b["default"].createElement("div", null, b["default"].createElement("div", {
                        onClick: function () {
                            return O["default"].to(O["default"].SESSION, "undo")
                        },
                        className: "undo-redo-icon",
                        title: "undo"
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.UNDO, ")"), b["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/undo.png")
                    })), b["default"].createElement("div", {
                        onClick: function () {
                            return O["default"].to(O["default"].SESSION, "redo")
                        },
                        className: "undo-redo-icon",
                        title: "redo"
                    }, g && b["default"].createElement("div", {
                        className: "hotkey"
                    }, "(", y.REDO, ")"), b["default"].createElement("img", {
                        src: chrome.extension.getURL("assets/redo.png")
                    }))))), b["default"].createElement("div", {
                        className: "action-list"
                    }, b["default"].createElement("div", {
                        className: "grid-row quick-row-actions not-in-list"
                    }, a ? b["default"].createElement("div", {
                        className: "cursor cursor-record grid-item",
                        onClick: function () {
                            return e.onSetRecordCursor(-1)
                        }
                    }, b["default"].createElement("div", {
                        className: "dots"
                    }, "•••")) : l ? b["default"].createElement("div", {
                        className: "cursor cursor-assert grid-item",
                        onClick: function () {
                            return e.onSetAssertCursor(-1)
                        }
                    }, b["default"].createElement("div", {
                        className: "dots"
                    }, "•••")) : b["default"].createElement("div", {
                        className: "add-row grid-item",
                        onClick: function () {
                            return e.onAddBlank(0)
                        }
                    }, "+")), (l || a) && d === -1 && b["default"].createElement("div", {
                        className: "record-cursor-container" + (l ? " is-asserting" : "")
                    }, b["default"].createElement("div", {
                        className: "record-cursor"
                    }, b["default"].createElement("div", {
                        className: "record-cursor-bar"
                    }))), b["default"].createElement("div", {
                        className: "nw-section"
                    }, P.map(function (t, n) {
                        return b["default"].createElement(x["default"], {
                            action: t,
                            isInComponent: "componentbuilder" === m.name,
                            idx: n,
                            components: p,
                            activeTest: h,
                            onNWRowClick: function (t, n) {
                                return e.onNWRowClick(t, n)
                            },
                            selectedRows: v,
                            isLast: n === P.length - 1,
                            isFirst: 0 === n,
                            isCursorHere: d === n && (a || l),
                            isAssertMode: l,
                            isRecording: a,
                            selectingForActionId: N,
                            selectionCandidate: w,
                            playbackResult: k,
                            playbackCursor: C,
                            playbackBreakpoints: T,
                            expandedActions: A
                        })
                    }), b["default"].createElement("div", {
                        className: "grid-row quick-row-actions not-in-list"
                    }, a ? b["default"].createElement("div", {
                        className: "cursor cursor-record grid-item",
                        onClick: function () {
                            return e.onSetRecordCursor(null)
                        }
                    }, b["default"].createElement("div", {
                        className: "dots"
                    }, "•••")) : l ? b["default"].createElement("div", {
                        className: "cursor cursor-assert grid-item",
                        onClick: function () {
                            return e.onSetAssertCursor(null)
                        }
                    }, b["default"].createElement("div", {
                        className: "dots"
                    }, "•••")) : b["default"].createElement("div", {
                        className: "add-row grid-item",
                        onClick: function () {
                            return e.onAddBlank(-1)
                        }
                    }, "+")), (l || a) && !(0, E["default"])(d) && b["default"].createElement("div", {
                        className: "record-cursor-container" + (l ? " is-asserting" : "")
                    }, b["default"].createElement("div", {
                        className: "record-cursor"
                    }, b["default"].createElement("div", {
                        className: "record-cursor-bar"
                    }))))))
                }
            }, {
                key: "onAssertModeChange",
                value: function () {
                    var e = this.props,
                        t = e.isAssertMode,
                        n = e.primaryTabId,
                        a = e.currentTabId;
                    n !== a ? O["default"].to(O["default"].SESSION, "setActiveTabAlert", !0) : O["default"].to(O["default"].SESSION, "setAssertMode", !t)
                }
            }, {
                key: "onStartRecording",
                value: function () {
                    var e = this.props,
                        t = e.isRecording,
                        n = e.primaryTabId,
                        a = e.currentTabId;
                    t || n === a ? O["default"].to(O["default"].SESSION, "startRecording") : O["default"].to(O["default"].SESSION, "setActiveTabAlert", !0)
                }
            }, {
                key: "onGetCode",
                value: function () {
                    O["default"].to(O["default"].SESSION, "generateCode"), O["default"].to(O["default"].SESSION, "pushRoute", new M["default"]("codeviewer"))
                }
            }, {
                key: "onSetAssertCursor",
                value: function (e) {
                    var t = this.props.isAssertMode;
                    O["default"].to(O["default"].SESSION, "setCursor", e), t || O["default"].to(O["default"].SESSION, "setAssertMode", !0)
                }
            }, {
                key: "onSetRecordCursor",
                value: function (e) {
                    var t = this.props.isRecording;
                    O["default"].to(O["default"].SESSION, "setCursor", e), t || O["default"].to(O["default"].SESSION, "startRecording", {
                        initialUrl: window.location.href,
                        width: window.innerWidth,
                        height: window.innerHeight
                    })
                }
            }, {
                key: "onResetPlayback",
                value: function () {
                    O["default"].to(O["default"].SESSION, "resetPlayback"), O["default"].to(O["default"].SESSION, "clearPlaybackResults")
                }
            }, {
                key: "onPausePlayback",
                value: function () {
                    var e = this.props,
                        t = e.activeTest,
                        n = e.playbackCursor,
                        a = (e.playbackResult, (0, v["default"])(t.actions, {
                            id: n
                        }));
                    O["default"].to(O["default"].SESSION, "playbackPause"), a !== -1 && t.actions[a + 1] && O["default"].to(O["default"].SESSION, "playbackPauseAt", t.actions[a + 1].id)
                }
            }, {
                key: "onClearPlaybackResults",
                value: function () {
                    O["default"].to(O["default"].SESSION, "clearPlaybackResults")
                }
            }, {
                key: "onStartPlayback",
                value: function () {
                    var e = this.props,
                        t = e.primaryTabId,
                        n = e.currentTabId;
                    n !== t ? O["default"].to(O["default"].SESSION, "setActiveTabAlert", !0) : O["default"].to(O["default"].SESSION, "startPlayback")
                }
            }, {
                key: "onStepOver",
                value: function () {
                    O["default"].to(O["default"].SESSION, "addStepOverBreakpoint"), O["default"].to(O["default"].SESSION, "startPlayback")
                }
            }, {
                key: "onShowCode",
                value: function () {
                    var e = this.props.currentRoute;
                    "testbuilder" === e.name ? O["default"].to(O["default"].SESSION, "pushRoute", new M["default"]("testbuildercode")) : O["default"].to(O["default"].SESSION, "pushRoute", new M["default"]("testbuilder"))
                }
            }, {
                key: "onVarNameChange",
                value: function (e, t) {
                    e.name = t, O["default"].to(O["default"].SESSION, "updateVar", e)
                }
            }, {
                key: "onVarDefaultvalueChange",
                value: function (e, t) {
                    e.defaultValue = t, O["default"].to(O["default"].SESSION, "updateVar", e)
                }
            }, {
                key: "onVarDelete",
                value: function (e) {
                    O["default"].to(O["default"].SESSION, "deleteVar", e)
                }
            }, {
                key: "onAddBlank",
                value: function (e) {
                    (0, E["default"])(e) && e > -1 ? O["default"].to(O["default"].SESSION, "insertNWAction", {
                        action: new C.BlankAction,
                        insertAt: e
                    }) : O["default"].to(O["default"].SESSION, "insertNWAction", {
                        action: new C.BlankAction
                    })
                }
            }, {
                key: "onNWRowClick",
                value: function (e, t) {
                    var n = this.props,
                        a = n.recentSelectedIndex,
                        r = n.selectedRows,
                        o = this.state.isShiftSelected,
                        i = this.props.activeTest.actions,
                        l = void 0 === i ? [] : i;
                    if (o && a > -1 && a !== t) {
                        var u = [];
                        if (a < t)
                            for (var c = a; c <= t; c++) u.push(l[c].id);
                        else
                            for (var c = t; c <= a; c++) u.push(l[c].id);
                        O["default"].to(O["default"].SESSION, "setSelectedRows", {
                            selectedRows: u
                        })
                    } else {
                        var u = r.slice(0),
                            s = u.indexOf(e.id);
                        s === -1 ? (u.push(e.id), O["default"].to(O["default"].SESSION, "setSelectedRows", {
                            selectedRows: u,
                            recentSelectedIndex: t
                        })) : (u.splice(s, 1), O["default"].to(O["default"].SESSION, "setSelectedRows", {
                            selectedRows: u,
                            recentSelectedIndex: null
                        }))
                    }
                }
            }, {
                key: "onDeleteActions",
                value: function () {
                    O["default"].to(O["default"].SESSION, "removeNWActions"), O["default"].to(O["default"].SESSION, "clearSelectedRows")
                }
            }, {
                key: "onShiftDown",
                value: function () {
                    this.setState({
                        isShiftSelected: !0
                    })
                }
            }, {
                key: "onShiftUp",
                value: function () {
                    this.setState({
                        isShiftSelected: !1
                    })
                }
            }, {
                key: "onRollup",
                value: function () {
                    O["default"].to(O["default"].SESSION, "createComponentFromSelectedRows")
                }
            }, {
                key: "onClearSelected",
                value: function () {
                    O["default"].to(O["default"].SESSION, "clearSelectedRows")
                }
            }]), t
        } (b["default"].PureComponent);
    t["default"] = L
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(139),
        o = a(r),
        i = n(3),
        l = a(i),
        u = n(2),
        c = a(u),
        s = n(6),
        d = a(s),
        f = n(5),
        p = a(f),
        m = n(4),
        h = a(m),
        v = n(1),
        g = a(v),
        E = n(7),
        y = (a(E), n(237), function (e) {
            function t(e) {
                (0, c["default"])(this, t);
                var n = (0, p["default"])(this, (t.__proto__ || (0, l["default"])(t)).call(this, e));
                return n.state = {
                    email: "",
                    password: "",
                    isProcessing: !1,
                    serverError: null,
                    successMessage: null
                }, n
            }
            return (0, h["default"])(t, e), (0, d["default"])(t, [{
                key: "render",
                value: function () {
                    var e = this,
                        t = this.state,
                        n = t.email,
                        a = t.isProcessing,
                        r = t.serverError,
                        o = t.successMessage,
                        i = this.props.title,
                        l = void 0 === i ? null : i;
                    return g["default"].createElement("form", {
                        className: "beta-access-form",
                        onSubmit: function (t) {
                            return e.onSubmit(t)
                        }
                    }, l && g["default"].createElement("h5", {
                        className: "form-row form-title"
                    }, l), g["default"].createElement("div", {
                        className: "form-row"
                    }, g["default"].createElement("input", {
                        type: "email",
                        placeholder: "email",
                        value: n,
                        onChange: function (t) {
                            return e.setState({
                                email: t.currentTarget.value
                            })
                        }
                    })), g["default"].createElement("div", {
                        className: "form-row"
                    }, a ? g["default"].createElement("button", {
                        type: "button",
                        className: "btn btn-primary"
                    }, "sending...") : g["default"].createElement("button", {
                        type: "submit",
                        className: "btn btn-primary"
                    }, "Request access")), r && g["default"].createElement("div", {
                        className: "form-row"
                    }, g["default"].createElement("div", {
                        className: "form-server-error"
                    }, r)), o && g["default"].createElement("div", {
                        className: "form-row"
                    }, g["default"].createElement("div", {
                        className: "form-server-success"
                    }, o)))
                }
            }, {
                key: "onSubmit",
                value: function (e) {
                    this.setState({
                        serverError: null,
                        isProcessing: !0
                    });
                    var t = this;
                    e.preventDefault();
                    var n = {
                        email: this.state.email,
                        target: "betaaccessextension"
                    };
                    fetch("https://www.snaptest.io/api/emailsignup", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: (0, o["default"])(n)
                    }).then(function (e) {
                        t.setState({
                            successMessage: "Request submitted.  You'll receive an email shortly with login details.",
                            serverError: null,
                            isProcessing: !1
                        })
                    })["catch"](function (e) {
                        t.setState({
                            serverError: e,
                            isProcessing: !1
                        })
                    })
                }
            }]), t
        } (g["default"].Component));
    t["default"] = y
}, function (e, t, n) {
    "use strict";

    function a(e) {
        return e && e.__esModule ? e : {
            "default": e
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = n(55),
        o = a(r),
        i = n(3),
        l = a(i),
        u = n(2),
        c = a(u),
        s = n(6),
        d = a(s),
        f = n(5),
        p = a(f),
        m = n(4),
        h = a(m),
        v = n(1),
        g = a(v),
        E = n(7),
        y = a(E),
        S = n(269),
        N = a(S),
        b = n(258),
        w = function (e) {
            function t(e) {
                (0, c["default"])(this, t);
                var n = (0, p["default"])(this, (t.__proto__ || (0, l["default"])(t)).call(this, e));
                return n.state = n.props.initialState, n
            }
            return (0, h["default"])(t, e), (0, d["default"])(t, [{
                key: "componentDidMount",
                value: function () {
                    b.bindHotkeys.call(this)
                }
            }, {
                key: "componentWillUnmount",
                value: function () {
                    b.unbindHotkeys.call(this)
                }
            }, {
                key: "componentWillMount",
                value: function () {
                    var e = chrome.runtime.connect({
                        name: "devtools-page"
                    });
                    e.onMessage.addListener(function (e) {
                        switch (e.action) {
                            case "stateChange":
                                this.setState(e.payload)
                        }
                    }.bind(this))
                }
            }, {
                key: "render",
                value: function () {
                    var e = this.state;
                    e.primaryTabId, e.currentTabId, e.isActivated, e.activeTabs;
                    return g["default"].createElement("div", {
                        className: "devtool-window"
                    }, g["default"].createElement(N["default"], (0, o["default"])({}, this.state, this.props)))
                }
            }, {
                key: "onSetTabAsActive",
                value: function () {
                    var e = this.state.currentTabId;
                    y["default"].to(y["default"].SESSION, "setActivated", !0), y["default"].to(y["default"].SESSION, "setActiveTab", e), y["default"].to(y["default"].SESSION, "shouldRefresh", !0)
                }
            }]), t
        } (g["default"].Component);
    t["default"] = w
}, , , , , function (e, t, n) {
    ! function (t, a) {
        e.exports = a(n(1))
    } (this, function (e) {
        return function (e) {
            function t(a) {
                if (n[a]) return n[a].exports;
                var r = n[a] = {
                    exports: {},
                    id: a,
                    loaded: !1
                };
                return e[a].call(r.exports, r, r.exports, t), r.loaded = !0, r.exports
            }
            var n = {};
            return t.m = e, t.c = n, t.p = "", t(0)
        } ([function (e, t, n) {
            "use strict";
            e.exports = n(1)
        }, function (e, t, n) {
            "use strict";

            function a(e) {
                return e && e.__esModule ? e : {
                    "default": e
                }
            }

            function r(e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }

            function o(e, t) {
                if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                return !t || "object" != typeof t && "function" != typeof t ? e : t
            }

            function i(e, t) {
                if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
                e.prototype = Object.create(t && t.prototype, {
                    constructor: {
                        value: e,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
            }

            function l() {
                var e = arguments.length <= 0 || void 0 === arguments[0] ? null : arguments[0];
                return function (t) {
                    var n = t || {},
                        a = n.keyValue,
                        l = n.keyCode,
                        s = n.keyName,
                        d = n.keyEventName;
                    return function (t) {
                        return function (n) {
                            function p(e) {
                                r(this, p);
                                var t = o(this, Object.getPrototypeOf(p).call(this, e));
                                return t.state = {
                                    keyValue: null,
                                    keyCode: null,
                                    keyName: null
                                }, t.handleKey = t.handleKey.bind(t), t
                            }
                            return i(p, n), c(p, [{
                                key: "render",
                                value: function () {
                                    return f["default"].createElement("div", null, f["default"].createElement(h, {
                                        keyValue: a,
                                        keyCode: l,
                                        keyEventName: d,
                                        keyName: s,
                                        onKeyHandle: this.handleKey
                                    }), f["default"].createElement(t, u({}, this.props, this.state)))
                                }
                            }, {
                                key: "handleKey",
                                value: function (t) {
                                    e && e(t, this.state) ? this.setState({
                                        keyValue: null,
                                        keyCode: null,
                                        keyName: null
                                    }) : this.setState({
                                        keyValue: (0, m.eventKey)(t),
                                        keyCode: t.keyCode,
                                        keyName: (0, m.eventKeyName)(t)
                                    })
                                }
                            }]), p
                        } (f["default"].Component)
                    }
                }
            }
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.keyToggleHandler = t.keyHandler = void 0;
            var u = Object.assign || function (e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var a in n) Object.prototype.hasOwnProperty.call(n, a) && (e[a] = n[a])
                }
                return e
            },
                c = function () {
                    function e(e, t) {
                        for (var n = 0; n < t.length; n++) {
                            var a = t[n];
                            a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(e, a.key, a)
                        }
                    }
                    return function (t, n, a) {
                        return n && e(t.prototype, n), a && e(t, a), t
                    }
                } (),
                s = n(2);
            Object.keys(s).forEach(function (e) {
                "default" !== e && Object.defineProperty(t, e, {
                    enumerable: !0,
                    get: function () {
                        return s[e]
                    }
                })
            });
            var d = n(3),
                f = a(d),
                p = n(4),
                m = n(5),
                h = function (e) {
                    function t(e) {
                        r(this, t);
                        var n = o(this, Object.getPrototypeOf(t).call(this, e));
                        return e.keyValue || e.keyCode || e.keyName || console.error("Warning: Failed propType: Missing prop `keyValue`, `keyCode` or `keyName` for `KeyHandler`."), e.keyName && console.error("Warning: Failed propType: Do not use deprecated prop `keyName`, use `keyValue` or `keyCode` instead for `KeyHandler`."), n.handleKey = n.handleKey.bind(n), n
                    }
                    return i(t, e), c(t, [{
                        key: "shouldComponentUpdate",
                        value: function () {
                            return !1
                        }
                    }]), c(t, [{
                        key: "componentDidMount",
                        value: function () {
                            p.canUseDOM && window.document.addEventListener(this.props.keyEventName, this.handleKey)
                        }
                    }, {
                        key: "componentWillUnmount",
                        value: function () {
                            p.canUseDOM && window.document.removeEventListener(this.props.keyEventName, this.handleKey)
                        }
                    }, {
                        key: "render",
                        value: function () {
                            return null
                        }
                    }, {
                        key: "handleKey",
                        value: function (e) {
                            var t = this.props,
                                n = t.keyValue,
                                a = t.keyCode,
                                r = t.keyName,
                                o = t.onKeyHandle;
                            if (o) {
                                var i = e.target;
                                i instanceof window.HTMLElement && (0, m.isInput)(i) || (0, m.matchesKeyboardEvent)(e, {
                                    keyValue: n,
                                    keyCode: a,
                                    keyName: r
                                }) && o(e)
                            }
                        }
                    }]), t
                } (f["default"].Component);
            h.propTypes = {
                keyValue: f["default"].PropTypes.string,
                keyCode: f["default"].PropTypes.number,
                keyEventName: f["default"].PropTypes.oneOf([s.KEYDOWN, s.KEYPRESS, s.KEYUP]),
                keyName: f["default"].PropTypes.string,
                onKeyHandle: f["default"].PropTypes.func
            }, h.defaultProps = {
                keyEventName: s.KEYUP
            }, t["default"] = h;
            t.keyHandler = l(), t.keyToggleHandler = l(m.matchesKeyboardEvent)
        }, function (e, t) {
            "use strict";
            Object.defineProperty(t, "__esModule", {
                value: !0
            });
            t.KEYDOWN = "keydown", t.KEYPRESS = "keypress", t.KEYUP = "keyup"
        }, function (t, n) {
            t.exports = e
        }, function (e, t, n) {
            var a;
            /*!
            		  Copyright (c) 2015 Jed Watson.
            		  Based on code that is Copyright 2013-2015, Facebook, Inc.
            		  All rights reserved.
            		*/
            ! function () {
                "use strict";
                var r = !("undefined" == typeof window || !window.document || !window.document.createElement),
                    o = {
                        canUseDOM: r,
                        canUseWorkers: "undefined" != typeof Worker,
                        canUseEventListeners: r && !(!window.addEventListener && !window.attachEvent),
                        canUseViewport: r && !!window.screen
                    };
                a = function () {
                    return o
                }.call(t, n, t, e), !(void 0 !== a && (e.exports = a))
            } ()
        }, function (e, t, n) {
            "use strict";

            function a(e) {
                return e && e.__esModule ? e : {
                    "default": e
                }
            }

            function r(e) {
                if (!e) return !1;
                var t = e.tagName,
                    n = o(e);
                return "INPUT" === t || "TEXTAREA" === t || n
            }

            function o(e) {
                return "function" == typeof e.getAttribute && !!e.getAttribute("contenteditable")
            }

            function i(e, t) {
                var n = t.keyCode,
                    a = t.keyValue,
                    r = t.keyName;
                return l(a) ? l(n) ? !l(r) && r === c(e) : n === e.keyCode : a === u(e)
            }

            function l(e) {
                return void 0 === e || null === e
            }

            function u(e) {
                var t = e.key,
                    n = e.keyCode,
                    a = e.type;
                if (t) {
                    var r = m[t] || t;
                    if ("Unidentified" !== r) return r
                }
                if (a === p.KEYPRESS) {
                    var o = s(e);
                    return 13 === o ? "Enter" : String.fromCharCode(o)
                }
                return a === p.KEYDOWN || a === p.KEYUP ? h[String(n)] || "Unidentified" : ""
            }

            function c(e) {
                return (0, f["default"])(e.keyCode)
            }

            function s(e) {
                var t = e.charCode,
                    n = e.keyCode;
                if ("charCode" in e) {
                    if (0 === t && 13 === n) return 13
                } else t = n;
                return t >= 32 || 13 === t ? t : 0
            }
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.isInput = r, t.matchesKeyboardEvent = i, t.eventKey = u, t.eventKeyName = c;
            var d = n(6),
                f = a(d),
                p = n(2),
                m = {
                    Esc: "Escape",
                    Spacebar: " ",
                    Left: "ArrowLeft",
                    Up: "ArrowUp",
                    Right: "ArrowRight",
                    Down: "ArrowDown",
                    Del: "Delete",
                    Win: "OS",
                    Menu: "ContextMenu",
                    Apps: "ContextMenu",
                    Scroll: "ScrollLock",
                    MozPrintableKey: "Unidentified"
                },
                h = {
                    8: "Backspace",
                    9: "Tab",
                    12: "Clear",
                    13: "Enter",
                    16: "Shift",
                    17: "Control",
                    18: "Alt",
                    19: "Pause",
                    20: "CapsLock",
                    27: "Escape",
                    32: " ",
                    33: "PageUp",
                    34: "PageDown",
                    35: "End",
                    36: "Home",
                    37: "ArrowLeft",
                    38: "ArrowUp",
                    39: "ArrowRight",
                    40: "ArrowDown",
                    45: "Insert",
                    46: "Delete",
                    112: "F1",
                    113: "F2",
                    114: "F3",
                    115: "F4",
                    116: "F5",
                    117: "F6",
                    118: "F7",
                    119: "F8",
                    120: "F9",
                    121: "F10",
                    122: "F11",
                    123: "F12",
                    144: "NumLock",
                    145: "ScrollLock",
                    224: "Meta"
                }
        }, function (e, t) {
            "use strict";

            function n(e) {
                return r[e.toLowerCase()] || e.toUpperCase().charCodeAt(0)
            }

            function a(e) {
                for (var t in r)
                    if (r.hasOwnProperty(t) && r[t] === e) return t;
                return String.fromCharCode(e).toLowerCase()
            }
            for (var r = {
                ctrl: 17,
                control: 17,
                alt: 18,
                option: 18,
                shift: 16,
                windows: 91,
                command: 91,
                esc: 27,
                escape: 27,
                "`": 192,
                "-": 189,
                "=": 187,
                backspace: 8,
                tab: 9,
                "\\": 220,
                "[": 219,
                "]": 221,
                ";": 186,
                "'": 222,
                enter: 13,
                "return": 13,
                ",": 188,
                ".": 190,
                "/": 191,
                space: 32,
                pause: 19,
                "break": 19,
                insert: 45,
                "delete": 46,
                home: 36,
                end: 35,
                pageup: 33,
                pagedown: 34,
                left: 37,
                up: 38,
                right: 39,
                down: 40,
                capslock: 20,
                numlock: 144,
                scrolllock: 145
            }, o = 1; o < 20; o++) r["f" + o] = 111 + o;
            e.exports = function (e) {
                return "string" == typeof e ? n(e) : "number" == typeof e ? a(e) : void 0
            }
        }])
    })
}, , function (e, t, n) {
    "use strict";

    function a(e, t) {
        var n = e.replace(o, t).replace(i, t).replace(l, t).replace(u, t).replace(c, t);
        return r(n, 255)
    }
    var r = n(305),
        o = /[\/\?<>\\:\*\|":]/g,
        i = /[\x00-\x1f\x80-\x9f]/g,
        l = /^\.+$/,
        u = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i,
        c = /[\. ]+$/;
    e.exports = function (e, t) {
        var n = t && t.replacement || "",
            r = a(e, n);
        return "" === n ? r : a(r, "")
    }
}, function (e, t, n) {
    "use strict";
    var a = n(306),
        r = n(308);
    e.exports = a.bind(null, r)
}, function (e, t) {
    "use strict";

    function n(e) {
        return e >= 55296 && e <= 56319
    }

    function a(e) {
        return e >= 56320 && e <= 57343
    }
    e.exports = function (e, t, r) {
        if ("string" != typeof t) throw new Error("Input must be string");
        for (var o, i, l = t.length, u = 0, c = 0; c < l; c += 1) {
            if (o = t.charCodeAt(c), i = t[c], n(o) && a(t.charCodeAt(c + 1)) && (c += 1, i += t[c]), u += e(i), u === r) return t.slice(0, c + 1);
            if (u > r) return t.slice(0, c - i.length + 1)
        }
        return t
    }
}, , function (e, t) {
    "use strict";

    function n(e) {
        return e >= 55296 && e <= 56319
    }

    function a(e) {
        return e >= 56320 && e <= 57343
    }
    e.exports = function (e) {
        if ("string" != typeof e) throw new Error("Input must be string");
        for (var t = e.length, r = 0, o = null, i = null, l = 0; l < t; l++) o = e.charCodeAt(l), a(o) ? r += null != i && n(i) ? 1 : 3 : o <= 127 ? r += 1 : o >= 128 && o <= 2047 ? r += 2 : o >= 2048 && o <= 65535 && (r += 3), i = o;
        return r
    }
}]);